Require Import ZArith Lia Bool List Btauto.
Require Import Std NSet NMap.
Require TEnv Typ.
Require Import FSetPositive.
Require Import FMapPositive.
Require Import Typing.

Definition is_not_guard (p:PP) (s: Annot.t stmt) :=
  ~ BSet.mem p (Annot.high s)  = true.

Ltac typ :=
  repeat match goal with
         | |- context[Typ.meet ?A ?A] => rewrite Typ.meet_idem
         | |- Typ.order ?A ?A = true => apply Typ.order_refl
         | |- TEnv.order ?A ?A => apply TEnv.order_refl
         | |- BSet.subset ?A ?A = true => apply BSet.subset_refl
         end.

Lemma free_upgrade :
  forall s p D k G G'
         (TYP : typeof_stmt D None k G s G')
         (NP  : is_not_guard p s),
    typeof_stmt D (Some p)  k G s G'.
Proof.
  intros *.
  intro.
  revert p.
  remember (@None PP) as N.
  revert HeqN.
  induction TYP.
  - intros.
    econstructor;eauto.
  - econstructor; eauto.
  - intros.
    eapply Twrite; eauto.
  -  intros.
     unfold is_not_guard in NP.
     simpl in NP.
     rewrite BSet.mem_union_iff in NP.
     apply not_true_is_false in NP.
     rewrite orb_false_iff in NP.
     destruct NP.
     apply <- not_true_iff_false in H.
     apply <- not_true_iff_false in H0.
     eapply Tseq;eauto.
  - intros.
    unfold is_not_guard in NP.
    simpl in NP.
    match goal with
    | |- typeof_stmt D ?P ?K ?G ?S ?G' =>
        assert (TC: typeof_stmt D P K G S
                  (if isPP (Some p0) p && negb (Typ.is_L t) then TEnv.classify p G' else G'))
    end.
    {
      subst.
      eapply Tif.
      - eauto.
      - reflexivity.
      - eapply IHTYP1;eauto.
        unfold is_not_guard. simpl.
        repeat rewrite BSet.mem_union_iff  in NP.
        apply not_true_iff_false in NP.
        repeat rewrite orb_false_iff in NP.
        rewrite <- ! not_true_iff_false in NP.
        tauto.
      - eapply IHTYP2;eauto.
        unfold is_not_guard. simpl.
        repeat rewrite BSet.mem_union_iff  in NP.
        apply not_true_iff_false in NP.
        repeat rewrite orb_false_iff in NP.
        rewrite <- ! not_true_iff_false in NP.
        tauto.
      - eapply IHTYP3;eauto.
        unfold is_not_guard. simpl.
        repeat rewrite BSet.mem_union_iff  in NP.
        apply not_true_iff_false in NP.
        repeat rewrite orb_false_iff in NP.
        rewrite <- ! not_true_iff_false in NP.
        tauto.
      - reflexivity.
      - reflexivity.
    }
    simpl in TC.
    subst a. simpl.
    destruct (Pos.eq_dec p p0); auto.
    + simpl in TC.
      destruct (Typ.is_L t) eqn: TCASE; auto.
      exfalso.
      subst p0.
      clear - H2 NP TCASE.
      rewrite Typ.guard_not_L in H2  by auto.
      simpl in H2.
      revert NP.
      subset.
  - intros.
    unfold is_not_guard in *. simpl in *.
    eapply Tfor; eauto.
Qed.


Lemma free_upgrade_n :
  forall s  p D k G G' n
         (TYP : typeof_stmt_n D None k G s G' n)
         (NP  : is_not_guard p s),
    exists n', typeof_stmt_n D (Some p)  k G s G' n'.
Proof.
  intros.
  apply typeof_stmt_stmt_n_eq  in TYP.
  destruct TYP as (G'' & s' & TYP & AO & GE).
  apply free_upgrade with (p:= p) in TYP; auto.
  apply typeof_stmt_n_stmt in TYP.
  destruct TYP.
  exists x.
  eapply typeof_stmt_n_order.
  eapply typeof_stmt_n_weak in H;
  eauto.
  apply TEnv.order_refl.
  apply Typ.order_refl.
  unfold is_not_guard in  *.
  intro.
  unfold annot_order in AO.
  dand. apply NP. subset.
Qed.

Close Scope nat_scope.

Definition is_some_equal (e:expr) (x:var) :=
  match e with
  | Op Equal (Var x') (Cst _) => Pos.eqb x x'
  | _ => false
  end.

Lemma is_some_equal_inv1 : forall  (e:expr) (x:var),
    is_some_equal e x = true ->
    exists c, e = Op Equal (Var x) (Cst c).
Proof.
  destruct e; simpl; try discriminate.
  destruct o; simpl; try discriminate.
  destruct e1 ; simpl ; try discriminate.
  destruct e2 ; simpl ; try discriminate.
  intros.
  apply Peqb_true_eq in H.
  subst. eexists;reflexivity.
Qed.

Lemma is_some_equal_inv2 : forall  (e:expr) (x:var),
    (exists c, e = Op Equal (Var x) (Cst c)) -> is_some_equal e x = true.
Proof.
  intros.
  destruct H. subst.
  simpl.
  apply Pos.eqb_refl.
Qed.

Lemma is_some_equal_inv : forall e x,
    is_some_equal e x = true <-> exists c, e = Op Equal (Var x) (Cst c).
Proof.
  split.
  apply is_some_equal_inv1.
  apply is_some_equal_inv2.
Qed.

Definition is_some_in (e: expr) (l: BSet.t) :=
  match e with
  | Op Equal (Var x') (Cst _) => BSet.mem x' l
  | _ => false
end.

Lemma is_some_in_inv1: forall (e: expr) (l: BSet.t),
  is_some_in e l = true -> (exists c x, e = Op Equal (Var x) (Cst c) /\ BSet.mem x l = true).
Proof.
  destruct e; simpl; try discriminate.
  destruct o; simpl; try discriminate.
  destruct e1; simpl; try discriminate.
  destruct e2; simpl; try discriminate.
  intros.
  eexists; eauto.
Qed.

Lemma is_some_in_inv2: forall (e: expr) (l: BSet.t),
  (exists c x, e = Op Equal (Var x) (Cst c) /\ BSet.mem x l = true) -> is_some_in e l = true.
Proof.
  intros.
  do 3 destruct H.
  subst.
  auto.
Qed.

Lemma is_some_in_inv: forall e l,
  is_some_in e l = true <-> exists c x, e = Op Equal (Var x) (Cst c) /\ BSet.mem x l = true.
Proof.
  intros.
  split.
  apply is_some_in_inv1.
  apply is_some_in_inv2.
Qed.

Fixpoint has_cond (p:stmt) (x:var) :=
  match p with
  | Sskip => false
  | Sassign y e => false
  | Swrite y e1 e2 => false
  | Sseq s1 s2 => has_cond (Annot.elt s1) x  || has_cond (Annot.elt s2) x
  | Sif _ e s1 s2 s3 => is_some_equal e x ||
                                 has_cond (Annot.elt s1) x  || has_cond (Annot.elt s2) x  || has_cond (Annot.elt s3) x
  | Sfor y _ _ s   => Pos.eqb x y || has_cond  (Annot.elt s) x
  end.


Definition subset (P Q: positive -> bool): Prop:=
  forall x, P x = true -> Q x = true.

Lemma subset_and : forall S1 S2 U,
    subset U (fun x : var => S1 x && S2 x)  ->
    subset U S1 /\ subset U S2.
Proof.
  unfold subset; intros.
  split ; intro x ; specialize (H x);
  rewrite andb_true_iff in H; tauto.
Qed.

Lemma and_swap_first : forall {A B: Prop},
    B -> (B -> A) -> A /\ B.
Proof.
  tauto.
Qed.

Lemma umod_Sassign : forall x e x0,
    unmod (Sassign x e) x0 = true <-> x <> x0.
Proof.
  simpl.
  intros. destruct (Pos.eq_dec x0 x); simpl; intuition.
Qed.



Lemma Tstmt_eq : forall D a k G1 r s g G1' G2',
    typeof_stmt D  a k
                G1 (Annot.mk r s g)  G2' ->
    G1' = G2' ->
    typeof_stmt D  a k
                G1 (Annot.mk r s g)  G1'.
Proof.
  intros.
  subst.
  auto.
Qed.

Fixpoint unmod_join (x:var) (si: stmt) : forall sf sr,
    stmt_join si sf = Some sr ->
    unmod sr x = unmod si x.
Proof.
  assert (A : forall a1 a2 ar ,
             annot_join stmt_join a1 a2 = Some ar ->
             unmod (Annot.elt ar) x = unmod (Annot.elt a1) x).
  {
    destruct a1,a2,ar; unfold annot_join ; simpl.
    destruct (stmt_join elt elt0) eqn:J ; try discriminate.
    apply unmod_join with (x:=x) in J.
    intros. inv H.
    auto.
  }
  intros.
  destruct si,sf; simpl in *; try discriminate.
  - inv H. simpl;auto.
  - destruct (Pos.eq_dec x0 x1); try discriminate.
    destruct (of_sb (left e1) && of_sb (expr_eq_dec e e0)); try discriminate.
    inv H. simpl. reflexivity.
  - destruct (Pos.eq_dec x0 x1); try discriminate.
    destruct (of_sb (left e) && of_sb (expr_eq_dec e1 e0) && of_sb (expr_eq_dec e2 e3) ); try discriminate.
    inv H.
    reflexivity.
  - destruct (annot_join stmt_join s1 s0) eqn:J1 ; try discriminate.
    destruct (annot_join stmt_join s2 s3) eqn:J2 ; try discriminate.
    apply A in J1.
    apply A in J2.
    inv H.
    simpl. rewrite J1. rewrite J2. reflexivity.
  - destruct (of_sb (Pos.eq_dec t t0) && of_sb (expr_eq_dec e e0)); try discriminate.
    destruct (annot_join stmt_join s1 s0) eqn:J1 ; try discriminate.
    destruct (annot_join stmt_join s2 s4) eqn:J2 ; try discriminate.
    destruct (annot_join stmt_join s3 s5) eqn:J3 ; try discriminate.
    apply A in J1. apply A in J2. apply A in J3.
    inv H.
    simpl. rewrite J1. rewrite J2. rewrite J3. reflexivity.
  - destruct (Pos.eq_dec x0 x1); try discriminate.
    subst.
    destruct (of_sb (left eq_refl) && of_sb (Nat.eq_dec c1 c0) &&
        of_sb (Nat.eq_dec c2 c3)); try discriminate.
    destruct (annot_join stmt_join s s0) eqn:J1 ; try discriminate.
    apply A in J1.
    inv H.
    simpl. congruence.
Qed.

Lemma unmod_same_typing (s:stmt) :
  forall D  G G1 G' x r g k k'
         (SUB : unmod s x = true)
         (ORD : TEnv.order G1 G)
         (ORDK : Typ.order k' k = true)
         (TSTM : typeof_stmt D None k G (Annot.mk r s g) G'),
  exists G1' s',
    typeof_stmt D None k' (TEnv.set  x Typ.L G1) s' (TEnv.set x Typ.L G1') /\
      TEnv.order G1' G' /\
      annot_order stmt_order s' (Annot.mk r s g).
Proof.
  pattern s.
  apply stmt_sub.
  - intros.
    inv TSTM.
    do 2 eexists ; split_and.
    eapply Tskip. auto.
    apply annot_stmt_order_refl.
  - intros.
    rewrite umod_Sassign in SUB.
    inv TSTM.
    apply typeof_expr_order with (G2:= TEnv.set x0 Typ.L G1) in H7.
    destruct H7 as (t' & r' & TE & O1 & S1).
    do 2 eexists ; split_and.
    rewrite TEnv.set_swap.
    eapply Tassign; eauto. eauto.
    apply TEnv.order_set;auto.
    apply Typ.order_join2;auto.
    unfold annot_order. simpl.
    tauto.
    eapply TEnv.order_trans; eauto.
    apply TEnv.order_set_L; auto.
  - intros. inv TSTM.
    apply typeof_expr_order with (G2:= TEnv.set x0 Typ.L G1) in H7.
    destruct H7 as (t' & r' & TE1 & O1 & S1).
    apply typeof_expr_order with (G2:= TEnv.set x0 Typ.L G1) in H9.
    destruct H9 as (t2' & r2' & TE2 & O2 & S2).
    do 2 eexists ; split_and.
    eapply Twrite with (x:=x).
    eapply TE1. eapply TE2.
    intro. subst.
    apply Typ.order_H_inv in O1. congruence.
    eapply Typ.order_trans.
    apply Typ.order_join2.
    apply Typ.order_join2. eauto. eauto.
    apply Typ.order_guard. eauto.
    apply Typ.order_refl. auto.
    reflexivity.
    auto.
    unfold annot_order. simpl.
    apply Typ.to_set_subset in O1.
    intuition idtac.
    subset.
    intros; subst. congruence.
    eapply TEnv.order_trans; eauto.
    apply TEnv.order_set_L; auto.
    eapply TEnv.order_trans; eauto.
    apply TEnv.order_set_L; auto.
  - intros.
    simpl in SUB.
    rewrite andb_true_iff in SUB.
    destruct SUB as (SUB1 & SUB2).
    apply seq_inv in TSTM.
    destruct TSTM as (G1' & S1 & S2 & L1 & L2).
    subst.
    destruct s1 as [r1 s1 g1].
    destruct s2 as [r2 s2 g2].
    simpl in SUB1. simpl in SUB2.
    eapply IHs1 with (x:=x) (G1 := G1) in S1;auto.
    destruct S1 as (G'' & s1' & T1 & ORDE1 & ORDS1).
    destruct s1' as [r1' s1' g1'].
    eapply IHs2 with (x:=x) (G1 :=  G'') in S2;auto.
    destruct S2 as (G2'' & s2' & T2 & ORDE2 & ORDS2).
    destruct s2' as [r2' s2' g2'].
    do 2 eexists; split_and.
    eapply Tseq.
    eapply T1.
    eapply T2. auto.
    unfold annot_order in *; simpl in *. intuition idtac.
    apply BSet.subset_union2;auto.
    unfold annot_order; simpl; tauto.
    unfold annot_order; simpl; tauto.
    apply BSet.subset_union2;auto.
    apply stmt_order_refl.
    auto.
    apply stmt_order_refl.
    auto.
  - intros.
    apply sif_inv in TSTM.
    + destruct TSTM as (G1' & G2 & G3 & k1 & t1 & rc & RES & TE & TS1 & TS2 & TS3 & RR & GG & KK).
      simpl in RES.
      subst.
    destruct s1 as [r1 s1 g1].
    destruct s2 as [r2 s2 g2].
    destruct s3 as [r3 s3 g3].
    simpl in SUB.
    rewrite! andb_true_iff in SUB.
    destruct SUB as ((SUB11 & SUB12) & SUB13).
    simpl in *.
    apply typeof_expr_order with (G2:= (G1 [x → Typ.L])) in TE.
    destruct TE as (t1' & r' & TE' & O1' & S1').
    apply IHs1 with (x:=x) (k':=(k' ⊔ Typ.guard t1' (Typ.singleton p)))  (G1:=G1)in TS1;auto.
    destruct TS1 as (GT & st & TT & ORDT & ORDST).
    destruct st as [r1'  st g1'].
    apply IHs2 with (x:=x) (k':= (k' ⊔ Typ.guard t1' (Typ.singleton p))) (G1:=G1) in TS2;auto.
    destruct TS2 as (GE & se & TEL & ORDE & ORDSE).
    destruct se as [r2'  se g2'].
    apply IHs3 with (x:=x) (k':=k') (G1:= TEnv.join GT GE) in TS3;auto.
    destruct TS3 as (GN & sn & TEN & ORDN & ORDSN).
    destruct sn as [r3' sn g3'].
    unfold  annot_order in *.
    simpl in *. dand.
    do 2 eexists ; split_and.
    match goal with
    | |- typeof_stmt _ _ _ _ _ ?R =>
    change R with (if isPP None p && negb (Typ.is_L t1')
        then TEnv.classify p R
                   else R)
    end.
    eapply Tif.
    eapply TE'.
    reflexivity.
    eapply TT.
    eapply TEL.
    rewrite TEnv.join_set.
    rewrite Typ.join_idem.
    eapply TEN.
    reflexivity. reflexivity.
    auto.
    simpl.
    repeat apply BSet.subset_union2;auto.
    simpl.
    intuition.
    unfold annot_order. simpl.
    intuition.
    unfold annot_order. simpl.
    intuition.
    unfold annot_order. simpl.
    intuition.
    simpl.
    repeat apply BSet.subset_union2;auto.
    apply Typ.to_set_subset.
    intros.
    apply Typ.guard_singleton_not_H in H.
    tauto.
    apply Typ.order_guard;auto.
    apply Typ.order_refl.
    apply stmt_order_refl.
    apply TEnv.join_lub; auto.
    eapply TEnv.order_trans;eauto.
    apply TEnv.join_ub_l.
    eapply TEnv.order_trans;eauto.
    rewrite TEnv.join_sym.
    apply TEnv.join_ub_l.
    apply stmt_order_refl.
    apply Typ.order_join2;auto.
    apply Typ.order_guard;auto.
    apply Typ.order_refl.
    apply stmt_order_refl.
    apply Typ.order_join2;auto.
    apply Typ.order_guard;auto.
    apply Typ.order_refl.
    eapply TEnv.order_trans;eauto.
    apply TEnv.order_set_L; auto.
(*    + simpl in *.
      rewrite! andb_true_iff in SUB.
      destruct SUB as ((SUB11 & SUB12) & SUB13).
      destruct s1 as [r1 s1 g1].
      destruct s2 as [r2 s2 g2].
      destruct s3 as [r3 s3 g3].
      simpl in *.
      destruct TSTM as (x1 & b & c & F & E & MEM & L & TT & SK1 & SK2 & GG & RR).
      subst.
      inv SK1. inv SK2.
      destruct b.
      {
        apply IHs1 with (x:=x) (k':= k') (G1 := G1) in TT.
        destruct TT as (G1' & s' & T1 & O1 & A1).
        destruct s' as [r' s' g'].
        do 2 eexists. split_and.
        eapply TifL with (p:=p).
        eauto. reflexivity.
        inv A1. inv H0. simpl in H2.
        eapply BSet.subset_mem_f; eauto.
        destruct (Pos.eq_dec x1 x). subst.
        rewrite TEnv.gss. reflexivity.
        rewrite TEnv.gso by assumption.
        specialize (ORD x1).
        rewrite L in *.
        apply Typ.order_is_L_r in ORD.
        auto. simpl. eapply T1.
        auto.
        simpl.
        unfold annot_order in A1; unfold annot_order; simpl in * ; dand.
        split_and;auto.
        unfold annot_order;simpl; tauto.
        apply annot_stmt_order_refl.
        apply annot_stmt_order_refl.
        apply stmt_order_refl.
        auto. auto.
        auto.
      }
      {
        inv TT.
        do 2 eexists. split_and.
        eapply TifL with (p:=p).
        eauto. reflexivity.
        eauto.
        destruct (Pos.eq_dec x1 x). subst.
        rewrite TEnv.gss. reflexivity.
        rewrite TEnv.gso by assumption.
        specialize (ORD x1).
        rewrite L in *.
        apply Typ.order_is_L_r in ORD.
        auto. simpl. constructor.
        auto.
        simpl.
        unfold annot_order; simpl in * ; dand.
        split_and;auto.
        unfold annot_order.
        split_and; auto.
        apply BSet.subset_refl.
        apply annot_stmt_order_refl.
        apply annot_stmt_order_refl.
        apply annot_stmt_order_refl.
        apply annot_stmt_order_refl.
      } *)
  - intros.
    apply sfor_inv in TSTM.
    destruct TSTM as (R & G2 & (G1' & Go &  T11 & ORD1 & ORD2 &  CL & T12)).
    simpl in SUB. rewrite andb_true_iff in SUB.
    destruct SUB as (SUB1 & SUB2).
    subst.
    assert (x0 <> x). { destruct (Pos.eq_dec x0 x); try discriminate ; auto. }
    simpl in *. subst.
    apply IHfor with (x:=x0) (k':=k') (G1:= TEnv.set x k' Go) in T11;auto.
    destruct T11 as (GG1 & [rs' si' gs']  & TF & OR & OS).
    rewrite TEnv.set_swap in TF by auto.
    do 2 eexists; split_and.
    eapply Tfor.
    eapply TF.
    + apply TEnv.order_set; auto.
      eapply TEnv.order_trans;eauto.
    + apply TEnv.order_set; auto.
      eapply TEnv.order_trans;eauto.
    + rewrite <- TEnv.set_classify_set.
      apply annot_stmt_order_Ca in OS.
      rewrite OS.
      rewrite CL. reflexivity.
      apply Typ.classify_set_L.
    +  apply TEnv.order_refl.
    + unfold annot_order in *; simpl in *.
      intuition idtac.
      unfold annot_order ; simpl;auto.
    + apply stmt_order_refl.
    + apply TEnv.order_set;auto.
      apply TEnv.order_refl.
Qed.

Fixpoint stmt_order_unmod (s: stmt) : forall s',
    stmt_order s s' ->
    forall x, unmod s x = unmod s' x.
Proof.
  intros. revert s s' H.
  assert (A: forall a a', annot_order stmt_order a a' -> unmod (Annot.elt a) x = unmod (Annot.elt a') x).
  {
    destruct a,a'. simpl.
    unfold annot_order; simpl.
    intros. apply stmt_order_unmod;auto.
    tauto.
  }
  destruct s; simpl.
  - destruct s'; try tauto.
  - destruct s'; try tauto.
    intros (E1 & E2).
    subst.
    simpl. auto.
  - destruct s'; try tauto.
    intros (E1 & E2).
    subst.
    simpl. auto.
  - destruct s'; try tauto.
    intros (A1 & A2).
    simpl.
    apply A in A1.
    apply A in A2.
    congruence.
  - destruct s'; try tauto.
    intros (E1 & e2 & A1 & A2 & A3).
    simpl.
    apply A in A1.
    apply A in A2.
    apply A in A3.
    congruence.
  - destruct s'; try tauto.
    intros (E1 & E2 & E3 & A1).
    simpl.
    apply A in A1.
    subst.
    congruence.
Qed.


Lemma annot_stmt_order_unmod :
  forall a a', annot_order stmt_order a a' -> forall x, unmod (Annot.elt a) x = unmod (Annot.elt a') x.
Proof.
  destruct a,a'. simpl.
  unfold annot_order; simpl.
  intros. apply stmt_order_unmod;auto.
  tauto.
Qed.

Fixpoint stmt_order_C (s:stmt) : forall s',
    stmt_order s s' ->
    C s = C s'.
Proof.
  intros. revert s s' H.
  assert (A: forall a a', annot_order stmt_order a a' -> C (Annot.elt a)  = C (Annot.elt a') ).
  {
    destruct a,a'. simpl.
    unfold annot_order; simpl.
    intros. apply stmt_order_C;auto.
    tauto.
  }
  destruct s; simpl.
  - destruct s'; try tauto.
  - destruct s'; try tauto.
  - destruct s'; try tauto.
  - destruct s'; try tauto.
    intros (A1 & A2).
    simpl.
    apply A in A1.
    apply A in A2.
    congruence.
  - destruct s'; try tauto.
    intros (E1 & e2 & A1 & A2 & A3).
    simpl.
    apply A in A1.
    apply A in A2.
    apply A in A3.
    congruence.
  - destruct s'; try tauto.
    intros (E1 & E2 & E3 & A1).
    simpl.
    apply A in A1.
    subst.
    congruence.
Qed.

Lemma annot_stmt_order_C :
  forall a a', annot_order stmt_order a a' -> C (Annot.elt a) = C (Annot.elt a').
Proof.
  destruct a,a'. simpl.
  unfold annot_order; simpl.
  intros. apply stmt_order_C;auto.
  tauto.
Qed.

Definition is_simple_env (D: var -> Typ.t):=
  forall x, Typ.is_simple (D x) = true.

Definition dwn_pre_annot (F : BSet.t -> stmt -> BSet.t -> Annot.t stmt) (a: Annot.t stmt) :=
  F (Annot.leaked a) (Annot.elt a) (Annot.high a).


Fixpoint dwn_stmt (r:BSet.t) (s: stmt)  (g: BSet.t) :=
  match s with
  | Sskip | Sassign _ _ | Swrite _ _ _ => Annot.mk r s BSet.empty
  | Sseq s1 s2 => Annot.mk r (Sseq (dwn_pre_annot dwn_stmt s1)  (dwn_pre_annot dwn_stmt s2)) (BSet.inter (C s) g)
  | Sif b e s1 s2 s3 =>
      Annot.mk r (Sif b e (dwn_pre_annot dwn_stmt s1) (dwn_pre_annot dwn_stmt s2) (dwn_pre_annot dwn_stmt s3))
        (BSet.inter g (C s))
  | Sfor x c1 c2 b => Annot.mk r (Sfor x c1 c2 (dwn_pre_annot dwn_stmt b)) (BSet.inter (C s) g)
  end.


Definition dwn_annot (a : Annot.t stmt) := dwn_stmt  (Annot.leaked a) (Annot.elt a) (Annot.high a).

Lemma typeof_expr_wk_downgrade_set :
  forall D G e s t r
         (SIMP : is_simple_env D),
    typeof_expr_wk D G e t r ->
    typeof_expr_wk D (TEnv.downgrade_set s G) e (Typ.downgrade_set s t) r.
Proof.
  intros.
  induction H.
  - rewrite <- TEnv.get_downgrade_set.
    constructor.
  - rewrite Typ.downgrade_set_L.
    constructor.
  - rewrite Typ.downgrade_set_join.
    constructor; auto.
  - rewrite! Typ.downgrade_set_join.
    constructor; auto.
  - rewrite! Typ.downgrade_set_join.
    eapply TWk.
    eapply Typ.order_join2.
    rewrite Typ.downgrade_set_simple.
    apply Typ.order_refl.  auto.
    apply Typ.order_refl.
    apply BSet.subset_union2.
    apply BSet.subset_refl.
    instantiate (1:= Typ.to_set (Typ.downgrade_set s t1)).
    apply Typ.to_set_subset.
    intros. congruence.
    eapply Typ.downgrade_set_order.
    eapply TRead_wk. auto.
    intro.
    apply Typ.downgrade_set_H_inv in H1. congruence.
  - eapply TWk; eauto.
    eapply Typ.downgrade_set_mono2;eauto.
Qed.

Lemma typeof_stmt_n_down_assign :
  forall n D a k Go Go' d
         (SIMP : is_simple_env D)
         (e : expr)
         (r g : BSet.t)
         (x : var)
         (t : Typ.t)
         (TassignExpr : typeof_expr_wk D Go e t r)
         (TassignWk : TEnv.order (Go [x → t ⊔ k]) Go'),
  typeof_stmt_n D a (Typ.downgrade_set d k)
    (TEnv.downgrade_set d Go) (Annot.mk r (Sassign x e) g)
    (TEnv.downgrade_set d Go') n.
Proof.
  intros.
  apply typeof_expr_wk_downgrade_set with (s:=d) in TassignExpr.
  eapply Tassign_n. eauto.
  apply TEnv.downgrade_set_mono2 with (s:=d) in TassignWk.
  eapply TEnv.order_trans; eauto.
  rewrite TEnv.downgrade_set_set.
  apply TEnv.order_set; auto.
  apply TEnv.order_refl.
  rewrite Typ.downgrade_set_join.
  eapply Typ.order_refl. auto.
Qed.

Lemma typeof_stmt_n_down_write : forall
    n d (D : var -> Typ.t)
    (a : option var)
    (k : Typ.t)
    (Go Go' : TEnv.t)
    (SIMP : is_simple_env D)
    (x : var)
    (e1 e2 : expr)
    (r g : BSet.t)
    (t1 : Typ.t)
    (r1 : BSet.t)
    (t2 : Typ.t)
    (r2 : BSet.t)
    (TwriteE1 : typeof_expr_wk D Go e1 t1 r1)
    (TwriteE2 : typeof_expr_wk D Go e2 t2 r2)
    (TwriteNOTH : t1 <> Typ.H)
    (TwriteO : Typ.order ((t1 ⊔ t2) ⊔ Typ.guard k Typ.H) (D x) = true)
    (TwriteLk : BSet.subset ((r1 ∪ r2) ∪ Typ.to_set t1) r = true)
    (TwriteWk : TEnv.order Go Go'),
  typeof_stmt_n D a (Typ.downgrade_set d k)
    (TEnv.downgrade_set d Go) (Annot.mk r (Swrite x e1 e2) g)
    (TEnv.downgrade_set d Go') n.
Proof.
  intros.
  apply typeof_expr_wk_downgrade_set with (s:=d) in TwriteE1.
  apply typeof_expr_wk_downgrade_set with (s:=d) in TwriteE2.
  eapply Twrite_n. eauto. eauto.
  intro.
  apply Typ.downgrade_set_H_inv in H. congruence.
  eapply Typ.order_trans with (2:= TwriteO).
  apply Typ.order_join2.
  apply Typ.order_join2.
  apply Typ.downgrade_set_order.
  apply Typ.downgrade_set_order.
  apply Typ.order_guard;auto.
  apply Typ.downgrade_set_order.
  eapply BSet.subset_trans;eauto.
  apply BSet.subset_union2;auto.
  apply BSet.subset_refl.
  apply Typ.to_set_subset.
  congruence.
  apply Typ.downgrade_set_order.
  apply TEnv.downgrade_set_mono2;auto.
  auto.
  auto.
Qed.

Lemma leaked_dwn_stmt (r: BSet.t) (s:stmt) (g: BSet.t) :
  Annot.leaked (dwn_stmt r s g) = r.
Proof.
  destruct s; simpl; auto.
Qed.

Lemma leaked_dwn_annot : forall s,
    Annot.leaked (dwn_annot s) = Annot.leaked s.
Proof.
  unfold dwn_annot.
  intros.
  rewrite leaked_dwn_stmt.
  reflexivity.
Qed.

Fixpoint order_stmt_dwn (s: stmt): forall s' r g r' g',
    stmt_order s s' ->
    BSet.subset r r' = true ->
    BSet.subset g g' = true ->
    annot_order stmt_order (dwn_stmt r s g) (dwn_stmt r' s' g').
Proof.
  destruct s; simpl.
  - destruct s';try tauto; simpl.
    unfold annot_order. simpl.
    intros; split_and; auto.
  - destruct s';try tauto; simpl.
    unfold annot_order. simpl.
    intros; dand; split_and; auto.
  - destruct s';try tauto; simpl.
    unfold annot_order. simpl.
    intros; dand; split_and; auto.
  - destruct s';try tauto; simpl.
    unfold annot_order. simpl.
    intros; dand; split_and; auto.
    apply order_stmt_dwn; auto.
    apply order_stmt_dwn; auto.
    apply stmt_order_C in H2.
    rewrite H2.
    apply stmt_order_C in H3.
    rewrite H3.
    subset.
  - destruct s';try tauto; simpl.
    unfold annot_order. simpl.
    intros; dand; split_and; auto.
    apply order_stmt_dwn; auto.
    apply order_stmt_dwn; auto.
    apply order_stmt_dwn; auto.
    apply stmt_order_C in H3.
    rewrite H3.
    apply stmt_order_C in H5.
    rewrite H5.
    apply stmt_order_C in H7.
    rewrite H7.
    subset.
  - destruct s';try tauto; simpl.
    unfold annot_order. simpl.
    intros; dand; split_and; auto.
    apply order_stmt_dwn; auto.
    apply stmt_order_C in H6.
    rewrite H6.
    subset.
Qed.

Lemma high_down_stmt (s:stmt) : forall r g,
    Annot.high (dwn_stmt r s g) = BSet.inter (C s) g.
Proof.
  destruct s; simpl.
  - intros.
    rewrite BSet.inter_empty.
    reflexivity.
  - intros.
    rewrite BSet.inter_empty.
    reflexivity.
  - intros.
    rewrite BSet.inter_empty.
    reflexivity.
  - intros.
    reflexivity.
  - intros.
    rewrite BSet.inter_sym.
    reflexivity.
  - intros.
    rewrite BSet.inter_sym.
    reflexivity.
Qed.


Lemma annot_order_mk_seq_dwn_annot : forall s1 s2 s,
    annot_order stmt_order (mk_seq s1 s2) s ->
    annot_order stmt_order (mk_seq (dwn_annot s1) (dwn_annot s2)) (dwn_annot s).
Proof.
  intros.
  destruct s as [r s g].
  unfold annot_order,mk_seq in *; simpl in *.
  dand. destruct s; try tauto.
  dand.
  simpl.
  split_and;auto.
  - rewrite! leaked_dwn_annot. auto.
  - unfold annot_order in H1.
    dand.
    apply order_stmt_dwn;auto.
  - unfold annot_order in H4.
    dand.
    apply order_stmt_dwn;auto.
  - unfold dwn_annot.
    rewrite! high_down_stmt.
    apply annot_stmt_order_Ca  in H1.
    apply annot_stmt_order_Ca  in H4.
    unfold Ca  in *.
    rewrite H1. rewrite H4.
    subset.
Qed.

Lemma downgrade_set_classify_if :
  forall s p c G,
         (c = true ->   BSet.inter s (BSet.singleton p) = BSet.empty) ->
    TEnv.downgrade_set s (Typing.classify_if p c G) = Typing.classify_if p c (TEnv.downgrade_set s G).
Proof.
  unfold Typing.classify_if. intros.
  destruct c eqn:C; auto.
  apply Map.uniq.
  intros.
  unfold TEnv.downgrade_set.
  unfold TEnv.classify.
  rewrite !Map.find_map.
  destruct (Map.find x G); simpl;auto.
  f_equal.
  rewrite <- !Typ.classify_set_classify.
  rewrite Typ.downgrade_set_classify_set.
  auto.
  apply H;auto.
Qed.

Lemma annot_order_mk_if_full_dwn_annot : forall p c rc t s1 s2 s3 s,
    annot_order stmt_order (mk_if_full p c rc t s1 s2 s3) s ->
  annot_order stmt_order (mk_if_full p c rc t (dwn_annot s1) (dwn_annot s2) (dwn_annot s3)) (dwn_annot s).
Proof.
  intros.
  destruct s as [r s g].
  unfold annot_order,mk_if_full in *; simpl in *.
  dand. destruct s; try tauto.
  dand. subst.
  simpl.
  split_and;auto.
  - rewrite! leaked_dwn_annot. auto.
  - unfold annot_order in H4.
    dand.
    apply order_stmt_dwn;auto.
  - unfold annot_order in H5.
    dand.
    apply order_stmt_dwn;auto.
  - unfold annot_order in H7.
    dand.
    apply order_stmt_dwn;auto.
  - unfold dwn_annot.
    rewrite! high_down_stmt.
    apply annot_stmt_order_Ca  in H4.
    apply annot_stmt_order_Ca  in H5.
    apply annot_stmt_order_Ca  in H7.
    unfold Ca  in *.
    rewrite H4. rewrite H5. rewrite H7.
    assert (BSet.subset (Typ.to_set (Typ.guard t (Typ.singleton t0))) (BSet.singleton t0) = true).
    {
      unfold Typ.guard.
      destruct (Typ.is_L t).
      simpl. subset.
      apply BSet.subset_refl.
    }
    subset.
Qed.

Fixpoint C_dwn_stmt (s:stmt) : forall r g,
    C (Annot.elt (dwn_stmt r s g)) = (C s).
Proof.
  destruct s; simpl.
  - reflexivity.
  - reflexivity.
  - reflexivity.
  - unfold dwn_pre_annot.
    simpl.
    rewrite! C_dwn_stmt.
    reflexivity.
  - unfold dwn_pre_annot.
    simpl.
    rewrite! C_dwn_stmt.
    reflexivity.
  - unfold dwn_pre_annot.
    simpl.
    rewrite! C_dwn_stmt.
    reflexivity.
Qed.


Lemma Ca_dwn_annot : forall ar,
    Ca (dwn_annot ar) = (Ca ar).
Proof.
  unfold Ca,dwn_annot.
  intros. rewrite C_dwn_stmt.
  reflexivity.
Qed.

Lemma annot_order_dwn_mk_for :
  forall x c1 c2 b s,
    annot_order stmt_order (mk_for x c1 c2 b) s ->
    annot_order stmt_order
    (mk_for x c1 c2 (dwn_annot b)) (dwn_annot s).
Proof.
  unfold mk_for,annot_order;simpl;intros.
  destruct  s as [r s g].
  simpl in *.
  destruct s; try tauto.
  simpl.
  dand; subst;split_and;auto.
  rewrite leaked_dwn_annot; auto.
  unfold annot_order in H6; simpl in *; dand.
  apply order_stmt_dwn;auto.
  unfold dwn_annot.
  rewrite high_down_stmt.
  apply annot_stmt_order_Ca in H6.
  unfold Ca in *.
  subset.
Qed.

Lemma typeof_stmt_n_down:
  forall n s D a k  Go Go' d
         (SIMP: is_simple_env D)
         (TYP: typeof_stmt_n D a k Go s Go' n),
    typeof_stmt_n D a (Typ.downgrade_set d k) (TEnv.downgrade_set d Go) (dwn_annot s)
      (TEnv.downgrade_set (BSet.diff d (Ca s)) Go') n.
Proof.
  induction n.
  - intros.
    inv TYP.
    + constructor.
      simpl.
      eapply TEnv.order_trans.
      apply TEnv.downgrade_set_mono2. eauto.
      apply TEnv.downgrade_set_mono1;auto.
      apply BSet.subset_diff_l.
    + simpl.
      eapply typeof_stmt_n_weak.
      eapply typeof_stmt_n_down_assign;eauto.
      apply annot_stmt_order_refl.
      apply TEnv.downgrade_set_mono1.
      apply BSet.subset_diff_l.
    + simpl.
      eapply typeof_stmt_n_weak.
      eapply typeof_stmt_n_down_write.
      auto. apply TwriteE1. apply TwriteE2. auto.
      eauto. eauto. eauto.
      apply annot_stmt_order_refl.
      apply TEnv.downgrade_set_mono1.
      apply BSet.subset_diff_l.
  - intros.
    inv TYP.
    + constructor.
      simpl.
      eapply TEnv.order_trans.
      apply TEnv.downgrade_set_mono2. eauto.
      apply TEnv.downgrade_set_mono1;auto.
      apply BSet.subset_diff_l.
    + simpl.
      eapply typeof_stmt_n_weak.
      eapply typeof_stmt_n_down_assign;eauto.
      apply annot_stmt_order_refl.
      apply TEnv.downgrade_set_mono1.
      apply BSet.subset_diff_l.
    + simpl.
      eapply typeof_stmt_n_weak.
      eapply typeof_stmt_n_down_write.
      auto. apply TwriteE1. apply TwriteE2. auto.
      eauto. eauto. eauto.
      apply annot_stmt_order_refl.
      apply TEnv.downgrade_set_mono1.
      apply BSet.subset_diff_l.
    + apply IHn with (d:= d) in  Tseq1;auto.
      apply IHn with (d:=BSet.diff d (Ca s1))  in  Tseq2;auto.
      apply typeof_stmt_n_order with (G1:= (TEnv.downgrade_set (BSet.diff d (Ca s1)) G1)) (k':= (Typ.downgrade_set d k)) in Tseq2.
      eapply Tseq_n with (s1:= (dwn_annot s1)) (s2:=dwn_annot s2).
      eapply Tseq1.
      eauto. eauto.
      destruct TseqWk.
      constructor.
      eapply TEnv.order_trans.
      apply TEnv.downgrade_set_mono2. eauto.
      apply TEnv.downgrade_set_mono1;auto.
      rewrite <- BSet.diff_union_r.
      apply BSet.subset_diff_2. apply BSet.subset_refl.
      { apply annot_stmt_order_Ca in weaken_stmt.
        rewrite <- weaken_stmt.
        apply BSet.subset_refl.
        (*apply annot_order_mk_seq_dwn_annot in weaken_stmt.
        unfold annot_order in weaken_stmt.
        simpl in weaken_stmt. dand. auto. *)
      }
      { apply annot_order_mk_seq_dwn_annot; auto.
      }
      apply TEnv.order_refl.
      apply Typ.downgrade_set_mono1;auto.
      apply BSet.subset_diff_l.
    +  apply IHn with (d:= BSet.diff d (Typ.to_set (Typ.guard (Typ.downgrade_set d t) (Typ.singleton p)))) in TifThen; auto.
       apply IHn with (d:= BSet.diff d (Typ.to_set (Typ.guard (Typ.downgrade_set d t) (Typ.singleton p)))) in TifElse; auto.
       apply IHn with (d:= BSet.diff d (BSet.union (Typ.to_set (Typ.guard (Typ.downgrade_set d t) (Typ.singleton p)))
                                          (BSet.union (Ca s1) (Ca s2)))) in TifNext; auto.
       apply typeof_expr_wk_downgrade_set with (s:=d) in TifE;auto.
       assert (SUB : Typ.order (Typ.guard (Typ.downgrade_set d t) (Typ.singleton p))
    (Typ.guard t (Typ.downgrade_set (BSet.diff d (Typ.to_set (Typ.guard (Typ.downgrade_set d t) (Typ.singleton p)))) (Typ.singleton p))) = true).
       {
         unfold Typ.guard.
         destruct (Typ.is_L t) eqn:ISL.
         - apply Typ.is_L_inv in ISL. subst.
           rewrite Typ.downgrade_set_L. simpl. rewrite BSet.is_empty_empty.
           apply Typ.order_refl.
         - destruct (Typ.is_L (Typ.downgrade_set d t)) eqn:ISD.
           + apply Typ.order_bot.
           + rewrite Typ.downgrade_set_singleton.
             rewrite BSet.mem_diff_iff.
             rewrite Typ.to_set_singleton.
             rewrite BSet.singleton_xx.
             rewrite andb_comm. simpl. apply BSet.subset_refl.
       }
       eapply Tif_n with (p:=p);auto.
       eauto.
       { rewrite Typ.downgrade_set_join in TifThen.
         rewrite Typ.downgrade_set_guard  in TifThen.
         eapply typeof_stmt_n_order.
         apply TifThen.
         apply TEnv.downgrade_set_mono1.
         apply BSet.subset_diff_l.
         apply Typ.order_join2.
         apply Typ.downgrade_set_mono1.
         apply BSet.subset_diff_l.
         auto.
       }
       { rewrite Typ.downgrade_set_join in TifElse.
         rewrite Typ.downgrade_set_guard  in TifElse.
         eapply typeof_stmt_n_order.
         apply TifElse.
         apply TEnv.downgrade_set_mono1.
         apply BSet.subset_diff_l.
         apply Typ.order_join2.
         apply Typ.downgrade_set_mono1.
         apply BSet.subset_diff_l.
         auto.
       }
       {
         eapply typeof_stmt_n_order.
         apply TifNext.
         rewrite TEnv.downgrade_set_join.
         apply TEnv.order_join2.
         - apply TEnv.downgrade_set_mono1.
           subset.
           (*
           rewrite (BSet.union_sym (Annot.high (dwn_annot s1))).
         rewrite! BSet.diff_union_r.
         apply BSet.subset_diff_2.
         apply BSet.subset_diff_l.
         unfold dwn_annot. rewrite high_down_stmt.
         subset. *)
         - apply TEnv.downgrade_set_mono1.
           subset.
           (*rewrite! BSet.diff_union_r.
         apply BSet.subset_diff_2.
         apply BSet.subset_diff_l.
         unfold dwn_annot. rewrite high_down_stmt.
         subset. *)
         - apply Typ.downgrade_set_mono1.
           apply BSet.subset_diff_l.
       }
       destruct Tifwk;constructor;auto.
       *  apply TEnv.downgrade_set_mono2 with (s:= (BSet.diff d (Ca  s))) in weaken_env.
          eapply TEnv.order_trans with (2:= weaken_env).
       rewrite downgrade_set_classify_if.
       eapply order_isPP.
       apply Typ.downgrade_set_order.
       rewrite <- BSet.diff_union_r.
       apply TEnv.downgrade_set_mono1.
       apply BSet.subset_diff_2.
       apply BSet.subset_refl.
       {
         apply annot_stmt_order_Ca in weaken_stmt.
         rewrite <- weaken_stmt.
         unfold mk_if_full. unfold Ca at 4. simpl.
         assert (BSet.subset (Typ.to_set (Typ.guard (Typ.downgrade_set d t) (Typ.singleton p))) (BSet.singleton p) = true).
         {
           unfold Typ.guard.
           destruct (Typ.is_L (Typ.downgrade_set d t)).
           simpl. subset.
           simpl. apply BSet.subset_refl.
         }
         unfold Ca.
         subset.
       }
       {
         intro.
         assert (SUBp : BSet.subset (BSet.singleton p) (Ca s) = true).
         {
           apply annot_stmt_order_Ca in weaken_stmt.
           rewrite <- weaken_stmt.
           unfold mk_if_full. unfold Ca;simpl.
           subset.
         }
         apply BSet.uniq.
         intros.
         subset; destruct (Pos.eq_dec x p);subset.
       }
       * apply annot_order_mk_if_full_dwn_annot in weaken_stmt; auto.
         eapply annot_order_trans;eauto.
         apply annot_order_mk_if; auto.
         apply annot_stmt_order_refl.
         apply annot_stmt_order_refl.
         apply annot_stmt_order_refl.
         apply BSet.subset_refl.
         apply Typ.downgrade_set_order.
    +
       assert (CaSame : Ca ar = Ca s).
       {
         destruct TforWk.
         apply annot_stmt_order_Ca in weaken_stmt.
         unfold Ca in weaken_stmt. simpl in weaken_stmt.
         apply weaken_stmt.
       }
      eapply IHn with (d:= (BSet.diff d (Ca s))) in TforIter.
       eapply Tfor_n.
       rewrite TEnv.downgrade_set_set in TforIter.
       eapply typeof_stmt_n_order.
       apply TforIter.
       apply TEnv.order_set.
       apply TEnv.downgrade_set_mono1.
       rewrite CaSame.
       subset.
       apply Typ.downgrade_set_mono1.
       apply BSet.subset_diff_l.
       apply Typ.downgrade_set_mono1.
       apply BSet.subset_diff_l.
       * eapply TEnv.order_trans.
         eapply TEnv.downgrade_set_mono2.
         eauto.
         eapply TEnv.downgrade_set_mono1.
         rewrite <- BSet.diff_union_r.
         apply BSet.subset_diff_l.
       *
         rewrite Ca_dwn_annot.
         rewrite <- TEnv.downgrade_set_classify_set.
         f_equal. auto.
         rewrite CaSame.
         apply BSet.inter_diff_empty.
       *
         destruct TforWk; constructor.
(*         assert (Sars : BSet.subset (Annot.high ar) (Annot.high s) = true).
         { unfold annot_order in weaken_stmt. dand.
           apply weaken_stmt3.
         }
         eapply TEnv.order_trans.
         eapply TEnv.downgrade_set_mono1.
         apply BSet.subset_diff_2.
         apply BSet.subset_refl.
         apply BSet.subset_refl. *)
         rewrite CaSame.
         rewrite <- BSet.diff_union_r.
         rewrite BSet.union_idem.
         apply TEnv.downgrade_set_mono2.
         auto.
         apply annot_order_dwn_mk_for;auto.
         apply weaken_stmt.
       * auto.
Qed.


(*
Lemma typeof_stmt_n_down1:
  forall n s D a k  Go Go' d x
         (SIMP: is_simple_env D)
         (TYP: typeof_stmt_n D a k Go s Go' n)
  ,
    typeof_stmt_n D a k (Go [x → Typ.downgrade_set d (TEnv.get x Go)]) (dwn_annot s)
      (Go' [x → Typ.downgrade_set (BSet.diff d (Ca s)) (TEnv.get x Go')]) n.
Proof.
  induction n.
  - intros.
    inv TYP.
    + constructor.
      simpl.

      apply TEnv.order_set. eauto.
      eapply Typ.order_trans.
      eapply Typ.downgrade_set_mono2;auto.
      apply Typ.downgrade_set_mono1.
      apply BSet.subset_diff_l.
    + simpl.


      eapply typeof_stmt_n_weak.
      eapply Tassign_n.


      eapply typeof_stmt_n_down_assign;eauto.
      apply annot_stmt_order_refl.
      apply TEnv.downgrade_set_mono1.
      apply BSet.subset_diff_l.
    + simpl.
      eapply typeof_stmt_n_weak.
      eapply typeof_stmt_n_down_write.
      auto. apply TwriteE1. apply TwriteE2. auto.
      eauto. eauto. eauto.
      apply annot_stmt_order_refl.
      apply TEnv.downgrade_set_mono1.
      apply BSet.subset_diff_l.
  - intros.
    inv TYP.
    + constructor.
      simpl.
      eapply TEnv.order_trans.
      apply TEnv.downgrade_set_mono2. eauto.
      apply TEnv.downgrade_set_mono1;auto.
      apply BSet.subset_diff_l.
    + simpl.
      eapply typeof_stmt_n_weak.
      eapply typeof_stmt_n_down_assign;eauto.
      apply annot_stmt_order_refl.
      apply TEnv.downgrade_set_mono1.
      apply BSet.subset_diff_l.
    + simpl.
      eapply typeof_stmt_n_weak.
      eapply typeof_stmt_n_down_write.
      auto. apply TwriteE1. apply TwriteE2. auto.
      eauto. eauto. eauto.
      apply annot_stmt_order_refl.
      apply TEnv.downgrade_set_mono1.
      apply BSet.subset_diff_l.
    + apply IHn with (d:= d) in  Tseq1;auto.
      apply IHn with (d:=BSet.diff d (Ca s1))  in  Tseq2;auto.
      apply typeof_stmt_n_order with (G1:= (TEnv.downgrade_set (BSet.diff d (Ca s1)) G1)) (k':= (Typ.downgrade_set d k)) in Tseq2.
      eapply Tseq_n with (s1:= (dwn_annot s1)) (s2:=dwn_annot s2).
      eapply Tseq1.
      eauto. eauto.
      destruct TseqWk.
      constructor.
      eapply TEnv.order_trans.
      apply TEnv.downgrade_set_mono2. eauto.
      apply TEnv.downgrade_set_mono1;auto.
      rewrite <- BSet.diff_union_r.
      apply BSet.subset_diff_2. apply BSet.subset_refl.
      { apply annot_stmt_order_Ca in weaken_stmt.
        rewrite <- weaken_stmt.
        apply BSet.subset_refl.
        (*apply annot_order_mk_seq_dwn_annot in weaken_stmt.
        unfold annot_order in weaken_stmt.
        simpl in weaken_stmt. dand. auto. *)
      }
      { apply annot_order_mk_seq_dwn_annot; auto.
      }
      apply TEnv.order_refl.
      apply Typ.downgrade_set_mono1;auto.
      apply BSet.subset_diff_l.
    +  apply IHn with (d:= BSet.diff d (Typ.to_set (Typ.guard (Typ.downgrade_set d t) (Typ.singleton p)))) in TifThen; auto.
       apply IHn with (d:= BSet.diff d (Typ.to_set (Typ.guard (Typ.downgrade_set d t) (Typ.singleton p)))) in TifElse; auto.
       apply IHn with (d:= BSet.diff d (BSet.union (Typ.to_set (Typ.guard (Typ.downgrade_set d t) (Typ.singleton p)))
                                          (BSet.union (Ca s1) (Ca s2)))) in TifNext; auto.
       apply typeof_expr_wk_downgrade_set with (s:=d) in TifE;auto.
       assert (SUB : Typ.order (Typ.guard (Typ.downgrade_set d t) (Typ.singleton p))
    (Typ.guard t (Typ.downgrade_set (BSet.diff d (Typ.to_set (Typ.guard (Typ.downgrade_set d t) (Typ.singleton p)))) (Typ.singleton p))) = true).
       {
         unfold Typ.guard.
         destruct (Typ.is_L t) eqn:ISL.
         - apply Typ.is_L_inv in ISL. subst.
           rewrite Typ.downgrade_set_L. simpl. rewrite BSet.is_empty_empty.
           apply Typ.order_refl.
         - destruct (Typ.is_L (Typ.downgrade_set d t)) eqn:ISD.
           + apply Typ.order_bot.
           + rewrite Typ.downgrade_set_singleton.
             rewrite BSet.mem_diff_iff.
             rewrite Typ.to_set_singleton.
             rewrite BSet.singleton_xx.
             rewrite andb_comm. simpl. apply BSet.subset_refl.
       }
       eapply Tif_n with (p:=p);auto.
       eauto.
       { rewrite Typ.downgrade_set_join in TifThen.
         rewrite Typ.downgrade_set_guard  in TifThen.
         eapply typeof_stmt_n_order.
         apply TifThen.
         apply TEnv.downgrade_set_mono1.
         apply BSet.subset_diff_l.
         apply Typ.order_join2.
         apply Typ.downgrade_set_mono1.
         apply BSet.subset_diff_l.
         auto.
       }
       { rewrite Typ.downgrade_set_join in TifElse.
         rewrite Typ.downgrade_set_guard  in TifElse.
         eapply typeof_stmt_n_order.
         apply TifElse.
         apply TEnv.downgrade_set_mono1.
         apply BSet.subset_diff_l.
         apply Typ.order_join2.
         apply Typ.downgrade_set_mono1.
         apply BSet.subset_diff_l.
         auto.
       }
       {
         eapply typeof_stmt_n_order.
         apply TifNext.
         rewrite TEnv.downgrade_set_join.
         apply TEnv.order_join2.
         - apply TEnv.downgrade_set_mono1.
           subset.
           (*
           rewrite (BSet.union_sym (Annot.high (dwn_annot s1))).
         rewrite! BSet.diff_union_r.
         apply BSet.subset_diff_2.
         apply BSet.subset_diff_l.
         unfold dwn_annot. rewrite high_down_stmt.
         subset. *)
         - apply TEnv.downgrade_set_mono1.
           subset.
           (*rewrite! BSet.diff_union_r.
         apply BSet.subset_diff_2.
         apply BSet.subset_diff_l.
         unfold dwn_annot. rewrite high_down_stmt.
         subset. *)
         - apply Typ.downgrade_set_mono1.
           apply BSet.subset_diff_l.
       }
       destruct Tifwk;constructor;auto.
       *  apply TEnv.downgrade_set_mono2 with (s:= (BSet.diff d (Ca  s))) in weaken_env.
          eapply TEnv.order_trans with (2:= weaken_env).
       rewrite downgrade_set_classify_if.
       eapply order_isPP.
       apply Typ.downgrade_set_order.
       rewrite <- BSet.diff_union_r.
       apply TEnv.downgrade_set_mono1.
       apply BSet.subset_diff_2.
       apply BSet.subset_refl.
       {
         apply annot_stmt_order_Ca in weaken_stmt.
         rewrite <- weaken_stmt.
         unfold mk_if_full. unfold Ca at 4. simpl.
         assert (BSet.subset (Typ.to_set (Typ.guard (Typ.downgrade_set d t) (Typ.singleton p))) (BSet.singleton p) = true).
         {
           unfold Typ.guard.
           destruct (Typ.is_L (Typ.downgrade_set d t)).
           simpl. subset.
           simpl. apply BSet.subset_refl.
         }
         unfold Ca.
         subset.
       }
       {
         intro.
         assert (SUBp : BSet.subset (BSet.singleton p) (Ca s) = true).
         {
           apply annot_stmt_order_Ca in weaken_stmt.
           rewrite <- weaken_stmt.
           unfold mk_if_full. unfold Ca;simpl.
           subset.
         }
         apply BSet.uniq.
         intros.
         subset; destruct (Pos.eq_dec x p);subset.
       }
       * apply annot_order_mk_if_full_dwn_annot in weaken_stmt; auto.
         eapply annot_order_trans;eauto.
         apply annot_order_mk_if; auto.
         apply annot_stmt_order_refl.
         apply annot_stmt_order_refl.
         apply annot_stmt_order_refl.
         apply BSet.subset_refl.
         apply Typ.downgrade_set_order.
    +
       assert (CaSame : Ca ar = Ca s).
       {
         destruct TforWk.
         apply annot_stmt_order_Ca in weaken_stmt.
         unfold Ca in weaken_stmt. simpl in weaken_stmt.
         apply weaken_stmt.
       }
      eapply IHn with (d:= (BSet.diff d (Ca s))) in TforIter.
       eapply Tfor_n.
       rewrite TEnv.downgrade_set_set in TforIter.
       eapply typeof_stmt_n_order.
       apply TforIter.
       apply TEnv.order_set.
       apply TEnv.downgrade_set_mono1.
       rewrite CaSame.
       subset.
       apply Typ.downgrade_set_mono1.
       apply BSet.subset_diff_l.
       apply Typ.downgrade_set_mono1.
       apply BSet.subset_diff_l.
       * eapply TEnv.order_trans.
         eapply TEnv.downgrade_set_mono2.
         eauto.
         eapply TEnv.downgrade_set_mono1.
         rewrite <- BSet.diff_union_r.
         apply BSet.subset_diff_l.
       *
         rewrite Ca_dwn_annot.
         rewrite <- TEnv.downgrade_set_classify_set.
         f_equal. auto.
         rewrite CaSame.
         apply BSet.inter_diff_empty.
       *
         destruct TforWk; constructor.
(*         assert (Sars : BSet.subset (Annot.high ar) (Annot.high s) = true).
         { unfold annot_order in weaken_stmt. dand.
           apply weaken_stmt3.
         }
         eapply TEnv.order_trans.
         eapply TEnv.downgrade_set_mono1.
         apply BSet.subset_diff_2.
         apply BSet.subset_refl.
         apply BSet.subset_refl. *)
         rewrite CaSame.
         rewrite <- BSet.diff_union_r.
         rewrite BSet.union_idem.
         apply TEnv.downgrade_set_mono2.
         auto.
         apply annot_order_dwn_mk_for;auto.
         apply weaken_stmt.
       * auto.
Qed.
*)


Lemma unmod_skip :
  forall r g x o ti G G' D k n
         (TI : Typ.order ti (TEnv.get x G) = true)
         (TskipWk : TEnv.order G G'),
  typeof_stmt_n D o k (G [x → ti]) (Annot.mk r Sskip g) (G' [x → ti]) n /\
  TEnv.order (G' [x → ti]) G'.
Proof.
  intros.
split.
      { constructor.
        apply TEnv.order_set; auto.
        apply Typ.order_refl.
      }
      {
        repeat intro.
        rewrite TEnv.get_set.
        destruct (Pos.eq_dec x0 x);subst;auto.
        eapply Typ.order_trans;eauto.
        apply TEnv.order_refl.
      }
Qed.

Lemma unmod_assign : forall
    n
    (D : var -> Typ.t)
    (o : option var)
    (G G' : TEnv.t)
    (x : var)
    (k ti : Typ.t)
    (e : expr)
    (r g : BSet.t)
    (x0 : var)
    (SUB : unmod (Sassign x0 e) x = true)
    (TI : Typ.order ti (TEnv.get x G) = true)
    (LEAK : forall p : var,
         BSet.mem p (C (Sassign x0 e)) = true ->
         isPP o p = false)
    (t : Typ.t)
    (TassignExpr : typeof_expr_wk D G e t r)
    (TassignWk : TEnv.order (G [x0 → t ⊔ k]) G'),
  typeof_stmt_n D o k (G [x → ti]) (Annot.mk r (Sassign x0 e) g)
    (G' [x → ti]) n /\ TEnv.order (G' [x → ti]) G'.
Proof.
  intros.
  simpl in SUB.
      destruct (Pos.eq_dec x x0); try discriminate.
      split.
      {
        apply typeof_expr_order_wk with (G2 := (G [x → ti])) in TassignExpr.
        destruct TassignExpr as (t1 & r1 & TassignExpr & O1 & S1).
        eapply Tassign_n.
        eapply TWk;eauto.
        repeat intro.
        specialize (TassignWk x1).
        destruct (Pos.eq_dec x1 x).
        * subst.
          rewrite TEnv.gss.
          rewrite TEnv.gso by auto.
          rewrite TEnv.gss.
          apply Typ.order_refl.
      * rewrite (TEnv.gso x1 x) by auto.
        eapply Typ.order_trans ; eauto.
        rewrite! TEnv.get_set.
        destruct (Pos.eq_dec x1 x); subst; try congruence.
        destruct (Pos.eq_dec x1 x0); subst; try congruence.
        apply Typ.order_refl.
        apply Typ.order_refl.
      * repeat intro.
        rewrite TEnv.get_set.
        destruct (Pos.eq_dec x1 x); subst;auto.
        apply Typ.order_refl.
      }
      {
        repeat intro.
        specialize (TassignWk x1).
        destruct (Pos.eq_dec x1 x).
        * subst.
        rewrite TEnv.gss.
        rewrite TEnv.gso in TassignWk by auto.
        eapply Typ.order_trans;eauto.
        * rewrite TEnv.gso by auto.
          apply Typ.order_refl.
      }
Qed.


Lemma unmod_write : forall
    n (D : var -> Typ.t)
    (o : option var)
    (G G' : TEnv.t)
    (x : var)
    (k ti : Typ.t)
    (x0 : var)
    (e1 e2 : expr)
    (r g : BSet.t)
    (SUB : unmod (Swrite x0 e1 e2) x = true)
    (TI : Typ.order ti (TEnv.get x G) = true)
    (LEAK : forall p : var,
         BSet.mem p (C (Swrite x0 e1 e2 )) =
         true -> isPP o p = false)
    (t1 : Typ.t)
    (r1 : BSet.t)
    (t2 : Typ.t)
    (r2 : BSet.t)
    (TwriteE1 : typeof_expr_wk D G e1 t1 r1)
    (TwriteE2 : typeof_expr_wk D G e2 t2 r2)
    (TwriteNOTH : t1 <> Typ.H)
    (TwriteO : Typ.order ((t1 ⊔ t2) ⊔ Typ.guard k Typ.H) (D x0) = true)
    (TwriteLk : BSet.subset ((r1 ∪ r2) ∪ Typ.to_set t1) r = true)
    (TwriteWk : TEnv.order G G'),
    typeof_stmt_n D o k (G [x → ti])
      (Annot.mk r (Swrite x0 e1 e2) g)
    (G' [x → ti]) n /\ TEnv.order (G' [x → ti]) G'.
Proof.
  intros.
  assert (LE : TEnv.order (G [x → ti]) G).
      {
        repeat intro.
        rewrite TEnv.get_set.
        destruct (Pos.eq_dec x1 x); auto.
        subst. auto.
        apply Typ.order_refl.
      }
      split.
      {
        apply typeof_expr_order_wk with (G2 := (G [x → ti])) in TwriteE1.
        destruct TwriteE1 as (t1' & r1' & TwriteE1 & O1 & S1).
        apply typeof_expr_order_wk with (G2 := (G [x → ti])) in TwriteE2.
        destruct TwriteE2 as (t2' & r2' & TwriteE2 & O2 & S2).
        econstructor.
        eapply TwriteE1.
        eapply TwriteE2.
        intro. subst.
        apply Typ.order_H_inv in O1. congruence.
        eapply Typ.order_trans;eauto.
        apply Typ.order_join2;auto.
        apply Typ.order_join2;auto.
        apply Typ.order_refl.
        eapply BSet.subset_trans;eauto.
        apply BSet.subset_union2.
        subset.
        apply Typ.to_set_subset. congruence.
        auto.
        apply TEnv.order_set;auto.
        apply Typ.order_refl.
        auto. auto.
      }
      {
      repeat intro.
      rewrite TEnv.get_set.
      destruct (Pos.eq_dec x1 x).
      subst.
      eapply Typ.order_trans;eauto.
      apply TEnv.order_refl.
      }
Qed.

Lemma unmod_same_typing_n :
  forall n s
         D o  G G' x k ti
         (SUB : unmod (Annot.elt s) x = true)
         (TSTM : typeof_stmt_n D o  k G s G' n)
         (TI   : Typ.order ti (TEnv.get x G) = true)
         (CC   : Typ.classify_set (Ca s) ti = ti)
         (LEAK : forall p, BSet.mem p (C (Annot.elt s)) = true -> isPP o p = false)
  ,
    typeof_stmt_n D o k (G[x → ti]) s (G'[x → ti]) n /\
      TEnv.order (TEnv.set x ti G') G'.
Proof.
  induction n.
  - intros.
    inv TSTM.
    + eapply unmod_skip; eauto.
    + eapply unmod_assign; eauto.
    + eapply unmod_write; eauto.
  - intros.
    inv TSTM.
    + eapply unmod_skip; eauto.
    + eapply unmod_assign; eauto.
    + eapply unmod_write; eauto.
    +
      eapply IHn with (x:=x) (ti:= ti) in Tseq1.
      destruct Tseq1 as (Tseq1 & SUB1).
(*      apply typeof_stmt_n_order with (2:= SUB1) (k':=k) in Tseq2.*)
      eapply IHn with (x:=x) (ti:=ti) in Tseq2.
      destruct Tseq2 as (Tseq2 & SUB2).
      split.
      eapply Tseq_n.
      eapply Tseq1.
      eapply Tseq2.
      {
        destruct TseqWk ; constructor.
        apply TEnv.order_set; auto.
        apply Typ.order_refl.
        auto.
      }
      {
        repeat intro.
        rewrite TEnv.get_set.
        destruct (Pos.eq_dec x0 x); subst.
        destruct TseqWk.
        specialize (SUB2 x).
        rewrite TEnv.gss in SUB2.
        eapply Typ.order_trans;eauto.
        apply Typ.order_refl.
      }
      {
        destruct TseqWk.
        apply annot_stmt_order_unmod with (x := x) in weaken_stmt.
        rewrite SUB in weaken_stmt.
        simpl in weaken_stmt.
        lia.
      }
      {
        specialize (SUB1 x).
        rewrite TEnv.gss in SUB1.
        eapply Typ.order_trans;eauto.
        apply Typ.order_refl.
      }
      {
        eapply Typ.classify_set_subset_fix; eauto.
        destruct TseqWk.
        apply annot_stmt_order_Ca in weaken_stmt.
        rewrite <-weaken_stmt.
        unfold Ca; simpl.
        subset.
      }
      {intros.
       apply LEAK.
       destruct TseqWk.
       apply annot_stmt_order_C in weaken_stmt.
       simpl in weaken_stmt. rewrite <- weaken_stmt.
       subset.
      }
      {
        destruct TseqWk.
        apply annot_stmt_order_unmod with (x := x) in weaken_stmt.
        rewrite SUB in weaken_stmt.
        simpl in weaken_stmt.
        lia.
      }
      auto.
      {
        eapply Typ.classify_set_subset_fix; eauto.
        destruct TseqWk.
        apply annot_stmt_order_Ca in weaken_stmt.
        rewrite <-weaken_stmt.
        unfold Ca; simpl.
        subset.
      }
      {intros.
       apply LEAK.
       destruct TseqWk.
       apply annot_stmt_order_C in weaken_stmt.
       simpl in weaken_stmt. rewrite <- weaken_stmt.
       subset.
      }
    +
      assert (ISPP : isPP o p = false).
      {
        apply LEAK.
        destruct Tifwk.
        apply annot_stmt_order_C in weaken_stmt.
        rewrite <- weaken_stmt.
        simpl. subset.
      }
      apply typeof_expr_order_wk with (G2 := (G [x → ti])) in TifE.
      destruct TifE as (t1' & r1' & TifE & O1 & S1).
      apply typeof_stmt_n_order with (k':= (k ⊔ Typ.guard t1' (Typ.singleton p))) (G1:=G) in TifThen.
      apply IHn with (x:=x) (ti:=ti) in TifThen.
      destruct TifThen as (TifThen & OT).
      apply typeof_stmt_n_order with (k':= (k ⊔ Typ.guard t1' (Typ.singleton p))) (G1:=G) in TifElse.
      apply IHn with (x:=x) (ti:=ti) in TifElse.
      destruct TifElse as (TifElse & OE).
(*      apply typeof_stmt_n_order with (k':=k) (G1 := TEnv.join G1 G2) in TifNext. *)
      apply IHn with (x:= x) (ti:=ti) in TifNext.
      destruct TifNext as (TifNext & O3).
      split.
      {eapply Tif_n.
      eapply TifE.
      reflexivity.
      eapply TifThen.
      eapply TifElse.
      rewrite TEnv.join_set.
      rewrite Typ.join_idem.
      eapply TifNext.
      destruct Tifwk; constructor;auto.
      {
        rewrite ISPP in *.
        simpl in *.
        apply TEnv.order_set;auto.
        apply Typ.order_refl.
      }
      { eapply annot_order_trans; eauto.
        apply annot_order_mk_if.
        apply annot_stmt_order_refl.
        apply annot_stmt_order_refl.
        apply annot_stmt_order_refl.
        subset. subset.
      }
      }
      {
        destruct Tifwk.
        rewrite ISPP in *.
        simpl in *.
        repeat intro.
        rewrite TEnv.get_set.
        destruct (Pos.eq_dec x0 x).
        subst.
        specialize (O3 x).
        rewrite! TEnv.gss in *.
        eapply Typ.order_trans;eauto.
        apply Typ.order_refl.
      }
      {
        destruct Tifwk.
        apply annot_stmt_order_unmod with (x := x) in weaken_stmt.
        rewrite SUB in weaken_stmt.
        simpl in weaken_stmt.
        lia.
      }
      {
        specialize (OT x).
        rewrite TEnv.gss in OT.
        eapply Typ.order_trans;eauto.
        rewrite TEnv.get_join.
        apply Typ.join_ub_l.
      }
      {
        eapply Typ.classify_set_subset_fix; eauto.
        destruct Tifwk.
        apply annot_stmt_order_Ca in weaken_stmt.
        rewrite <-weaken_stmt.
        unfold Ca; simpl.
        subset.
      }
      {
        intros.
        apply LEAK.
        destruct Tifwk.
        apply annot_stmt_order_C in weaken_stmt.
        rewrite <- weaken_stmt.
        simpl. subset.
      }
      {
        destruct Tifwk.
        apply annot_stmt_order_unmod with (x := x) in weaken_stmt.
        rewrite SUB in weaken_stmt.
        simpl in weaken_stmt.
        lia.
      }
      auto.
      {
        eapply Typ.classify_set_subset_fix; eauto.
        destruct Tifwk.
        apply annot_stmt_order_Ca in weaken_stmt.
        rewrite <-weaken_stmt.
        unfold Ca; simpl.
        subset.
      }
      {
        intros.
        apply LEAK.
        destruct Tifwk.
        apply annot_stmt_order_C in weaken_stmt.
        rewrite <- weaken_stmt.
        simpl. subset.
      }
      apply TEnv.order_refl.
      {
        apply Typ.order_join2.
        apply Typ.order_refl.
        apply Typ.order_guard;auto.
        apply Typ.order_refl.
      }
      {
        destruct Tifwk.
        apply annot_stmt_order_unmod with (x := x) in weaken_stmt.
        rewrite SUB in weaken_stmt.
        simpl in weaken_stmt.
        lia.
      }
      auto.
            {
        eapply Typ.classify_set_subset_fix; eauto.
        destruct Tifwk.
        apply annot_stmt_order_Ca in weaken_stmt.
        rewrite <-weaken_stmt.
        unfold Ca; simpl.
        subset.
      }

      {
        intros.
        apply LEAK.
        destruct Tifwk.
        apply annot_stmt_order_C in weaken_stmt.
        rewrite <- weaken_stmt.
        simpl. subset.
      }
      apply TEnv.order_refl.
      {
        apply Typ.order_join2.
        apply Typ.order_refl.
        apply Typ.order_guard;auto.
        apply Typ.order_refl.
      }
      { repeat intro.
        rewrite TEnv.get_set.
        destruct (Pos.eq_dec x0 x);subst;auto.
        apply TEnv.order_refl.
      }
        +
          assert (DIF : x <> x0).
          {
            destruct TforWk.
            apply annot_stmt_order_unmod with (x:=x) in weaken_stmt.
            rewrite SUB in weaken_stmt.
            simpl in weaken_stmt. intro.
            subst.
            rewrite andb_true_iff in weaken_stmt.
            destruct (Pos.eq_dec x0 x0); try discriminate.
            simpl in *. intuition congruence.
            congruence.
          }
      apply IHn with (x:=x) (ti:= ti) in TforIter.
      destruct TforIter as (TforIter & O).
      split.
      rewrite TEnv.set_swap in TforIter by auto.
      eapply Tfor_n.
      eapply TforIter.
      apply TEnv.order_set;auto.
      apply Typ.order_refl.
      rewrite <- TEnv.set_classify_set.
      rewrite TforFix.
      reflexivity.
      {
        eapply Typ.classify_set_subset_fix; eauto.
        destruct TforWk.
        apply annot_stmt_order_Ca in weaken_stmt.
        rewrite <-weaken_stmt.
        unfold Ca; simpl.
        subset.
      }
      destruct TforWk.
      constructor;eauto.
      apply TEnv.order_set;auto.
      apply Typ.order_refl.
      {
        destruct TforWk.
        repeat intro.
        rewrite TEnv.get_set.
        destruct (Pos.eq_dec x1 x); subst.
        eapply Typ.order_trans;eauto.
        eapply Typ.order_trans;eauto.
        apply Typ.order_refl.
      }
      {
        destruct TforWk.
        apply annot_stmt_order_unmod with (x:=x) in weaken_stmt.
        rewrite SUB in weaken_stmt.
        simpl in weaken_stmt. lia.
      }
      {
        rewrite TEnv.gso by auto.
        specialize (TforLastO x).
        eapply Typ.order_trans;eauto.
      }
{
        eapply Typ.classify_set_subset_fix; eauto.
        destruct TforWk.
        apply annot_stmt_order_Ca in weaken_stmt.
        rewrite <-weaken_stmt.
        unfold Ca; simpl.
        subset.
      }
      {
        intros.
        apply LEAK.
        destruct TforWk.
        apply annot_stmt_order_C in weaken_stmt.
        rewrite <- weaken_stmt.
        simpl. subset.
      }
Qed.

Fixpoint has_cond_join (x : var) (si : stmt) :
    forall  (sf sr : stmt),
      stmt_join si sf = Some sr -> has_cond sr x = has_cond si x.
Proof.
    assert (A : forall a1 a2 ar ,
             annot_join stmt_join a1 a2 = Some ar ->
             has_cond (Annot.elt ar) x = has_cond (Annot.elt a1) x).
  {
    destruct a1,a2,ar; unfold annot_join ; simpl.
    destruct (stmt_join elt elt0) eqn:J ; try discriminate.
    apply has_cond_join with (x:=x) in J.
    intros. inv H.
    auto.
  }
  intros.
  destruct si,sf; simpl in *; try discriminate.
  - inv H. simpl;auto.
  - destruct (Pos.eq_dec x0 x1); try discriminate.
    destruct (of_sb (left e1) && of_sb (expr_eq_dec e e0)); try discriminate.
    inv H. simpl. reflexivity.
  - destruct (        of_sb (Pos.eq_dec x0 x1) && of_sb (expr_eq_dec e1 e0) &&
        of_sb (expr_eq_dec e2 e3)
             ); try discriminate.
    inv H. reflexivity.
  - destruct (annot_join stmt_join s1 s0) eqn:J1 ; try discriminate.
    destruct (annot_join stmt_join s2 s3) eqn:J2 ; try discriminate.
    apply A in J1.
    apply A in J2.
    inv H.
    simpl. rewrite J1. rewrite J2. reflexivity.
  - rewrite andb_comm in H. destruct (expr_eq_dec e e0); try discriminate.
    subst.
    destruct (of_sb (left eq_refl) && of_sb (Pos.eq_dec t t0)); try discriminate.
    destruct (annot_join stmt_join s1 s0) eqn:J1 ; try discriminate.
    destruct (annot_join stmt_join s2 s4) eqn:J2 ; try discriminate.
    destruct (annot_join stmt_join s3 s5) eqn:J3 ; try discriminate.
    apply A in J1. apply A in J2. apply A in J3.
    inv H.
    simpl. rewrite J1. rewrite J2. rewrite J3. reflexivity.
  - destruct (Pos.eq_dec x0 x1); try discriminate.
    subst.
    destruct (of_sb (left eq_refl) && of_sb (Nat.eq_dec c1 c0) &&
        of_sb (Nat.eq_dec c2 c3)); try discriminate.
    destruct (annot_join stmt_join s s0) eqn:J1 ; try discriminate.
    apply A in J1.
    inv H.
    simpl. congruence.
Qed.




Definition seq (s1 s2 : Annot.t stmt) :=
  Annot.mk (BSet.union (Annot.leaked s1) (Annot.leaked s2))
    (Sseq s1 s2)
    (BSet.union (Annot.high s1) (Annot.high s2)).

Definition sfor (x:var) (c1 c2:nat) (s:Annot.t stmt) :=
  Annot.mk (Annot.leaked s) (Sfor x c1 c2 s) (Annot.high s).

Definition sif (p:positive) (e:expr) (s1 s2 s3: Annot.t stmt) :=
  Annot.mk (BSet.union (Annot.leaked s1) (BSet.union (Annot.leaked s2) (Annot.leaked s3)))
           (Sif p e s1 s2 s3)
           (BSet.union (Annot.high s1) (BSet.union (Annot.high s2) (Annot.high s3))).



Lemma classify_delta:
  forall D t p
    (SIM: is_simple_env D),
  Typ.classify p (D t) = D t.
Proof.
  intros.
  unfold Typ.classify.
  unfold is_simple_env in SIM.
  specialize SIM with t.
  destruct (D t); eauto.
  unfold Typ.is_simple in *. simpl in *.
  rewrite orb_comm in SIM.
  simpl in SIM.
  apply BSet.is_empty_true in SIM.
  subst. simpl. reflexivity.
Qed.

Lemma order_mem_f:
  forall p t1 t2
    (ORD: Typ.order t1 t2 = true)
    (MEM: Typ.mem p t2 = false),
    Typ.mem p t1 = false.
Proof.
  unfold Typ.mem, Typ.order.
  intros.
  destruct t1, t2; auto; try discriminate.
  eapply BSet.subset_mem_f  in ORD; eauto.
Qed.

Lemma typeof_expr_classify :
  forall D G r p t e
    (TYP : typeof_expr D G e t r)
    (MEM : BSet.mem p r = false)
    (SIM : is_simple_env D),
    typeof_expr D (TEnv.classify p G) e (Typ.classify p t) r.
Proof.
  intros.
  induction TYP.
    - rewrite <- TEnv.get_classify.
      apply TVar.
    - apply TCst.
    - rewrite Typ.classify_join.
      rewrite BSet.mem_union_iff in MEM.
      rewrite orb_false_iff in MEM. inv MEM.
      apply TOp; auto.
    - rewrite! Typ.classify_join.
      rewrite! BSet.mem_union_iff in MEM.
      rewrite! orb_false_iff in MEM. inv MEM. inv H0.
      apply TCond; auto.
    - rewrite Typ.classify_join.
      rewrite classify_delta; auto.
      rewrite BSet.mem_union_iff in MEM.
      rewrite orb_false_iff in MEM. inv MEM.
      specialize (IHTYP H0).
      assert (Typ.mem p t1 = false).
      { destruct t1; auto. destruct H. auto. }
      rewrite Typ.classify_mem; auto.
      apply TRead; auto.
      replace t1 with (Typ.classify p t1); auto.
      apply Typ.classify_mem; auto.
Qed.

Lemma typeof_expr_wk_classify :
  forall D G r p t e
    (TYP : typeof_expr_wk D G e t r)
    (MEM : BSet.mem p r = false)
    (SIM : is_simple_env D),
    typeof_expr_wk D (TEnv.classify p G) e (Typ.classify p t) r.
Proof.
  intros.
  induction TYP.
    - rewrite <- TEnv.get_classify.
      apply TVar_wk.
    - apply TCst_wk.
    - rewrite Typ.classify_join.
      rewrite BSet.mem_union_iff in MEM.
      rewrite orb_false_iff in MEM. inv MEM.
      apply TOp_wk; auto.
    - rewrite! Typ.classify_join.
      rewrite! BSet.mem_union_iff in MEM.
      rewrite! orb_false_iff in MEM. inv MEM. inv H0.
      apply TCond_wk; auto.
    - rewrite Typ.classify_join.
      rewrite classify_delta; auto.
      rewrite BSet.mem_union_iff in MEM.
      rewrite orb_false_iff in MEM. inv MEM.
      specialize (IHTYP H0).
      assert (Typ.mem p t1 = false).
      { destruct t1; auto. destruct H. auto. }
      rewrite Typ.classify_mem; auto.
      apply TRead_wk; auto.
      replace t1 with (Typ.classify p t1); auto.
      apply Typ.classify_mem; auto.
    -  eapply TWk.
      apply Typ.classify_mono; eauto.
      eauto.
      apply IHTYP.
        subset.
Qed.



Lemma classify_if:
  forall p (b: bool) p0 G',
  TEnv.classify p
   (if b then TEnv.classify p0 G' else G') =
   if b then TEnv.classify p0 (TEnv.classify p G') else TEnv.classify p G'.
Proof.
  intros.
  destruct b; auto.
  apply TEnv.classify_swap.
Qed.

Lemma non_empty_mem:
  forall x s,
  BSet.mem x s = true ->
  BSet.is_empty s = false.
Proof.
  intros.
  destruct (BSet.is_empty s) eqn:E;auto.
  apply BSet.is_empty_true in E.
  subst.
  rewrite BSet.mem_empty in H.
  discriminate.
Qed.


Lemma empty_mem:
  forall x s,
    BSet.is_empty s = true ->
    BSet.mem x s = false.
Proof.
  intros.
  apply BSet.is_empty_true in H.
  subst.
  rewrite BSet.mem_empty.
  reflexivity.
Qed.


Lemma typeof_stmt_n_classify_assign :
  forall n
         (D : var -> Typ.t)
         (G G' : TEnv.t)
         (p : var)
         (e : expr)
         (r g : BSet.t)
         (x : var)
         (MEM : BSet.mem p r = false)
         (SIM : is_simple_env D)
         (t : Typ.t)
         (TassignExpr : typeof_expr_wk D G e t r)
         (TassignWk : TEnv.order (G [x → t ⊔ Typ.L]) G'),
  typeof_stmt_n D None  Typ.L (TEnv.classify p G)
    (Annot.mk r (Sassign x e) g) (TEnv.classify p G') n.
Proof.
 intros.
 econstructor.
 apply typeof_expr_wk_classify; eauto.
 change Typ.L with (Typ.classify p Typ.L).
 rewrite <- Typ.classify_join.
 rewrite <- TEnv.set_classify.
 apply TEnv.classify_mono; auto.
Qed.


Lemma typeof_stmt_n_classify_write :
  forall n
         (D : var -> Typ.t)
         (G G' : TEnv.t)
         (p x : var)
         (e1 e2 : expr)
         (r g : BSet.t)
         (MEM : BSet.mem p r = false)
         (SIM : is_simple_env D)
         (t1 : Typ.t)
         (r1 : BSet.t)
         (t2 : Typ.t)
         (r2 : BSet.t)
         (TwriteE1 : typeof_expr_wk D G e1 t1 r1)
         (TwriteE2 : typeof_expr_wk D G e2 t2 r2)
         (TwriteNOTH : t1 <> Typ.H)
         (TwriteO : Typ.order ((t1 ⊔ t2) ⊔ Typ.guard Typ.L Typ.H) (D x) = true)
         (TwriteLk : BSet.subset ((r1 ∪ r2) ∪ Typ.to_set t1) r = true)
         (TwriteWk : TEnv.order G G'),
  typeof_stmt_n D None  Typ.L (TEnv.classify p G)
    (Annot.mk r (Swrite x e1 e2) g) (TEnv.classify p G') n.
Proof.
  intros.
  simpl in *.
  assert (typeof_expr_wk D (TEnv.classify p G) e1 (Typ.classify p t1) r1).
  { apply typeof_expr_wk_classify; auto.
    subset.
  }
  assert (typeof_expr_wk D (TEnv.classify p G) e2 (Typ.classify p t2) r2).
  { apply typeof_expr_wk_classify; auto.
    subset.
  }
  subst.
  eapply Twrite_n. eauto. eauto.
  subst.
  *  intro.
     apply Typ.classify_H_inv in H1.
     destruct H1. congruence.
     revert H1. subset.
  *
      unfold Typ.guard in *. simpl in *.
      rewrite BSet.is_empty_empty.
      rewrite Typ.join_bot_r in *.
      specialize (SIM x).
      unfold Typ.is_simple in SIM.
      rewrite orb_true_iff in SIM. destruct SIM.
      { apply Typ.is_L_inv in H1. rewrite H1 in *.
        apply Typ.order_is_L_r in TwriteO.
        apply Typ.join_L_inv in TwriteO. dand; subst.
        rewrite Typ.classify_L.
        rewrite Typ.join_idem. apply Typ.order_refl.
      }
      { apply Typ.is_H_inv in H1. rewrite H1.
        apply Typ.order_top. }
    * rewrite Typ.classify_id.
      auto.
      intro. unfold Typ.pp_in in H1.
      revert H1. subset.
    *  apply TEnv.classify_mono;auto.
Qed.

Lemma typeof_stmt_n_classify :
  forall n D k G G' s  p
         (MEM : BSet.mem p (Annot.leaked s) = false)
         (CON: k = Typ.L)
         (EMP:  Annot.high s = BSet.empty)
         (SIM : is_simple_env D),
    typeof_stmt_n D None k G s G' n ->
    typeof_stmt_n D None k (TEnv.classify p G) s (TEnv.classify p G' ) n.
Proof.
  induction n.
  - intros. inv H.
    + constructor.
      apply TEnv.classify_mono. auto.
    + eapply typeof_stmt_n_classify_assign;eauto.
    + eapply  typeof_stmt_n_classify_write; eauto.
  - intros. inv H.
    + constructor.
      apply TEnv.classify_mono. auto.
    + eapply typeof_stmt_n_classify_assign;eauto.
    + eapply  typeof_stmt_n_classify_write; eauto.
    + assert (MEM1 : BSet.mem p (Annot.leaked s1) = false).
      {
        destruct TseqWk.
        unfold annot_order in weaken_stmt ;
          simpl in *. destruct s.
        simpl in *.
        destruct elt ; simpl; try tauto.
        dand.
        subset.
      }
      assert (MEM2 : BSet.mem p (Annot.leaked s2) = false).
      {
        destruct TseqWk.
        unfold annot_order in weaken_stmt ;
          simpl in *. destruct s.
        simpl in *.
        destruct elt ; simpl; try tauto.
        dand.
        subset.
      }
      assert (HIGHS1 : Annot.high s1 = BSet.empty /\ Annot.high s2 = BSet.empty).
      {
        destruct TseqWk.
        unfold annot_order in weaken_stmt ;
          simpl in *. destruct s.
        simpl in *.
        destruct elt ; simpl; try tauto.
        dand.
        subst.
        apply BSet.subset_is_empty  in weaken_stmt3.
        apply BSet.union_empty_inv in weaken_stmt3.
        tauto.
      }
      dand.
      apply IHn with (p:= p) in Tseq1;auto.
      apply IHn with (p:= p) in Tseq2;auto.
      eapply Tseq_n.
      eauto. eauto.
      destruct TseqWk; constructor;auto.
      apply TEnv.classify_mono;auto.
    +
      (*IF *)
      destruct Tifwk.
      unfold annot_order in weaken_stmt.
      simpl in weaken_stmt.
      destruct s as [r s g] ; simpl in weaken_stmt.
      destruct s ; try tauto.
      dand. subst.
      simpl in EMP. subst.
      apply BSet.subset_is_empty  in weaken_stmt3.
      apply BSet.union_empty_inv in weaken_stmt3.
      dand.
      apply BSet.union_empty_inv in weaken_stmt2.
      dand.
      apply BSet.union_empty_inv in weaken_stmt6.
      dand.
      assert (LT:  t = Typ.L).
      {
      apply Typ.to_set_empty_inv in weaken_stmt8.
      apply Typ.guard_L in weaken_stmt8.
      destruct weaken_stmt8. auto.
      destruct H.
      apply Typ.singleton_not_L in H0; tauto.
      intro.
      apply Typ.guard_singleton_not_H in H; tauto.
    }
    assert (TYPS: (Typ.L ⊔ Typ.guard t (Typ.singleton p)) = Typ.L).
    {
      unfold Typ.guard. subst.  simpl.
      rewrite BSet.is_empty_empty.
      simpl.
      rewrite BSet.union_idem.
      reflexivity.
    } subst.
    apply IHn with (p:=p) in TifThen;auto.
    apply IHn with (p:=p) in TifElse;auto.
    apply IHn with (p:=p) in TifNext;auto.
    assert (Typ.L =  (Typ.classify p Typ.L)).
    { subst. reflexivity.
    }
    assert (Typ.guard Typ.L (Typ.singleton t0) = Typ.guard (Typ.classify p Typ.L) (Typ.singleton t0)).
    { subst.
      reflexivity.
    }
    rewrite H.
    eapply Tif_n with (p:= t0).
    apply typeof_expr_wk_classify; auto. eauto.
    { simpl in MEM.
      subset.
    }
    rewrite <- H0.
    rewrite <- H.
    reflexivity.
    eapply TifThen.
    eapply TifElse.
    rewrite <- TEnv.classify_join. eauto.
    constructor.
    rewrite <- H.
    simpl. apply TEnv.classify_mono;auto.
    unfold annot_order; simpl.
    rewrite weaken_stmt1 in *.
    rewrite weaken_stmt3 in *.
    rewrite weaken_stmt2 in *.
    rewrite! BSet.union_idem.
    rewrite <- H.
    rewrite Typ.guard_L_l.
    split_and;auto.
    simpl in *.
    subset.
    simpl in * ; subset.
    simpl in * ; subset.
  +
    assert (AR: Annot.high ar = BSet.empty /\ BSet.mem p (Annot.leaked ar) = false).
    {
      destruct ar as [r ar g].
      destruct TforWk.
      unfold annot_order in weaken_stmt.
      destruct s; simpl in *.
      destruct elt ; try tauto.
      dand ; subst.
      split.
      apply BSet.subset_is_empty in weaken_stmt3;auto.
      subset.
    }
    dand.
    eapply Tfor_n with  (Go:=TEnv.classify p Go).
    { change Typ.L with (Typ.classify p Typ.L) at 2.
    rewrite <- TEnv.set_classify.
    eapply IHn; auto.
    eapply AR1.
    auto.
    eauto.
    }
(*    { change Typ.L with (Typ.classify p Typ.L) at 2.
    rewrite <- TEnv.set_classify.
    eapply IHn; auto.
    eauto.
    } *)
    apply TEnv.classify_mono; auto.
    rewrite <- TEnv.classify_swap_set.
    congruence.
    instantiate (1:=c2).
    instantiate (1:=c1).
    destruct TforWk ; constructor ; auto.
    apply TEnv.classify_mono; auto.
Qed.


Inductive wf_stm: BSet.t -> Annot.t stmt -> BSet.t -> Prop :=
  | WFskip : forall l, wf_stm l skip l
  | WFassign : forall l x e r, wf_stm l (Annot.mk r (Sassign x e) BSet.empty) l
  | WFwrite : forall l x e1 e2 r, wf_stm l (Annot.mk r (Swrite x e1 e2) BSet.empty) l
  | WFseq : forall l l1 l2 s1 s2, wf_stm l s1 l1 -> wf_stm l1 s2 l2 -> wf_stm l (seq s1 s2) l2
  | WFif : forall l l1 l2 l3 p e s1 s2 s3, is_some_in e l = false -> wf_stm l s1 l1 -> wf_stm l s2 l2 -> wf_stm (BSet.union l1 l2) s3 l3 -> wf_stm l (sif p e s1 s2 s3) l3
  | WFfor : forall l l1 x c1 c2 s, BSet.mem x l = false -> wf_stm (BSet.add x l) s l1 -> unmod (Annot.elt s) x = true -> wf_stm l (sfor x c1 c2 s) l1.

Lemma x_in_l_wf_stm (s: stmt) :
  forall x l r g l',
  wf_stm l (Annot.mk r s g) l' -> BSet.mem x l = true -> BSet.mem x l' = true.
Proof.
  pattern s.
  apply stmt_sub; intros; inv H; auto.
    - assert (BSet.mem x l1 = true).
      destruct s1.
      eapply IHs1; eauto.
      apply stmt_order_refl.
      destruct s2.
      eapply IHs2; eauto.
      apply stmt_order_refl.
    - assert (BSet.mem x l1 = true).
      destruct s1.
      eapply IHs1; eauto.
      apply stmt_order_refl.
      assert (BSet.mem x l2 = true).
      destruct s2.
      eapply IHs2; eauto.
      apply stmt_order_refl.
      destruct s3.
      eapply IHs3; eauto.
      apply stmt_order_refl.
      rewrite BSet.mem_union_iff.
      rewrite orb_true_iff; auto.
    - assert (BSet.mem x0 (BSet.add x l) = true).
      rewrite BSet.union_singleton.
      rewrite BSet.mem_union_iff.
      rewrite orb_true_iff; auto.
      eapply IHfor; eauto.
      apply stmt_order_refl.
Qed.



Definition is_var_expr (e:expr) :=
  match e with
  | Var _ => True
  | _     => False
  end.


Definition upd_annot {A B: Type} (b:B) (a: Annot.t A) : Annot.t B :=
  Annot.mk (Annot.leaked a) b (Annot.high a).




Lemma pre_annot_order_refl:
  forall {A: Type} (P : A -> A -> Prop)
         (REFL : forall x, P x x),
  forall a : Annot.t A, annot_order P a a.
Proof.
  intros.
  unfold annot_order.
  split_and ; auto.
  subset. subset.
Defined.




Lemma pre_annot_order_trans:
  forall {A: Type} (P: A -> A -> Prop)
         (TRANS : forall s1 s2 s3, P s1 s2 -> P s2 s3 -> P s1 s3),
  forall a1 a2 a3, annot_order P a1 a2 -> annot_order P a2 a3 -> annot_order P a1 a3.
Proof.
  unfold annot_order.
  destruct a1,a2,a3; simpl.
  intros. dand.
  split_and.
  subset.
  eapply TRANS;eauto.
  subset.
Defined.


Fixpoint seqlist (l: list (Annot.t stmt)): Annot.t stmt :=
   match l with
   | cons hd tl => let s := seqlist tl in
                Annot.mk (BSet.union (Annot.leaked hd) (Annot.leaked s))
                         (Sseq hd s)
                         (BSet.union (Annot.high hd) (Annot.high s))
   | nil => skip
   end.



Lemma tmk_seq : forall D a k G s1 G1 s2 G2,
    typeof_stmt D a k G s1 G1 ->
    typeof_stmt D a k G1 s2 G2 ->
    typeof_stmt D a k G (mk_seq s1 s2) G2.
Proof.
  intros.
  unfold mk_seq.
  destruct s1,s2; simpl in *.
  econstructor;eauto.
Qed.


Inductive sep_rel (p:PP) : Annot.t stmt -> Annot.t stmt -> Annot.t stmt -> Prop :=
| SEP_LEAK : forall s, sep_rel p s s skip
| SEP_SAFE     : forall s, BSet.mem p (Annot.leaked s) = false ->
                           sep_rel p s skip s
| SEP_CONS_LEAK : forall s1 s2 l1 l2 r g,
    sep_rel p s2 l1 l2 ->
    sep_rel p (Annot.mk r (Sseq s1 s2) g) (mk_seq s1 l1) l2
| SEP_CONS_SAFE :
  forall s1 s2 l2 r g lk ls,
    sep_rel p s2 skip l2 ->
    sep_rel p s1 lk ls ->
    sep_rel p (Annot.mk r (Sseq s1 s2) g) lk (mk_seq ls l2).

Lemma sep_exists_stmt (p:PP) (s:Annot.t stmt): exists l1 l2,
  sep_rel p s l1 l2.
Proof.
  intros.
  do 2 eexists.
  econstructor.
Qed.


Lemma typeof_seqlist_cons :
  forall D a k G G1 G2 s l,
         typeof_stmt D a  k G s G1 ->
         typeof_stmt D a  k G1 (seqlist l) G2 ->
         typeof_stmt D a  k G (seqlist (s::l)) G2.
Proof.
  simpl.
  intros.
  destruct s,  (seqlist l).
  simpl.
  econstructor. eauto.
  eauto.
Qed.

Lemma typeof_sep :
  forall D a k G s G' lk ls p
         (SEP: sep_rel p s lk ls)
         (TYP : typeof_stmt D a k G s G'),
  exists G0,
  typeof_stmt D a k G lk G0 /\
  typeof_stmt D a k G0 ls G'.
Proof.
  intros.
  revert G G' TYP.
  induction SEP.
  - intros.
    eexists G'.
    split_and. auto. constructor.
  - intros.
    exists G.
    split_and.
    constructor. auto.
  - intros.
    apply seq_inv in TYP.
    destruct TYP as (G1 & TS1 & TS2 & RR & GG).
    apply IHSEP in TS2.
    destruct TS2  as (G11 & Tl1 & Tl2).
    exists G11.
    split ; auto.
    eapply tmk_seq;eauto.
  - intros.
    apply seq_inv in TYP.
    destruct TYP as (G1 & TS1 & TS2 & RR & GG).
    apply IHSEP2 in TS1.
    apply IHSEP1 in TS2.
    destruct TS1  as (G11 & T11 & T12).
    destruct TS2  as (G12 & T21 & T22).
    inv T21.
    exists G11.
    split ; auto.
    eapply tmk_seq;eauto.
Qed.


Lemma sseq_n_inv :
  forall D a k G G' s1 s2 r g n,
         typeof_stmt_n D a k G (Annot.mk r (Sseq s1 s2) g) G' (S n) ->
         exists G1,
           typeof_stmt_n D a k G s1 G1 n /\
             typeof_stmt_n D a k G1 s2 G' n.
Proof.
  intros. inv H.
  - assert (AO : annot_order stmt_order s0 s1 /\annot_order stmt_order s3 s2).
  {
    destruct TseqWk.
    unfold annot_order in weaken_stmt.
    simpl in *. dand.
    split ; auto.
  }
  dand.
  apply typeof_stmt_n_weak with (s':= s1) (G2:=G1) in Tseq1; auto.
  apply typeof_stmt_n_weak with (s':= s2) (G2:=G') in Tseq2; auto.
  eexists;split.
  eapply typeof_stmt_incr_n. eauto.
  lia.
  eapply typeof_stmt_incr_n. eauto.
  lia.
  destruct TseqWk;auto.
  apply TEnv.order_refl.
  - destruct Tifwk.
    unfold annot_order in weaken_stmt.
    simpl in *. tauto.
(*  - destruct TifLWk.
    unfold annot_order in weaken_stmt.
    simpl in *. tauto. *)
  - destruct TforWk.
    unfold annot_order in weaken_stmt.
    simpl in *. tauto.
Qed.


Lemma skip_n_inv :
  forall D a k G G' n,
    typeof_stmt_n D a k G skip G' n ->
    TEnv.order G G'.
Proof.
  intros.
  inv H; auto.
  - destruct TseqWk.
    unfold annot_order in *.
    simpl in *. tauto.
  - destruct Tifwk.
    unfold annot_order in *.
    simpl in *. tauto.
(*  - destruct TifLWk.
    unfold annot_order in *.
    simpl in *. tauto. *)
  - destruct TforWk.
    unfold annot_order in *.
    simpl in *. tauto.
Qed.

Lemma typeof_sep_n :
  forall D a k G s G' lk ls p n
         (SEP: sep_rel p s lk ls)
         (TYP : typeof_stmt_n D a  k G s G' n),
  exists G0 n1 n2,
  typeof_stmt_n D a k G lk G0 n1/\
  typeof_stmt_n D a k G0 ls G' n2.
Proof.
  intros.
  revert n G G' TYP.
  induction SEP.
  - intros.
    eexists G'. exists n. exists n.
    split_and. auto. eapply Tskip_n. apply TEnv.order_refl.
  - intros.
    exists G. exists n. exists n.
    split_and.
    eapply Tskip_n. apply TEnv.order_refl.
    auto.
  - intros.
    destruct n. inv TYP.
    apply sseq_n_inv in TYP.
    destruct TYP as (G1 & T1 & T2).
    apply IHSEP in T2.
    destruct T2  as (G11 & n1 & n2 & Tl1 & Tl2).
    exists G11.
    exists (S (max n n1)).
    exists n2.
    split;auto.
    eapply tmk_seq_n.
    eapply typeof_stmt_incr_n;eauto. lia.
    eapply typeof_stmt_incr_n;eauto. lia.
  - intros.
    destruct n. inv TYP.
    apply sseq_n_inv in TYP.
    destruct TYP as (G1 & T1 & T2).
    apply IHSEP2 in T1.
    destruct T1  as (G11 & n1 & n2 & Tlk1 & Ts1).
    apply IHSEP1 in T2.
    destruct T2  as (G21 & n1' & n2' & Tlk' & Ts1').
    exists G11.
    exists n1.
    exists (S (max n2 n2')).
    split;auto.
    apply skip_n_inv in Tlk'.
    eapply tmk_seq_n.
    eapply typeof_stmt_incr_n;eauto. lia.
    apply typeof_stmt_incr_n with (n:=n2');eauto; try lia.
    eapply typeof_stmt_n_order; auto.
    eauto. auto. apply Typ.order_refl.
Qed.



Definition annot_upd_join {A B C: Type} (c:C)
  (a1: Annot.t A)
  (a2: Annot.t B) :=
  Annot.mk (BSet.union (Annot.leaked a1) (Annot.leaked a2))
           c
           (BSet.union (Annot.high a1) (Annot.high a2)).

Definition annot_add_leak {A: Type} (r: BSet.t) (a : Annot.t A) :=
  Annot.mk (BSet.union (Annot.leaked a) r) (Annot.elt a) (Annot.high a).

(*Fixpoint ctx_to_stm (p: PP) (c : ctx): stmt :=
  match c with
  | CHere x  s1 s2 => (Sif p (Var x) s1 s2 skip)
  | CSeq1 s1 c => Sseq s1 (Annot.map (ctx_to_stm p) c)
  | CSeq2 c s2 => Sseq (Annot.map (ctx_to_stm p) c) s2
  | CElse e s1 c p' => Sif p' e s1  (Annot.map (ctx_to_stm p) c)  skip
  | CThen e c s2 p' => Sif p' e (Annot.map (ctx_to_stm  p) c) s2 skip
  | CFor x c1 c2 c => Sfor x c1 c2 (Annot.map (ctx_to_stm p) c)
  | CDef s => Annot.elt s
  end.
*)

Definition mk_if p e (s1 s2 s3: Annot.t stmt) :=
  Annot.mk (BSet.union (Annot.leaked s1) (BSet.union (Annot.leaked s2) (Annot.leaked s3)))
           (Sif p e s1 s2 s3)
           (BSet.union (Annot.high s1) (BSet.union (Annot.high s2) (Annot.high s3))).

Definition mk_ifh p e (s1 s2 s3: Annot.t stmt) :=
  Annot.mk (BSet.union (Annot.leaked s1) (BSet.union (Annot.leaked s2) (Annot.leaked s3)))
           (Sif p e s1 s2 s3)
           (BSet.union (Annot.high s1) (BSet.union (Annot.high s2) (BSet.union (Annot.high s3) (BSet.singleton p)))).




Definition is_Sskip (s:stmt) :=
  match s with
  | Sskip => true
  | _     => false
  end.

Lemma is_Sskip_inv : forall s,
    is_Sskip s = true <-> s = Sskip.
Proof.
  destruct s; simpl; intuition congruence.
Qed.

Definition is_skip (s: Annot.t stmt) :=
  BSet.is_empty (Annot.leaked s) && is_Sskip (Annot.elt s) &&
      BSet.is_empty (Annot.high s).

Lemma is_skip_inv : forall s, is_skip s = true <-> s = skip.
Proof.
  unfold skip.
  unfold is_skip.
  intros.
  rewrite! andb_true_iff.
  rewrite !BSet.is_empty_true.
  destruct s; simpl.
  generalize (is_Sskip_inv elt).
  intuition subst; try congruence.
  inv H. simpl. reflexivity.
Qed.


Fixpoint xsep (p:PP) (r: BSet.t) (s:stmt) (g: BSet.t) :=
  if BSet.mem p r
  then
    match s with
    | Sseq s1 s2 =>   let (lk,ss) := xsep p (Annot.leaked s2) (Annot.elt s2) (Annot.high s2)  in
                      if is_skip lk
                      then let (lk',ss') := xsep p (Annot.leaked s1) (Annot.elt s1) (Annot.high s1)  in
                        (lk',mk_seq ss' ss)
                      else (mk_seq s1 lk,ss)
    |  _   => (Annot.mk r s g, skip)
    end
  else (skip,Annot.mk r s g).

Lemma xsep_eq (p:PP) (r: BSet.t) (s:stmt) (g: BSet.t) :
  xsep p r s g =
  if BSet.mem p r
  then
    match s with
    | Sseq s1 s2 =>   let (lk,ss) := xsep p (Annot.leaked s2) (Annot.elt s2) (Annot.high s2)  in
                      if is_skip lk
                      then let (lk',ss') := xsep p (Annot.leaked s1) (Annot.elt s1) (Annot.high s1)  in
                        (lk',mk_seq ss' ss)
                      else (mk_seq s1 lk,ss)
    |  _   => (Annot.mk r s g, skip)
    end
  else (skip,Annot.mk r s g).
Proof.
  destruct s; reflexivity.
Qed.


Definition sep (p:PP) (s:Annot.t stmt) := xsep p (Annot.leaked s) (Annot.elt s) (Annot.high s).

Fixpoint sep_rel_xsep (p:PP) (s:stmt) : forall r g, sep_rel p (Annot.mk r s g) (fst (xsep p r s g)) (snd (xsep p r s g)).
Proof.
  intros.
  rewrite xsep_eq.
  destruct (BSet.mem p r) eqn:MEM.
  - destruct s.
    + simpl. constructor.
    + simpl. constructor.
    + simpl. constructor.
    +
      generalize (sep_rel_xsep p (Annot.elt s2) (Annot.leaked s2)
                    (Annot.high s2)) as SEP2.
      destruct (xsep p (Annot.leaked s2) (Annot.elt s2) (Annot.high s2)) as (lk2,ss2).
      destruct (is_skip lk2) eqn:LK.
      {
        generalize (sep_rel_xsep p (Annot.elt s1) (Annot.leaked s1)
                    (Annot.high s1)) as SEP1.
        destruct (xsep p (Annot.leaked s1) (Annot.elt s1) (Annot.high s1)) as (lk1,ss1).
        simpl.
        apply is_skip_inv in LK.
        subst.
        intros.
        eapply SEP_CONS_SAFE; auto.
        destruct s2 ; auto.
        destruct s1;auto.
      }
      { simpl. intros.
        eapply SEP_CONS_LEAK; auto.
        destruct s2;auto.
      }
    +  simpl. constructor.
    +  simpl. constructor.
  - simpl.
    apply SEP_SAFE.
    simpl; auto.
Qed.

Lemma sep_rel_sep (p:PP) (s:Annot.t stmt) :  sep_rel p s (fst (sep p s )) (snd (sep p s )).
Proof.
  unfold xsep. destruct s. apply sep_rel_xsep.
Qed.


Fixpoint has_next (s:stmt) : bool :=
  match s with
  | Sif p' e s1 s2 s3 => negb (is_skip s3) || has_next (Annot.elt s1) || has_next (Annot.elt s2)
  | Sseq s1 s2   => (has_next (Annot.elt  s1)) || (has_next (Annot.elt s2))
  | Sfor _ _ _ b => has_next (Annot.elt b)
  | Sskip | Sassign _ _ | Swrite _ _ _ => false
  end.


Fixpoint scope_increase (p:PP)(fresh:PP) (ctx:stmt) (lk:Annot.t stmt) : Annot.t stmt :=
  match ctx with
  | Sif p' e s1 s2 s3 => (* s3 is empty *)
      if Pos.eq_dec p p' (* e is a variable *)
      then mk_ifh p e s1 s2 (mk_seq s3 lk)
      else match BSet.mem p (Ca s1) , BSet.mem p (Ca s2) with
           | true , false => mk_if p' e (scope_increase p fresh (Annot.elt s1) lk) (mk_seq s2 lk) s3
           | false, true  => mk_if p' e  (mk_seq s1 lk) (scope_increase p fresh (Annot.elt s2) lk) s3
           | false , false | true, true => (* p is does not exists *) mk_seq (mk_if p' e s1 s2 s3) lk
           end
  | Sseq s1 s2 =>
      match BSet.mem p (Ca s1) , BSet.mem p (Ca s2) with
      | true , false =>  let (lk',ss) := sep p (mk_seq s2 lk) in
                         mk_seq (scope_increase p fresh (Annot.elt s1) lk') ss
      | false , true =>  mk_seq s1 (scope_increase p fresh (Annot.elt s2) lk)
      |   _  ,   _   => (* p does not exists *) mk_seq (mk_seq s1 s2) lk
      end
  | Sfor x c1 c2 b => mk_seq (mk_for x c1 c2 (scope_increase p fresh (Annot.elt b) skip)) lk
  | Sskip | Sassign _ _ | Swrite _ _ _  => lk
  end.


Lemma typeof_stmt_opt_skip : forall D a k r g G G' n,
    TEnv.order G G' ->
  exists (s' : Annot.t stmt) (G'' : TEnv.t),
    annot_order stmt_order s' (Annot.mk r Sskip  g) /\
    typeof_stmt_n D a  k G s' G'' n /\
    TEnv.order G'' G' /\
      (*min_high (Annot.elt s') (Annot.high s') /\*)
    (forall p : var,
     TEnv.pp_in p G'' ->
     TEnv.pp_in p G \/
     BSet.mem p (Annot.high s') = true \/ Typ.pp_in p k).
Proof.
  exists skip. exists G.
  split_and; auto.
  + unfold annot_order.
    simpl. split_and;auto.
  + constructor. apply TEnv.order_refl.
(*  + simpl. reflexivity.*)
Qed.

Lemma typeof_expr_opt :
  forall (D : var -> Typ.t)
         (G : TEnv.t)
         (e : expr)
         (r : BSet.t)
         (t : Typ.t)
         (SIM: is_simple_env D)
         (TY : typeof_expr_wk D G e t r),
  exists (t' : Typ.t) (r' : BSet.t),
    typeof_expr_wk D G e t' r' /\
      Typ.order t' t = true /\
      BSet.subset r' r = true /\
      (forall p : var,
     Typ.pp_in p t' -> TEnv.pp_in p G).
Proof.
  intros. induction TY.
  - exists (TEnv.get x G).
    exists BSet.empty.
    split_and.
    constructor.
    apply TEnv.order_refl.
    apply BSet.subset_refl.
    intros.
    unfold TEnv.pp_in.
    exists x;auto.
  - do 2 eexists.
    split_and.
    + constructor.
    + apply Typ.order_refl.
    + rewrite BSet.subset_refl. reflexivity.
    + unfold Typ.pp_in.
      simpl. intros. rewrite BSet.mem_empty in H.
    discriminate.
  - destruct IHTY1 as (t1' & r1 & TY1' & NO1).
    destruct IHTY2 as (t2' & r2 & TY2' & NO2).
    dand.
    do 2 eexists.
    split_and.
    + econstructor. eauto. eauto.
    + apply Typ.order_join2;auto.
    + subset.
    + intros.
      apply Typ.pp_in_union_if    in H.
      intuition.
  - destruct IHTY1 as (t1' & r1 & TY1' & NO1).
    destruct IHTY2 as (t2' & r2 & TY2' & NO2).
    destruct IHTY3 as (t3' & r3 & TY3' & NO3).
    dand.
    do 2 eexists.
    split_and.
    + econstructor. eauto. eauto. eauto.
    + apply Typ.order_join2;auto.
      apply Typ.order_join2;auto.
    + apply BSet.subset_union2;auto.
      apply BSet.subset_union2;auto.
    + intros.
      apply Typ.pp_in_union_if    in H.
      destruct H. intuition.
      apply Typ.pp_in_union_if    in H.
      intuition.
  - destruct IHTY as (t1' & r1 & TY' & NO).
    dand.
    do 2 eexists.
    split_and.
    + econstructor.
      eauto.
      intro. subst.
      apply Typ.order_H_inv in NO0.
      congruence.
    + apply Typ.order_join2;auto.
      apply Typ.order_refl.
    + apply BSet.subset_union2;auto.
      apply Typ.to_set_subset; auto.
    + intros.
      apply Typ.pp_in_union_if    in H0.
      intuition.
      unfold Typ.pp_in in H1.
      specialize (SIM t).
      unfold Typ.is_simple in SIM.
      rewrite orb_true_iff in SIM.
      destruct SIM.
      apply Typ.is_L_inv in H0. rewrite H0 in H1.
      simpl in H1. rewrite BSet.mem_empty in H1. congruence.
      apply Typ.is_H_inv in H0. rewrite H0 in H1.
      simpl in H1. rewrite BSet.mem_empty in H1. congruence.
  - destruct IHTY as (t2 & r2 & IHTY).
    dand.
    do 2 eexists.
    split_and. eauto.
    eapply Typ.order_trans;eauto.
    eapply BSet.subset_trans;eauto.
    intros.
    intuition.
Qed.

Lemma typeof_stmt_opt_assign :
  forall n (D : var -> Typ.t)
         (a : option var)
         (k : Typ.t)
         (G G' : TEnv.t)
         (e : expr)
         (r g : BSet.t)
         (x : var)
         (t : Typ.t)
         (SIMP : is_simple_env D)
         (TassignExpr : typeof_expr_wk D G e t r)
         (TassignWk : TEnv.order (G [x → t ⊔ k]) G'),
  exists  (s' : Annot.t stmt) (G'' : TEnv.t),
    annot_order stmt_order s' (Annot.mk r (Sassign x e)  g) /\
    typeof_stmt_n D a k G s' G'' n /\
    TEnv.order G'' G' /\
(*    min_high (Annot.elt s') (Annot.high s') /\*)
    (forall p : var,
     TEnv.pp_in p G'' ->
     TEnv.pp_in p G \/
     BSet.mem p (Annot.high s') = true \/ Typ.pp_in p k).
Proof.
  intros.
  apply typeof_expr_opt in TassignExpr;auto.
  destruct TassignExpr as (t1 & r1 & TE).
  dand.
  exists (Annot.mk r1 (Sassign x e) BSet.empty).
  exists (G [x → t1 ⊔ k]).
  split_and.
  - unfold annot_order.
    simpl. split_and;auto.
  - econstructor.
    eauto. apply TEnv.order_refl.
  - eapply TEnv.order_trans;eauto.
    apply TEnv.order_set; auto.
    apply TEnv.order_refl.
    apply Typ.order_join2;auto.
    apply Typ.order_refl.
(*  - simpl. reflexivity.*)
  - intros.
    unfold TEnv.pp_in in H.
    destruct H.
    rewrite TEnv.get_set in H.
    destruct (Pos.eq_dec x0 x);auto.
    + subst.
      simpl.
      apply Typ.pp_in_union_if in H.
      destruct H.
      intuition. tauto.
    + left. eexists x0;auto.
Qed.

Lemma typeof_stmt_opt_write : forall
    n (D : var -> Typ.t)
  (a : option var)
  (k : Typ.t)
  (G G' : TEnv.t)
  (SIMP : is_simple_env D)
  (x : var)
  (e1 e2 : expr)
  (r g : BSet.t)
  (t1 : Typ.t)
  (r1 : BSet.t)
  (t2 : Typ.t)
  (r2 : BSet.t)
  (TwriteE1 : typeof_expr_wk D G e1 t1 r1)
  (TwriteE2 : typeof_expr_wk D G e2 t2 r2)
  (TwriteNOTH : t1 <> Typ.H)
  (TwriteO : Typ.order ((t1 ⊔ t2) ⊔ Typ.guard k Typ.H) (D x) =
            true)
  (TwriteLk : BSet.subset ((r1 ∪ r2) ∪ Typ.to_set t1) r = true)
  (TwriteWk : TEnv.order G G'),
  exists  (s' : Annot.t stmt) (G'' : TEnv.t),
    annot_order stmt_order s' (Annot.mk r (Swrite x e1 e2) g) /\
    typeof_stmt_n D a k G s' G'' n /\
    TEnv.order G'' G' /\
(*    min_high (Annot.elt s') (Annot.high s') /\*)
    (forall p : var,
     TEnv.pp_in p G'' ->
     TEnv.pp_in p G \/
     BSet.mem p (Annot.high s') = true \/ Typ.pp_in p k).
Proof.
  intros.
  apply typeof_expr_opt in TwriteE1; auto.
  apply typeof_expr_opt in TwriteE2;auto.
  destruct TwriteE1 as (t1' & r1' & TwriteE1).
  destruct TwriteE2 as (t2' & r2' & TwriteE2).
  dand.
  exists (Annot.mk (BSet.union (BSet.union r1' r2')
                               (Typ.to_set t1'))
                      (Swrite x e1 e2) BSet.empty).
  exists G.
  split_and;auto.
  - unfold annot_order.
    simpl. split_and;auto.
    eapply BSet.subset_trans.
    apply BSet.subset_union2;auto.
    apply BSet.subset_union2;auto.
    eauto. eauto.
    eapply Typ.to_set_subset;eauto.
    auto.
  - econstructor.
    eauto. eauto. intro.
    subst. apply Typ.order_H_inv in TwriteE1.
    congruence.
    eapply Typ.order_trans.
    apply Typ.order_join2;auto.
    apply Typ.order_join2;auto.
    eauto. eauto.
    apply Typ.order_refl.
    auto.
    apply BSet.subset_refl;auto.
    apply TEnv.order_refl.
Qed.

Lemma high_mk_seq : forall s1 s2,
    (Annot.high (mk_seq s1 s2)) =
      BSet.union (Annot.high s1)  (Annot.high s2).
Proof.
  unfold mk_seq;simpl. auto.
Qed.

Lemma pp_in_classify_if : forall px p c G,
    TEnv.pp_in px
        (Typing.classify_if p c G) -> TEnv.pp_in px G.
Proof.
  unfold Typing.classify_if.
  intros.
  destruct c;auto.
  destruct H. rewrite TEnv.get_classify in H.
  unfold Typ.pp_in in H.
  unfold Typ.classify in H.
  destruct (Typ.mem p (TEnv.get x G)).
  *  simpl in H. rewrite BSet.mem_empty in H. discriminate.
  *  unfold TEnv.pp_in. exists x;auto.
Qed.

Lemma pp_in_join  : forall p G1 G2,
    TEnv.pp_in p (TEnv.join G1 G2)  ->
      (TEnv.pp_in p G1 \/  TEnv.pp_in p G2).
Proof.
  unfold TEnv.pp_in, Typ.pp_in.
  intros.
  destruct H.
  rewrite TEnv.get_join in H.
  rewrite Typ.to_set_join   in H.
  destruct (Typ.is_H (TEnv.get x G1) || Typ.is_H (TEnv.get x G2)) eqn:HH.
  rewrite BSet.mem_empty in H. discriminate.
  rewrite BSet.mem_union_iff in H.
  rewrite orb_true_iff in H.
  destruct H.
  left ; eexists ; eauto.
  right ; eexists ; eauto.
Qed.

Lemma pp_in_set : forall p x k G,
    TEnv.pp_in p (G [ x → k]) ->
    TEnv.pp_in p G \/ Typ.pp_in p k.
Proof.
  intros.
  unfold TEnv.pp_in in *.
  destruct H as (x' & IN).
  rewrite TEnv.get_set in IN.
  destruct (Pos.eq_dec x' x). tauto.
  left. exists x'.
  auto.
Qed.



Fixpoint stmt_order_strip (s:stmt) :
  stmt_order (strip s) s.
Proof.
  assert (forall a, annot_order stmt_order (astrip a) a).
  {
    unfold annot_order,astrip.
    simpl. split_and;auto.
  }
  destruct s; simpl;auto.
  - unfold Annot.from.
    split;auto.
    apply H.
    apply H.
  - unfold Annot.from.
    split_and;auto.
    apply H.
    apply H.
    apply H.
  - unfold Annot.from.
    split_and;auto.
    apply H.
Qed.

Lemma annot_stmt_join_exists : forall a1 a2 a,
  annot_order stmt_order a1 a ->
  annot_order stmt_order a2 a ->
  exists a', annot_join stmt_join a1 a2 = Some a' /\
               annot_order stmt_order a' a.
Proof.
  intros.
  pose proof (annot_join_refl a) as JR.
  apply stmt_join2_exists with (1:=H) (2:= H0) in JR.
  auto.
Qed.

Lemma annot_order_mk_for : forall x x' c1 c1' c2 c2' a a',
    x = x' -> c1 = c1' -> c2 = c2' ->
    annot_order stmt_order a a' ->
    annot_order stmt_order (mk_for x c1 c2 a) (mk_for x' c1' c2' a').
Proof.
  intros.
  unfold annot_order; simpl; split_and; auto.
  unfold annot_order in H2 ; simpl in *; tauto.
  unfold annot_order in H2 ; simpl in *; tauto.
Qed.




Lemma Ca_mk_seq : forall s1 s2, (Ca (mk_seq s1 s2)) = BSet.union (Ca s1) (Ca s2).
Proof.
  unfold mk_seq.
  unfold Ca;simpl.
  reflexivity.
Qed.


Definition apre_wf_high (P : stmt -> BSet.t -> Prop) (a: Annot.t stmt) := P (Annot.elt a) (Annot.high a).


Fixpoint wf_high (s:stmt) (g : BSet.t) : Prop :=
  BSet.subset g (C s) = true /\
  match s with
  | Sskip | Sassign _ _ | Swrite _ _ _ => g = BSet.empty
  | Sif t c s1 s2 s3 => apre_wf_high wf_high s1
                        /\
                          apre_wf_high wf_high s2
                        /\
                          apre_wf_high wf_high s3
  | Sseq s1 s2 => apre_wf_high  wf_high s1
                  /\
                    apre_wf_high wf_high s2
  | Sfor x c1 c2 b => apre_wf_high wf_high b
  end.

Definition awf_high (a : Annot.t stmt) :=
  wf_high (Annot.elt a) (Annot.high a).

















Fixpoint annot_order_dwn_annot (s: stmt): forall r g,
    annot_order stmt_order (dwn_stmt r s g) (Annot.mk r s g).
Proof.
  destruct s; simpl; intros.
  - unfold annot_order; simpl; split_and;auto.
    apply BSet.subset_refl.
  - unfold annot_order; simpl; split_and;auto.
    apply BSet.subset_refl.
  - unfold annot_order; simpl; split_and;auto.
    apply BSet.subset_refl.
  - unfold annot_order; simpl; split_and;auto.
    apply BSet.subset_refl.
    apply annot_order_dwn_annot.
    apply annot_order_dwn_annot.
    subset.
  - unfold annot_order; simpl; split_and;auto.
    apply BSet.subset_refl.
    apply annot_order_dwn_annot.
    apply annot_order_dwn_annot.
    apply annot_order_dwn_annot.
    subset.
  - unfold annot_order; simpl; split_and;auto.
    apply BSet.subset_refl.
    apply annot_order_dwn_annot.
    subset.
Qed.

Lemma typeof_stmt_opt_n:
  forall n s D a k G G'
         (SIMP: is_simple_env D)
         (TYP: typeof_stmt_n D a  k G s G' n),
  exists s' G1',
    annot_order stmt_order s' s /\
      typeof_stmt_n D a k G s' G1' n /\ TEnv.order G1' G' /\
      (forall p, TEnv.pp_in p G1' -> TEnv.pp_in p G \/ BSet.mem p (Ca s') = true \/ Typ.pp_in p k).
Proof.
  intros.
  apply typeof_stmt_n_down with (d:= BSet.diff (TEnv.Leak G')  (BSet.union (TEnv.Leak G) (Typ.to_set k))) in TYP;auto.
  assert ((Typ.downgrade_set (BSet.diff (TEnv.Leak G') (TEnv.Leak G ∪ Typ.to_set k)) k = k)).
  {
    rewrite Typ.downgrade_set_idem; auto.
    rewrite BSet.diff_union_r.
    rewrite BSet.inter_sym.
    rewrite BSet.inter_diff_empty.
    reflexivity.
  }
  assert (TEnv.downgrade_set (BSet.diff (TEnv.Leak G') (TEnv.Leak G ∪ Typ.to_set k)) G = G).
  {
    rewrite TEnv.downgrade_set_idem.
    reflexivity.
    rewrite BSet.union_sym.
    rewrite BSet.diff_union_r.
    rewrite BSet.inter_diff_empty.
    reflexivity.
  }
  rewrite H in *.
  rewrite H0 in *.
  exists (dwn_annot s).
  eexists.
  split_and.
  - unfold dwn_annot.
    apply annot_order_dwn_annot.
  - eauto.
  - apply TEnv.downgrade_set_order.
  - intros.
    unfold Typ.pp_in.
    rewrite TEnv.pp_in_Leak in *.
    rewrite Ca_dwn_annot.
    rewrite TEnv.downgrade_set_Leak in H1.
    rewrite BSet.mem_diff_iff in H1.
    rewrite andb_true_iff in H1.
    rewrite negb_true_iff in H1.
    rewrite BSet.mem_diff_iff in H1.
    rewrite andb_false_iff in H1.
    rewrite negb_false_iff in H1.
    rewrite BSet.mem_diff_iff in H1.
    rewrite andb_false_iff in H1.
    rewrite negb_false_iff in H1.
    rewrite BSet.mem_union_iff in H1.
    rewrite orb_true_iff in H1.
    dand.
    intuition congruence.
Qed.


Lemma typeof_stmt_n_not_in:
  forall n s D a k G G' p
         (SIMP : is_simple_env D)
         (TYP: typeof_stmt_n D a k G s G' n)
         (NOTS : ~ (BSet.mem p (Ca s) = true))
         (NOTG : ~ TEnv.pp_in p G)
         (NOTK : ~ Typ.pp_in p k),
  exists n'  G'',
    typeof_stmt_n D a k G s G'' n' /\ TEnv.order G'' G' /\
    ~ TEnv.pp_in p G''.
Proof.
  intros.
  apply typeof_stmt_opt_n in TYP; auto.
  destruct TYP as (s' & G'' & ORD1 & TYP  & ORD & ALL).
  specialize (ALL p).
  exists n, G''.
  split_and;auto.
  -
    eapply typeof_stmt_n_weak. eauto.
    auto.
    apply TEnv.order_refl.
  -
    intro. specialize (ALL H).
    destruct ALL as [ALL| [ALL | ALL]];
    auto.
    apply annot_stmt_order_Ca in ORD1.
    rewrite ORD1 in *.
    auto.
Qed.


Lemma annot_high_for:
  forall x c1 c2 s,
  Annot.high (sfor x c1 c2 s) = Annot.high s.
Proof.
  auto.
Qed.

Lemma annot_join_order:
  forall s1 s2 sr, annot_join stmt_join s1 s2 = Some sr -> annot_order stmt_order s1 sr.
Proof.
  intros.
  unfold annot_join in H.
  remember (stmt_join (Annot.elt s1) (Annot.elt s2)).
  destruct o; auto.
  inversion H.
  unfold annot_order.
  split_and; simpl.
      - apply BSet.subset_union_l.
      - eapply stmt_join_order; eauto.
      - apply BSet.subset_union_l.
      - inv H.
Qed.

Lemma annot_order_annot:
  forall s s',
  annot_order stmt_order s s' ->
  BSet.subset (Annot.high s) (Annot.high s') = true /\
  BSet.subset (Annot.leaked s) (Annot.leaked s') = true.
Proof.
  intros.
  unfold annot_order in H.
  intuition idtac.
Qed.

Lemma annot_join_annot_order:
  forall s1 s2 sr, annot_join stmt_join s1 s2 = Some sr ->
  BSet.subset (Annot.high s1) (Annot.high sr) = true /\
  BSet.subset (Annot.leaked s1) (Annot.leaked sr) = true /\
  BSet.subset (Annot.high s2) (Annot.high sr) = true /\
  BSet.subset (Annot.leaked s2) (Annot.leaked sr) = true.
Proof.
  intros.
  pose proof (annot_join_order s1 s2 sr H).
  pose proof (annot_join_sym s1 s2).
  rewrite H1 in H.
  clear H1.
  pose proof (annot_join_order s2 s1 sr H).
  pose proof (annot_order_annot s1 sr H0).
  pose proof (annot_order_annot s2 sr H1).
  intuition idtac.
Qed.

Lemma isPP_refl:
  forall p, (isPP (Some p) p = true).
Proof.
  intros.
  unfold isPP. destruct Pos.eq_dec; auto.
Qed.


Fixpoint high_empty (s:stmt) :=
  match s with
  | Sif p e s1 s2 s3 => high_empty (Annot.elt s1) /\
                          high_empty (Annot.elt s2) /\
                          high_empty (Annot.elt s3) /\
                          Annot.high s1 = BSet.empty /\
                          Annot.high s2 = BSet.empty /\
                          Annot.high s3 = BSet.empty
  | Sseq s1 s2 => high_empty (Annot.elt s1) /\
                    high_empty (Annot.elt s2) /\
                    Annot.high s1 = BSet.empty /\
                    Annot.high s2 = BSet.empty
  | Sfor _ _ _ s  => high_empty (Annot.elt s) /\ Annot.high s = BSet.empty
  | _ => True
  end.

Definition annot_high_empty (s: Annot.t stmt) :=
  Annot.high s = BSet.empty /\ high_empty (Annot.elt s).


Fixpoint wf_annot (s:stmt) (g: BSet.t) :=
  match s with
  | Sskip | Sassign _ _ | Swrite _ _ _ => g = BSet.empty
  | Sseq s1 s2 => BSet.subset g (C s) = true /\
                    wf_annot (Annot.elt s1) (Annot.high s1) /\
                    wf_annot (Annot.elt s2) (Annot.high s2)
  | Sif p e s1 s2 s3 => BSet.subset g (C s) = true /\
                    wf_annot (Annot.elt s1) (Annot.high s1) /\
                          wf_annot (Annot.elt s2) (Annot.high s2) /\
                          wf_annot (Annot.elt s3) (Annot.high s3)
  | Sfor x c1 c2 b => BSet.subset g (C s) = true /\
                        wf_annot (Annot.elt b) (Annot.high b)
  end.


Fixpoint right_most_high_pp (p:PP) (s:stmt) (g:BSet.t) : Prop :=
  match s with
  | Sskip | Sassign _ _ | Swrite _ _ _ => False
  | Sseq s1 s2 => (xorb (BSet.mem p (Ca s1)) (BSet.mem p (Ca s2)) = true) /\
                    ((BSet.mem p (Ca s1) = true -> right_most_high_pp p (Annot.elt s1) (Annot.high s1) /\ annot_high_empty s2 )) /\
                    (BSet.mem p (Ca s2) = true -> right_most_high_pp p (Annot.elt s2) (Annot.high s2))
  | Sif p' e s1 s2 s3 =>
      (p = p' /\ is_var_expr e /\ BSet.mem p (Annot.high s1) = false /\
         BSet.mem p (Ca s2) = false /\ BSet.mem p (Ca s3) = false
      )

      \/
        ((p <> p') /\ BSet.mem p' g = false /\
          xorb (BSet.mem p (Ca s1)) (BSet.mem p (Ca s2)) = true /\
           (BSet.mem p (Ca s1) = true -> right_most_high_pp p (Annot.elt s1) (Annot.high s1)) /\
           (BSet.mem p (Ca s2) = true -> right_most_high_pp p (Annot.elt s2) (Annot.high s2)))
  | Sfor x c1 c2 b =>
      right_most_high_pp p (Annot.elt b) (Annot.high b)
  end.




Lemma xorb_true_iff : forall b1 b2,
    xorb b1 b2 =   true ->
    (b1 = true /\ b2 = false) \/ (b1 = false /\ b2 = true).
Proof.
  destruct b1,b2 ; simpl; intuition congruence.
Qed.

Lemma if_prop : forall (b:bool)(T E:Prop),
    (if b then T else E) <-> ((b = true -> T) /\ (b = false -> E)).
Proof.
  destruct b; intuition congruence.
Qed.


Lemma is_var_expr_type_of_expr : forall D G e ti r,
    typeof_expr D G e ti r ->
    is_var_expr e ->
    typeof_expr D G e ti BSet.empty.
Proof.
  destruct e ; simpl; try tauto.
  intros. inv H. constructor.
Qed.

Lemma annot_high_empty_skip :
  annot_high_empty skip.
Proof.
  split. reflexivity. exact I.
Qed.

Lemma annot_high_empty_mk_seq : forall s1 s2,
    annot_high_empty s1 ->
    annot_high_empty s2 ->
    annot_high_empty (mk_seq s1 s2).
Proof.
  intros.
  unfold annot_high_empty.
  simpl.
  unfold annot_high_empty in H,H0.
  dand.
  rewrite H0. rewrite H1.
  rewrite BSet.union_empty_l.
  split_and;auto.
Qed.



Lemma high_sep_leak :
  forall p s lk ls
         (WF : annot_high_empty  s)
         (SEP : sep_rel p s lk ls),
    annot_high_empty  lk.
Proof.
  intros.
  induction SEP.
  - auto.
  - apply annot_high_empty_skip.
  -
    unfold annot_high_empty in WF.
    simpl in WF.
    dand. subst.
    apply annot_high_empty_mk_seq; auto.
    unfold annot_high_empty;split;auto.
    apply IHSEP; auto.
    unfold annot_high_empty;split;auto.
  - apply IHSEP2;auto.
    unfold annot_high_empty in WF.
    simpl in *. unfold annot_high_empty; tauto.
Qed.

Lemma high_sep_safe :
  forall p s lk ls
         (WF : annot_high_empty s)
         (SEP : sep_rel p s lk ls),
    annot_high_empty  ls.
Proof.
  intros.
  induction SEP.
  -  apply annot_high_empty_skip.
  - auto.
  -
    eapply IHSEP.
    unfold annot_high_empty in WF.
    simpl in WF. dand.
    unfold annot_high_empty; split; auto.
  -
    unfold annot_high_empty in WF.
    simpl in WF. dand.
    apply annot_high_empty_mk_seq; auto.
    apply IHSEP2.
    unfold annot_high_empty; split; auto.
    apply IHSEP1.
    unfold annot_high_empty; split; auto.
Qed.

Lemma leak_sep_safe :
  forall p s lk ls
         (SEP : sep_rel p s lk ls),
  BSet.mem p (Annot.leaked  ls) = false.
Proof.
  intros.
  induction SEP.
  - intros. subst.
    simpl. subset.
  - simpl.
    subset.
  - auto.
  - simpl.
    subset.
Qed.


Lemma typeof_stmt_order' : forall n (s : Annot.t stmt) (D : var -> Typ.t)
         (a : option var)  (k k' : Typ.t)
         (G2 G1 : TEnv.t)  (G1' : TEnv.t),
    typeof_stmt_n D a k G1 s G1' n ->
    TEnv.order G2 G1 ->
    Typ.order k' k = true ->
    exists n',
      typeof_stmt_n D a k' G2 s G1' n'.
Proof.
  intros.
  destruct s as [r s g].
  eapply typeof_stmt_n_order in H;eauto.
Qed.



Lemma annot_order_mk_if_ifh : forall p e s1 s2 s3,
    annot_order stmt_order (mk_if p e s1 s2 s3) (mk_ifh p e s1 s2 s3).
Proof.
  unfold annot_order,mk_if,mk_ifh.
  simpl. intros.
  split_and;auto.
  subset. apply annot_stmt_order_refl.
  apply annot_stmt_order_refl.
  apply annot_stmt_order_refl.
  subset.
Qed.

Lemma annot_order_mk_ifh : forall p e s1 s2 s3 s1' s2' s3',
    annot_order stmt_order s1 s1' ->
    annot_order stmt_order s2 s2' ->
    annot_order stmt_order s3 s3' ->
    annot_order stmt_order (mk_ifh p e s1 s2 s3) (mk_ifh p e s1' s2' s3').
Proof.
  unfold annot_order,mk_if,mk_ifh.
  simpl. intros.
  dand.
  split_and;auto.
  - subset.
  - unfold annot_order; split_and;auto.
  - unfold annot_order; split_and;auto.
  - unfold annot_order; split_and;auto.
  - subset.
Qed.

Lemma weaken_mk_seq : forall s1 s2 G s G',
    weaken (mk_seq s1 s2) G s G' ->
    exists s1' s2' r g, annot_order stmt_order s1 s1' /\
                      annot_order stmt_order s2 s2' /\
                      s = Annot.mk r (Sseq s1' s2') g.
Proof.
  intros.
  destruct H.
  unfold annot_order in weaken_stmt.
  simpl in *.
  destruct s. simpl in *. destruct elt;try tauto.
  dand. do 4 eexists.
  split_and ;auto. auto.
  auto.
Qed.

Lemma pp_in_L : forall p, Typ.pp_in p Typ.L <-> False.
Proof.
  unfold Typ.pp_in.
  intros. simpl. rewrite BSet.mem_empty. intuition congruence.
Qed.


Lemma if_n_inv : forall n D k G r p e s1 s2 g G',
    typeof_stmt_n D None k G (Annot.mk r (Sif p e s1 s2 skip) g)  G' (S n) ->
    (exists t lk G1 G2,
      typeof_expr_wk D G e t lk /\
        typeof_stmt_n D None (Typ.join k (Typ.guard t (Typ.singleton p))) G s1 G1 n /\
        typeof_stmt_n D None (Typ.join k (Typ.guard t (Typ.singleton p))) G s2 G2 n /\
        TEnv.order (TEnv.join G1 G2) G' /\
      (t <> Typ.L  -> BSet.mem p g = true))
.
Proof.
  intros.
  inv H.
  - destruct TseqWk.
    unfold annot_order in weaken_stmt.
    simpl in *; tauto.
  - destruct Tifwk.
    unfold annot_order in weaken_stmt.
    simpl in *; try tauto.
    dand;subst.
    apply annot_order_skip_r in weaken_stmt7.
    subst. simpl in *.
    do 4 eexists.
    split_and.
    + eauto.
    + eapply typeof_stmt_n_weak in  TifThen.
      eauto. auto. apply TEnv.order_refl.
    + eapply typeof_stmt_n_weak in  TifElse.
      eauto. auto. apply TEnv.order_refl.
    + apply skip_n_inv in TifNext.
      eapply TEnv.order_trans;eauto.
    + intro.
      rewrite Typ.guard_not_L in weaken_stmt3.
      simpl in weaken_stmt3. subset.
      rewrite <- Typ.is_L_inv  in H. destruct (Typ.is_L t); congruence.
  - destruct TforWk.
    unfold annot_order in weaken_stmt.
    simpl in *; tauto.
Qed.



Lemma for_n_inv : forall D a k G x c1 c2 b r g G' n,
    typeof_stmt_n D a  k G (Annot.mk r (Sfor x c1 c2 b) g) G' (S n) ->
    exists Go ar,
      typeof_stmt_n D a  k  (Go [x → k]) ar Go n /\
      TEnv.order G Go  /\
        ([Ca ar] Go) = Go /\
        annot_order stmt_order  ar b  /\
        TEnv.order Go G' /\
        BSet.subset (Annot.high ar) g = true/\
        BSet.subset (Annot.leaked ar) r = true.
Proof.
  intros.
  inv H.
  - inv TseqWk.
    unfold annot_order in weaken_stmt.
    simpl in *. tauto.
  - inv Tifwk.
    unfold annot_order in weaken_stmt.
    simpl in *. tauto.
  -
    exists Go, ar.
    destruct TforWk.
    unfold annot_order in weaken_stmt.
    simpl in *. dand; subst.
    split_and;auto.
Qed.


Lemma is_var_expr_typeof_expr_wk : forall D G e ti r,
    typeof_expr_wk D G e ti r ->
    is_var_expr e ->
    typeof_expr_wk D G e ti BSet.empty.
Proof.
  intros.
  induction H; simpl in H0; try tauto.
  - constructor.
  - specialize (IHtypeof_expr_wk H0).
    apply TWk with (t1' := t1') (v1':= BSet.empty) in IHtypeof_expr_wk;auto.
Qed.

Definition annot_right_most_high_pp (p:PP) (s: Annot.t stmt) :=
  right_most_high_pp p (Annot.elt s) (Annot.high s) /\
  BSet.mem p (Ca s) = true.

Lemma typeof_expr_wk_no_lk : forall D G e lk,
    typeof_expr_wk D G e Typ.L lk ->
    typeof_expr_wk D G e Typ.L BSet.empty.
Proof.
  intros.
  remember Typ.L as L.
  revert HeqL.
  induction H; intros.
  - constructor.
  - constructor.
  - apply Typ.join_L_inv in HeqL.
    destruct HeqL; subst.
    rewrite <- BSet.union_idem.
    econstructor; eauto.
  - apply Typ.join_L_inv in HeqL.
    destruct HeqL; subst.
    apply Typ.join_L_inv in H3.
    destruct H3 ; subst.
    rewrite <- BSet.union_idem.
    pattern BSet.empty at 2.
    rewrite <- BSet.union_idem.
    econstructor; eauto.
  - apply Typ.join_L_inv in HeqL.
    destruct HeqL ; subst.
    change BSet.empty with (Typ.to_set Typ.L).
    specialize (IHtypeof_expr_wk eq_refl).
    rewrite <- BSet.union_empty_l.
    eapply TRead_wk.
    auto. auto.
  - subst.
    apply Typ.order_is_L_r in H.
    subst. auto.
Qed.

Lemma annot_order_mk_if_full_if : forall t e s1 s2 s3,
  annot_order stmt_order
    (mk_if_full t e BSet.empty Typ.L s1 s2 s3)
    (mk_if t e s1 s2 s3).
Proof.
  unfold mk_if_full,mk_if.
  unfold annot_order; simpl.
  intros.
  split_and;auto.
  subset.
  apply annot_stmt_order_refl.
  apply annot_stmt_order_refl.
  apply annot_stmt_order_refl.
  subset.
Qed.



Fixpoint wf_annot_subset (s:stmt): forall g,
    wf_annot s g -> BSet.subset g (C s)  = true.
Proof.
  destruct s; simpl; intros; subst; dand; auto.
Qed.

Lemma wf_annot_subset_Ca (a:Annot.t stmt) : forall g,
    wf_annot (Annot.elt a) g -> BSet.subset g (Ca a)  = true.
Proof.
  unfold Ca;intros.
  apply wf_annot_subset; auto.
Qed.

Fixpoint C_xsep (s: stmt) : forall p r g s1 s2,
    xsep p r s g = (s1, s2) -> C s = (Ca s1 ∪ Ca s2).
Proof.
  destruct s; simpl.
  - intros.
    destruct (BSet.mem p r).
    inv H. unfold Ca; simpl. rewrite BSet.union_idem. reflexivity.
    inv H. unfold Ca; simpl. rewrite BSet.union_idem. reflexivity.
  - intros.
    destruct (BSet.mem p r); inv H; unfold Ca; simpl.
    rewrite BSet.union_idem. reflexivity.
    rewrite BSet.union_idem. reflexivity.
  - intros.
    destruct (BSet.mem p r); inv H; unfold Ca; simpl.
    rewrite BSet.union_idem. reflexivity.
    rewrite BSet.union_idem. reflexivity.
  - intros.
    destruct (BSet.mem p r); inv H; unfold Ca; simpl.
    destruct (xsep p (Annot.leaked s2) (Annot.elt s2) (Annot.high s2)) eqn:SEP.
    apply C_xsep in SEP.
    destruct (is_skip t) eqn:SK.
    apply is_skip_inv in SK. subst.
    destruct (xsep p (Annot.leaked s1) (Annot.elt s1) (Annot.high s1)) eqn:SEP'.
    inv H1.
    apply C_xsep in SEP'.
    simpl. rewrite SEP. rewrite SEP'.
    unfold Ca; simpl. rewrite BSet.union_empty_l.
    rewrite BSet.union_assoc. reflexivity.
    inv H1.
    rewrite SEP.
    unfold Ca; simpl.
    rewrite BSet.union_assoc. reflexivity.
    rewrite BSet.union_empty_l.
    reflexivity.
  - intros.
    destruct (BSet.mem p r) eqn:M; inv H.
    unfold Ca; simpl.
    rewrite BSet.union_empty. reflexivity.
    unfold Ca; simpl.
    rewrite BSet.union_empty_l. reflexivity.
  - intros.
    destruct (BSet.mem p r) eqn:M; inv H.
    unfold Ca; simpl.
    rewrite BSet.union_empty. reflexivity.
    unfold Ca; simpl.
    rewrite BSet.union_empty_l. reflexivity.
Qed.

Lemma C_sep : forall p s s1 s2,
    sep p s = (s1, s2) ->
    Ca s = BSet.union (Ca s1) (Ca s2).
Proof.
  unfold sep.
  intros.
  apply C_xsep in H.
  auto.
Qed.

Fixpoint C_scope_increase (p:PP) (fresh:PP) (s:stmt) : forall (acc:Annot.t stmt),
    C (Annot.elt (scope_increase p fresh s acc)) =
      BSet.union (C s) (Ca acc).
Proof.
  destruct s; simpl.
  - intros. rewrite BSet.union_empty_l. reflexivity.
  - intros. rewrite BSet.union_empty_l. reflexivity.
  - intros. rewrite BSet.union_empty_l. reflexivity.
  - intros.
    destruct (BSet.mem p (Ca s1)) eqn:Cs1.
    destruct (BSet.mem p (Ca s2)) eqn:Cs2.
    simpl. reflexivity.
    destruct (sep p (mk_seq s2 acc)) eqn:SEP.
    simpl.
    rewrite C_scope_increase.
    apply C_sep in SEP.
    unfold Ca in *. simpl in SEP.
    rewrite <- !BSet.union_assoc.
    rewrite SEP.
    reflexivity.
    destruct (BSet.mem p (Ca s2)).
    simpl.
    rewrite C_scope_increase.
    rewrite BSet.union_assoc. reflexivity.
    simpl.
    reflexivity.
  - intros.
    destruct (Pos.eq_dec p t).
    simpl. subst. unfold Ca.
    rewrite !BSet.union_assoc.
    reflexivity.
    destruct (BSet.mem p (Ca s1)) eqn:Cs1.
    destruct (BSet.mem p (Ca s2)) eqn:Cs2.
    + simpl.
      reflexivity.
    + simpl.
      rewrite C_scope_increase.
      rewrite !BSet.union_assoc.
      apply BSet.uniq. intros.
      unfold Ca.
      subset.
    + destruct (BSet.mem p (Ca s2)).
      simpl.
      rewrite C_scope_increase.
      unfold Ca.
      apply BSet.uniq. intros.
      subset.
      simpl.
      apply BSet.uniq. intros.
      subset.
  - intros.
    rewrite C_scope_increase.
    unfold Ca.
    simpl. rewrite BSet.union_empty.
    reflexivity.
Qed.

    

Theorem scope_increase_security:
  forall fresh p n c s D G G'

         (WF2 : has_next (Annot.elt c) = false)
         (WFS: annot_high_empty s)
         (WFH : annot_right_most_high_pp p c)
         (WFA : wf_annot (Annot.elt c) (Annot.high c))
         (TYP: typeof_stmt_n D None Typ.L G (mk_seq c s) G' n)
         (SIM: is_simple_env D)
         (GNP: ~ TEnv.pp_in p G),
  exists n', typeof_stmt_n D (Some p) Typ.L G (scope_increase p fresh (Annot.elt c) s) (TEnv.classify p G') n'.
Proof.
  intros.
  destruct n. inv TYP.
  apply sseq_n_inv in TYP.
  destruct TYP as (G1 & TC & TS).
  revert TS.
  generalize n as m.
  revert n c s G G1  G' WFA  WFH  WFS TC  WF2 (*HIGH*) SIM GNP.
  induction n.
  { intros. unfold annot_right_most_high_pp in WFH.
    dand.
    inv TC ; simpl in *; try tauto. }
  intros.
  unfold annot_right_most_high_pp in WFH.
  dand.
  destruct c as [r c g].
  simpl. destruct c.
  - (* skip *) simpl in *. tauto.
  - (* assign *) simpl in *. tauto.
  - (* write *) simpl in *. tauto.
  - (* sequence *)
    simpl.
    simpl in WFH0.
    dand.
    apply xorb_true_iff in WFH2.
    destruct WFH2; dand.
    { (* condition on the left *)
      rewrite H0.
      rewrite H1.
      rewrite H0 in *. rewrite H1 in *. clear_true_false.
      specialize (WFH0 eq_refl). dand.
      apply sseq_n_inv in TC.
      destruct TC as (G2 & Ts1 & Ts2).
      assert (SEP:= sep_rel_sep p (mk_seq s2 s)).
      destruct (sep p (mk_seq s2 s)) as (lk,ss).
      simpl in SEP.
      assert (TYPSEP : typeof_stmt_n D None Typ.L G2 (mk_seq  s2 s) G' (S (max n m))).
      {
        unfold mk_seq. simpl.
      eapply Tseq_n;eauto.
      eapply typeof_stmt_incr_n;eauto. lia.
      eapply typeof_stmt_incr_n;eauto. lia.
      apply weaken_refl.
    }
    eapply typeof_sep_n in TYPSEP; eauto.
    destruct TYPSEP as (G0 & n1 & n2 & TFST & TSND).
    assert (SEPHIGH : annot_high_empty (mk_seq s2 s)).
    {
      apply annot_high_empty_mk_seq;auto.
    }
    eapply IHn with (c:= s1) (s:=lk) (G:= G) (G':=G0) in Ts1;auto.
    destruct Ts1 as (n' & TSI).
    apply typeof_stmt_n_classify with (p:=p) in TSND.
    apply free_upgrade_n with (p:=p) in TSND.
    destruct TSND as (n2' & TSND).
    exists (S (max n2' n')).
    eapply tmk_seq_n.
    eapply typeof_stmt_incr_n;eauto. lia.
    eapply typeof_stmt_incr_n;eauto. lia.
    { apply high_sep_safe     in SEP; auto.
      intro.
      unfold annot_high_empty in SEP.
      dand.
      rewrite SEP0 in H.
      rewrite BSet.mem_empty in H. discriminate.
    }
    {
      apply leak_sep_safe     in SEP; auto.
    }
    reflexivity.
    { apply high_sep_safe     in SEP;auto.
      destruct SEP ; auto.
    }
    auto.
    { simpl in WFA. tauto. }
      { unfold annot_right_most_high_pp.
      split ; auto.
    }
      { apply high_sep_leak in SEP;auto. }
    { simpl in WF2.
    rewrite orb_false_iff in WF2. tauto.
    }
    eauto.
    }
    { (* condition is on the right *)
      dand.
      rewrite H0. rewrite H1.
      rewrite H0 in *. rewrite H1 in *.
      clear_true_false.
      specialize (WFH4 eq_refl).
      apply sseq_n_inv in TC.
      destruct TC as (G2 & Ts1 & Ts2).
      apply typeof_stmt_n_not_in with (p:=p) in Ts1;auto.
      destruct Ts1 as (n' & G'' & Ts1 & OG'' & NOTpp).
      apply typeof_stmt_n_order with (G1:= G'') (k':= Typ.L) in Ts2;
        auto.
      eapply IHn with (c:= s2) (s:=s) (G:= G'') (G':=G') in Ts2;auto.
      destruct Ts2 as (n2' & TSI).
      eapply free_upgrade_n with (p:=p) in Ts1;auto.
      destruct Ts1 as (n3 & Ts1).
      exists (S (max n3 n2')).
      eapply tmk_seq_n.
      eapply typeof_stmt_incr_n;eauto. lia.
      eapply typeof_stmt_incr_n;eauto. lia.
      { intro.
        simpl in WFA.
        dand.
        apply wf_annot_subset_Ca  in WFA2.
        revert H.
        subset.
      }
      { simpl in WFA.
        tauto.
      }
        {
        unfold annot_right_most_high_pp.
        split; auto.
      }
      { simpl in WF2.
        rewrite orb_false_iff in WF2. tauto.
      }
      {
        eauto.
      }
      {
        intro. congruence.
      }
      {
        rewrite pp_in_L. tauto. }
    }
  - simpl in WFH0,WFH1.
    (* This is a condition *)
    simpl.
    destruct(Pos.eq_dec p t).
    { (* This is the high condition *)
      destruct WFH0 ; dand; try congruence.
      subst t. simpl in WF2.
      rewrite! orb_false_iff in WF2.
      dand. rewrite negb_false_iff in WF2.
      apply is_skip_inv in WF2. subst.
      apply if_n_inv in TC.
      {
        destruct TC as (t & lk & G2 & G3 &
                         TE & TTHEN & TELSE & ORD & MEM).
        eapply free_upgrade_n with (p:=p) in TTHEN;auto.
        destruct TTHEN as (n1& TTHEN).
        eapply free_upgrade_n with (p:=p) in TELSE;auto.
        destruct TELSE as (n1'& TELSE).
        eapply free_upgrade_n with (p:=p) in TS;auto.
        destruct TS as (n3& TS).

        apply typeof_stmt_n_order with (k':= Typ.L) (G1 := TEnv.join G2 G3) in TS.
        apply is_var_expr_typeof_expr_wk in TE.
        exists (S (S(max n1 (max n1' n3)))).
        eapply Tif_n.
        eauto.
        reflexivity.
        eapply typeof_stmt_incr_n. eapply TTHEN. lia.
        eapply typeof_stmt_incr_n. eapply TELSE. lia.
        eapply tmk_seq_n.
        eapply Tskip_n.
        apply TEnv.order_refl.
        eapply typeof_stmt_incr_n. eapply TS. lia.
        constructor.
        - simpl. destruct (Pos.eq_dec p p);simpl; try congruence.
          simpl.  unfold Typing.classify_if.
          destruct (negb (Typ.is_L t)).
          + apply TEnv.order_refl.
          + apply TEnv.order_classify.
        - unfold annot_order.
          simpl. split_and;auto.
          subset.
          apply annot_stmt_order_refl.
          apply annot_stmt_order_refl.
          apply annot_stmt_order_refl.
          unfold annot_high_empty in WFS.
          dand. rewrite WFS0.
          assert (BSet.subset(Typ.to_set (Typ.guard t (Typ.singleton p))) (BSet.singleton p)= true).
          {
            unfold Typ.guard.
            destruct (Typ.is_L t).
            simpl. subset.
            simpl. subset.
          }
          subset.
        - auto.
        - auto.
        - apply Typ.order_refl.
        - unfold is_not_guard.
          unfold annot_high_empty in WFS.
          dand. rewrite WFS0. subset.
        - unfold is_not_guard.
          simpl in WFA.
          dand.
          apply wf_annot_subset_Ca in WFA1.
          subset.
        - unfold is_not_guard. congruence.
      }
    }
    (* This is not the high conditional *)
    simpl in WF2.
    rewrite! orb_false_iff in WF2.
    dand. rewrite negb_false_iff in WF2.
    apply is_skip_inv in WF2. subst.
    destruct WFH0; dand; try congruence.
    apply xorb_true_iff in H1.
    destruct H1 as [HTHEN | HELSE]; dand.
    { (* The high conditional is within the 'then' *)
      rewrite HTHEN0. rewrite HTHEN1.
      rewrite HTHEN0 in *. rewrite HTHEN1 in *.
      clear_true_false.
      specialize (H3 eq_refl).
      apply if_n_inv in TC.
      {
      destruct TC as (ti & lk & G2 & G3 & TE & TS1 & TS2 & ORD & MEM).
      (* This is necessarily a Low conditionnal *)
      assert (ti = Typ.L).
      {
        destruct (Typ.is_L ti) eqn:ISL.
        apply Typ.is_L_inv in ISL; auto.
        apply Typ.is_L_inv_false in ISL.
        intuition congruence.
      }
      assert (TS':= TS).
      subst.
      apply typeof_expr_wk_no_lk in TE.
      apply typeof_stmt_n_order with (k':= Typ.L) (G1 := G2) in TS; auto.
      apply typeof_stmt_n_order with (k':= Typ.L) (G1 := G3) in TS'; auto.
      subst.
      rewrite Typ.guard_L_l in *.
      rewrite Typ.join_idem in *.
      apply IHn with (c:= s1) (s:=s) (G':= G') (m:=m) in TS1.
      destruct TS1 as (n1 & TS1).
      assert (exists n, typeof_stmt_n D None Typ.L G (mk_seq s2 s) G' n).
      {
        exists (S (max n  m)).
        eapply tmk_seq_n.
        eapply typeof_stmt_incr_n;eauto. clear. lia.
        eapply typeof_stmt_incr_n;eauto. clear. lia.
      }
      destruct H as (n' & TS2s).
      apply free_upgrade_n with (p:=p) in TS2s.
      destruct TS2s as (n2' & TS2s).
      exists (S (max n1 n2')).
      eapply Tif_n with (p:=t).
      eauto. rewrite Typ.guard_L_l. rewrite Typ.join_idem. reflexivity.
      eapply typeof_stmt_incr_n. eapply TS1. clear ; lia.
      apply typeof_stmt_n_weak with (s':= mk_seq s2 s) (G2 := TEnv.classify p G') in TS2s.
      eapply typeof_stmt_incr_n. eapply TS2s. clear ; lia.
      apply annot_stmt_order_refl.
      apply TEnv.order_classify.
      eapply Tskip_n.
      rewrite TEnv.join_idem.
      apply TEnv.order_refl.
      constructor.
      simpl. rewrite BSet.is_empty_empty.
      rewrite andb_comm. simpl.  apply TEnv.order_refl.
      apply annot_order_mk_if_full_if.
      {
        unfold is_not_guard.
        simpl in WFA.
        dand.
        apply wf_annot_subset_Ca in WFA1.
        unfold mk_seq. simpl.
        unfold annot_high_empty in WFS.
        dand. rewrite WFS0.
        subset.
      }
      { simpl in WFA; dand;auto. }
      unfold annot_right_most_high_pp.
      split ; auto.
      congruence.
      auto.
      congruence.
      auto.
      auto.
      auto.
      eapply TEnv.order_trans;eauto.
      rewrite TEnv.join_sym.
      apply TEnv.join_ub_l.
      eapply TEnv.order_trans;eauto.
      apply TEnv.join_ub_l.
      }
    }
    {
      (* This is similar with the H conditional in the Else branch *)
{ (* The high conditional is within the 'then' *)
      rewrite HELSE0. rewrite HELSE1.
      rewrite HELSE0 in *. rewrite HELSE1 in *.
      clear_true_false.
      specialize (H5 eq_refl).
      apply if_n_inv in TC.
      {
      destruct TC as (ti & lk & G2 & G3 & TE & TS1 & TS2 & ORD & MEM).
      (* This is necessarily a Low conditionnal *)
      assert (ti = Typ.L).
      {
        destruct (Typ.is_L ti) eqn:ISL.
        apply Typ.is_L_inv in ISL; auto.
        apply Typ.is_L_inv_false in ISL.
        intuition congruence.
      }
      assert (TS':= TS).
      subst.
      apply typeof_expr_wk_no_lk in TE.
      apply typeof_stmt_n_order with (k':= Typ.L) (G1 := G2) in TS; auto.
      apply typeof_stmt_n_order with (k':= Typ.L) (G1 := G3) in TS'; auto.
      subst.
      rewrite Typ.guard_L_l in *.
      rewrite Typ.join_idem in *.
      apply IHn with (c:= s2) (s:=s) (G':= G') (m:=m) in TS2.
      destruct TS2 as (n1 & TS2).
      assert (exists n, typeof_stmt_n D None Typ.L G (mk_seq s1 s) G' n).
      {
        exists (S (max n  m)).
        eapply tmk_seq_n.
        eapply typeof_stmt_incr_n;eauto. clear. lia.
        eapply typeof_stmt_incr_n;eauto. clear. lia.
      }
      destruct H as (n' & TS1s).
      apply free_upgrade_n with (p:=p) in TS1s.
      destruct TS1s as (n2' & TS1s).
      exists (S (max n1 n2')).
      eapply Tif_n with (p:=t).
      eauto. rewrite Typ.guard_L_l. rewrite Typ.join_idem. reflexivity.
      apply typeof_stmt_n_weak with (s':= mk_seq s1 s) (G2 := TEnv.classify p G') in TS1s.
      eapply typeof_stmt_incr_n. eapply TS1s. clear ; lia.
      apply annot_stmt_order_refl.
      apply TEnv.order_classify.
      eapply typeof_stmt_incr_n. eapply TS2. clear ; lia.
      eapply Tskip_n.
      rewrite TEnv.join_idem.
      apply TEnv.order_refl.
      constructor.
      simpl. rewrite BSet.is_empty_empty.
      rewrite andb_comm. simpl.  apply TEnv.order_refl.
      apply annot_order_mk_if_full_if.
      {
        unfold is_not_guard.
        simpl in WFA.
        dand.
        apply wf_annot_subset_Ca in WFA2.
        unfold mk_seq. simpl.
        unfold annot_high_empty in WFS.
        dand. rewrite WFS0.
        subset.
      }
      { simpl in WFA; dand;auto. }
      unfold annot_right_most_high_pp.
      split ; auto.
      congruence.
      auto.
      congruence.
      auto.
      auto.
      auto.
      eapply TEnv.order_trans;eauto.
      rewrite TEnv.join_sym.
      apply TEnv.join_ub_l.
      eapply TEnv.order_trans;eauto.
      apply TEnv.join_ub_l.
      }
    }
    }
  - (* For *)
    simpl.
    apply for_n_inv in TC.
    destruct TC as (Go & ar & TC & TCR).
    dand.
    assert (SUBP : BSet.subset (BSet.singleton p) (Ca ar) = true).
    {
      simpl in WFH1.
      unfold Ca in WFH1.
      simpl in WFH1.
      apply annot_stmt_order_Ca in TCR1.
      rewrite TCR1.
      unfold Ca.
      apply BSet.subset_mem_iff.
      intros. rewrite BSet.mem_singleton in H.
      subst. auto.
    }
    (* Il faut nettoyer Go *)
    set (d:= (BSet.diff (TEnv.Leak Go) (TEnv.Leak G))).
    apply typeof_stmt_n_down with (d:= d) in TC;auto.
    rewrite TEnv.downgrade_set_set in TC.
    rewrite Typ.downgrade_set_L in TC.
    apply typeof_stmt_n_weak with (s':= s0)
                                  (G2 := TEnv.downgrade_set d Go)
      in TC.
    apply IHn with (c:= s0) (s:=skip) (G':=(TEnv.downgrade_set d Go)) (m:=O) in TC; auto.
    destruct TC as (n1 & TC).
    rewrite <- TEnv.classify_set_classify in TC.
    replace (([BSet.singleton p] TEnv.downgrade_set d Go)) with
      (TEnv.downgrade_set d Go) in TC.
    apply free_upgrade_n with (p:= p) in TS.
    destruct TS as (n2 & TS).

    exists (S (max (S n1) n2)).
    apply tmk_seq_n with (G1:= G1).
    {
      eapply typeof_stmt_incr_n.
        eapply Tfor_n.
      -  apply TC.
      -
        repeat intro.
        rewrite TEnv.get_downgrade_set.
        apply Typ.downgrade_set_still_ordered;auto.
        unfold d.
        generalize (TEnv.leak_var x0 G).
        generalize (TEnv.leak_var x0 Go).
        generalize ((Typ.to_set (TEnv.get x0 Go))) as xGo.
        generalize (Typ.to_set (TEnv.get x0 G)) as xG.
        intros xG xGo S1 S2.
        subset.
        destruct (BSet.mem x1 xG) eqn:MxG; intuition try congruence.
        - unfold Ca.
          rewrite C_scope_increase.
          change (C (Annot.elt s0)) with (Ca s0).
          unfold Ca at 2.
          simpl. rewrite BSet.union_empty.
          apply annot_stmt_order_Ca  in TCR1.
          rewrite <- TCR1 in *.
          rewrite <- TEnv.downgrade_set_classify_set.
          congruence.
          unfold d.
          rewrite <- TCR2.
          specialize (TEnv.Leak_classify_set (Ca ar) Go).
          intros.
          apply BSet.uniq.
          intros.
          subset.
          destruct (BSet.mem x0 (TEnv.Leak ([Ca ar] Go))); intuition congruence.
        - constructor.
          eapply TEnv.order_trans.
          apply TEnv.downgrade_set_order.
          auto.
          apply annot_order_mk_for; auto.
          apply annot_stmt_order_refl.
        -  lia.
      }
      eapply typeof_stmt_incr_n.
      eapply typeof_stmt_n_weak.
      apply TS.
      apply annot_stmt_order_refl.
      apply TEnv.order_classify.
      lia.
      unfold is_not_guard.
      destruct WFS.
      rewrite H. subset.
      {
        rewrite <- TEnv.downgrade_set_classify_set.
        f_equal.
        symmetry.
        eapply TEnv.classify_set_fix_subset;eauto.
        unfold d.
        rewrite <- TCR2.
        generalize (TEnv.Leak_classify_set (Ca ar) Go).
        intro S.
        subset.
        destruct (Pos.eq_dec x0 p); tauto.
      }
      simpl in WFA.
      tauto.
      simpl in WFH0. unfold annot_right_most_high_pp. split;auto.
      apply annot_high_empty_skip.
      { intro.
        rewrite TEnv.pp_in_Leak in H.
        apply TEnv.mem_leak_set in H.
        rewrite BSet.mem_union_iff in H.
        rewrite orb_true_iff in H.
        destruct H.
        rewrite TEnv.downgrade_set_Leak in H.
        rewrite BSet.mem_diff_iff in H.
        rewrite andb_true_iff in H.
        unfold d in H.
        rewrite BSet.mem_diff_iff in H.
        rewrite negb_true_iff in H.
        rewrite andb_false_iff in H.
        dand.
        destruct H1. congruence.
        rewrite negb_false_iff in H.
        rewrite TEnv.pp_in_Leak in GNP.
        congruence.
        simpl in H. rewrite BSet.mem_empty in H. discriminate.
  }
  constructor.
      apply TEnv.order_refl.
      eapply annot_order_trans with (a2:= ar).
      destruct ar.
      eapply annot_order_dwn_annot.
      auto.
      rewrite <- TCR2.
      rewrite TEnv.downgrade_set_classify_set_idem.
      apply TEnv.order_refl.
Qed.

