Require Import ZArith Lia Bool.
Require Import Std NSet.
Require TEnv Typ.
Require Import FSetPositive.
Require Import FMapPositive.
Require Import NMap.

#[global] Notation PP := positive.

#[global] Notation "A ∪ B" := (BSet.union A B) (at level 100).

Notation var := positive.

Notation "A ⊔ B" := (Typ.join A B) (at level 300).

Module Annot.

  Record t (A: Type) :=
    mk
      {
        leaked : BSet.t;
        elt    : A;
        high   : BSet.t
      }.

  Definition from {A: Type} (x:A) :=
    mk _ BSet.empty x BSet.empty.


  Definition map {A B: Type} (f : A -> B) (a:t A) :=
    mk _ (leaked _ a) (f (elt _ a)) (high _ a).

  Definition upd {A:Type} (e:A) (a: t A) :=
    mk _ (leaked _ a) e (high _ a).

  Lemma mk_dest : forall {A: Type} (s: t A),
      mk _ (leaked _ s) (elt _ s) (high _ s) = s.
  Proof.
    destruct s; reflexivity.
  Qed.


End Annot.

Arguments Annot.mk {A}.
Arguments Annot.elt {A}.
Arguments Annot.leaked {A}.
Arguments Annot.high {A}.

Notation "S '^' r '_' g" := (@Annot.mk _ r S g) (at level 100).


Inductive op := Equal | Add | And | Le | Lt | Not.

Inductive expr :=
| Var (x:var)
| Cst (c:nat)
| Op  (o:op) (e1 e2:expr)
| Cond (e1 e2 e3:expr)
| Read (x:var) (e:expr).

Inductive stmt :=
| Sskip
| Sassign (x: var) (e:expr)
| Swrite  (x: var) (e1:expr) (e2:expr)
| Sseq    (s1:@Annot.t stmt) (s2:@Annot.t stmt)
| Sif (t:positive) (e:expr) (s1 s2 s3: @Annot.t stmt)
| Sfor (x:var) (c1 c2:nat) (s:@Annot.t stmt).

Definition skip := Annot.mk BSet.empty Sskip BSet.empty.

Definition annot_order {A: Type} (P : A -> A -> Prop) (a1 a2: Annot.t A) :=
  BSet.subset (Annot.leaked a1) (Annot.leaked a2) = true /\
    P (Annot.elt a1) (Annot.elt a2)  /\
    BSet.subset (Annot.high a1) (Annot.high a2) = true.

Fixpoint stmt_order (s1 s2:stmt) : Prop:=
    match s1, s2 with
    | Sskip , Sskip => True
    | Sassign x1 e1 , Sassign x2 e2 => x1 = x2 /\ e1 = e2
    | Swrite x1 e1 e1' , Swrite x2 e2 e2' =>
        x1 = x2 /\ e1 = e2 /\ e1' = e2'
    | Sseq s1 s1',
        Sseq s2 s2' =>  annot_order stmt_order s1 s2 /\
                         annot_order stmt_order s1' s2'
    | Sif p1 e1 s1 s1' s1'' ,
      Sif p2 e2 s2 s2' s2'' =>
        p1 = p2 /\ e1 = e2 /\
          annot_order stmt_order s1 s2 /\
          annot_order stmt_order s1' s2' /\
          annot_order stmt_order s1'' s2''
    | Sfor x1 n1 n1' s1 , Sfor x2 n2 n2' s1' =>
        x1 = x2 /\ n1 = n2 /\ n1' = n2' /\ annot_order stmt_order s1 s1'
    | _ , _ => False
    end.


Lemma annot_order_skip :
  annot_order stmt_order skip skip.
Proof.
  unfold annot_order.
  simpl. tauto.
Qed.

Lemma annot_stmt_order : forall s1 s2,
    stmt_order (Annot.elt s1) (Annot.elt s2) ->
    BSet.subset (Annot.leaked s1) (Annot.leaked s2) = true ->
    BSet.subset (Annot.high s1) (Annot.high s2) = true ->
    annot_order stmt_order s1 s2.
Proof.
  destruct s1,s2 ; unfold annot_order ; simpl.
  tauto.
Qed.

Fixpoint stmt_order_refl (s:stmt) :
  stmt_order s s.
Proof.
  destruct s ; simpl; auto.
  - split.
    apply annot_stmt_order; try apply BSet.subset_refl.
    destruct s1 ; auto.
    apply annot_stmt_order; try apply BSet.subset_refl.
    destruct s2 ; auto.
  - split_and ; auto.
    apply annot_stmt_order; try apply BSet.subset_refl.
    destruct s1 ; auto.
    apply annot_stmt_order; try apply BSet.subset_refl.
    destruct s2 ; auto.
    apply annot_stmt_order; try apply BSet.subset_refl.
    destruct s3 ; auto.
  - split_and;auto.
    apply annot_stmt_order; try apply BSet.subset_refl.
    destruct s; auto.
Qed.


Lemma annot_stmt_order_refl (s:Annot.t stmt) :
  annot_order stmt_order s s.
Proof.
  destruct s; unfold annot_order.
  simpl; split_and.
  apply BSet.subset_refl.
  apply stmt_order_refl.
  apply BSet.subset_refl.
Qed.

Definition annot_join {A B C: Type} (J : A -> B -> option C) (a1 : Annot.t A)
                      (a2 : Annot.t B)
  :=
  match J (Annot.elt a1) (Annot.elt a2) with
  | Some a =>
      Some (Annot.mk (BSet.union  (Annot.leaked a1) (Annot.leaked a2))
               a
               (BSet.union (Annot.high a1) (Annot.high a2)))
  | None => None
  end.

Fixpoint expr_eq_dec (e1 e2: expr): {e1 = e2} + {e1 <> e2}.
Proof.
  generalize Pos.eq_dec.
  generalize Nat.eq_dec.
  decide equality.
  decide equality.
Qed.


Fixpoint stmt_join (s1 s2:stmt) : option stmt :=
    match s1, s2 with
    | Sskip , Sskip => Some Sskip
    | Sassign x1 e1 , Sassign x2 e2 =>
        if of_sb (Pos.eq_dec x1 x2) && of_sb (expr_eq_dec e1 e2)
        then Some s1 else None
    | Swrite x1 e1 e1' , Swrite x2 e2 e2' =>
        if of_sb (Pos.eq_dec x1 x2) && of_sb (expr_eq_dec e1 e2) &&
             of_sb (expr_eq_dec e1' e2')
        then Some s1 else None
    | Sseq s1 s1',
      Sseq s2 s2' =>
        match annot_join stmt_join s1 s2 , annot_join stmt_join s1' s2' with
        | Some sa , Some sb => Some (Sseq sa sb)
        | _ , _ => None
        end
    | Sif p1 e1 s1 s1' s1'' ,
      Sif p2 e2 s2 s2' s2'' =>
        if of_sb (Pos.eq_dec p1 p2) && of_sb (expr_eq_dec e1 e2)
        then
        match annot_join stmt_join s1 s2 , annot_join stmt_join s1' s2' , annot_join stmt_join s1'' s2'' with
        | Some sa , Some sb , Some sc=> Some (Sif p1 e1 sa sb sc)
        | _ , _ , _ => None
        end
        else None
    | Sfor x1 n1 n1' s1 , Sfor x2 n2 n2' s1' =>
        if of_sb (Pos.eq_dec x1 x2) && of_sb (Nat.eq_dec n1 n2) && of_sb (Nat.eq_dec n1' n2')
        then match annot_join stmt_join s1 s1' with
             | Some s => Some (Sfor x1 n1 n1' s)
             | None   => None
             end
        else None
    | _ , _ => None
    end.

Lemma annot_join_stmt_inv : forall s1 r1 g1 s2 r2 g2 rs s gs,
    annot_join stmt_join (Annot.mk r1 s1 g1) (Annot.mk r2 s2 g2) =
      Some (Annot.mk rs s gs) ->
    rs = BSet.union r1 r2 /\ gs = BSet.union g1 g2 /\
      stmt_join s1 s2 = Some s.
Proof.
  unfold annot_join; simpl.
  intros. destruct (stmt_join s1 s2); intuition congruence.
Qed.

Lemma pre_annot_order_join_exists : forall
    (stmt_join_exists : forall s1 s1' s2' s' : stmt,
                     stmt_order s1 s1' ->
                     stmt_join s1' s2' = Some s' -> exists s : stmt, stmt_join s1 s2' = Some s /\ stmt_order s s'),
  forall a1' a1 a2 j : Annot.t stmt,
  annot_join stmt_join a1 a2 = Some j ->
  annot_order stmt_order a1' a1 ->
  exists j' : Annot.t stmt, annot_join stmt_join a1' a2 = Some j' /\ annot_order stmt_order j' j.
Proof.
    intros.
    unfold annot_join in *.
    unfold annot_order in H0.
    destruct a1, a2,a1' in *.
    simpl in *.
    destruct (stmt_join elt elt0) eqn:J'; try discriminate.
    inv H.
    dand.
    eapply stmt_join_exists in J'; eauto.
    destruct J'. destruct H.
    rewrite H. eexists. split. reflexivity.
    apply annot_stmt_order; simpl; auto.
    apply BSet.subset_union2;auto.
    apply BSet.subset_refl.
    apply BSet.subset_union2;auto.
    apply BSet.subset_refl.
Defined.

Fixpoint stmt_join_exists (s1:stmt) : forall s1' s2' s',
    stmt_order s1 s1' ->
    stmt_join s1' s2' = Some s' ->
    exists s, stmt_join s1 s2' = Some s /\ stmt_order s s'.
Proof.
  destruct s1; destruct s1'; simpl ; try tauto;
    destruct s2'; simpl ; try discriminate.
  - intros. inv H0. eexists.
    split_and.
    reflexivity. simpl. exact I.
  - intros.
    destruct H; subst.
    eexists. split. apply H0. apply stmt_order_refl.
  - intros.
    dand ; subst.
    eexists; split_and; eauto.
    apply stmt_order_refl.
  - intros.
    dand.
    destruct (annot_join stmt_join s0 s4) eqn: J1 ; try discriminate.
    destruct (annot_join stmt_join s3 s5) eqn: J2 ; try discriminate.
    inv H0.
    eapply pre_annot_order_join_exists in J1; eauto.
    eapply pre_annot_order_join_exists in J2; eauto.
    destruct J1 as (j1 & EQ1 & LE1).
    destruct J2 as (j2 & EQ2 & LE2).
    eexists. rewrite EQ1. rewrite EQ2.
    split.
    reflexivity.
    simpl. tauto.
  -  intros.
    dand.
    destruct (of_sb (Pos.eq_dec t0 t1) && of_sb (expr_eq_dec e0 e1)) eqn:C; try discriminate.
    rewrite andb_true_iff in C.
    destruct C.
    subst. rewrite H. rewrite H5.
    simpl.
    destruct (annot_join stmt_join s0 s6) eqn:J1; try discriminate.
    destruct (annot_join stmt_join s4 s7) eqn:J2; try discriminate.
    destruct (annot_join stmt_join s5 s8) eqn:J3; try discriminate.
    eapply pre_annot_order_join_exists in J1; eauto.
    eapply pre_annot_order_join_exists in J2; eauto.
    eapply pre_annot_order_join_exists in J3; eauto.
    destruct J1 as (j1 & EQ1 & LE1).
    destruct J2 as (j2 & EQ2 & LE2).
    destruct J3 as (j3 & EQ3 & LE3).
    inv H0.
    eexists. rewrite EQ1. rewrite EQ2. rewrite EQ3.
    split.
    reflexivity.
    simpl.
    tauto.
  - intros.
    dand ; subst.
    destruct (         of_sb (Pos.eq_dec x0 x1) && of_sb (Nat.eq_dec c0 c4) &&
         of_sb (Nat.eq_dec c3 c5)
); try discriminate.
    destruct (annot_join stmt_join s0 s1) eqn:J1; try discriminate.
    eapply pre_annot_order_join_exists in J1; eauto.
    inv H0.
    destruct J1 as (j1 & EQ1 & LE1).
    eexists. rewrite EQ1. split. reflexivity.
    simpl. tauto.
Qed.

Lemma annot_order_join_exists :
  forall a1' a1 a2 j : Annot.t stmt,
  annot_join stmt_join a1 a2 = Some j ->
  annot_order stmt_order a1' a1 ->
  exists j' : Annot.t stmt, annot_join stmt_join a1' a2 = Some j' /\ annot_order stmt_order j' j.
Proof.
    intros.
    eapply pre_annot_order_join_exists; eauto.
    apply stmt_join_exists.
Qed.

Lemma of_sb_dec : forall (A:Type) (eq : forall (x y:A), {x  = y } + { x <> y}),
    forall x,
      of_sb  (eq x x) = true.
Proof.
  intros.
  destruct (eq x x); simpl ; congruence.
Qed.

Fixpoint stmt_join_refl (s1:stmt) :
    stmt_join s1 s1 = Some s1.
Proof.
  assert (A : forall a, annot_join stmt_join a  a = Some a).
  {
    unfold annot_join. intro. rewrite stmt_join_refl.
    rewrite! BSet.union_idem.
    rewrite Annot.mk_dest. reflexivity.
  }
  destruct s1 ; simpl; repeat rewrite of_sb_dec; repeat rewrite A; auto.
Qed.

Lemma annot_join_refl : forall a,
    annot_join stmt_join a  a = Some a.
Proof.
    unfold annot_join. intro. rewrite stmt_join_refl.
    rewrite! BSet.union_idem.
    rewrite Annot.mk_dest. reflexivity.
Qed.


Definition subterm (s:stmt) (s2:stmt) :=
  match s2 with
  | Sskip => False
  | Sassign _ _ => False
  | Swrite _  _ _ => False
  | Sseq a b => Annot.elt a = s \/ Annot.elt b = s
  | Sif _ _ a b c => Annot.elt a = s \/ Annot.elt b = s \/ Annot.elt c = s
  | Sfor _ _ _ a  => Annot.elt a = s
  end.


Inductive shape : Type :=
| Zero
| Unary (s:shape)
| Binary (s1 s2:shape)
| Ternary (s1 s2 s3:shape).


Fixpoint shape_of (s:stmt) : shape :=
  match s with
  | Sskip | Sassign _ _ | Swrite _ _ _ => Zero
  | Sseq s1 s2 => Binary (shape_of (Annot.elt s1)) (shape_of (Annot.elt s2))
  | Sif _ _ s1 s2 s3 => Ternary (shape_of (Annot.elt s1))
                          (shape_of (Annot.elt s2))
                          (shape_of (Annot.elt s3))
  | Sfor _ _ _ s' => Unary (shape_of (Annot.elt s'))
  end.

Fixpoint stmt_order_shape (s1:stmt) : forall s,
    stmt_order s1 s -> shape_of s1 = shape_of s.
Proof.
  intros.
  destruct s1 ; destruct s ; simpl in * ;try tauto.
  - intros.
    unfold annot_order in *.
    intuition idtac.
    f_equal;
      apply stmt_order_shape;auto.
  - intros.
    unfold annot_order in *.
    intuition idtac.
    f_equal;
      apply stmt_order_shape;auto.
  - intros.
    unfold annot_order in *.
    intuition idtac.
    f_equal;
      apply stmt_order_shape;auto.
Qed.


Section Ind.

  Variable P : stmt -> Prop.

  Variable Pskip : P Sskip.
  Variable Passign : forall x e, P (Sassign x e).
  Variable Pwrite : forall x e1 e2, P (Swrite x e1 e2).
  Variable Pseq   : forall s1 s2
                           (IHs1 : forall s, stmt_order s (Annot.elt s1) -> P s)
                           (IHs2 : forall s, stmt_order s (Annot.elt s2) -> P s),
                           P (Sseq s1 s2).

  Variable Pif   : forall p e s1 s2 s3
                          (IHs1 : forall s, stmt_order s (Annot.elt s1) ->  P s)
                          (IHs2 : forall s, stmt_order s (Annot.elt s2) ->  P s)
                          (IHs3 : forall s, stmt_order s (Annot.elt s3) ->  P s),
      P (Sif p e s1 s2 s3).

  Variable Pfor   : forall r1 s2 g1  x c1 c2
                           (IHfor : forall s1, stmt_order s1 s2 -> P s1),
      P (Sfor x c1 c2 (Annot.mk r1 s2 g1)).

  Lemma stmt_sub (s:stmt) : P s.
  Proof.
    remember (shape_of s) as sh.
    revert s Heqsh.
    induction sh.
    - intros.
      destruct s ; try discriminate.
      apply Pskip.
      apply Passign.
      apply Pwrite.
    - intros.
      destruct s ; try discriminate.
      assert (sh = shape_of (Annot.elt s)).
      { simpl in Heqsh.
        congruence.
      }
      destruct s as [r1 s g1].
      eapply Pfor. intros.
      eapply IHsh.
      apply stmt_order_shape  in H0; auto.
      simpl in *. congruence.
    - intros.
      destruct s; try discriminate.
      simpl in Heqsh. inv Heqsh.
      eapply Pseq; intros.
      eapply IHsh1.  apply stmt_order_shape in H.  auto.
      eapply IHsh2.  apply stmt_order_shape in H.  auto.
    - intros.
      destruct s; try discriminate.
      inv Heqsh.
      eapply Pif; intros.
      apply IHsh1; auto.
      apply stmt_order_shape in H.  auto.
      apply IHsh2; auto.
      apply stmt_order_shape in H.  auto.
      apply IHsh3; auto.
      apply stmt_order_shape in H.  auto.
  Qed.

End Ind.


Module Sem.
  Import List.

  Definition VTEnv := @Map.t nat.

  Definition Denv := @Map.t (nat * (nat -> nat)).

  Definition get_var (x:var) (se:VTEnv) := Map.get 0 x se.
  Definition get_array (x:var) (d:Denv) := Map.get (0,fun _ => 0)  x d.

  Definition bool_of_nat (n:nat) :=
    match n with
    | O => false
    | _ => true
    end.

  Definition nat_of_bool (b:bool) :=
    match b with
    | false => O
    | true  => S O
    end.

  Definition eop (o:op) : nat -> nat -> nat :=
    match o with
    | Typing.Equal => fun v1 v2 => nat_of_bool (Nat.eqb v1 v2)
    | Typing.Add   => Nat.add
    | Typing.And =>  fun v1 v2 => nat_of_bool (andb (bool_of_nat v1) (bool_of_nat v2))
    | Le  =>  fun v1 v2 => nat_of_bool (Nat.leb v1 v2)
    | Lt => fun v1 v2 => nat_of_bool (Nat.ltb v1 v2)
    | Not => fun v1 _ => nat_of_bool (negb (bool_of_nat v1))
    end.

  Inductive eval_expr (a:Denv) (ev:VTEnv) : expr -> nat -> list nat -> Prop :=
  | EVar : forall x, eval_expr a ev (Var x) (Map.get 0 x ev) nil
  | ECst : forall n, eval_expr a ev (Cst n) n nil
  | EOp : forall e1 v1 t1 e2 v2 t2 o,
      eval_expr a ev e1 v1 t1 ->
      eval_expr a ev e2 v2 t2 ->
      eval_expr a ev (Op o e1 e2) (eop o v1 v2) (t1 ++ t2)
  | ECond : forall e1 v1 t1 e2 v2 t2 e3 v3 t3,
      eval_expr a ev e1 v1 t1 ->
      eval_expr a ev e2 v2 t2 ->
      eval_expr a ev e3 v3 t3 ->
      eval_expr a ev (Cond e1 e2 e3) (if bool_of_nat v1  then v2 else v3) (t1 ++ t2 ++ t3)
  | ERead : forall x e v t,
      eval_expr a ev e v t ->
      v < fst (get_array x a) ->
      eval_expr a ev (Read x e) (snd (get_array x a) v) (t++ v::nil).


  Definition setn (x:nat) (v:nat) (a : nat -> nat) :=
    fun x' => if Nat.eq_dec x' x then v else a x'.


  Definition set_array (t:positive) (i:nat) (v:nat) (e:Denv) : Denv:=
    Map.add t (fst (get_array t e) ,  setn i v (snd (get_array t e))) e.

  Inductive sem : Denv -> VTEnv -> Annot.t stmt -> Denv -> VTEnv -> list nat -> Prop:=
  | SemSkip : forall d se , sem d se skip d se nil
  | SemAssign : forall d se x n e l,
      eval_expr d se e n l ->
      sem d se (Annot.from (Sassign x e)) d (Map.add x n se) l
  | SemWrite : forall d se e1 e2 n1 n2 l1 l2 t,
      eval_expr d se e1 n1 l1 ->
      eval_expr d se e2 n2 l2 ->
      n1 < fst (get_array t d)  ->
      sem d se (Annot.from (Swrite t e1 e2)) (set_array t n1 n2 d) se (l1 ++ l2++n1::nil)
  | SemSeq : forall d se s1 d1 se1 d2 s2 se2 l1 l2,
      sem d  se  s1  d1 se1 l1 ->
      sem d1 se1 s2  d2 se2 l2 ->
      sem d se (Annot.from (Sseq s1 s2)) d2 se2 (l1 ++ l2)
  | SemIf : forall d se e l n s1 s2 s3 l1 l2 d1 d2 se1 se2 p ,
      eval_expr d se e n l ->
      sem d se (if (bool_of_nat n) then s1 else s2) d1 se1 l1 ->
      sem d1 se1 s3 d2 se2 l2 ->
      sem d se (Annot.from (Sif p e s1 s2 s3)) d2 se2  (n :: l ++ l1 ++ l2)
  | SemFor : forall s x c1 c2 d se d1 se1 d2 se2 l1 l2,
      sem d se (Annot.from (if Nat.eqb c1 c2 then Sskip else (Sfor x c1 (c2 - 1) s))) d1 se1 l1 ->
      sem d1 se1 (Annot.from (Sseq (Annot.from (Sassign x (Cst c2))) s)) d2 se2 l2 ->
      sem d se (Annot.from (Sfor x c1 c2 s)) d2 se2 (l1++l2).


  Lemma swrite :
    forall d se e1 e2 n1 n2 l1 l2 t d',
      eval_expr d se e1 n1 l1 ->
      eval_expr d se e2 n2 l2 ->
      n1 < fst (get_array t d)  ->
      d' = (set_array t n1 n2 d) ->
      sem d se (Annot.from (Swrite t e1 e2)) d' se (l1 ++ l2++n1::nil).
  Proof.
    intros. subst.
    econstructor; eauto.
  Qed.

  Lemma sfor :
    forall s x c1 c2
           (LE : c1 <= c2)
           d se d1 se1 d2 se2 l1 l2,
      sem d se (Annot.from (Sseq (Annot.from (Sassign x (Cst c1))) s)) d1 se1 l1 ->
      sem d1 se1 (Annot.from (if Nat.eqb c1 c2 then Sskip else (Sfor x (S c1) c2 s))) d2 se2 l2 ->
      sem d se (Annot.from (Sfor x c1 c2 s)) d2 se2 (l1++l2).
Proof.
  intros s x c1 c2.
  remember (c2 - c1) as n.
  revert c1 c2 Heqn.
  induction n.
  - intros.
    assert (c1 = c2) by lia. subst.
    rewrite Nat.eqb_refl in H0.
    inv H0.
    rewrite app_nil_r.
    change l1 with  (nil ++ l1).
    eapply SemFor.
    rewrite Nat.eqb_refl. constructor.
    auto.
  - intros.
    assert (c1 <> c2) by lia.
    rewrite <- Nat.eqb_neq in H1.
    rewrite H1 in H0.
    inv H0.
    rewrite app_assoc.
    eapply SemFor.
    rewrite H1.
    eapply IHn;eauto.
    lia. lia.
    assert ((S c1 =? c2) = (c1 =? c2 - 1)).
    {
      lia. }
    rewrite H0 in H11.
    eauto. auto.
Qed.


  Lemma sfor_inv :
    forall s x c1 c2
           (LE : c1 <= c2)
           d se  d2 se2 l,
      sem d se (Annot.from (Sfor x c1 c2 s)) d2 se2 l ->
      exists l1 l2 d1 se1, (l = l1 ++ l2)%list /\
                      sem d se (Annot.from (Sseq (Annot.from (Sassign x (Cst c1))) s)) d1 se1 l1 /\
                      sem d1 se1 (Annot.from (if Nat.eqb c1 c2 then Sskip else (Sfor x (S c1) c2 s))) d2 se2 l2.
Proof.
  intros s x c1 c2.
  remember (c2 - c1) as n.
  revert c1 c2 Heqn.
  induction n.
  - intros.
    assert (c1 = c2) by lia. subst.
    inv H.
    rewrite Nat.eqb_refl in *.
    inv H9.
    simpl.
    rewrite <- (app_nil_r l2).
    do 4 eexists.
    split_and.
    reflexivity.
    eauto.
    constructor.
  - intros.
    inv H.
    assert (C1 : c1 <> c2) by lia.
    rewrite <- Nat.eqb_neq in C1.
    rewrite C1 in *.
    apply IHn in H9; try lia.
    destruct H9 as (l2' & l3' & d2' & se2' & EQ & S1 & S2).
    subst.
    rewrite <- app_assoc.
    do 4 eexists.
    split_and. reflexivity.
    eauto.
    econstructor.
    assert ((S c1 =? c2) = (c1 =? c2 - 1)).
    {
      lia. }
    rewrite H.
    eauto.
    auto.
Qed.




End Sem.






Notation "[ g ] G" := (TEnv.classify_set g G) (at level 300).

(*Notation "{{ g }} G" := (TEnv.classify_vars g G) (at level 300).*)

Notation "E [ x  → v ]" := (TEnv.set x v E) (at level 300).

Notation "A ⊓ B" := (TEnv.meet A B) (at level 300).

(*Notation "A ⊓ [ p ] B" := (TEnv.meet_only p A B) (at level 300).*)

Inductive typeof_expr (D : var -> Typ.t) (G : TEnv.t) : expr -> Typ.t -> BSet.t -> Prop :=
| TVar : forall x, typeof_expr D G (Var x) (TEnv.get x G) BSet.empty
| TCst : forall x, typeof_expr D G (Cst x) Typ.L BSet.empty
| TOp  : forall o e1 e2 t1 v1 t2 v2 ,
    typeof_expr D G e1 t1 v1 ->
    typeof_expr D G e2 t2 v2 ->
    typeof_expr D G (Op o e1 e2) (Typ.join t1 t2) (BSet.union v1 v2)
| TCond : forall e1 e2 e3 t1 v1 t2 v2 t3 v3,
    typeof_expr D G e1 t1 v1 ->
    typeof_expr D G e2 t2 v2 ->
    typeof_expr D G e3 t3 v3 ->
    typeof_expr D G (Cond e1 e2 e3) (Typ.join t1 (Typ.join t2 t3)) (BSet.union v1 (BSet.union v2 v3))
| TRead : forall t e t1 v1,
    typeof_expr D G e t1 v1 ->
    t1 <> Typ.H ->
    typeof_expr D G (Read t e) (Typ.join (D t) t1) (BSet.union v1 (Typ.to_set t1)).

Inductive typeof_expr_wk (D : var -> Typ.t) (G : TEnv.t) : expr -> Typ.t -> BSet.t -> Prop :=
| TVar_wk : forall x, typeof_expr_wk D G (Var x) (TEnv.get x G) BSet.empty
| TCst_wk : forall x, typeof_expr_wk D G (Cst x) Typ.L BSet.empty
| TOp_wk  : forall o e1 e2 t1 v1 t2 v2 ,
    typeof_expr_wk D G e1 t1 v1 ->
    typeof_expr_wk D G e2 t2 v2 ->
    typeof_expr_wk D G (Op o e1 e2) (Typ.join t1 t2) (BSet.union v1 v2)
| TCond_wk : forall e1 e2 e3 t1 v1 t2 v2 t3 v3,
    typeof_expr_wk D G e1 t1 v1 ->
    typeof_expr_wk D G e2 t2 v2 ->
    typeof_expr_wk D G e3 t3 v3 ->
    typeof_expr_wk D G (Cond e1 e2 e3) (Typ.join t1 (Typ.join t2 t3)) (BSet.union v1 (BSet.union v2 v3))
| TRead_wk : forall t e t1 v1,
    typeof_expr_wk D G e t1 v1 ->
    t1 <> Typ.H ->
    typeof_expr_wk D G (Read t e) (Typ.join (D t) t1) (BSet.union v1 (Typ.to_set t1))
| TWk : forall e t1 v1 t1' v1',
    Typ.order t1 t1' = true ->
    BSet.subset v1 v1' = true ->
    typeof_expr_wk D G e t1 v1 ->
    typeof_expr_wk D G e t1' v1'.

Lemma etypeof_expr : forall D G e t1 r1 t r,
    typeof_expr D G e t r ->
    r = r1 -> t = t1 ->
    typeof_expr D G e t1 r1.
Proof.
  congruence.
Qed.

Fixpoint bvars_of_expr (e:expr)  : BSet.t :=
  match e with
  | Var y => BSet.singleton y
  | Cst _ => BSet.empty
  | Op _ e1 e2 => BSet.union (bvars_of_expr e1) (bvars_of_expr e2)
  | Cond e1 e2 e3 => BSet.union (bvars_of_expr e1) (BSet.union (bvars_of_expr e2) (bvars_of_expr e3))
  | Read _ e => bvars_of_expr e
  end.


Lemma typeof_expr_same : forall D G G' e t r
    (DOM : forall x, BSet.mem x (bvars_of_expr e) = true -> TEnv.get x G  = TEnv.get x G')
,
  typeof_expr D G e t r -> typeof_expr D G' e t r.
Proof.
  intros.
  induction H.
  - rewrite DOM.
    constructor.
    simpl. rewrite BSet.singleton_xx.
    reflexivity.
  - constructor.
  - constructor ; auto.
    simpl in DOM.
    apply IHtypeof_expr1.
    intros ; apply DOM.
    rewrite !BSet.mem_union_iff. rewrite !orb_true_iff. tauto.
    apply IHtypeof_expr2.
    intros ; apply DOM.
    simpl.
    rewrite !BSet.mem_union_iff. rewrite !orb_true_iff. tauto.
  -  constructor ; auto.
    simpl in DOM.
    apply IHtypeof_expr1.
    intros ; apply DOM.
    rewrite !BSet.mem_union_iff. rewrite !orb_true_iff. tauto.
    apply IHtypeof_expr2.
    intros ; apply DOM.
    simpl.
    rewrite !BSet.mem_union_iff. rewrite !orb_true_iff. tauto.
    apply IHtypeof_expr3.
    intros ; apply DOM.
    simpl.
    rewrite !BSet.mem_union_iff. rewrite !orb_true_iff. tauto.
  - constructor ; auto.
Qed.

Definition isPP (o:option PP) (p:PP) : bool :=
  match o with
  | None => false
  | Some p' => of_sb (Pos.eq_dec p p')
  end.

Fixpoint C (s:stmt) : BSet.t :=
  match s with
  | Sseq s1 s2 => BSet.union (C (Annot.elt s1)) (C (Annot.elt s2))
  | Sfor _ _ _ b => C (Annot.elt b)
  | Sif p _ s1 s2 s3 => BSet.union (BSet.singleton p)
                          (BSet.union (C (Annot.elt s1))
                             (BSet.union (C (Annot.elt s2)) (C (Annot.elt s3))))
  |  _ => BSet.empty
  end.

Fixpoint stmt_order_C (s1: stmt) : forall  s2,
    stmt_order s1 s2 ->
    C s1 = C s2.
Proof.
  assert (A : forall a1 a2, annot_order stmt_order a1 a2 -> C (Annot.elt a1) = C (Annot.elt a2)).
  { intros. destruct a1,a2.
    unfold annot_order in H. simpl in H. dand.
    simpl. apply stmt_order_C; auto.
  }
  intros.
  destruct s1,s2 ; simpl in H; try tauto.
  - dand.
    apply A in H0.
    apply A in H1.
    simpl. congruence.
  - dand. subst.
    apply A in H1. apply A in H3. apply A in H5.
    simpl.
    rewrite H1. rewrite H3. rewrite H5. congruence.
  - simpl.
    dand. subst.
    apply A in H4. auto.
Qed.

Definition Ca (a:Annot.t stmt) : BSet.t := C (Annot.elt a).

Lemma annot_stmt_order_Ca : forall (a1 a2: Annot.t stmt),
    annot_order stmt_order a1 a2 ->
    Ca a1 = Ca a2.
Proof.
  intros. destruct a1,a2.
  unfold annot_order in H. simpl in H. dand.
  simpl. apply stmt_order_C in H2; auto.
Qed.

Inductive typeof_stmt (D : var -> Typ.t) :  option PP -> Typ.t -> TEnv.t -> @Annot.t stmt -> TEnv.t -> Prop :=
| Tskip : forall a K G, typeof_stmt D  a K G skip G
| Tassign :
  forall a K G e t r x,
    typeof_expr D G e t r ->
    typeof_stmt D  a K G (Annot.mk r (Sassign x e) BSet.empty) (TEnv.set x (Typ.join t K) G)
| Twrite : forall a  K G x e1 t1 r1 e2 t2 r2 r,
    typeof_expr D G e1 t1 r1 ->
    typeof_expr D G e2 t2 r2 ->
    t1 <> Typ.H ->
    Typ.order (Typ.join (Typ.join t1 t2) (Typ.guard K Typ.H)) (D x) = true ->
    r = BSet.union (BSet.union r1 r2) (Typ.to_set t1) ->
    typeof_stmt D a K G (Annot.mk r (Swrite x e1 e2) BSet.empty) G
| Tseq : forall  a k s1 r1 g1 s2 r2 g2 G G1 G2,
    typeof_stmt D  a k G (Annot.mk r1 s1 g1) G1 ->
    typeof_stmt D  a k G1 (Annot.mk r2 s2 g2) G2 ->
    typeof_stmt D  a k G (Annot.mk (BSet.union r1 r2)
                                      (Sseq (Annot.mk r1 s1 g1)
                                            (Annot.mk r2 s2 g2))
                                      (BSet.union g1 g2)) G2
| Tif  : forall  a c G t r p rc k' k G1 G2 G' g g1 g2 g3 s1 s2 s3 r1 r2 r3,
    typeof_expr D G c t rc ->
    k' = Typ.join k (Typ.guard t (Typ.singleton p)) ->
    typeof_stmt D  a k' G (Annot.mk r1 s1 g1) G1 ->
    typeof_stmt D  a k' G (Annot.mk r2 s2 g2) G2 ->
    typeof_stmt D  a k (TEnv.join G1 G2) (Annot.mk r3 s3 g3) G' ->
    r = BSet.union r1 (BSet.union r2 (BSet.union r3 rc))  ->
    g = BSet.union g1 (BSet.union g2 (BSet.union g3 (Typ.to_set (Typ.guard t (Typ.singleton p))))) ->
    typeof_stmt D a k G (Annot.mk r (Sif p c (Annot.mk r1 s1 g1)
                                            (Annot.mk r2 s2 g2)
                                            (Annot.mk r3 s3 g3)) g) (if isPP a p && negb (Typ.is_L t) then TEnv.classify p G' else G')
| Tfor : forall a x k c1 c2 s Go G G1 ,
    typeof_stmt D  a k (TEnv.set x k Go) s G1 ->
    TEnv.order G1 Go ->
    TEnv.order G Go ->
    TEnv.classify_set (Ca s)  Go = Go ->
    typeof_stmt D  a k
      G (Annot.mk (Annot.leaked s) (Sfor x c1 c2 s)
                  (Annot.high s))
           Go.


Definition TTyping := TEnv.t -> @Annot.t stmt -> TEnv.t -> nat -> Prop.

Record weaken (s: Annot.t stmt) (G:TEnv.t) (s': Annot.t stmt) (G' : TEnv.t)  : Prop :=
  {
    weaken_env : TEnv.order G G';
    weaken_stmt : annot_order stmt_order s s'
  }.

Definition mk_seq (s1 s2: Annot.t stmt) : Annot.t stmt :=
  Annot.mk (BSet.union (Annot.leaked s1)  (Annot.leaked s2))
           (Sseq s1 s2)
           (BSet.union (Annot.high s1)  (Annot.high s2)).

Definition mk_if_full p e (rc:BSet.t) (t:Typ.t) (s1 s2 s3: Annot.t stmt) :=
  Annot.mk (Annot.leaked s1 ∪ (Annot.leaked s2 ∪ (Annot.leaked s3 ∪ rc)))
    (Sif p e s1 s2 s3) (Annot.high s1 ∪ (Annot.high s2 ∪ (Annot.high s3 ∪ Typ.to_set (Typ.guard t (Typ.singleton p))))) .

Definition classify_if (p: PP) (c: bool) (G: TEnv.t) :=
  (if c then TEnv.classify p G else G).

Definition mk_for x c1 c2 s :=
  Annot.mk  (Annot.leaked s)
           (Sfor x c1 c2 s)  (Annot.high s).


Inductive typeof_stmt_n (D : var -> Typ.t) :  option PP -> Typ.t -> TEnv.t -> @Annot.t stmt -> TEnv.t -> nat -> Prop :=
| Tskip_n : forall a K G G' n r g
                   (TskipWk : TEnv.order G G'),
    typeof_stmt_n D  a K G (Annot.mk r Sskip g) G' n
| Tassign_n :
  forall a K G G' e t r g x n
         (TassignExpr : typeof_expr_wk D G e t r)
         (TassignWk : TEnv.order (TEnv.set x (Typ.join t K) G) G'),
    typeof_stmt_n D  a K G (Annot.mk r (Sassign x e) g) G' n
| Twrite_n : forall a K G G' x e1 t1 r1 e2 t2 r2 r g n
                    (TwriteE1 : typeof_expr_wk D G e1 t1 r1)
                    (TwriteE2 : typeof_expr_wk D G e2 t2 r2)
                    (TwriteNOTH : t1 <> Typ.H)
                    (TwriteO  : Typ.order (Typ.join (Typ.join t1 t2) (Typ.guard K Typ.H)) (D x) = true)
                    (TwriteLk : BSet.subset (BSet.union (BSet.union r1 r2) (Typ.to_set t1)) r = true)
                    (TwriteWk : TEnv.order G G'),
    typeof_stmt_n D a K G (Annot.mk r (Swrite x e1 e2) g) G' n
| Tseq_n : forall  a k s1 s2 s'  G G1 G2 G2' n
                   (Tseq1 : typeof_stmt_n D  a k G s1 G1 n)
                   (Tseq2 : typeof_stmt_n D  a k G1 s2 G2 n)
                   (TseqWk : weaken (mk_seq s1 s2) G2 s'  G2'),
    typeof_stmt_n D  a k G s' G2' (S n)
| Tif_n  : forall  a c G t p rc k' k G1 G2 G' s1 s2 s3 n sif G''
                   (TifE : typeof_expr_wk D G c t rc)
                   (TifK'   : k' =  (Typ.join k (Typ.guard t (Typ.singleton p))))
                   (TifThen : typeof_stmt_n D  a k' G s1 G1 n)
                   (TifElse : typeof_stmt_n D  a k' G s2 G2 n)
                   (TifNext : typeof_stmt_n D  a k (TEnv.join G1 G2) s3 G' n)
                   (Tifwk   : weaken (mk_if_full p c rc t s1 s2 s3) (classify_if p (isPP a p && negb (Typ.is_L t)) G')
                                     sif G'')
  ,
    typeof_stmt_n D a k G sif G''  (S n)
| Tfor_n : forall a x k c1 c2  ar Go G sfor G2' n
                  (TforIter : typeof_stmt_n D  a
                k (TEnv.set x k Go) ar Go n)
                          (TforLastO : TEnv.order G Go)
                  (TforFix   : TEnv.classify_set (Ca ar)  Go = Go)
                  (TforWk    : weaken (mk_for x c1 c2 ar) Go sfor G2'),
                  typeof_stmt_n D  a k
                  G sfor  G2' (S  n).


Lemma typeof_stmt_incr_n : forall n D a k G s G',
    typeof_stmt_n D a k G s G' n ->
    forall n', n' >= n ->
               typeof_stmt_n D a k G s G' n'.
Proof.
  intros until 1.
  induction H.
  - intros. constructor;auto.
  - intros. eapply Tassign_n. eauto. auto.
  - intros. econstructor;eauto.
  - intros.
    destruct n' ; try lia.
    specialize (IHtypeof_stmt_n1 n' (ltac:(lia))).
    specialize (IHtypeof_stmt_n2 n' (ltac:(lia))).
    eapply Tseq_n. eauto. eauto. eauto.
  -  intros.
    destruct n' ; try lia.
    specialize (IHtypeof_stmt_n1 n' (ltac:(lia))).
    specialize (IHtypeof_stmt_n2 n' (ltac:(lia))).
    specialize (IHtypeof_stmt_n3 n' (ltac:(lia))).
    subst.
    eapply Tif_n. eauto. reflexivity.
    eapply IHtypeof_stmt_n1.
    eapply IHtypeof_stmt_n2.
    eapply IHtypeof_stmt_n3.
    auto.
(*  - intros.
    destruct n' ; try lia.
    specialize (IHtypeof_stmt_n n' (ltac:(lia))).
    eapply TifL_n; eauto. *)
  - intros.
    destruct n' ; try lia.
    specialize (IHtypeof_stmt_n n' (ltac:(lia))).
    eapply Tfor_n;eauto.
Qed.

Lemma typeof_expr_order : forall e D t G2 G1 r,
    TEnv.order G2 G1 ->
    typeof_expr D G1 e t r ->
    exists t' r',
      typeof_expr D G2 e t' r' /\ Typ.order t' t = true /\ BSet.subset r' r = true.
Proof.
  intros.
  revert G2 H.
  induction H0.
  - intros.
    do 2 eexists.
    repeat split.
    econstructor. auto.
    apply BSet.subset_refl.
  - intros.
    do 2 eexists; repeat split.
    econstructor. reflexivity.
    apply BSet.subset_refl.
  - intros.
    destruct (IHtypeof_expr1 _ H) as (t1' & r1' & T1 & O1 & S1).
    destruct (IHtypeof_expr2 _ H) as (t2' & r2' & T2 & O2 & S2).
    do 2 eexists; repeat split.
    econstructor; eauto.
    apply Typ.order_join2; auto.
    apply BSet.subset_union2; auto.
  - intros.
    destruct (IHtypeof_expr1 _ H) as (t1' & r1' & T1 & O1 & S1).
    destruct (IHtypeof_expr2 _ H) as (t2' & r2' & T2 & O2 & S2).
    destruct (IHtypeof_expr3 _ H) as (t3' & r3' & T3 & O3 & S3).
    do 2 eexists; repeat split.
    econstructor; eauto.
    apply Typ.order_join2; auto.
    apply Typ.order_join2; auto.
    apply BSet.subset_union2; auto.
    apply BSet.subset_union2; auto.
  - intros.
    destruct (IHtypeof_expr _ H1) as (t1' & r1' & T1 & O1 & S1).
    do 2 eexists; repeat split.
    econstructor; eauto.
    intro. subst.
    apply Typ.order_H_inv in O1.
    congruence.
    apply Typ.order_join2; auto.
    apply Typ.order_refl.
    apply BSet.subset_union2; auto.
    apply Typ.to_set_subset; auto.
(*  - intros.
    apply IHtypeof_expr in H2.
    destruct H2 as (t' & r' & TE' & O1 & O2).
    do 2 eexists.
    split_and.
    eauto.
    eapply Typ.order_trans;eauto.
    eapply BSet.subset_trans;eauto. *)
Qed.


Lemma annot_order_skip_r : forall s,
    annot_order stmt_order s skip ->
    s = skip.
Proof.
  intros.
  destruct s ; simpl in H ; try tauto.
  unfold annot_order in H. dand.
  simpl in *. apply BSet.subset_is_empty in H3.
  apply BSet.subset_is_empty in H0.
  subst. destruct elt eqn:E in H2; simpl in *; try tauto.
  subst. reflexivity.
Qed.

Lemma pre_annot_join_sym : forall
    (stmt_join_sym : forall s1 s2 : stmt, stmt_join s1 s2 = stmt_join s2 s1),
  forall a1 a2 : Annot.t stmt, annot_join stmt_join a1 a2 = annot_join stmt_join a2 a1.
Proof.
    destruct a1,a2; simpl.
    unfold annot_join.
    simpl.
    rewrite stmt_join_sym.
    destruct (stmt_join elt0 elt); auto.
    f_equal.
    f_equal.
    rewrite BSet.union_sym. reflexivity.
    rewrite BSet.union_sym. reflexivity.
Defined.


Fixpoint stmt_join_sym (s1:stmt) : forall s2,
    stmt_join s1 s2 = stmt_join s2 s1.
Proof.
  intro.
  destruct s1, s2; simpl; auto.
  - destruct (Pos.eq_dec x x0), (Pos.eq_dec x0 x);
      destruct (expr_eq_dec e e0), (expr_eq_dec e0 e); try congruence;
      simpl; try reflexivity.
    congruence.
  - destruct (Pos.eq_dec x x0), (Pos.eq_dec x0 x);
      destruct (expr_eq_dec e1 e0), (expr_eq_dec e0 e1);
      destruct (expr_eq_dec e2 e3), (expr_eq_dec e3 e2);
      simpl; try reflexivity; congruence.
  - rewrite pre_annot_join_sym by auto.
    destruct (annot_join stmt_join s2 s1) eqn:J1.
    rewrite pre_annot_join_sym by auto.
    reflexivity.
    reflexivity.
  -
    destruct (Pos.eq_dec t0 t), (expr_eq_dec e0 e),
      (Pos.eq_dec t t0) , (expr_eq_dec e e0); try congruence; try reflexivity.
    simpl. subst.
    rewrite pre_annot_join_sym by auto.
    destruct (annot_join stmt_join s2 s1 ); try reflexivity.
    rewrite pre_annot_join_sym by auto.
    destruct (annot_join stmt_join s4 s0); try reflexivity.
    rewrite pre_annot_join_sym by auto.
    reflexivity.
  -
    destruct (Pos.eq_dec x x0) ,(Nat.eq_dec c1 c0) ,
      (Nat.eq_dec c2 c3),
      (Pos.eq_dec x0 x) , (Nat.eq_dec c0 c1) ,
      (Nat.eq_dec c3 c2); try congruence; try reflexivity.
    subst.
    simpl.
    rewrite pre_annot_join_sym by auto.
    reflexivity.
Qed.


Lemma annot_join_sym :
  forall a1 a2 : Annot.t stmt, annot_join stmt_join a1 a2 = annot_join stmt_join a2 a1.
Proof.
  intros. apply pre_annot_join_sym.
  apply stmt_join_sym.
Qed.

Lemma pre_annot_order_trans : forall (TRANS : forall s1 s2 s3,
    stmt_order s1 s2 ->
    stmt_order s2 s3 -> stmt_order s1 s3),
forall a1 a2 a3,
             annot_order stmt_order a1 a2 ->
             annot_order stmt_order a2 a3 ->
             annot_order stmt_order a1 a3.
Proof.
  intro.
  destruct a1,a2,a3; unfold annot_order; simpl.
  intuition idtac.
  eapply BSet.subset_trans; eauto.
  eapply TRANS;eauto.
  eapply BSet.subset_trans; eauto.
Defined.

Fixpoint stmt_order_trans (s1:stmt) : forall s2 s3,
    stmt_order s1 s2 ->
    stmt_order s2 s3 -> stmt_order s1 s3.
Proof.
  assert (T := pre_annot_order_trans stmt_order_trans).
  intros.
  destruct s1; destruct s2; destruct s3; simpl in *; try tauto; try intuition congruence.
  -  intuition idtac.
    eapply T ; eauto.
    eapply T; eauto.
  -  intuition idtac; try congruence.
     eapply T;eauto.
     eapply T;eauto.
     eapply T;eauto.
  - intuition idtac ; try congruence.
     eapply T;eauto.
Qed.


Lemma annot_order_trans :
forall a1 a2 a3,
             annot_order stmt_order a1 a2 ->
             annot_order stmt_order a2 a3 ->
             annot_order stmt_order a1 a3.
Proof.
  do 3 intro.
  eapply pre_annot_order_trans;eauto.
  apply stmt_order_trans.
Qed.

Lemma stmt_join2_exists : forall s1 s1' s2 s2' sr',
    annot_order stmt_order s1 s1' ->
    annot_order stmt_order s2 s2' ->
    annot_join stmt_join s1' s2' = Some sr' ->
    exists sr,
      annot_join stmt_join s1 s2 = Some sr /\
        annot_order stmt_order sr sr'.
Proof.
  intros.
  assert (SJE := stmt_join_exists).
  eapply annot_order_join_exists in H1; eauto.
  destruct H1 as (j1 & AJ & AO).
  rewrite annot_join_sym in AJ.
  eapply annot_order_join_exists in AJ; eauto.
  destruct AJ as (j2 & AJ2 & AO2).
  rewrite annot_join_sym in AJ2.
  eexists ; split ; eauto.
  eapply annot_order_trans;eauto.
Qed.


Fixpoint stmt_join_order (s1: stmt) : forall s2 sr,
    stmt_join s1 s2 = Some sr ->
    stmt_order s1 sr.
Proof.
  assert (AJ : forall a1 a2 ar, annot_join stmt_join a1 a2 = Some ar ->
                                 annot_order stmt_order a1 ar).
  {
    unfold annot_order,annot_join.
    intros.
    destruct (stmt_join (Annot.elt a1) (Annot.elt a2)) eqn:J; try discriminate.
    inv H.
    apply stmt_join_order in J.
    simpl. intuition auto.
    apply BSet.subset_union_l.
    apply BSet.subset_union_l.
  }
  intros.
  destruct s1,s2; simpl in *; try inv H; try tauto.
  - destruct (of_sb (Pos.eq_dec x x0) && of_sb (expr_eq_dec e e0));
      try congruence.
    inv H1. tauto.
  - (destruct         (of_sb (Pos.eq_dec x x0) && of_sb (expr_eq_dec e1 e0) &&
         of_sb (expr_eq_dec e2 e3))
    );
      try congruence.
    inv H1. tauto.
  - destruct (annot_join stmt_join s1 s2) eqn: AJ1;try discriminate.
    destruct (annot_join stmt_join s0 s3) eqn: AJ2;try discriminate.
    inv H1.
    apply AJ in AJ1.
    apply AJ in AJ2.
    tauto.
  - destruct (of_sb (Pos.eq_dec t t0) && of_sb (expr_eq_dec e e0)); try discriminate.
    destruct (annot_join stmt_join s1 s2) eqn: AJ1;try discriminate.
    destruct (annot_join stmt_join s0 s4) eqn: AJ2;try discriminate.
    destruct (annot_join stmt_join s3 s5) eqn: AJ3;try discriminate.
    inv H1.
    apply AJ in AJ1.
    apply AJ in AJ2.
    apply AJ in AJ3.
    tauto.
  -
    destruct (of_sb (Pos.eq_dec x x0) && of_sb (Nat.eq_dec c1 c0) &&
                of_sb (Nat.eq_dec c2 c3)); try congruence.
    destruct (annot_join stmt_join s s0) eqn: AJ1;try discriminate.
    inv H1.
    apply AJ in AJ1.
    tauto.
Qed.

Lemma annot_join_stmt_order : forall a1 a2 ar,
    annot_join stmt_join a1 a2 = Some ar ->
    annot_order stmt_order a1 ar.
Proof.
    unfold annot_order,annot_join.
    intros.
    destruct (stmt_join (Annot.elt a1) (Annot.elt a2)) eqn:J; try discriminate.
    inv H.
    apply stmt_join_order in J.
    simpl. intuition auto.
    apply BSet.subset_union_l.
    apply BSet.subset_union_l.
Qed.




Lemma tmk_seq_n : forall D a k G s1 s2 G1 G2 n,
    typeof_stmt_n D a k G s1 G1 n ->
    typeof_stmt_n D a k G1 s2 G2 n ->
    typeof_stmt_n D a k G (mk_seq s1 s2) G2 (S n).
Proof.
  unfold mk_seq.
  intros.
  destruct s1,s2;simpl in *.
  eapply Tseq_n. eauto. eauto.
  constructor. apply TEnv.order_refl.
  apply annot_stmt_order_refl.
Qed.


Definition mk_if p e (s1 s2 s3: Annot.t stmt) :=
  Annot.mk (BSet.union (Annot.leaked s1) (BSet.union (Annot.leaked s2) (Annot.leaked s3)))
           (Sif p e s1 s2 s3)
           (BSet.union (Annot.high s1) (BSet.union (Annot.high s2) (Annot.high s3))).

Lemma typeof_stmt_n_order_skip : forall D a G1 G r g k' k G' n,
    TEnv.order G1 G ->
    Typ.order k' k = true ->
    TEnv.order G G' ->
    typeof_stmt_n D a k' G1 (Annot.mk r Sskip g) G' n.
Proof.
  constructor. eapply TEnv.order_trans;eauto.
Qed.


Lemma typeof_expr_order_wk : forall e D t G2 G1 r,
    TEnv.order G2 G1 ->
    typeof_expr_wk D G1 e t r ->
    exists t' r',
      typeof_expr_wk D G2 e t' r' /\ Typ.order t' t = true /\ BSet.subset r' r = true.
Proof.
  intros.
  revert G2 H.
  induction H0.
  - intros.
    do 2 eexists.
    repeat split.
    econstructor. auto.
    apply BSet.subset_refl.
  - intros.
    do 2 eexists; repeat split.
    econstructor. reflexivity.
    apply BSet.subset_refl.
  - intros.
    destruct (IHtypeof_expr_wk1 _ H) as (t1' & r1' & T1 & O1 & S1).
    destruct (IHtypeof_expr_wk2 _ H) as (t2' & r2' & T2 & O2 & S2).
    do 2 eexists; repeat split.
    econstructor; eauto.
    apply Typ.order_join2; auto.
    apply BSet.subset_union2; auto.
  - intros.
    destruct (IHtypeof_expr_wk1 _ H) as (t1' & r1' & T1 & O1 & S1).
    destruct (IHtypeof_expr_wk2 _ H) as (t2' & r2' & T2 & O2 & S2).
    destruct (IHtypeof_expr_wk3 _ H) as (t3' & r3' & T3 & O3 & S3).
    do 2 eexists; repeat split.
    econstructor; eauto.
    apply Typ.order_join2; auto.
    apply Typ.order_join2; auto.
    apply BSet.subset_union2; auto.
    apply BSet.subset_union2; auto.
  - intros.
    destruct (IHtypeof_expr_wk _ H1) as (t1' & r1' & T1 & O1 & S1).
    do 2 eexists; repeat split.
    econstructor; eauto.
    intro. subst.
    apply Typ.order_H_inv in O1.
    congruence.
    apply Typ.order_join2; auto.
    apply Typ.order_refl.
    apply BSet.subset_union2; auto.
    apply Typ.to_set_subset; auto.
  - intros.
    apply IHtypeof_expr_wk in H2.
    destruct H2 as (t' & r' & TE' & O1 & O2).
    do 2 eexists.
    split_and.
    eauto.
    eapply Typ.order_trans;eauto.
    eapply BSet.subset_trans;eauto.
Qed.


Lemma typeof_stmt_n_order_assign :
  forall D a k G G1 k' e t r x G' g n
         (ORD1 : TEnv.order G1 G)
         (ORD2 : Typ.order k' k = true)
         (TassignExpr : typeof_expr_wk D G e t r)
         (TassignWk : TEnv.order (G [x → t ⊔ k]) G'),
  typeof_stmt_n D a k' G1 (Annot.mk r (Sassign x e) g) G' n.
Proof.
  intros.
  eapply typeof_expr_order_wk in TassignExpr;eauto.
  destruct TassignExpr as (t' & r' & T1 & O1 & S1).
  assert (TE : typeof_expr_wk D G1 e t r).
  { eapply TWk; eauto.
  }
  eapply Tassign_n. eauto.
  eapply TEnv.order_trans; eauto.
  apply TEnv.order_set;auto.
  apply Typ.order_join2;auto.
  apply Typ.order_refl.
Qed.

Lemma typeof_stmt_n_order_write :
  forall D a e2 G1 G G' k' k e1 t1 t2 r1 r2 x r g n
    (TE1 : typeof_expr_wk D G e1 t1 r1)
    (TE2 : typeof_expr_wk D G e2 t2 r2),
    TEnv.order G1 G ->
         Typ.order k' k = true ->
         t1 <> Typ.H ->
         Typ.order ((t1 ⊔ t2) ⊔ Typ.guard k Typ.H) (D x) = true ->
         BSet.subset
           ((r1 ∪ r2)
              ∪ Typ.to_set t1) r =
           true ->
         TEnv.order G G' ->
  typeof_stmt_n D a k' G1
    (Annot.mk r (Swrite x e1 e2) g) G' n.
Proof.
  intros.
  eapply typeof_expr_order_wk in TE1; eauto.
  eapply typeof_expr_order_wk in TE2; eauto.
  destruct TE1 as (t1' & r1' & TE1 & OE1 & OE1').
  destruct TE2 as (t2' & r2' & TE2 & OE2 & OE2').
  eapply Twrite_n. eapply TE1.  eapply TE2.
  intro. subst.
  apply Typ.order_H_inv in OE1. congruence.
  eapply Typ.order_trans.
  eapply Typ.order_join2.
  apply Typ.order_join2.
  eauto. eauto. apply Typ.order_guard.
  eauto. apply Typ.order_refl.
  eauto.
  eapply BSet.subset_trans with (2:= H3).
  apply BSet.subset_union2; auto.
  apply BSet.subset_union2; auto.
  assert (BSet.subset (Typ.to_set t1') (Typ.to_set t1) = true).
  {
    apply Typ.to_set_subset.
    intros.
    subst. congruence.
    auto.
    }
    auto.
  eapply TEnv.order_trans;eauto.
Qed.

Lemma order_isPP : forall a p t1' t GN G'
    (TE : Typ.order t1' t = true)
    (OE : TEnv.order GN G'),
    TEnv.order (classify_if p ((isPP a p) && negb (Typ.is_L t1')) GN)
      (classify_if p ((isPP a p) && negb (Typ.is_L t)) G').
Proof.
  intros.
  unfold classify_if.
  destruct (Typ.is_L t) eqn:ISL.
  -
  apply Typ.is_L_inv in ISL.
  subst. apply Typ.order_is_L_r in TE.
  subst.  simpl. rewrite BSet.is_empty_empty.
  simpl. destruct (isPP a p && false);auto.
  apply TEnv.classify_mono; auto.
  -  destruct (Typ.is_L t1') eqn:ISLT1.
     + simpl. destruct (isPP a p); simpl; auto.
     eapply TEnv.order_trans;eauto.
     eapply TEnv.order_classify.
     +
        simpl.
        destruct (isPP a p) ; simpl; auto.
        apply TEnv.classify_mono;auto.
Qed.

Lemma annot_order_mk_if :
  forall p c r1 t1 s1 s2 s3
         r1' t1' s1' s2' s3',
    annot_order stmt_order s1' s1 ->
    annot_order stmt_order s2' s2 ->
    annot_order stmt_order s3' s3 ->
    BSet.subset r1' r1 = true ->
    Typ.order t1' t1 = true ->
    annot_order stmt_order (mk_if_full p c r1' t1' s1' s2' s3')
      (mk_if_full p c r1 t1 s1 s2 s3).
Proof.
  unfold mk_if_full.
  intros.
  unfold annot_order in *; simpl in *; dand.
  split_and; auto.
  subset.
  unfold annot_order; split_and;auto.
  unfold annot_order; split_and;auto.
  unfold annot_order; split_and;auto.
  apply BSet.subset_union2;auto.
  apply BSet.subset_union2;auto.
  apply BSet.subset_union2;auto.
  apply Typ.to_set_subset.
  intros.
  apply Typ.guard_singleton_not_H in H.
  tauto.
  apply Typ.order_guard. auto.
  apply Typ.order_refl.
Qed.

Lemma typeof_stmt_n_order : forall n D a k G G1 k' s G',
    typeof_stmt_n D a k G s G' n ->
    TEnv.order G1 G ->
    Typ.order k' k = true ->
    typeof_stmt_n D a k' G1 s G' n.
Proof.
  induction n.
  - intros. inv H.
    + eapply typeof_stmt_n_order_skip; eauto.
    + eapply typeof_stmt_n_order_assign;eauto.
    + eapply typeof_stmt_n_order_write; eauto.
  - intros.
    inv H.
    + eapply typeof_stmt_n_order_skip; eauto.
    + eapply typeof_stmt_n_order_assign;eauto.
    + eapply typeof_stmt_n_order_write; eauto.
    + eapply IHn with (k':=k') (G1:= G1) in Tseq1; auto.
      eapply IHn with (k':=k') (G1:= G2) in Tseq2; auto.
      eapply Tseq_n.
      eauto. eauto.
      auto.
      apply TEnv.order_refl.
    +  eapply typeof_expr_order_wk in TifE; eauto.
       destruct TifE as (t1' & r1' & TE1 & OE1 & OE1').
      eapply IHn with (k':=(k' ⊔ Typ.guard t1' (Typ.singleton p))) (G1:= G1) in TifThen; auto.
      eapply IHn with (k':=(k' ⊔ Typ.guard t1' (Typ.singleton p))) (G1:= G1) in TifElse; auto.
      eapply IHn with (k':=k') (G1:= TEnv.join G2 G3) in TifNext; auto.
      eapply Tif_n.
      eapply TE1.
      reflexivity.
      eapply TifThen.
      eapply TifElse.
      eapply TifNext.
      destruct Tifwk.
      constructor; auto.
      eapply TEnv.order_trans.
      apply order_isPP. eauto. eapply TEnv.order_refl.
      auto.
      eapply annot_order_trans;eauto.
      apply annot_order_mk_if; auto.
      apply annot_stmt_order_refl.
      apply annot_stmt_order_refl.
      apply annot_stmt_order_refl.
      apply TEnv.order_refl.
      apply Typ.order_join2;auto.
      apply Typ.order_guard;auto.
      apply Typ.order_refl.
      apply Typ.order_join2;auto.
      apply Typ.order_guard;auto.
      apply Typ.order_refl.
(*    +
      apply IHn with (G1:= G1) (k':= k') in TifL0.
      eapply TifL_n.
      eauto. reflexivity.
      eauto.
      specialize (H0 x).
      rewrite TifLLow in H0.
      apply Typ.order_is_L_r in H0. auto.
      eauto.
      destruct TifLWk.
      constructor ; auto.
      auto.
      auto. *)
    + apply IHn   with (G1:= TEnv.set x k' Go) (k':= k') in TforIter; auto.
      eapply Tfor_n.
      eauto. eauto.
      auto. eapply TEnv.order_trans;eauto.
      auto.
      eauto.
      apply TEnv.order_set;auto.
      apply TEnv.order_refl.
Qed.

Lemma annot_order_mk_seq : forall s1 s1' s2 s2',
    annot_order stmt_order s1 s1' ->
    annot_order stmt_order s2 s2' ->
    annot_order stmt_order (mk_seq s1 s2) (mk_seq s1' s2').
Proof.
  unfold mk_seq,  annot_order.
  simpl. unfold annot_order. intros. dand.
  split_and; subset.
Qed.

Lemma typeof_stmt_stmt_n_skip : forall D a G1 G G' k' k r g,
    TEnv.order G1 G ->
    Typ.order k' k = true ->
    TEnv.order G G' ->
  exists (G'' : TEnv.t) (s' : Annot.t stmt),
    typeof_stmt D a k' G1 s' G'' /\
    annot_order stmt_order s' (Annot.mk r Sskip g) /\ TEnv.order G'' G'.
Proof.
  intros.
  exists G1. exists skip.
  split_and. constructor.
  unfold annot_order; split_and; simpl; auto.
  eapply TEnv.order_trans;eauto.
Qed.

Lemma typeof_expr_wk_expr : forall D G e t r,
    typeof_expr_wk D G e t r ->
    exists t' r', typeof_expr D G e t' r' /\ Typ.order t' t = true /\ BSet.subset r' r = true.
Proof.
  intros.
  induction H.
  - do 2 eexists; split_and.
    econstructor; eauto.
    apply TEnv.order_refl.
    apply BSet.subset_refl.
  - do 2 eexists; split_and.
    econstructor; eauto.
    apply Typ.order_refl.
    apply BSet.subset_refl.
  - destruct IHtypeof_expr_wk1 as (t1' & r1 & T1 & O1 & O1').
    destruct IHtypeof_expr_wk2 as (t2' & r2 & T2 & O2 & O2').
    do 2 eexists.
    split_and.
    econstructor; eauto.
    apply Typ.order_join2;auto.
    apply BSet.subset_union2;auto.
  - destruct IHtypeof_expr_wk1 as (t1' & r1 & T1 & O1 & O1').
    destruct IHtypeof_expr_wk2 as (t2' & r2 & T2 & O2 & O2').
    destruct IHtypeof_expr_wk3 as (t3' & r3 & T3 & O3 & O3').
    do 2 eexists.
    split_and.
    econstructor; eauto.
    apply Typ.order_join2;auto.
    apply Typ.order_join2;auto.
    apply BSet.subset_union2;auto.
    apply BSet.subset_union2;auto.
  - destruct IHtypeof_expr_wk as (t1' & r1 & T1 & O1 & O1').
    do 2 eexists.
    split_and.
    econstructor; eauto.
    intro. subst.
    apply Typ.order_H_inv in O1.
    congruence.
    apply Typ.order_join2;auto.
    apply Typ.order_refl.
    apply BSet.subset_union2;auto.
    apply Typ.to_set_subset.
    congruence.
    auto.
  - destruct IHtypeof_expr_wk as (t2 & r1 & T1 & O1 & O1').
    do 2 eexists.
    split_and.
    eauto. eapply Typ.order_trans;eauto.
    eapply BSet.subset_trans;eauto.
Qed.

Lemma typeof_expr_order_wk' : forall e D t G2 G1 r,
    TEnv.order G2 G1 ->
    typeof_expr_wk D G1 e t r ->
    exists t' r',
      typeof_expr D G2 e t' r' /\ Typ.order t' t = true /\ BSet.subset r' r = true.
Proof.
  intros.
  eapply typeof_expr_wk_expr in H0.
  destruct H0 as (t1 & r1 & TY & O1 & O2).
  eapply typeof_expr_order in TY;eauto.
  destruct TY as (t1' & r1' & TY & O1' & O2').
  do 2 eexists. split_and.
  eauto.
  eapply Typ.order_trans;eauto.
  eapply BSet.subset_trans;eauto.
Qed.



Lemma typeof_stmt_stmt_n_assign :
  forall D a e t r g x G1 G G' k' k,
    TEnv.order G1 G ->
    Typ.order k' k = true ->
    typeof_expr_wk D G e t r ->
    TEnv.order (G [x → t ⊔ k]) G' ->
  exists (G'' : TEnv.t) (s' : Annot.t stmt),
    typeof_stmt D a k' G1 s' G'' /\
    annot_order stmt_order s' (Annot.mk r (Sassign x e) g) /\
    TEnv.order G'' G'.
Proof.
  intros.
  apply typeof_expr_order_wk' with (G2 := G1) in H1; auto.
  destruct H1 as (t' & r' & TE & O1 & O2).
  exists (G1 [x → t' ⊔ k']).
  eexists.
  split_and.
  econstructor. eauto.
  unfold annot_order; simpl; split_and; auto.
  eapply TEnv.order_trans ;eauto.
  apply TEnv.order_set;auto.
  apply Typ.order_join2;auto.
Qed.

Lemma typeof_stmt_n_write : forall
    (D : var -> Typ.t)
    (a : option var)
    (k : Typ.t)
    (G G1 : TEnv.t)
    (k' : Typ.t)
    (G' : TEnv.t)
    (x : var)
    (e1 e2 : expr)
    (r g : BSet.t)
    (H0 : TEnv.order G1 G)
    (H1 : Typ.order k' k = true)
    (t1 : Typ.t)
    (r1 : BSet.t)
    (t2 : Typ.t)
    (r2 : BSet.t)
    (TwriteE1 : typeof_expr_wk D G e1 t1 r1)
    (TwriteE2 : typeof_expr_wk D G e2 t2 r2)
    (TwriteNOTH : t1 <> Typ.H)
    (TwriteO : Typ.order ((t1 ⊔ t2) ⊔ Typ.guard k Typ.H) (D x) = true)
    (TwriteLk : BSet.subset ((r1 ∪ r2) ∪ Typ.to_set t1) r = true)
    (TwriteWk : TEnv.order G G'),
  exists (G'' : TEnv.t) (s' : Annot.t stmt),
    typeof_stmt D a k' G1 s' G'' /\
    annot_order stmt_order s' (Annot.mk r (Swrite x e1 e2) g) /\
    TEnv.order G'' G'.
Proof.
  intros.
  eapply typeof_expr_order_wk' in TwriteE1; eauto.
  destruct TwriteE1 as (t1' & r1' & TE & OE1  & AOE1).
  eapply typeof_expr_order_wk' in TwriteE2; eauto.
  destruct TwriteE2 as (t2' & r2' & TE2 & OE2  & AOE2).
  eexists.
  exists (Annot.mk (BSet.union (BSet.union r1' r2') (Typ.to_set t1')) (Swrite x e1 e2) BSet.empty).
  split_and.
  econstructor.
  eauto. eauto.
  intro. subst.
  apply Typ.order_H_inv in OE1. subst.
  congruence.
  { eapply Typ.order_trans.
  eapply Typ.order_join2.
  apply Typ.order_join2. eauto. eauto.
  apply Typ.order_guard. eauto.
  apply Typ.order_refl.
  auto. }
  reflexivity.
  unfold annot_order.
  simpl. split_and;auto.
  assert (BSet.subset (Typ.to_set t1') (Typ.to_set t1) = true).
  {
    apply Typ.to_set_subset.
    intros.
    subst. congruence.
    auto.
  }
  eapply BSet.subset_trans.
  eapply BSet.subset_union2.
  eapply BSet.subset_union2.  eauto.
  eauto.
  eauto.
  auto.
  eapply TEnv.order_trans;eauto.
Qed.

Lemma Typ_to_set_mono1 : forall t1 t2 t,
    Typ.order t1 t2 = true->
    BSet.subset (Typ.to_set (Typ.guard t1 t))
              (Typ.to_set (Typ.guard t2 t)) = true.
Proof.
  intros.
  apply Typ.to_set_subset;auto.
  - intros.
    unfold Typ.guard in *.
    destruct (Typ.is_L t2) eqn:ISL.
    discriminate.
    subst.
    destruct (Typ.is_L t1);auto.
    inv H1. subset. discriminate.
  -  apply Typ.order_guard;auto.
     apply Typ.order_refl.
Qed.

Lemma typeof_stmt_stmt_n : forall n D a k G G1 k' s G',
    typeof_stmt_n D a k G s G' n ->
    TEnv.order G1 G ->
    Typ.order k' k = true ->
    exists G'' s',
      typeof_stmt D a k' G1 s' G'' /\  annot_order stmt_order s' s /\ TEnv.order G'' G'.
Proof.
  induction n.
  intros.
  - inv H.
    + eapply typeof_stmt_stmt_n_skip; eauto.
    + eapply typeof_stmt_stmt_n_assign;eauto.
    + eapply typeof_stmt_n_write; eauto.
  - intros.
    inv H.
    + eapply typeof_stmt_stmt_n_skip; eauto.
    + eapply typeof_stmt_stmt_n_assign;eauto.
    + eapply typeof_stmt_n_write; eauto.
    + eapply IHn in Tseq1; eauto.
      destruct Tseq1 as (G'' & s1' & Ts1 & OS1 & OG1).
      eapply IHn in Tseq2 ;eauto.
      destruct Tseq2 as (G2'' & s2' & Ts2 & OS2 & OG2).
      exists G2''.
      exists (mk_seq s1' s2').
      split_and.
      unfold mk_seq. destruct s1',s2'; simpl in *.
      eapply Tseq. eauto.
      eauto.
      eapply annot_order_trans;eauto.
      apply annot_order_mk_seq. eauto.
      eauto. destruct TseqWk; auto.
      destruct TseqWk.
      eapply TEnv.order_trans;eauto.
    +
      apply typeof_expr_order_wk' with (G2:= G1) in TifE.
      destruct TifE as (t1' & r' & TE & OT & OR).
      apply IHn with (G1 := G1) (k':= (k' ⊔ Typ.guard t1' (Typ.singleton p)))in TifThen.
      destruct TifThen as (G2' & s1' & TS1 & OS1 & OT1).
      apply IHn with (G1 := G1) (k':= (k' ⊔ Typ.guard t1' (Typ.singleton p)))in TifElse.
      destruct TifElse as (G3' & s2' & TS2 & OS2 & OT2).
      apply IHn with (G1 := TEnv.join G2' G3') (k':= k')in TifNext.
      destruct TifNext as (GR & s3' & Ts3 & OS3 & OT3).
      destruct s1' as [r1' s1' g1'].
      destruct s2' as [r2' s2' g2'].
      destruct s3' as [r3' s3' g3'].
      inv Tifwk.
      eexists.
      eexists.
      split_and.
      eapply Tif.
      eauto. reflexivity.
      eapply TS1.
      eapply TS2.
      eauto.
      reflexivity.
      reflexivity.
      {
        eapply annot_order_trans;eauto.
        unfold annot_order; simpl; split_and;auto.
        unfold annot_order in * ; simpl in *; dand;subset.
        unfold annot_order in * ; simpl in *; dand.
        repeat apply BSet.subset_union2;auto.
        apply Typ_to_set_mono1;auto.
      }
      change (    match andb (isPP a p) (negb (Typ.is_L t1')) return TEnv.t with
    | true => TEnv.classify p GR
    | false => GR
    end
             ) with (classify_if p (isPP a p && negb (Typ.is_L t1')) GR).
      eapply TEnv.order_trans; eauto.
      apply order_isPP.
      eauto. auto.
      intro.
      rewrite! TEnv.get_join.
      apply Typ.order_join2;auto.
      auto.
      auto.
      apply Typ.order_join2; auto.
      apply Typ.order_guard;auto.
      apply Typ.order_refl.
      auto.
      apply Typ.order_join2; auto.
      apply Typ.order_guard;auto.
      apply Typ.order_refl.
      auto.
(*    + destruct b.
      { apply IHn with (G1:= G1) (k':= k') in TifL0.
        destruct TifL0 as (G22 & s' & TS & TO & TO').
        destruct s' as [r'  s'  g'].
        destruct TifLWk.
        unfold annot_order in *.
        simpl in *.
        destruct s as [r0 s g0].
        simpl in *. destruct s; simpl in weaken_stmt0 ; try tauto.
        dand; subst.
        exists G22.
        eexists.
        split_and.
        apply TifL with (x:=x) (c:=c)(s1:=s') (p:=t) (g:=g').
        eauto. reflexivity.
        {
          simpl.
          unfold annot_order in TO3.
          dand ; simpl in *.
          subset. destruct (BSet.mem t g'); auto. intuition congruence.
        }
        specialize (H0 x).
        rewrite TifLLow in H0.
        apply Typ.order_is_L_r in H0. auto.
        simpl. eauto.
        {  simpl.
           subset.
        }
        { simpl. split_and;auto.
          unfold annot_order; simpl; split_and;auto.
          destruct s0; simpl in *.
          unfold annot_order in *; simpl in * ; dand; subset.
          eapply stmt_order_trans;eauto.
          unfold annot_order in *; simpl in * ; dand; auto.
          destruct s0; simpl in *.
          unfold annot_order in *; simpl in * ; dand; subset.
      }
      {
        simpl.
        subset.
      }
      eapply TEnv.order_trans;eauto.
      auto.
      auto.
      }
      {
      apply IHn with (G1:= G1) (k':= k') in TifL0.
      destruct TifL0 as (G22 & s' & TS & TO & TO').
      apply annot_order_skip_r in TO.
      subst.
      inv TS.
      destruct TifLWk.
      destruct  s as [r0 s g0].
      unfold annot_order in weaken_stmt0.
      simpl in *. destruct s ; simpl in * ; try tauto.
      dand;subst.
      destruct s2,s3; simpl in *.
      unfold annot_order in *.
      simpl in *.
      destruct elt0,elt; try tauto.
      exists G22.
      eexists.
      split_and.
      apply TifL with (x:=x) (c:=c)(s1:=s1) (p:=t) (r:=r)(g:=g).
      eauto. reflexivity.
      {
          simpl.
          rewrite BSet.mem_empty. reflexivity.
        }
        specialize (H0 x).
        rewrite TifLLow in H0.
        apply Typ.order_is_L_r in H0. auto.
        simpl. constructor.
        { simpl.
          subset.
        }
        {
          simpl.
          split_and;auto.
        }
        simpl. subset.
        eapply TEnv.order_trans;eauto.
        auto.
        auto.
      } *)
    + apply IHn with (G1:= TEnv.set x k' Go) (k':= k') in TforIter.
      destruct TforIter as (G'' & ai' & T1 & O1 & O2).
      eexists Go.
      destruct TforWk.
      eexists.
      split_and.
      eapply Tfor. eauto.
      auto.
      eapply TEnv.order_trans;eauto.
      apply annot_stmt_order_Ca in O1.
      congruence.
      eapply annot_order_trans;eauto.
      unfold annot_order. simpl.
      split_and;auto.
      unfold annot_order in O1; dand;auto.
      unfold annot_order in O1; dand;auto.
      auto.
      apply TEnv.order_set;auto.
      apply TEnv.order_refl.
      auto.
Qed.

Lemma typeof_stmt_stmt_n_eq : forall n D a k G s G',
    typeof_stmt_n D a k G s G' n ->
    exists G'' s',
      typeof_stmt D a k G s' G'' /\  annot_order stmt_order s' s /\ TEnv.order G'' G'.
Proof.
  intros.
  eapply typeof_stmt_stmt_n in H.
  destruct H as (G'' & s' & H).
  dand.
  do 2 eexists. split_and;eauto.
  apply TEnv.order_refl.
  apply Typ.order_refl.
Qed.


Lemma typeof_stmt_n_weak_skip : forall n
    (D : var -> Typ.t)
    (a : option var)
    (k : Typ.t)
    (G G' G2 : TEnv.t)
    (r g : BSet.t)
    (s' : Annot.t stmt)
    (H0 : annot_order stmt_order (Annot.mk r Sskip  g) s')
    (H1 : TEnv.order G' G2)
    (TskipWk : TEnv.order G G'),
  typeof_stmt_n D a k G s' G2 n.
Proof.
  intros.
  unfold annot_order in H0.
  simpl in *. destruct s'; simpl in *.
  destruct elt; simpl in * ; try tauto.
  eapply Tskip_n.
  eapply TEnv.order_trans;eauto.
Qed.

Lemma typeof_expr_expr_wk : forall D G e t r,
    typeof_expr D G e t r ->
    typeof_expr_wk D G e t r.
Proof.
  intros.
  induction H.
  - constructor; auto.
  - constructor; auto.
  -  econstructor; eauto.
  -  econstructor; eauto.
  - econstructor;eauto.
Qed.

Lemma typeof_stmt_n_weak_assign :
  forall n
         (D : var -> Typ.t)
         (a : option var)
         (k : Typ.t)
         (G G' G2 : TEnv.t)
         (e : expr)
         (r g : BSet.t)
         (x : var)
         (s' : Annot.t stmt)
         (H0 : annot_order stmt_order (Annot.mk r (Sassign x e) g) s')
         (t : Typ.t)
         (H1 : TEnv.order G' G2)
         (TassignExpr : typeof_expr_wk D G e t r)
         (TassignWk : TEnv.order (G [x → t ⊔ k]) G'),
  typeof_stmt_n D a k G s' G2 n.
Proof.
  intros.
  unfold annot_order in H0.
  simpl in *. destruct s'; simpl in *.
  destruct elt; simpl in * ; try tauto.
  dand ; subst.
  eapply Tassign_n. eapply TWk in TassignExpr.
  apply TassignExpr.
  apply Typ.order_refl.
  auto.
  eapply TEnv.order_trans;eauto.
Qed.

Lemma typeof_stmt_n_weak_write :
  forall n
         (D : var -> Typ.t)
         (a : option var)
         (k : Typ.t)
         (G G' G2: TEnv.t)
         (x : var)
         (e1 e2 : expr)
         (r g : BSet.t)
         (s' : Annot.t stmt)
         (H0 : annot_order stmt_order (Annot.mk r (Swrite x e1 e2) g) s')
         (t1 : Typ.t)
         (r1 : BSet.t)
         (t2 : Typ.t)
         (r2 : BSet.t)
         (TwriteE1 : typeof_expr_wk D G e1 t1 r1)
         (TwriteE2 : typeof_expr_wk D G e2 t2 r2)
         (TwriteNOTH : t1 <> Typ.H)
         (TwriteO : Typ.order ((t1 ⊔ t2) ⊔ Typ.guard k Typ.H) (D x) = true)
         (TwriteLk : BSet.subset ((r1 ∪ r2) ∪ Typ.to_set t1) r = true)
         (TwriteWk : TEnv.order G G')
         (ORD : TEnv.order G' G2)
  ,
  typeof_stmt_n D a k G s' G2 n.
Proof.
  intros.
  unfold annot_order in H0.
  simpl in *. destruct s'; simpl in *.
  destruct elt; simpl in * ; try tauto.
  dand ; subst.
  eapply Twrite_n.
  eauto.
  eauto.
  auto.
  auto.
  subset.
  eapply TEnv.order_trans;eauto.
Qed.

Lemma weaken_trans : forall s1 G1 s2 G2 s3 G3,
    weaken s1 G1 s2 G2 ->
    weaken s2 G2 s3 G3 ->
    weaken s1 G1 s3 G3.
Proof.
  intros.
  destruct H. destruct H0.
  constructor.
  eapply TEnv.order_trans;eauto.
  eapply annot_order_trans;eauto.
Qed.

Lemma weaken_refl : forall s1 G1,
    weaken s1 G1 s1 G1.
Proof.
  intros.
  constructor.
  eapply TEnv.order_refl;eauto.
  eapply annot_stmt_order_refl;eauto.
Qed.


Lemma typeof_stmt_n_weak : forall n D a k G s G',
    typeof_stmt_n D a k G s G' n ->
    forall s', annot_order stmt_order s s' ->
               forall G2, TEnv.order G' G2  ->
               typeof_stmt_n D a k G s' G2 n.
Proof.
  induction n.
  - intros.
    inv H.
    + eapply typeof_stmt_n_weak_skip; eauto.
    + eapply typeof_stmt_n_weak_assign; eauto.
    + eapply typeof_stmt_n_weak_write; eauto.
  - intros. inv H.
    + eapply typeof_stmt_n_weak_skip; eauto.
    + eapply typeof_stmt_n_weak_assign; eauto.
    + eapply typeof_stmt_n_weak_write; eauto.
    + eapply Tseq_n.
      apply Tseq1.
      eapply Tseq2.
      eapply weaken_trans;eauto.
      constructor ; auto.
    + eapply Tif_n.
      eapply TifE.
      reflexivity.
      eapply TifThen.
      eapply TifElse.
      eapply TifNext.
      eapply weaken_trans;eauto.
      constructor ; auto.
(*    + eapply TifL_n.
      eauto. reflexivity.
      eauto. auto.
      eauto.
      eapply weaken_trans;eauto.
      constructor ; auto. *)
    + eapply Tfor_n.
      eauto. eauto.
      auto. auto. auto.
      eapply weaken_trans;eauto.
      constructor ; auto.
Qed.

Lemma typeof_stmt_n_stmt : forall D a k G s G',
    typeof_stmt D a k G s G' ->
    exists n, typeof_stmt_n D a k G s G' n.
Proof.
  intros.
  induction H.
  - exists 0. eapply Tskip_n. apply TEnv.order_refl.
  - exists 0. apply typeof_expr_expr_wk in H.
    eapply Tassign_n. eauto.
    apply TEnv.order_refl.
  - subst.  exists 0.
    apply typeof_expr_expr_wk in H.
    apply typeof_expr_expr_wk in H0.
    eapply Twrite_n.
    apply H. apply H0. auto.
    auto. apply BSet.subset_refl. apply TEnv.order_refl.
  - destruct IHtypeof_stmt1 as (n1 & T1).
    destruct IHtypeof_stmt2 as (n2 & T2).
    apply typeof_stmt_incr_n with (n':= max n1 n2) in T1 ; try lia.
    apply typeof_stmt_incr_n with (n':= max n1 n2) in T2 ; try lia.
    exists (S (max n1 n2)).
    econstructor ;eauto.
    constructor.
    apply TEnv.order_refl.
    apply annot_stmt_order_refl.
  - apply typeof_expr_expr_wk in H.
    destruct IHtypeof_stmt1 as (n1 & T1).
    destruct IHtypeof_stmt2 as (n2 & T2).
    destruct IHtypeof_stmt3 as (n3 & T3).
    apply typeof_stmt_incr_n with (n':= max n1 (max n2 n3)) in T1 ; try lia.
    apply typeof_stmt_incr_n with (n':= max n1 (max n2 n3)) in T2 ; try lia.
    apply typeof_stmt_incr_n with (n':= max n1 (max n2 n3)) in T3 ; try lia.
    subst.
    exists (S (max n1 (max n2 n3))).
    eapply Tif_n.
    eauto.
    reflexivity.
    eapply T1. eapply T2. eapply T3.
    constructor.
    apply TEnv.order_refl.
    apply annot_stmt_order_refl.
(*  - destruct IHtypeof_stmt as (n1 & T1).
    exists (S n1).
    eapply TifL_n;eauto.
    constructor.
    apply TEnv.order_refl.
    apply annot_stmt_order_refl. *)
  - destruct IHtypeof_stmt as (n1 & T1).
    exists (S n1).
    eapply Tfor_n with (ar:=s).
    eapply typeof_stmt_n_weak.
    apply T1.
    apply annot_stmt_order_refl.
    auto. auto.
    auto.
    apply weaken_refl.
Qed.



Fixpoint strip (s:stmt) :=
  match s with
  | Sseq s1 s2 => Sseq (Annot.from (strip (Annot.elt s1))) (Annot.from (strip (Annot.elt s2)))
  | Sif p e s1 s2 s3 => Sif p e (Annot.from (strip (Annot.elt s1))) (Annot.from (strip (Annot.elt s2))) (Annot.from (strip (Annot.elt s3)))
  | Sfor x c1 c2 s => Sfor x c1 c2 (Annot.from (strip (Annot.elt s)))
  | _ => s
  end.


Definition astrip (s: Annot.t stmt) := Annot.from (strip (Annot.elt s)).


Fixpoint strip_stm_join (s1 : stmt) : forall s2 sr,
    stmt_join s1 s2 = Some sr ->
    strip s1 = strip s2 /\ strip s2 = strip sr.
Proof.
  assert (A : forall a1 a2 ar, annot_join stmt_join a1 a2 = Some ar ->
            astrip a1 = astrip a2 /\ astrip a2 = astrip ar).
  {
    intros. destruct a1,a2,ar.
    unfold annot_join,astrip in *; simpl in *.
    destruct (stmt_join elt elt0) eqn:J; try discriminate.
    inv H. apply strip_stm_join in J. intuition congruence.
  }
  destruct s1; intros.
  - destruct s2 ; try discriminate.
    inv H. simpl; tauto.
  - destruct s2 ; try discriminate.
    simpl in H.
    destruct (of_sb (Pos.eq_dec x x0) && of_sb (expr_eq_dec e e0)) eqn:EQ;
      try discriminate.
    inv H.
    destruct (Pos.eq_dec x x0); destruct (expr_eq_dec e e0);
      simpl in EQ; try discriminate; simpl ; split_and;congruence.
  - destruct s2 ; try discriminate.
    simpl in H.
    destruct (of_sb (Pos.eq_dec x x0) && of_sb (expr_eq_dec e1 e0) &&
        of_sb (expr_eq_dec e2 e3)) eqn:EQ; try discriminate.
    inv H.
    destruct (Pos.eq_dec x x0);
      destruct (expr_eq_dec e1 e0) ;
      destruct (expr_eq_dec e2 e3); try discriminate.
    simpl ; split_and ; congruence.
  - destruct s0 ; try discriminate.
    inv H.
    destruct (annot_join stmt_join s1 s0) eqn:AJ1; try discriminate.
    destruct (annot_join stmt_join s2 s3) eqn:AJ2; try discriminate.
    inv H1.
    simpl.
    apply A in AJ1.
    apply A in AJ2.
    unfold astrip in AJ1,AJ2.
    intuition congruence.
  - destruct s0 ; try discriminate.
    inv H.
    destruct (of_sb (Pos.eq_dec t t0) && of_sb (expr_eq_dec e e0))
      eqn : EQ ; try discriminate.
    destruct (annot_join stmt_join s1 s0) eqn:AJ1; try discriminate.
    destruct (annot_join stmt_join s2 s4) eqn:AJ2; try discriminate.
    destruct (annot_join stmt_join s3 s5) eqn:AJ3; try discriminate.
    inv H1.
    simpl.
    apply A in AJ1.
    apply A in AJ2.
    apply A in AJ3.
    destruct (Pos.eq_dec t t0); destruct (expr_eq_dec e e0); try discriminate.
    unfold astrip in AJ1,AJ2,AJ3.
    intuition congruence.
  - destruct s2 ; try discriminate.
    inv H.
    destruct (of_sb (Pos.eq_dec x x0) && of_sb (Nat.eq_dec c1 c0) &&
                of_sb (Nat.eq_dec c2 c3)) eqn:EQ; try discriminate.
    destruct (annot_join stmt_join s s0) eqn:AJ1; try discriminate.
    inv H1.
    apply A in AJ1.
    destruct (Pos.eq_dec x x0); destruct (Nat.eq_dec c1 c0);
      destruct (Nat.eq_dec c2 c3); try discriminate.
    unfold astrip in AJ1.
    simpl.
    intuition congruence.
Qed.

Lemma astrip_stm_join : forall a1 a2 ar, annot_join stmt_join a1 a2 = Some ar ->
            astrip a1 = astrip a2 /\ astrip a2 = astrip ar.
Proof.
  intros. destruct a1,a2,ar.
  unfold annot_join,astrip in *; simpl in *.
  destruct (stmt_join elt elt0) eqn:J; try discriminate.
  inv H. apply strip_stm_join in J. intuition congruence.
Qed.

Fixpoint strip_stm_join_ex (s1:stmt): forall s2,
    strip s1 = strip s2  ->
    exists sr', stmt_join s1 s2 = Some sr' /\ strip sr' = strip s1.
Proof.
  assert (A : forall a1 a2 ,
             strip (Annot.elt a1) = strip (Annot.elt a2)  ->
             exists ar', annot_join stmt_join a1 a2 = Some ar' /\
                           strip (Annot.elt ar') = strip (Annot.elt a1)).
  {
    unfold astrip,annot_join.
    destruct a1,a2; simpl.
    intros.
    unfold Annot.from in H. inv H.
    apply strip_stm_join_ex in H1.
    destruct H1. dand.
    rewrite H0. eexists;split.
    reflexivity. simpl. congruence.
  }
  destruct s1; simpl.
  -  destruct s2 ; simpl in * ; try congruence.
     intros. exists Sskip. split; auto.
  -  destruct s2 ; simpl in * ; try congruence.
     intros. inv H.
     destruct (Pos.eq_dec x0 x0); try congruence.
     destruct (expr_eq_dec e0 e0); try congruence.
     eexists. split. reflexivity. simpl; congruence.
  -  destruct s2 ; simpl in * ; try congruence.
     intros. inv H.
     destruct (Pos.eq_dec x0 x0); try congruence.
     destruct (expr_eq_dec e0 e0); try congruence.
     destruct (expr_eq_dec e3 e3); try congruence.
     eexists. split. reflexivity. reflexivity.
  -  destruct s0 ; simpl in * ; try congruence.
     intros. inv H.
     apply A in H1.
     apply A in H2.
     destruct H1; destruct H2 ; dand.
     rewrite H0. rewrite H1.
     eexists.
     split. reflexivity. simpl.
     congruence.
  -  destruct s0 ; simpl in * ; try congruence.
     intros. inv H.
     apply A in H3.
     apply A in H4.
     apply A in H5.
     destruct H3; destruct H4;destruct H5 ; dand.
     destruct (Pos.eq_dec t0 t0); try congruence.
     destruct (expr_eq_dec e0 e0); try congruence.
     simpl.
     rewrite H0. rewrite H1. rewrite H2.
     eexists. split. reflexivity. simpl. congruence.
  - destruct s2 ; simpl in * ; try congruence.
     intros. inv H.
     apply A in H4.
     destruct H4 ; dand.
     destruct (Pos.eq_dec x0 x0); try congruence.
     destruct (Nat.eq_dec c0 c0); try congruence.
     destruct (Nat.eq_dec c3 c3); try congruence.
     simpl.
     rewrite H0.
     eexists. split. reflexivity. simpl; congruence.
Qed.


Lemma Tstmt_eq : forall D a k G1 r s g G1' G2',
    typeof_stmt D  a k
                G1 (Annot.mk r s g)  G2' ->
    G1' = G2' ->
    typeof_stmt D  a k
                G1 (Annot.mk r s g)  G1'.
Proof.
  intros.
  subst.
  auto.
Qed.

Fixpoint bvars (p:stmt) :=
  match p with
  | Sskip => BSet.empty
  | Sassign y e => BSet.union (BSet.singleton y) (bvars_of_expr e)
  | Swrite y e1 e2 => BSet.union (bvars_of_expr e1) (bvars_of_expr e2)
  | Sseq s1 s2 => BSet.union (bvars (Annot.elt s1)) (bvars (Annot.elt s2))
  | Sif _ e s1 s2 s3 => BSet.union (bvars_of_expr e) (BSet.union (bvars (Annot.elt s1))
                                                        (BSet.union (bvars (Annot.elt s2)) (bvars (Annot.elt s3))))
  | Sfor y _ _ s   => BSet.union (BSet.singleton y) (bvars (Annot.elt s))
  end.

Notation " D , A , Li , K  |- G1 {{ s }} G2" :=
  (typeof_stmt D A Li K G1 s G2) (at level 300).

Notation "P ∪ ( x , v )" := (PositiveMap.add x v P ) (at level 300).


Lemma seq_inv : forall D  a k G s1  s2  r g G',
    typeof_stmt D  a  k G (Annot.mk r (Sseq s1 s2) g) G' ->
    exists G1,
      typeof_stmt D  a k G s1 G1 /\
        typeof_stmt D  a k G1 s2 G' /\
        r = BSet.union (Annot.leaked s1) (Annot.leaked s2) /\
        g = BSet.union (Annot.high s1) (Annot.high s2).
Proof.
  intros.
  inv H.
  eexists ; split_and ; eauto.
Qed.

Lemma sif_inv : forall a D p e k G r g s1 s2 s3 G',
    typeof_stmt D a k G (Annot.mk r (Sif p e s1 s2 s3) g) G' ->
    (exists G1 G2 G3 k' t1 rc,
        (G' = if isPP a p && negb (Typ.is_L t1)
               then TEnv.classify p G3
               else G3)
        /\
        typeof_expr D G e t1 rc /\
        typeof_stmt D  a k' G s1 G1 /\
        typeof_stmt D  a k' G s2 G2 /\
        typeof_stmt D  a k (TEnv.join G1 G2) s3 G3 /\
        r = BSet.union (Annot.leaked s1) (BSet.union (Annot.leaked s2)
                                                       (BSet.union (Annot.leaked s3) rc))  /\
        g = BSet.union (Annot.high s1) (BSet.union (Annot.high s2)
                                                     (BSet.union (Annot.high s3) (Typ.to_set (Typ.guard t1 (Typ.singleton p))))) /\
        k' = Typ.join k (Typ.guard t1 (Typ.singleton p))).
Proof.
  intros.
  inv H.
  do 6 eexists; split_and;eauto.
Qed.

Lemma sfor_inv : forall D  a k G G' x c1 c2 s r g,
    typeof_stmt D a k G (Annot.mk r (Sfor x c1 c2 s) g)  G' ->
    r = Annot.leaked s /\
      g = Annot.high s  /\
      exists G1 Go,
        typeof_stmt D  a  k (TEnv.set x k  Go) s  G1 /\
          TEnv.order G1 Go  /\
          TEnv.order G Go  /\
          TEnv.classify_set (Ca s) Go = Go /\
          G' = Go.
Proof.
  intros.
  inv H. split_and;auto.
  do 2 eexists;split_and; eauto.
Qed.


Lemma typeof_stmt_order (s:stmt) :
  forall D a k k' G2 G1 r g G1'
         (TYP : typeof_stmt D a k G1 (Annot.mk r s g) G1')
         (ORD : TEnv.order G2 G1)
         (ORDK: Typ.order k' k = true),
    exists G2' s',
      typeof_stmt D a k' G2 s' G2' /\
        TEnv.order G2' G1' /\
        annot_order stmt_order s' (Annot.mk r s g).
Proof.
  pattern s.
  apply stmt_sub.
  - intros. inv TYP.
    do 3 eexists; split_and.
    eapply Tskip.
    + auto.
    + unfold annot_order. simpl.
      tauto.
  - intros. inv TYP.
    eapply typeof_expr_order in H7; eauto.
    destruct H7 as (t1 & r1 & TE & O1 & S1).
    do 3 eexists; split_and.
    eapply Tassign. eauto.
    apply TEnv.order_set; auto.
    apply Typ.order_join2;auto.
    unfold annot_order; simpl.
    tauto.
  - intros.
    inv TYP.
    eapply typeof_expr_order in H7; eauto.
    destruct H7 as (t1' & r1' & TE1 & OE1 & SE1).
    eapply typeof_expr_order in H9; eauto.
    destruct H9 as (t2' & r2' & TE2 & OE2 & SE2).
    do 3 eexists; split_and.
    eapply Twrite with (x:=x).
    eapply TE1. eapply TE2.
    intro ; subst.
    rewrite Typ.order_H_inv  in OE1. congruence.
    eapply Typ.order_trans.
    apply Typ.order_join2.
    apply Typ.order_join2. eauto.
    eauto. apply Typ.order_guard.
    eauto.
    apply Typ.order_refl.
    auto.
    reflexivity.
    auto.
    unfold annot_order; simpl.
    split_and; auto.
    apply Typ.to_set_subset  in OE1; auto.
    subset.
  - intros.
    apply seq_inv in TYP.
    destruct TYP as (G2' & T1 & T2 & RR & GG).
    subst.
    destruct s1 as [r1 s1 g1].
    apply IHs1 with (k':=k') (G2:=G2) in T1; auto.
    destruct T1 as (G22 & s1' & T1 & O1 & S1).
    destruct s2 as [r2 s2 g2].
    apply IHs2 with (k':=k') (G2:=G22) in T2; auto.
    destruct T2 as (Gf & s2' & T2 & O2 & S3).
    destruct s1' as [r1' s1' g1'].
    destruct s2' as [r2' s2' g2'].
    do 3 eexists; split_and.
    eapply Tseq.
    eapply T1.
    eapply T2.
    auto.
    unfold annot_order; simpl.
    unfold annot_order in S1,S3.
    simpl in *.
    intuition idtac.
    + apply BSet.subset_union2;auto.
    + unfold annot_order; simpl; tauto.
    + unfold annot_order; simpl; tauto.
    + apply BSet.subset_union2;auto.
    + apply stmt_order_refl.
    + apply stmt_order_refl.
  - intros.
    apply sif_inv in TYP.
    destruct TYP as (G2' & G3 & G4 & K' & t1 & rc & EQ1 & TE & T1 & T2 & T3 & RR & GG & K ).
      subst.
      eapply typeof_expr_order in TE; eauto.
      destruct TE as (t1' & r1' & TE & O1 & S1).
      destruct s1 as [r1 s1 g1].
      apply IHs1 with (k':= (k' ⊔ Typ.guard t1' (Typ.singleton p))) (G2:= G2) in T1.
      destruct T1 as (G22 & s' & T1 & O1' & OS1).
      destruct s' as [rr1' s1' gg1'].
      destruct s2 as [r2 s2 g2].
      apply IHs2 with (k':= (k' ⊔ Typ.guard t1' (Typ.singleton p))) (G2:= G2) in T2.
      destruct T2 as (G22' & s2' & T2 & O2 & OS2).
      destruct s2' as [rr2' s2' gg2'].
      destruct s3 as [r3 s3 g3].
      apply IHs3 with (k':= k') (G2:= (TEnv.join G22 G22')) in T3.
      destruct T3 as (GR & s3' & T3 & O3 & OS3).
      destruct s3' as [rr3' s3' gg3'].
      unfold annot_order in OS1, OS2, OS3.
      simpl in OS1, OS2, OS3.
      intuition idtac.
      do 2 eexists ; split_and.
      eapply Tif.
      eauto.
      reflexivity.
      eapply T1.
      eapply T2.
      eapply T3.
      reflexivity.
      reflexivity.
      apply order_isPP;auto.
      unfold annot_order. simpl.
      split_and; auto.
      repeat apply BSet.subset_union2; auto.
      unfold annot_order; simpl.
      split_and;auto.
      unfold annot_order; simpl.
      split_and;auto.
      unfold annot_order; simpl.
      split_and;auto.
      repeat apply BSet.subset_union2; auto.
      apply Typ.to_set_subset.
      intros.
      apply Typ.guard_singleton_not_H in H4. tauto.
      apply Typ.order_guard; auto.
      apply Typ.order_refl.
      apply stmt_order_refl.
      apply TEnv.join_lub;auto.
      eapply TEnv.order_trans;eauto.
      apply TEnv.join_ub_l.
      rewrite TEnv.join_sym.
      eapply TEnv.order_trans;eauto.
      apply TEnv.join_ub_l.
      auto.
      apply stmt_order_refl.
      auto.
      apply Typ.order_join2;auto.
      apply Typ.order_guard; auto.
      apply Typ.order_refl.
      apply stmt_order_refl.
      auto.
      apply Typ.order_join2;auto.
      apply Typ.order_guard; auto.
      apply Typ.order_refl.
(*    + destruct TYP as (x & b & c & F & E & MEM & TL & TYP & S1 & S2 & GS1 & LS1).
      subst.
      destruct b.
      {
        destruct s1 as [r1 s1 g1].
        apply (IHs1 _ (stmt_order_refl _)) with (k':= k') (2:= ORD) in TYP .
        destruct TYP as (G2' & s' & T1 & O1' & A1').
        destruct s' as [r' s' g'].
        do 2 eexists; split_and.
        - eapply TifL. eauto.
          reflexivity.
          simpl in A1'.
          inv A1'. inv H0.
          simpl in H2.
          eapply BSet.subset_mem_f; eauto.
          specialize (ORD x).
          rewrite TL in ORD.
          eapply Typ.order_is_L_r; eauto.
          simpl.  eauto.
        - auto.
        - unfold annot_order in *; simpl in *.
          intuition eauto.
          unfold annot_order. simpl.
          tauto.
          apply annot_order_skip.
          apply annot_order_skip.
        - auto.
      }
      {
        inv TYP.
        destruct  s1 as [r1 s1 g1].
        do 2 eexists; split_and.
        - eapply TifL. eauto.
          reflexivity.
          eauto.
          specialize (ORD x).
          rewrite TL in ORD.
          eapply Typ.order_is_L_r; eauto.
          simpl. eapply Tskip.
        - auto.
        - unfold annot_order in *; simpl in *.
          split_and.
          apply BSet.subset_refl.
          reflexivity.
          reflexivity.
          unfold annot_order; auto.
          split_and; auto.
          simpl. apply BSet.subset_refl.
          apply annot_stmt_order_refl.
          apply BSet.subset_refl.
          apply annot_order_skip.
          apply annot_order_skip.
          apply BSet.subset_refl.
      } *)
  - intros.
    apply sfor_inv in TYP.
    destruct TYP as (RR & GG & (G2' & Go & T1 & O1 & O2 & C1 & T2)).
    simpl in RR.
    simpl in GG. subst.
    apply IHfor with (k':=k') (G2:=TEnv.set x k' Go) in T1.
    destruct T1 as (GT & s' & TF & ORDT & ORDS).
    subst.
    do 2 eexists.
    split_and.
    eapply Tfor.
    eapply TF.
    eapply TEnv.order_trans;eauto.
    eapply TEnv.order_trans;eauto.
    {
      eapply TEnv.classify_set_fix_subset. eauto.
      apply annot_stmt_order_Ca in ORDS.
      rewrite ORDS.
      apply BSet.subset_refl.
 }
    (*
      rewrite <- C1 at 2.
    f_equal.
    apply annot_stmt_order_Ca; auto. *)
    apply TEnv.order_refl.
    unfold annot_order; simpl;split_and;auto.
    unfold annot_order in ORDS; simpl in ORDS; dand;auto.
    unfold annot_order in ORDS; simpl in ORDS; dand;auto.
    apply stmt_order_refl.
    apply TEnv.order_set;auto.
    apply TEnv.order_refl.
    auto.
Qed.

Fixpoint unmod (p : stmt) (x:var)  :=
  match p with
  | Sskip => true
  | Sassign y e => negb (of_sb (Pos.eq_dec x y))
  | Swrite y e1 e2 => negb (of_sb (Pos.eq_dec x y))
  | Sseq s1 s2 => unmod (Annot.elt s1) x && unmod (Annot.elt s2) x
  | Sif _ _ s1 s2 s3 => unmod (Annot.elt s1) x &&
                          unmod (Annot.elt s2) x &&
                          unmod (Annot.elt s3) x
  | Sfor y _ _ s   => negb (of_sb (Pos.eq_dec x y)) && unmod (Annot.elt s) x
  end.
