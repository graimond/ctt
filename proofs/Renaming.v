Require Import Bool Lia ZArith .
Require Import FMapPositive.
Require Import Std NSet.
Require Import NMap.
Require Typ.

Notation var := positive.


Definition same_dom {A: Type} (e1 e2:@Map.t A) := forall x, Map.dom e1 x <-> Map.dom e2 x.

Definition wf_rename (rho: @Map.t var)  :=
  forall x1 x2 x', Map.find x1 rho = Some x' ->
                   Map.find x2 rho = Some x' -> x1 = x2.


Lemma wf_rename_list : forall rho,
    wf_rename rho ->
    forall x1 x2 x',
      List.In (x1,x') (Map.elements rho) ->
      List.In (x2,x') (Map.elements rho) ->
      x1 = x2.
Proof.
  intros.
  unfold wf_rename in H.
  eapply H with (x' := x').
  apply Map.find_elements;auto.
  apply Map.find_elements;auto.
Qed.

Definition rename_opt {A: Type}  (rho : @Map.t var) (m : @Map.t A) :=
  Map.map_option (fun x => Map.find x m) (Map.invert rho).

Definition rename {A: Type} (def: A) (rho : @Map.t var) (m : @Map.t A) :=
  Map.map (fun x => Map.get def x m) (Map.invert rho).

Lemma dom_rename : forall {A: Type} (def:A) (rho : @Map.t var) (m : @Map.t A),
  forall x, Map.dom (rename def rho m) x <-> Map.codom rho x.
Proof.
  unfold rename.
  intros.
  rewrite Map.dom_map.
  rewrite Map.dom_invert.
  tauto.
Qed.

Definition ojoin (o1 o2 : option var) :=
  match o1, o2 with
  | None , None => None
  | Some v1 , None | None , Some v1 => Some v1
  | Some v1 , Some v2 => Some v1
  end.

Definition join (rho1 rho2 : @Map.t var) : @Map.t var :=
  Map.map2 ojoin rho1 rho2.

Definition rename_var (rho : @Map.t positive) (x:positive) : positive :=
  match Map.find x rho with
  | None => x
  | Some x' => x'
  end.

(** comb [x -> x1] [x -> x2] = [x1 -> x2] i.e x2 <- x1 i.e x2 := x1 *)
Definition comb (rho1 rho2 : @Map.t var) : @Map.t var :=
  Map.map (fun  x => rename_var rho2 x) (Map.invert rho1).


Definition copy_vars (fr: positive) (s : BSet.t) : var * @Map.t var :=
  BSet.fold (fun x '(fr,m) => (Pos.succ fr, Map.add x fr m)) s (fr, Map.empty).

Lemma find_rename_opt_var :
  forall {A: Type} x rho (m: @Map.t A) r
         (WF: wf_rename rho)
         (DOM : Map.dom rho x),
    Map.find x m = Some r ->
    Map.find (rename_var rho x) (rename_opt rho m) = Some r.
Proof.
  unfold rename_var.
  intros.
  unfold rename_opt.
  destruct (Map.find x rho) eqn:FD.
  - rewrite Map.find_map_option.
    specialize (Map.find_invert p rho).
    destruct (Map.find p (Map.invert rho)).
    + simpl. intros.
      assert (p0 = x).
      { eapply WF;eauto.  }
      congruence.
    + simpl. intro. specialize (H0 x). congruence.
  - simpl.
    unfold Map.dom in DOM. congruence.
Qed.



Lemma same_dom_rename : forall {A: Type} (d:A) (rho: @Map.t var) (e1 e2 : @Map.t A),
  same_dom (rename d rho e1) (rename d rho e2).
Proof.
  unfold same_dom,Map.dom.
  unfold rename.
  intros.
  rewrite ! Map.find_map.
  specialize (Map.find_invert x rho).
  destruct (Map.find x (Map.invert rho)).
  - simpl. intuition congruence.
  - simpl. tauto.
Qed.

Record wf (V : BSet.t) (rho : @Map.t var) : Type :=
  {
    wf_dom : forall x, BSet.mem x V = true -> Map.dom rho x;
    wf_prf  : wf_rename rho
  }.

Lemma wf_or : forall P Q rho,
    wf (BSet.union P  Q) rho ->
    wf P rho /\ wf Q rho.
Proof.
  intros.
  destruct H ; split ; constructor ;auto.
  - intros. apply wf_dom0.
  rewrite BSet.mem_union_iff. rewrite orb_true_iff. tauto.
  - intros. apply wf_dom0.
  rewrite BSet.mem_union_iff. rewrite orb_true_iff. tauto.
Qed.


Lemma find_rename_correct :
  forall {A: Type} rho (d:A) e x x'
         (WF : wf_rename rho),
    Map.find x rho = Some x' ->
    Map.find x' (rename d rho e) = Some (Map.get d x e).
Proof.
  unfold rename.
  intros.
  rewrite Map.find_map.
  generalize (Map.find_invert x' rho).
  destruct (Map.find x' (Map.invert rho)) eqn:FINV.
  - simpl.
    intros.
    f_equal.
    f_equal.
    f_equal.
    unfold wf_rename in WF.
    eapply WF; eauto.
  - simpl.
    intros.
    specialize (H0 x). congruence.
Qed.

Lemma rename_correct :
  forall {A: Type} (d:A) rho e x x'
         (WF : wf_rename rho),
    Map.find x rho = Some x' ->
    Map.get d x' (rename d rho e) = Map.get d x e.
Proof.
  intros.
  unfold Map.get at 1.
  erewrite find_rename_correct;eauto.
  reflexivity.
Qed.

Lemma rename_set :
  forall {A: Type} rho (G:@Map.t A) x  x' ti
         (WF : wf_rename rho)
  ,
    Map.find x rho = Some x' ->
    rename_opt rho (Map.add x ti G) = Map.add x' ti (rename_opt rho G).
Proof.
  intros.
  apply Map.uniq.
  intros.
  unfold rename_opt.
  rewrite Map.find_map_option.
  rewrite Map.find_add.
  rewrite Map.find_map_option.
  generalize (Map.find_invert x0 rho).
  destruct (Map.find x0 (Map.invert rho)) eqn:FIND;auto.
  + destruct (Pos.eq_dec x0 x'); subst;auto.
    simpl.
    destruct (Pos.eq_dec p x); auto.
    subst. rewrite Map.gss. reflexivity.
    intros.
    exfalso. apply n.
    eapply WF; eauto.
    simpl.
    rewrite Map.find_add.
    destruct (Pos.eq_dec p x); auto.
    subst.
    congruence.
    + intros.
      simpl. destruct (Pos.eq_dec x0 x'); auto.
      subst.
      exfalso. apply (H0 x). auto.
Qed.
