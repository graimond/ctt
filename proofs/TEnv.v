Require Import Bool Lia ZArith .
Require Import FMapPositive.
Require Import Std NSet.
Require Import NMap Renaming.
Require Typ.

Notation var := positive.
Notation PP := positive.

Definition t := @Map.t Typ.t.

Definition get := Map.get Typ.L.


Definition is_fresh (p:PP) (env:t) :=
  forall x, Typ.is_fresh p (get x env) = true.


Definition set (x:var) (v: Typ.t) (env:t) := Map.add x v env.


(*Lemma set_same : forall x G,
    set x (get x G) G = G.
Proof.
  unfold get,set, Map.get.
  intros.
  destruct (Map.find x G) eqn:FIND.
  - simpl.
    apply Map.uniq.
    intros.
    rewrite Map.find_add.
    destruct (Pos.eq_dec x0 x);auto.
    congruence.
  - simpl. reflexivity.
Qed.
 *)

Lemma set_twice : forall x v  G,
    set x v (set x v G) = set x v G.
Proof.
  unfold set.
  intros.
  apply Map.uniq.
  intros.
  rewrite ! Map.find_add.
  destruct (Pos.eq_dec x0 x); auto.
Qed.

Lemma gso : forall x x' v e,
    x <> x' ->
    get x (set x' v e) = get x e.
Proof.
  unfold get,Map.get,set.
  intros.
  rewrite Map.gso by assumption.
  reflexivity.
Qed.

Lemma gss : forall x v e,
    get x (set x v e) = v.
Proof.
  unfold get,Map.get,set.
  intros.
  rewrite Map.gss.
  reflexivity.
Qed.


Lemma get_set : forall x x' v e,
    get x (set x' v e) = if Pos.eq_dec x x' then v else get x e.
Proof.
  unfold get,Map.get,set.
  intros. rewrite Map.find_add.
  destruct (Pos.eq_dec x x');auto.
Qed.





Definition order (e1 e2:t) :=
  forall x, Typ.order (get x e1) (get x e2) = true.


Definition option_order (o1 o2 : option Typ.t) :=
  Typ.order (Typ.of_option o1) (Typ.of_option o2).

Definition pp_in (p: PP) (G: t):=
  exists x, Typ.pp_in p (get x G).
  

Definition classify (p: PP) (e:t) :=   Map.map (Typ.classify p) e.

Lemma pp_in_classify:
  forall p G,
  ~ pp_in p G -> classify p G = G.
Proof.
  unfold classify, pp_in.
  intros.
  rewrite not_exists_forall in H.
  apply Map.uniq.
  intros.
  rewrite Map.find_map.
  specialize (H x).
  unfold get,Map.get in H. destruct (Map.find x G); simpl in *;auto.
  rewrite Typ.classify_id;auto.
Qed.

Lemma get_map :
  forall x f e
         (STRICT : Typ.L = f (Typ.L)),
    get x (Map.map f e) = f (get x e).
Proof.
  unfold get,Map.get.
  intros.
  rewrite Map.find_map.
  destruct (Map.find x e) eqn:FIND.
  simpl; auto.
  simpl. apply STRICT.
Qed.

Lemma get_classify : forall x p e,
    get x (classify p e) = Typ.classify p (get x e).
Proof.
  intros.
  apply get_map.
  reflexivity.
Qed.

Lemma order_classify : forall e p,
    order e  (classify p e).
Proof.
  unfold order.
  intros.
  rewrite get_classify.
  apply Typ.order_classify.
Qed.

Definition classify_set (p: BSet.t) (e:t) :=
  Map.map (fun v => Typ.classify_set p v) e.

Lemma get_classify_set : forall x p e,
    get x (classify_set p e) = Typ.classify_set p (get x e).
Proof.
  intros.
  apply get_map.
  rewrite Typ.classify_set_L.
  reflexivity.
Qed.


Lemma set_classify : forall x p G t,
    classify p (set x t G) = set x (Typ.classify p t) (classify p G).
Proof.
  intros.
  apply Map.uniq.
  intros.
  unfold classify,set.
  rewrite! Map.find_map.
  rewrite Map.find_add.
  destruct (Pos.eq_dec x0 x); subst.
  + rewrite Map.gss. reflexivity.
  + rewrite Map.gso by auto.
    rewrite !Map.find_map.
    reflexivity.
Qed.

Lemma classify_mono: forall e1 e2 p,
    order e1 e2 ->
    order (classify p e1) (classify p e2).
Proof.
  unfold order; intros.
  rewrite! get_classify.
  apply Typ.classify_mono.
  apply H.
Qed.

Lemma classify_set_mono2 : forall e1 e2 s,
    order e1 e2 ->
    order (classify_set s e1) (classify_set s e2).
Proof.
  unfold order; intros.
  rewrite! get_classify_set.
  apply Typ.classify_set_mono2.
  apply H.
Qed.


Lemma classify_set_mono1 : forall e s1 s2,
    BSet.subset s1 s2 = true ->
    order (classify_set s1 e) (classify_set s2 e).
Proof.
  unfold order; intros.
  rewrite! get_classify_set.
  apply Typ.classify_set_mono1;auto.
Qed.


Lemma order_classify_set : forall e s,
    order e (classify_set s e).
Proof.
  unfold order; intros.
  rewrite! get_classify_set.
  apply Typ.order_classify_set.
Qed.

Lemma classify_set_empty : forall e,
    classify_set BSet.empty e = e.
Proof.
  unfold classify_set.
  intros.
  apply Map.uniq.
  intros. rewrite Map.find_map.
  destruct (Map.find x e); auto.
Qed.

Lemma order_refl : forall (e:t), order e e.
Proof.
  unfold order;auto.
  intros. apply Typ.order_refl.
Qed.


Lemma order_trans : forall (e1 e2 e3:t),
    order e1 e2 -> order e2 e3 -> order e1 e3.
Proof.
  unfold order;intuition.
  eapply Typ.order_trans;eauto.
Qed.


Lemma order_if_classify : forall (b:bool) p e ,
    order e  (if b then (classify p e) else e).
Proof.
  destruct b.
  intros.
  apply order_classify.
  intro. apply order_refl.
Qed.

Lemma order_if_2 : forall (b:bool) e1 e2 e1' e2',
    order e1 e1' -> order e2 e2' ->
    order (if b then e1 else e2)  (if b then e1' else e2').
Proof.
  destruct b; auto.
Qed.


Lemma set_swap : forall x y v1 v2 e,
    x <> y ->
    set x v1 (set y v2 e) = set y v2 (set x v1 e).
Proof.
  unfold set.
  intros.
  apply Map.add_swap; auto.
Qed.


Lemma order_set : forall e1 e2 x t1 t2,
    order e1 e2 ->
    Typ.order t1 t2 = true->
    order (set x t1 e1) (set x t2 e2).
Proof.
  unfold order ; intros.
  destruct (Pos.eq_dec x x0); subst.
  rewrite ! gss. auto.
  rewrite ! gso by auto.
  apply H.
Qed.


Lemma classify_set_same : forall g e,
    classify_set g (classify_set g e) = classify_set g e.
Proof.
  unfold classify_set.
  intros.
  apply Map.uniq.
  intros.
  rewrite! Map.find_map.
  destruct (Map.find x e); simpl;auto.
  rewrite Typ.classify_set_g1_g2.
  rewrite BSet.union_idem. reflexivity.
Qed.



Lemma set_classify_set : forall g x v e
                                (CLAS : Typ.classify_set g v = v),
    set x v (classify_set g e) = classify_set g (set x v e).
Proof.
  intros.
  apply Map.uniq.
  intros.
  unfold set.
  unfold classify_set.
  rewrite! Map.find_map.
  rewrite Map.find_add.
  rewrite !Map.find_map.
  destruct (Pos.eq_dec x0 x); subst.
  rewrite Map.gss. simpl. rewrite CLAS.
  reflexivity.
  rewrite Map.gso by auto. reflexivity.
Qed.

Lemma classify_set_morph : forall g g' e,
    g =  g' ->
    classify_set g e = classify_set g' e.
Proof.
  intros.
  congruence.
Qed.



Definition omeet (o1 o2:option Typ.t) :=
  match o1 , o2 with
  | None , None => None
  | Some t , None | None , Some t => None
  | Some t1, Some t2 => Some (Typ.meet t1 t2)
  end.



Definition meet := Map.map2 omeet.

Lemma meet_sym : forall e1 e2, meet e1 e2 = meet e2 e1.
Proof.
  unfold meet.
  intros.
  apply Map.uniq.
  intros.
  rewrite! Map.find_map2; auto.
  destruct (Map.find x e1), (Map.find x e2); simpl; auto.
  rewrite Typ.meet_sym.
  reflexivity.
Qed.

Lemma get_meet : forall x e1 e2, get x (meet e1 e2) = Typ.meet (get x e1) (get x e2).
Proof.
  unfold get,Map.get,meet.
  intros.
  rewrite Map.find_map2 by reflexivity.
  destruct (Map.find x e1) eqn:FE1;
    destruct (Map.find x e2) eqn:FE2; simpl;auto.
  - rewrite Typ.meet_bot_r. reflexivity.
  - destruct t0; auto.
    rewrite BSet.inter_empty. reflexivity.
  - rewrite BSet.inter_empty. reflexivity.
Qed.


Lemma meet_assoc : forall e1 e2 e3,
    meet (meet e1 e2) e3 = meet e1 (meet e2 e3).
Proof.
  unfold meet.
  intros.
  apply Map.uniq.
  intros.
  rewrite ! Map.find_map2 by reflexivity.
  destruct (Map.find x e1), (Map.find x e2) , (Map.find x e3); simpl; auto.
  rewrite Typ.meet_assoc. reflexivity.
Qed.


Lemma meet_idem : forall e,
    meet e e = e.
Proof.
  unfold meet.
  intros.
  apply Map.uniq.
  intros. rewrite Map.find_map2 by reflexivity.
  destruct (Map.find x e); simpl;auto.
  rewrite Typ.meet_idem. reflexivity.
Qed.

Lemma classify_set_twice : forall s1 s2 e,
    classify_set s1 (classify_set s2 e) = classify_set (BSet.union s1 s2) e.
Proof.
  unfold classify_set. intros.
  apply Map.uniq.
  intros.
  rewrite !Map.find_map.
  destruct (Map.find x e); simpl; auto.
  rewrite Typ.classify_set_g1_g2.
  reflexivity.
Qed.

Definition ojoin (o1 o2:option Typ.t) :=
  match o1 , o2 with
  | None , None => None
  | Some t , None | None , Some t => Some t
  | Some t1, Some t2 => Some (Typ.join t1 t2)
  end.

Definition join := Map.map2 ojoin.


Lemma join_sym : forall e1 e2, join e1 e2 = join e2 e1.
Proof.
  unfold join.
  intros.
  apply Map.uniq.
  intros.
  rewrite! Map.find_map2 by auto.
  destruct (Map.find x e1), (Map.find x e2) ; simpl;auto.
  rewrite Typ.join_sym. reflexivity.
Qed.

Lemma get_join : forall x e1 e2, get x (join e1 e2) = Typ.join (get x e1) (get x e2).
Proof.
  unfold get,Map.get,join.
  intros.
  rewrite Map.find_map2 by auto.
  destruct (Map.find x e1) eqn:FE1;
    destruct (Map.find x e2) eqn:FE2.
  - simpl. reflexivity.
  - simpl. rewrite Typ.join_bot_r. reflexivity.
  - unfold ojoin.
    rewrite Typ.join_bot_l. reflexivity.
  - rewrite Typ.join_idem. reflexivity.
Qed.


Lemma join_ub_l : forall e1 e2,
    order e1 (join e1 e2).
Proof.
  unfold order,join.
  intros.
  rewrite get_join.
  apply Typ.join_ub_l.
Qed.

Lemma join_lub : forall e1 e2 e3,
    order e1 e3 ->
    order e2 e3 ->
    order (join e1 e2) e3.
Proof.
  repeat intro.
  rewrite get_join.
  apply Typ.join_lub; auto.
Qed.

Lemma join_idem : forall (e :t), join e e =  e.
Proof.
  intros.
  apply Map.uniq.
  unfold join.
  intros.
  rewrite Map.find_map2 by reflexivity.
  destruct (Map.find x e); simpl;auto.
  rewrite Typ.join_idem.
  reflexivity.
Qed.

Lemma classify_join:
  forall G1 G2 p, classify p (join G1 G2) =
                    join (classify p G1) (classify p G2).
Proof.
  unfold join.
  unfold classify.
  intros.
  apply Map.uniq.
  intros.
  rewrite Map.find_map2 by reflexivity.
  rewrite Map.find_map.
  rewrite Map.find_map2 by reflexivity.
  rewrite! Map.find_map.
  destruct (Map.find x G1) ; destruct (Map.find x G2); simpl;auto.
  rewrite Typ.classify_join.
  reflexivity.
Qed.


Lemma order_meet2 : forall e1 e2 e1' e2',
    order e1 e1' ->
    order e2 e2' ->
    order (meet e1 e2) (meet e1' e2').
Proof.
  repeat intro.
  rewrite! get_meet.
  eapply Typ.order_meet2; auto.
Qed.

Lemma order_join2 : forall e1 e2 e1' e2',
    order e1 e1' ->
    order e2 e2' ->
    order (join e1 e2) (join e1' e2').
Proof.
  repeat intro.
  rewrite! get_join.
  eapply Typ.order_join2; auto.
Qed.



Lemma order_set_order : forall G G' x ti,
    order (set x ti G) G' ->
    order (set x ti G) (set x ti G').
Proof.
  unfold order; simpl.
  intros.
  destruct (Pos.eq_dec x0 x); subst.
  rewrite! gss.
  apply Typ.order_refl.
  rewrite! gso by assumption.
  specialize (H x0).
  rewrite gso in H by assumption.
  auto.
Qed.

Lemma order_set_L : forall G x,
    order (set x Typ.L G) G.
Proof.
  unfold order; intros.
  destruct (Pos.eq_dec x0 x); subst.
  rewrite gss.
  apply Typ.order_bot.
  rewrite gso by assumption.
  apply Typ.order_refl.
Qed.


Lemma classify_set_classify : forall x e,
    classify_set (BSet.singleton x) e = classify x e.
Proof.
  unfold classify_set, classify.
  intros. apply Map.uniq.
  intro.
  rewrite ! Map.find_map.
  destruct (Map.find x0 e); simpl;auto.
  rewrite Typ.classify_set_classify.
  reflexivity.
Qed.

Lemma classify_swap:
  forall p p' G,
    classify p (classify p' G) = classify p' (classify p G).
Proof.
  intros.
  rewrite <- !classify_set_classify.
  rewrite !classify_set_twice.
  f_equal.
  rewrite BSet.union_sym.
  reflexivity.
Qed.

Lemma classify_swap_set:
  forall p g G,
    classify p (classify_set g G) = classify_set g (classify p G).
Proof.
  intros.
  rewrite <- !classify_set_classify.
  rewrite !classify_set_twice.
  f_equal.
  rewrite BSet.union_sym.
  reflexivity.
Qed.


Lemma join_set :forall e1 e2 x t1 t2,
    join (set x t1 e1) (set x t2 e2) = set x (Typ.join t1 t2) (join e1 e2).
Proof.
  unfold join.
  unfold set.
  intros.
  apply Map.uniq.
  intros.
  rewrite Map.find_map2 by reflexivity.
  rewrite !Map.find_add.
  destruct (Pos.eq_dec x0 x); auto.
  rewrite Map.find_map2 by reflexivity.  reflexivity.
Qed.

Lemma classify_set_fix_subset : forall g1 g2 G,
    classify_set g1 G = G ->
    BSet.subset g2 g1 = true ->
    classify_set g2 G = G.
Proof.
  intros.
  rewrite <- H at 1.
  rewrite classify_set_twice.
  assert (BSet.union g2 g1 = g1).
  {
    apply BSet.uniq.
    intro.
    rewrite BSet.mem_union_iff.
    specialize (BSet.subset_mem1 x _ _ H0).
    destruct (BSet.mem x g2),(BSet.mem x g1); intuition congruence.
  }
  rewrite H1. auto.
Qed.

Definition mk_pair (o1:option var) (o2:option Typ.t) :=
  match o1 , o2 with
  | None  , _   => None
  | Some x , None => Some (x,Typ.L)
  | Some x , Some t => Some (x,t)
  end.


Definition Leak (e : t) :=
  Map.fold (fun _ v acc => BSet.union (Typ.to_set v) acc) e BSet.empty.


Lemma leak_acc : forall l acc,
      (List.fold_left
         (fun (a : BSet.t) (p : positive * Typ.t) =>
            BSet.union (Typ.to_set (snd p)) a) l
         acc) =
        BSet.union acc  (List.fold_left
         (fun (a : BSet.t) (p : positive * Typ.t) =>
            BSet.union (Typ.to_set (snd p)) a) l  BSet.empty).
Proof.
  induction l ; simpl;auto.
  -  intros. rewrite BSet.union_empty. reflexivity.
  - intros.
    rewrite IHl.
    symmetry.
    rewrite IHl.
    rewrite BSet.union_empty.
    rewrite <- (BSet.union_sym acc).
    rewrite BSet.union_assoc.
    f_equal.
Qed.


Lemma leak_var : forall x e,
    BSet.subset (Typ.to_set (get x e)) (Leak e) = true.
Proof.
  unfold Leak.
  intros.
  rewrite Map.fold_1.
  unfold get,Map.get.
  destruct (Map.find x e) eqn:F.
  - apply Map.find_elements in F.
  revert F. revert x t0.
  induction (Map.elements e).
  + simpl. tauto.
  + simpl. intros.
    destruct F; subst. simpl.
    *
      rewrite leak_acc.
      rewrite BSet.union_empty.
      apply BSet.subset_union_l.
    *
      rewrite leak_acc.
      apply IHl in H.
      eapply BSet.subset_trans;eauto.
      rewrite BSet.union_sym.
      eapply BSet.subset_union_l.
  -  simpl. eapply BSet.subset_empty_l.
Qed.


Lemma mem_leak_complete : forall p e,
    BSet.mem p (Leak e) = true -> exists x, BSet.mem p (Typ.to_set (get x e)) = true.
Proof.
  intros p e.
  unfold Leak.
  rewrite Map.fold_1.
  assert (ACC1 : forall x v,  List.In (x,v) (Map.elements e) ->
                              get x e = v).
  {
    intros.
    apply Map.find_elements in H.
    unfold get,Map.get. rewrite H. reflexivity.
  }
  induction (Map.elements e).
  -  simpl. rewrite BSet.mem_empty. congruence.
  - simpl.
    rewrite leak_acc.
    rewrite BSet.mem_union_iff.
    rewrite BSet.union_empty.
    rewrite orb_true_iff.
    assert (ACC1' : (forall (x : PositiveMap.key) (v : Typ.t),
         List.In (x, v) l -> get x e = v)).
    {
      intros. apply ACC1. simpl. tauto.
    }
    specialize (IHl ACC1').
    clear ACC1'.
    intros. destruct H.
    + exists (fst a).
      destruct a as [x v]. simpl in * ; subst.
      rewrite ACC1 with (v0:=v); auto.
    + apply IHl;auto.
Qed.

Lemma mem_leak_correct : forall x p e,
    BSet.mem p (Typ.to_set (get x e)) = true ->
    BSet.mem p (Leak e) = true.
Proof.
  intros x p e.
  unfold Leak.
  rewrite Map.fold_1.
  unfold get,Map.get.
  destruct (Map.find x e) eqn:F.
  - apply Map.find_elements in F.
    revert F. revert x t0.
  induction (Map.elements e).
  + simpl. tauto.
  + simpl. intros.
    destruct F; subst. simpl.
    * rewrite leak_acc.
      rewrite ! BSet.mem_union_iff.
      rewrite ! orb_true_iff.
      tauto.
    * apply IHl in H0; auto.
      rewrite leak_acc.
      rewrite ! BSet.mem_union_iff.
      rewrite !orb_true_iff.
      tauto.
  - simpl.
    rewrite BSet.mem_empty.
    congruence.
Qed.

Lemma leak_set : forall x ti e,
    BSet.subset (Leak (set x ti e)) (BSet.union (Leak e) (Typ.to_set ti)) = true.
Proof.
  intros.
  apply BSet.subset_mem2.
  intros.
  rewrite BSet.mem_union_iff.
  rewrite orb_true_iff.
  apply mem_leak_complete in H.
  destruct H.
  destruct (Pos.eq_dec x1 x). subst.
  rewrite gss in H. tauto.
  rewrite gso in H by assumption.
  left.
  eapply mem_leak_correct; eauto.
Qed.


Lemma Leak_classify_set : forall s G,
    BSet.subset (Leak (classify_set s G)) (BSet.diff (Leak G) s) = true.
Proof.
  intros.
  apply BSet.subset_mem_iff.
  intros.
  rewrite BSet.mem_diff_iff.
  apply mem_leak_complete in H.
  destruct H.
  rewrite get_classify_set in H.
  rewrite Typ.to_set_classify_set in H.
  destruct (BSet.is_empty (BSet.inter s (Typ.to_set (get x0 G)))) eqn:INT.
  apply BSet.is_empty_true in INT.
  apply (f_equal (BSet.mem x)) in INT.
  rewrite BSet.mem_inter_iff in INT.
  rewrite H in INT. rewrite BSet.mem_empty in INT.
  apply mem_leak_correct in H. rewrite H.
  simpl. rewrite andb_comm in INT. simpl in INT. rewrite INT.
  reflexivity.
  rewrite BSet.mem_empty in H. discriminate.
Qed.



Lemma mem_leak_set : forall p x ti e,
    BSet.mem p (Leak (set x ti e)) = true -> BSet.mem p  (BSet.union (Leak e) (Typ.to_set ti)) = true.
Proof.
  intros.
  generalize (leak_set x ti e).
  intro.
  rewrite BSet.subset_mem_iff in H0.
  specialize (H0 p).
  subset.
Qed.

Lemma order_update_r : forall GU G1 G2
                              (SD : same_dom G1 G2),
    order G1 G2 ->
    order (Map.update GU G1) (Map.update GU G2).
Proof.
  repeat intro.
  generalize (H x).
  unfold get,Map.get.
  rewrite! Map.find_update.
  destruct (Map.find x G1) eqn:F1;
    destruct (Map.find x G2) eqn:F2;
    destruct (Map.find x GU) eqn:FU;
    unfold from_option;auto.
  - intros.
    apply Typ.order_is_L_r in H0.
    subst. apply Typ.order_bot.
  - specialize (SD x).
    unfold Map.dom in SD. intuition congruence.
  - intros. apply Typ.order_refl.
Qed.


Lemma update_set : forall GU G x t,
    (*t <> Typ.L ->*)
    Map.update GU (set x t G) = set x t (Map.update GU G).
Proof.
    unfold set.
    intros.
    apply Map.uniq.
    intros.
    rewrite Map.find_update.
    rewrite! Map.find_add.
    rewrite Map.find_update.
    destruct (Pos.eq_dec x0 x);subst;auto.
Qed.

(*Lemma update_set' : forall GU G x t,
    t <> Typ.L ->
    Map.update GU (set x t G) = set x t (Map.update GU G).
Proof.
    unfold set.
    intros.
    rewrite Map.find_update.
    unfold Map.update.
    destruct (Map.find x G) eqn:FD.
    -
      apply Map.uniq.
      intros.
      rewrite Map.find_map2;auto.
      rewrite! Map.find_add.
      rewrite Map.find_map2 by auto.
      destruct (Pos.eq_dec x0 x);subst;auto.
    - destruct (Map.find x GU) eqn:FG.
      destruct (Typ.is_L t0) eqn:T0.
      apply Typ.is_L_inv in T0. subst.
      congruence.
      apply Map.uniq.
      intros.
      rewrite Map.find_map2;auto.
      rewrite! Map.find_add.
      rewrite Map.find_map2 by auto.
      destruct (Pos.eq_dec x0 x);subst;auto.
      destruct (Typ.is_L t0) eqn:T0.
      apply Typ.is_L_inv in T0. congruence.
      apply Map.uniq.
      intros.
      rewrite Map.find_map2;auto.
      rewrite! Map.find_add.
      rewrite Map.find_map2 by auto.
      destruct (Pos.eq_dec x0 x);subst;auto.
Qed.
*)


Lemma join_update :
  forall G e1 e2
         (SAMEDOM : same_dom e1 e2),
    join (Map.update G e1) (Map.update G e2) =
      Map.update G (join e1 e2).
Proof.
  intros.
  apply Map.uniq.
  unfold join,Map.update.
  intros.
  rewrite !Map.find_map2 by reflexivity.
  unfold same_dom,Map.dom in SAMEDOM.
  specialize (SAMEDOM x).
  destruct (Map.find x G) eqn:F1;
    destruct (Map.find x e1) eqn:F2;
    destruct (Map.find x e2) eqn:F3; simpl;
    intuition try congruence.
  rewrite Typ.join_idem. reflexivity.
Qed.

Definition rename (rho: @Map.t var) (e:t) :=
  Renaming.rename Typ.L rho e.


Lemma join_update_rename :
  forall G rho e1 e2,
    join (Map.update G (rename rho e1)) (Map.update G (rename rho e2)) =
      Map.update G (join (rename rho e1) (rename rho e2)).
Proof.
  intros.
  apply join_update.
  apply same_dom_rename.
Qed.

Lemma join_rename : forall rho e1 e2,
    join (rename rho e1) (rename rho e2) =
      rename rho (join e1 e2).
Proof.
  intros.
  apply Map.uniq.
  intros. unfold join,rename.
  rewrite !Map.find_map2 by reflexivity.
  unfold Renaming.rename.
  rewrite !Map.find_map.
  specialize (Map.find_invert x rho).
  destruct (Map.find x (Map.invert rho)); simpl;auto.
  intros.
  change (from_option Typ.L (Map.find p e1)) with (get p e1).
  change (from_option Typ.L (Map.find p e2)) with (get p e2).
  rewrite <- get_join.
  reflexivity.
Qed.

Lemma dom_join : forall x G1 G2,
    Map.dom (join G1 G2) x <-> (Map.dom G1 x \/ Map.dom G2 x).
Proof.
  unfold Map.dom, join.
  intros.
  rewrite Map.find_map2 by reflexivity.
  destruct (Map.find x G1); destruct (Map.find x G2); simpl; intuition congruence.
Qed.

Lemma get_rename_set :
  forall rho G x  x' ti
         (WF : wf_rename rho)
(*         (NOTL : Typ.is_L ti = false)*)
  ,
    Map.find x rho = Some x' ->
    forall y,
      get y (rename rho (set x ti G)) =
        get y (set x' ti (rename rho G)).
Proof.
  intros.
  destruct (Pos.eq_dec y x').
  - subst. rewrite gss.
    unfold get,rename.
    erewrite rename_correct;auto.
    rewrite gss. reflexivity.
    auto.
  - rewrite gso by assumption.
    unfold get,Map.get,rename, Map.get,Renaming.rename.
    rewrite! Map.find_map.
    specialize (Map.find_invert y rho).
    destruct (Map.find y (Map.invert rho)) eqn:FRO.
    + simpl. intros.
      unfold Map.get.
      unfold set.
      rewrite Map.find_add.
      destruct (Pos.eq_dec p x).
      subst. congruence.
      reflexivity.
    +  simpl. reflexivity.
Qed.



Lemma rename_set :
  forall rho G x  x' ti
         (WF : wf_rename rho)
  ,
    Map.find x rho = Some x' ->
    rename rho (set x ti G) = set x' ti (rename rho G).
Proof.
  intros.
  unfold set.
  destruct (Map.find x G) eqn:FD.
  - unfold rename.
    apply Map.uniq.
    intros.
    unfold Renaming.rename.
    rewrite Map.find_map.
    rewrite Map.find_add.
    rewrite Map.find_map.
    generalize (Map.find_invert x0 rho).
    destruct (Map.find x0 (Map.invert rho)) eqn:FIND;auto.
    intros.
    + destruct (Pos.eq_dec x0 x'); subst;auto.
      simpl.
      destruct (Pos.eq_dec p x); auto.
      subst. unfold Map.get. rewrite Map.gss. reflexivity.
      exfalso. apply n.
      eapply WF; eauto.
      simpl.
      unfold Map.get.
      rewrite Map.find_add.
      destruct (Pos.eq_dec p x); auto.
      subst.
      rewrite FD.
      congruence.
    + intros.
      simpl. destruct (Pos.eq_dec x0 x'); auto.
      subst.
      exfalso. apply (H0 x). auto.
  - unfold rename.
    unfold Renaming.rename.
    apply Map.uniq.
    intros.
    rewrite ! Map.find_add.
    rewrite! Map.find_map.
    generalize (Map.find_invert x0 rho).
    unfold Map.get.
    destruct (Map.find x0 (Map.invert rho)) eqn:FIND;auto.
    simpl.
    +  destruct (Pos.eq_dec x0 x').
       subst.
       intros.
       rewrite Map.find_add.
       destruct (Pos.eq_dec p x); subst. auto.
       exfalso.
       apply n.
       eapply WF;eauto.
       intros.
       unfold get.
       rewrite Map.find_add.
       destruct (Pos.eq_dec p x); subst. rewrite FD.
       congruence.
       reflexivity.
    +  simpl.
       intros.
       destruct (Pos.eq_dec x0 x'); subst;auto.
       exfalso.
       apply (H0 x);auto.
Qed.

(*Lemma rename_set' :
  forall rho G x  x' ti
         (WF : wf_rename rho)
         (D1  : Map.dom G x)
  ,
    Map.find x rho = Some x' ->
    rename rho (set x ti G) = set x' ti (rename rho G).
Proof.
  intros.
  unfold set.
  destruct (Map.find x G) eqn:FD.
  - unfold rename.
    rewrite find_rename_correct with (x0:=x) by auto.
    apply Map.uniq.
    intros.
    unfold Renaming.rename.
    rewrite Map.find_map.
    rewrite Map.find_add.
    rewrite Map.find_map.
    generalize (Map.find_invert x0 rho).
    destruct (Map.find x0 (Map.invert rho)) eqn:FIND;auto.
    intros.
    + destruct (Pos.eq_dec x0 x'); subst;auto.
      simpl.
      destruct (Pos.eq_dec p x); auto.
      subst. unfold Map.get. rewrite Map.gss. reflexivity.
      exfalso. apply n.
      eapply WF; eauto.
      simpl.
      unfold Map.get.
      rewrite Map.find_add.
      destruct (Pos.eq_dec p x); auto.
      subst.
      rewrite FD.
      congruence.
    + intros.
      simpl. destruct (Pos.eq_dec x0 x'); auto.
      subst.
      exfalso. apply (H0 x). auto.
  - unfold Map.dom in D1.
    congruence.
Qed.
*)
Lemma order_rename : forall G1 G2 rho,
    order G1 G2 ->
    order (rename rho G1) (rename rho G2).
Proof.
  repeat intro.
  unfold get, rename.
  unfold Map.get.
  unfold Renaming.rename.
  rewrite ! Map.find_map.
  destruct (Map.find x (Map.invert rho)) eqn:FD.
  - intros. simpl. apply H.
  - simpl. apply BSet.subset_refl.
Qed.


Lemma join_assoc : forall e1 e2 e3,
    join (join e1 e2) e3 = join e1 (join e2 e3).
Proof.
  unfold join.
  intros.
  apply Map.uniq.
  intros.
  rewrite ! Map.find_map2 by reflexivity.
  destruct (Map.find x e1), (Map.find x e2) , (Map.find x e3); simpl; auto.
  rewrite Typ.join_assoc. reflexivity.
Qed.

Definition is_simple (G: t):=
  forall x, Typ.is_simple (get x G) = true.

Lemma  is_simple_join : forall G1 G2,
    is_simple G1 -> is_simple G2 ->
    is_simple (join G1 G2).
Proof.
  unfold is_simple.
  intros.
  rewrite get_join.
  apply Typ.is_simple_join; auto.
Qed.

Definition downgrade := Map.map Typ.downgrade.

Lemma get_downgrade : forall x G,
    (Typ.downgrade (get x G)) =  get x (downgrade G).
Proof.
  intros. unfold downgrade,get.
  unfold Map.get.
  rewrite Map.find_map.
  destruct (Map.find x G); simpl;auto.
Qed.

Lemma order_downgrade : forall G1 G2,
    order G1 G2 ->
    order (downgrade G1) (downgrade G2).
Proof.
  unfold order.
  intros.
  rewrite <- !get_downgrade.
  apply Typ.order_downgrade_mono.
  auto.
Qed.

Lemma downgrade_join : forall G1 G2,
  downgrade (join G1 G2) = join (downgrade G1) (downgrade G2).
Proof.
  intros.
  apply Map.uniq.
  intros.
  unfold downgrade,join.
  intros.
  rewrite !Map.find_map.
  rewrite !Map.find_map2 by auto.
  rewrite !Map.find_map.
  destruct (Map.find x G1) , (Map.find x G2); simpl; auto.
  rewrite Typ.downgrade_join. reflexivity.
Qed.

Lemma downgrade_idem : forall G,
    downgrade (downgrade G) = downgrade G.
Proof.
  unfold downgrade.
  intro.
  apply Map.uniq.
  intros.
  rewrite !Map.find_map.
  destruct (Map.find x G);simpl; auto.
  rewrite Typ.downgrade_idem. reflexivity.
Qed.

Definition  downgrade_set (s: BSet.t) :=
  Map.map (Typ.downgrade_set s).

Lemma get_downgrade_set : forall x s G,
    get x (downgrade_set s G) = Typ.downgrade_set s (get x G).
Proof.
  intros. unfold downgrade_set,get.
  unfold Map.get.
  rewrite Map.find_map.
  destruct (Map.find x G); simpl;auto.
  rewrite BSet.diff_empty_l. reflexivity.
Qed.

Lemma downgrade_set_mono2 : forall s G1 G2,
    order G1 G2 ->
    order (downgrade_set s G1) (downgrade_set s G2).
Proof.
  unfold order; intros.
  rewrite! get_downgrade_set.
  apply Typ.downgrade_set_mono2.
  auto.
Qed.


Lemma downgrade_set_mono1 : forall s1 s2 G ,
    BSet.subset s2 s1 = true ->
    order (downgrade_set s1 G) (downgrade_set s2 G).
Proof.
  repeat intro.
  rewrite! get_downgrade_set.
  apply Typ.downgrade_set_mono1;auto.
Qed.


Lemma downgrade_set_set : forall x ti s G,
    downgrade_set s (set x ti G) = set x (Typ.downgrade_set s ti) (downgrade_set s G).
Proof.
  unfold downgrade_set.
  intros.
  apply Map.uniq.
  intros.
  rewrite Map.find_map.
  unfold set.
  rewrite! Map.find_add.
  destruct (Pos.eq_dec x0 x). reflexivity.
  rewrite Map.find_map.
  reflexivity.
Qed.

Lemma downgrade_set_join : forall s G1 G2,
    downgrade_set s (join G1 G2) = join (downgrade_set s G1) (downgrade_set s G2).
Proof.
  unfold downgrade_set.
  intros.
  apply Map.uniq.
  intros.
  rewrite !Map.find_map.
  unfold join.
  rewrite !Map.find_map2 by auto.
  rewrite !Map.find_map.
  destruct (Map.find x G1), (Map.find x G2) ; simpl; auto.
  rewrite Typ.downgrade_set_join. reflexivity.
Qed.

Lemma downgrade_set_classify_set :
  forall s1 s2 ti
         (I : BSet.inter s1 s2 = BSet.empty),
    downgrade_set s1 (classify_set s2 ti) = classify_set s2 (downgrade_set s1 ti).
Proof.
  unfold downgrade_set,classify_set.
  intros.
  apply Map.uniq.
  intros.
  rewrite! Map.find_map.
  destruct (Map.find x ti);simpl;auto.
  rewrite Typ.downgrade_set_classify_set;auto.
Qed.


Lemma  classify_set_downgrade_set_idem :
  forall s1 s2 e,
         BSet.subset s2 s1 = true ->
         classify_set s2 (downgrade_set s1 e) = downgrade_set s1 e.
Proof.
  intros.
  apply Map.uniq;  intros.
  unfold classify_set,downgrade_set.
  rewrite! Map.find_map by auto.
  destruct (Map.find x e); simpl;auto.
  rewrite Typ.classify_set_downgrade_set_idem by auto.
  reflexivity.
Qed.

Lemma downgrade_set_idem : forall e d,
    BSet.inter d (Leak e) = BSet.empty ->
    downgrade_set d e = e.
Proof.
  intros.
  apply Map.uniq.
  intros.
  unfold downgrade_set.
  rewrite Map.find_map.
  destruct (Map.find x e) eqn:FD; auto.
  simpl. f_equal.
  apply Typ.downgrade_set_idem.
  apply BSet.uniq.
  intros.
  apply (f_equal (BSet.mem x0)) in H.
  subset.
  left. intro. apply H0.
  eapply mem_leak_correct with (x:=x).
  unfold get. unfold Map.get. rewrite FD. simpl. auto.
Qed.

Lemma downgrade_set_classify_set_idem : forall G d s,
    downgrade_set (BSet.diff d s) (classify_set s G) = downgrade_set d (classify_set s G).
Proof.
  intros.
  apply Map.uniq. unfold downgrade_set,classify_set.
  intros.
  rewrite !Map.find_map.
  destruct (Map.find x G); simpl;auto.
  f_equal.
  rewrite Typ.classify_set_eq.
  unfold Typ.classify_set'.
  destruct t0 ; simpl; auto.
  destruct (BSet.is_empty (BSet.inter s s0)) eqn:E; simpl;auto.
  f_equal.
  apply BSet.uniq. intros.
  apply BSet.is_empty_true  in E.
  apply (f_equal (BSet.mem x0)) in E.
  rewrite BSet.mem_empty in E.
  rewrite !BSet.mem_diff_iff.
  rewrite BSet.mem_inter_iff in E.
  rewrite andb_false_iff in E.
  destruct E as [E|E]; rewrite E; simpl; auto.
  rewrite <- (andb_comm true).
  simpl. reflexivity.
Qed.


Lemma downgrade_set_order :
  forall d G,
         order (downgrade_set d G) G.
Proof.
  repeat intro.
  rewrite get_downgrade_set.
  apply Typ.downgrade_set_order.
Qed.

Lemma pp_in_Leak : forall p G,
    pp_in p G <-> BSet.mem p (Leak G) = true.
Proof.
  unfold pp_in. split;intros.
  destruct H. unfold Typ.pp_in in H.
  eapply mem_leak_correct; eauto.
  apply mem_leak_complete in H.
  destruct H.
  exists x. auto.
Qed.

Lemma downgrade_set_Leak : forall d G,
    Leak (TEnv.downgrade_set d G) = BSet.diff (Leak G) d.
Proof.
  intros.
  apply BSet.uniq.
  intros.
  rewrite BSet.mem_diff_iff.
  destruct (BSet.mem x (Leak (downgrade_set d G))) eqn:MD.
  - apply mem_leak_complete in MD.
    destruct MD. rewrite get_downgrade_set in H.
    rewrite Typ.to_set_downgrade_set in H.
    rewrite BSet.mem_diff_iff in H.
    rewrite andb_true_iff in H.
    destruct H.
    apply mem_leak_correct in H. rewrite H. auto.
  - symmetry. rewrite andb_false_iff.
    destruct (BSet.mem x (Leak G)) eqn:ML;auto.
    apply mem_leak_complete in ML.
    destruct ML.
    destruct (negb (BSet.mem x d)) eqn:MD';auto.
    exfalso.
    revert MD.
    apply eq_true_false_abs.
    apply mem_leak_correct with (x:= x0).
    rewrite get_downgrade_set.
    rewrite Typ.to_set_downgrade_set.
    rewrite BSet.mem_diff_iff.
    rewrite MD'. rewrite andb_comm. auto.
Qed.


Lemma update_classify_set : forall s G G',
    classify_set s (Map.update G G') = Map.update (classify_set s G) (classify_set s G').
Proof.
  intros.
  apply Map.uniq.
  unfold classify_set,Map.update.
  intros.
  rewrite Map.find_map.
  rewrite! Map.find_map2 by auto.
  rewrite !Map.find_map.
  destruct (Map.find x G), (Map.find x G') ; simpl; auto.
Qed.

Lemma classify_set_idem : forall s G,
    BSet.inter (Leak G) s = BSet.empty ->
    classify_set s G = G.
Proof.
  intros.
  apply Map.uniq.
  unfold classify_set.
  intros.
  rewrite Map.find_map.
  destruct (Map.find x G) eqn:F;auto.
  simpl.
  f_equal.
  apply Typ.classify_set_idem.
  subset.
  right. intros.
  change t0 with (from_option Typ.L (Some t0)) in H.
  rewrite <- F in H.
  apply mem_leak_correct in H.
  congruence.
Qed.

Lemma classify_set_rename : forall s rho G,
    classify_set s (rename rho G) = rename rho (classify_set s G).
Proof.
  unfold classify_set,rename.
  unfold Renaming.rename.
  intros.
  apply Map.uniq; intros.
  rewrite !Map.find_map.
  destruct ((Map.find x (Map.invert rho))); auto.
  simpl.
  f_equal.
  unfold Map.get.
  rewrite Map.find_map.
  destruct (Map.find p G);auto.
  simpl. apply Typ.classify_set_L.
Qed.

Definition no_sharing (rho1 rho2: @Map.t var) :=
  forall x y x', Map.find x rho1 = Some x' -> Map.find y rho2 = Some x' -> x = y.

(*Lemma rename_join : forall r1 r2 G
                           (SHAR: no_sharing r1 r2)
                           (WF1: wf_rename r1)
                           (WF2: wf_rename r2)
  ,
    rename (Renaming.join r1 r2) G =
    join (rename r1 G) (rename r2 G).
Proof.
  intros.
  apply Map.uniq.
  intro.
  unfold rename.
  unfold Renaming.rename.
  rewrite! Map.find_map.
  unfold join.
  rewrite Map.find_map2 by reflexivity.
  rewrite! Map.find_map.
  generalize (Map.find_invert x (Renaming.join r1 r2)).
  destruct (Map.find x (Map.invert (Renaming.join r1 r2))).
  - unfold Renaming.join.
    rewrite Map.find_map2 by reflexivity.
    intros.
    generalize (Map.find_invert x r1).
    generalize (Map.find_invert x r2).
    destruct (Map.find x (Map.invert r1));
      destruct (Map.find x (Map.invert r2)).
    + intros.
      simpl.
      destruct (Map.find p r1) eqn:FR1;
      destruct (Map.find p r2) eqn:FR2; simpl in H; try discriminate.
      *  inv H. assert (p0 = p). eapply WF1;eauto.
         subst.
         assert (p = p1). { eapply SHAR;eauto. }
         subst. rewrite Typ.join_idem.
         reflexivity.
      * inv H.
        assert (p0 = p). eapply WF1;eauto.
        subst.
        assert (p = p1). { eapply SHAR;eauto. }
        subst. rewrite Typ.join_idem.
        reflexivity.
      * inv H.
        assert (p1 = p). eapply WF2;eauto.
        subst.
        assert (p0 = p). { eapply SHAR;eauto. }
        subst. rewrite Typ.join_idem.
        reflexivity.
    +  intros.
       simpl.
       destruct (Map.find p r1) eqn:FR1;
         destruct (Map.find p r2) eqn:FR2; simpl in H; try discriminate.
       * inv H.
        assert (p = p0). eapply WF1;eauto.
        subst. reflexivity.
       * inv H.
        assert (p = p0). eapply WF1;eauto.
        subst. reflexivity.
       * inv H.
        assert (p0 = p). { eapply SHAR;eauto. }
        subst. congruence.
    + intros.
      simpl.
      destruct (Map.find p r1) eqn:FR1;
        destruct (Map.find p r2) eqn:FR2; simpl in H; try discriminate.
      * inv H.
        specialize (H1 p). congruence.
      * inv H.
        specialize (H1 p). congruence.
      * inv H.
        assert (p0 = p). { eapply WF2;eauto. }
        subst. congruence.
    + intros.
      specialize (H0 p).
      specialize (H1 p).
      destruct (Map.find p r1) ; destruct (Map.find p r2) ; simpl in *;
        congruence.
  - simpl.
    intros.
    generalize (Map.find_invert x r1).
    destruct (Map.find x (Map.invert r1)).
    + simpl. intro.
    specialize (H p).
    unfold Renaming.join in H. rewrite Map.find_map2 in H by reflexivity.
    rewrite H0 in H. simpl in H. destruct (Map.find p r2) ; congruence.
    + intros.
      simpl.
      generalize (Map.find_invert x r2).
      destruct (Map.find x (Map.invert r2)).
      * simpl. intro.
        specialize (H p).
        unfold Renaming.join in H. rewrite Map.find_map2 in H by reflexivity.
        rewrite H1 in H.
        destruct (Map.find p r1) eqn:MF; simpl in H ; try congruence.



    simpl in H. destruct (Map.find p r2) ; congruence.
    intros.
    simpl.




    destruct (Map.find x (Map.invert r2)); simpl ; auto.
*)


#[global] Notation "A ⊔ B" := (TEnv.join A B) (at level 300).

#[global] Notation "A + B" := (Map.update A B).
