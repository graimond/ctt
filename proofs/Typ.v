Require Import Bool ZArith.
Require Import Std NSet.

Inductive t := H | I (s : BSet.t).

Definition L := I BSet.empty.

Definition of_option (o: option t) :=
  match o with
  | None => L
  | Some ti => ti
  end.


Definition is_fresh (p:positive) (ty : t) :=
  match ty with
  | I s  => negb (BSet.mem p s)
  | H => true
  end.

Definition is_H (ti:t) :=
  match ti with
  | I s => false
  | _    => true
  end.

Definition is_L (ti:t) :=
  match ti with
  | I s => BSet.is_empty s
  | _    => false
  end.


Definition is_simple (x:t) := is_L x || is_H x.


Lemma is_L_inv : forall ti, is_L ti = true <-> ti = L.
Proof.
  intro.
  destruct ti ; simpl; try intuition congruence.
  intros. intuition try congruence. discriminate.
  rewrite BSet.is_empty_true.
  split; intro. subst;reflexivity.
  inv H0;reflexivity.
Qed.

Lemma is_H_inv : forall ti, is_H ti = true <-> ti = H.
Proof.
  intro.
  destruct ti ; simpl; try intuition congruence.
Qed.


Lemma is_L_inv_false : forall ti, is_L ti = false -> ti <> L.
Proof.
  intro.
  repeat intro. subst. discriminate.
Qed.


Definition join (t1 t2:t) : t :=
  match t1, t2 with
  | H , _   | _ , H     => H
  | I s1 , I s2 => I (BSet.union s1 s2)
  end.

Lemma is_fresh_join : forall p t1 t2,
    is_fresh p t1 = true ->
    is_fresh p t2 = true ->
    is_fresh p (join t1 t2) = true.
Proof.
  intros.
  destruct t1,t2; simpl ;auto.
  rewrite BSet.mem_union_iff.
  simpl in *.
  rewrite negb_true_iff in *.
  rewrite H0. rewrite H1.
  reflexivity.
Qed.

Lemma is_simple_join : forall t1 t2,
    is_simple t1 = true ->
    is_simple t2 = true ->
    is_simple (join t1 t2) = true.
Proof.
  unfold is_simple.
  intros.
  rewrite orb_true_iff in *.
  generalize (is_L_inv t1).
  generalize (is_H_inv t1).
  generalize (is_L_inv t2).
  generalize (is_H_inv t2).
  intuition subst; intuition auto.
Qed.



Lemma inj_I : forall s1 s2, I s1 = I s2 -> s1 = s2.
Proof.
  congruence.
Qed.

Lemma join_L_inv : forall t1 t2,
    (join t1 t2) = L ->  t1 =L /\ t2 = L.
Proof.
  destruct t1,t2 ; try discriminate.
  simpl. intros.  unfold L in H0.
  apply inj_I in H0.
  apply BSet.union_empty_inv   in H0.
  destruct H0; subst.
  split ; reflexivity.
Qed.

Definition meet (t1 t2:t) :t :=
  match t1, t2 with
  | H , x | x , H   => x
  | I s1 , I s2 => I (BSet.inter s1 s2)
  end.

Definition order (t1 t2:t) : bool :=
  match t1, t2 with
  | _ , H   => true
  | H ,   _  => false
  | I s1  , I s2 => BSet.subset s1 s2
  end.


Definition to_set (ti:t) :=
  match ti with
  | H => BSet.empty
  | I s  => s
  end.

Definition mem (p: positive) (t: t) :=
  match t with
  | H => true
  | I s => BSet.mem p s
  end.


Lemma mem_union_iff : forall x t1 t2,
    mem x (join t1 t2) = (mem x t1 || mem x t2).
Proof.
  unfold mem,join.
  intros.
  destruct t1, t2; eauto.
  - symmetry.
    apply orb_true_r.
  - apply BSet.mem_union_iff.
Qed.

  
Definition pp_in (p: positive) (t: t):= BSet.mem p (to_set  t) = true.

Lemma to_set_join : forall t1 t2,
    to_set (join t1 t2) = if is_H t1 || is_H t2 then BSet.empty
                          else BSet.union (to_set t1) (to_set t2).
Proof.
  destruct t1,t2; simpl; auto.
Qed.



Lemma pp_in_union_if : forall x t1 t2,
    pp_in x (join t1 t2) -> pp_in x t1 \/ pp_in x t2.
Proof.
  unfold pp_in.
  intros.
  rewrite to_set_join in H0.
  destruct (is_H t1 || is_H t2) eqn:isH.
  rewrite BSet.mem_empty in H0. discriminate.
  subset.
Qed.

(*
Lemma pp_in_order : forall x t1 t2,
  order t1 t2 = true -> pp_in x t2 -> pp_in x t1.
Proof.
  intros.
  unfold pp_in in *.
  destruct t1, t2; auto; simpl in H0; try discriminate.

  - inv H1.
    - unfold pp_in in *.
*)

Definition guard (t1 t2:t) : t :=
  if is_L t1 then L else t2.

Lemma guard_L_l : forall t2,
    guard L t2 = L.
Proof.
  unfold guard.
  intros.
  destruct (is_L L) eqn:ISL; auto.
  apply is_L_inv_false in ISL.
  congruence.
Qed.



Lemma guard_not_L : forall t1 t2,
    is_L t1 = false ->
    guard t1 t2 = t2.
Proof.
  unfold guard.
  intros. rewrite H0.
  reflexivity.
Qed.

Lemma guard_L : forall t1 t2,
    guard t1 t2 = L ->
    t1 = L \/ (t1 <> L /\ t2 = Typ.L).
Proof.
  unfold guard.
  intros. destruct (is_L t1) eqn:IL.
  apply is_L_inv in IL.  intuition congruence.
  apply is_L_inv_false in IL.  intuition congruence.
Qed.




Lemma order_is_L_r : forall t1,
    order t1 L = true -> t1 = L.
Proof.
  unfold order.
  destruct t1; simpl.
  discriminate.
  intros.
  unfold L.
  f_equal.
  apply BSet.uniq.
  intros.
  rewrite BSet.mem_empty.
  rewrite BSet.subset_mem_iff in H0.
  destruct (BSet.mem x s) eqn: MS; auto.
  specialize (H0 x MS).
  rewrite BSet.mem_empty in H0.
  discriminate.
Qed.


Lemma guard_order_L : forall t1 t2 t1',
    order t1' t1 = true ->
    guard t1 t2 = Typ.L ->
    guard t1' t2 = Typ.L.
Proof.
  unfold guard.
  intros.
  destruct (is_L t1) eqn:L1.
  apply is_L_inv in L1. subst.
  apply order_is_L_r in H0.
  rewrite H0.
  reflexivity.
  subst.
  destruct (is_L t1');auto.
Qed.

Definition singleton (p:positive) := I (BSet.singleton p).

Lemma singleton_not_L : forall p ,
    singleton p = L -> False.
Proof.
  intros. unfold singleton in H0.
  apply inj_I in H0.
  apply (f_equal (BSet.mem p)) in H0.
  rewrite BSet.singleton_xx in H0.
  rewrite BSet.mem_empty in H0.
  discriminate.
Qed.

Lemma to_set_singleton : forall x,
    to_set (singleton x) = BSet.singleton x.
Proof.
  reflexivity.
Qed.

Lemma guard_singleton_not_H : forall t1 t2,
guard t1 (singleton t2) = Typ.H -> False.
Proof.
  unfold guard. intros.
  destruct (is_L t1); discriminate.
Qed.

Lemma to_set_empty_inv : forall ti,
    ti <> H ->
    to_set ti = BSet.empty -> ti = L.
Proof.
  unfold to_set. destruct ti; intros;subst; try congruence.
  reflexivity.
Qed.


Lemma singleton_xx:
  forall p,
    mem p (singleton p) = true.
Proof.
  intros.
  unfold mem, singleton.
  apply BSet.singleton_xx.
Qed.

Lemma order_mem : forall x t1 t2,
    order t1 t2 = true ->
    mem x t1 = true ->
    mem x t2 = true.
Proof.
  unfold mem,order.
  intros.
  destruct t1, t2; eauto; try discriminate.
  eapply BSet.subset_mem1; eauto.
Qed.

Lemma order_join2 : forall t1 t1' t2 t2',
    order t1 t1' = true ->
    order t2 t2' = true ->
    order (join t1 t2) (join t1' t2') = true.
Proof.
  unfold order; intros.
  destruct t1, t1', t2, t2'; auto; simpl.
  eapply BSet.subset_union2; eauto.
Qed.

Lemma order_bot : forall ti,
    order L ti = true.
Proof.
  destruct ti; simpl; auto.
Qed.



Lemma to_set_subset : forall t1' t1,
    (t1 = H -> forall s, t1' = I s -> BSet.subset s BSet.empty = true) ->
    order t1' t1 = true ->
    BSet.subset (to_set t1') (to_set t1) = true.
Proof.
  unfold order,to_set.
  destruct t1' eqn:T1,t1 eqn:T2; simpl; auto.
Qed.

Lemma to_set_guard_empty : forall ti s,
    BSet.is_empty (to_set (guard ti s)) = true ->
    (ti <> L -> (to_set s) = BSet.empty -> False) ->
    ti = L.
Proof.
  unfold guard.
  intros. destruct (is_L ti) eqn:ISL; auto.
  apply is_L_inv in ISL;auto.
  rewrite BSet.is_empty_true in H0.
  apply is_L_inv_false in ISL.
  tauto.
Qed.



Lemma order_guard : forall t1 t2 t1' t2',
    order t1 t2 = true ->
    order t1' t2' = true ->
    order (guard t1 t1') (guard t2 t2') = true.
Proof.
  intros.
  unfold guard.
  destruct (is_L t1) eqn:L1; auto.
  destruct (is_L t2) eqn:L2;auto.
  { apply is_L_inv in L1. subst.
    apply order_bot.
  }
  destruct (is_L t2) eqn:L2;auto.
  apply is_L_inv in L2. subst.
  apply order_is_L_r in H0. subst.
  discriminate.
Qed.


Lemma order_H_inv : forall ti,
    order H ti = true <-> ti = H.
Proof.
  simpl.
  destruct ti ; try intuition  congruence.
Qed.

Lemma order_top : forall ti,
    order ti H = true.
Proof.
  destruct ti ; try intuition  congruence.
Qed.


Definition classify (p:positive) (x:t) : t:=
  if mem p x then H else x.
(*match x with
    | H  => H
    | I  s1 =>  if BSet.mem p s1 then H else I s1
    end.*)

Definition downgrade (x:t) :=
  match x with
  | H => H
  | I _ => L
  end.

Lemma downgrade_L : downgrade L = L.
Proof. reflexivity. Qed.

Lemma classify_L : forall p,classify p L = L.
Proof.
  reflexivity.
Qed.

Lemma classify_id : forall p ti,
    ~ pp_in p ti ->
    classify p ti = ti.
Proof.
  unfold pp_in,classify.
  intros. unfold mem.
  destruct ti ; simpl in *; auto.
  destruct (BSet.mem p s); congruence.
Qed.

Definition downgrade_set (s: BSet.t) (ti:t) :=
  match ti with
  | H => ti
  | I s1 => I (BSet.diff s1 s)
  end.

Lemma downgrade_set_L : forall s,
    Typ.downgrade_set s L = L.
Proof.
  simpl. unfold L. intro.
  rewrite BSet.diff_empty_l.
  reflexivity.
Qed.

Lemma downgrade_set_mono2 : forall s t1 t2,
    order t1 t2 = true ->
    order (downgrade_set s t1) (downgrade_set s t2) = true.
Proof.
  unfold downgrade_set.
  intros.
  destruct t1,t2;simpl; auto.
  simpl in H0.
  subset.
Qed.

Lemma downgrade_set_mono1 : forall s1 s2 ti ,
    BSet.subset s2 s1 = true ->
    order (downgrade_set s1 ti) (downgrade_set s2 ti) = true.
Proof.
  unfold downgrade_set.
  intros.
  destruct ti; simpl;auto.
  subset.
Qed.


Lemma classify_L_inv : forall p ti,
    classify p ti = L -> ti = L.
Proof.
  unfold classify. intros.
  destruct ti ; simpl in H0.
  discriminate.
  destruct (BSet.mem p s) eqn:M; try discriminate.
  auto.
Qed.

Lemma classify_H_inv : forall p ti,
    classify p ti = H -> ti = H \/ BSet.mem p (Typ.to_set ti) = true.
Proof.
  unfold classify,mem.
  intros.
  destruct ti. tauto.
  simpl. destruct (BSet.mem p s); intuition congruence.
Qed.

Lemma order_classify : forall p x, order x (classify p x) = true.
Proof.
  destruct x; simpl; auto.
  unfold classify.
  destruct (mem p (I s)) eqn:M; auto.
  apply BSet.subset_refl.
Qed.

Lemma classify_mono : forall p t1 t2,
    order t1 t2 = true ->
    order (classify p t1) (classify p t2) = true.
Proof.
  unfold classify.
  destruct t1,t2 ; simpl; auto.
  -  discriminate.
  - intro.
    destruct (BSet.mem p s) eqn:M1.
    reflexivity.
    reflexivity.
  - intro.
    destruct (BSet.mem p s) eqn:M1.
    eapply BSet.subset_mem1 in H0; eauto.
    rewrite H0. reflexivity.
    destruct (BSet.mem p s0) ; simpl; auto.
Qed.

Lemma classify_mem: forall p t,
    mem p t = false ->
    classify p t = t.
Proof.
  unfold classify.
  intros.
  destruct t0; auto; simpl.
  inv H0. rewrite H2. auto.
Qed.

Definition classify_set (s:BSet.t) (x:t) :=
  BSet.fold (fun x acc => classify x acc) s x.

Lemma classify_set_L : forall p,
    classify_set p L = L.
Proof.
  unfold classify_set.
  intros.
  rewrite BSet.fold_1.
  induction (BSet.elements p).
  -  simpl. reflexivity.
  - simpl.
    auto.
Qed.

Lemma classify_join:
  forall t1 t2 p, classify p (join t1 t2) =
                    join (classify p t1) (classify p t2).
Proof.
  intros.
  unfold classify.
  destruct t1, t2; auto; simpl.
  - destruct (BSet.mem p s); auto.
  - remember (BSet.mem p s).
    remember (BSet.mem p s0).
    rewrite BSet.mem_union_iff.
    destruct b, b0; rewrite <- Heqb; rewrite <- Heqb0; auto.
Qed.


Lemma order_refl : forall x, order x x = true.
Proof.
  unfold order.
  destruct x; try reflexivity.
  apply BSet.subset_refl.
Qed.

Lemma order_trans : forall x1 x2 x3,
    order x1 x2 = true ->
    order x2 x3 = true ->
    order x1 x3 = true.
Proof.
  unfold order.
  destruct x1,x2,x3; auto; try congruence.
  - eapply BSet.subset_trans; eauto.
Qed.


Lemma downgrade_set_order : forall s ti,
  order (downgrade_set s ti) ti = true.
Proof.
  unfold downgrade_set.
  destruct ti.
  apply order_refl.
  simpl. apply BSet.subset_diff_l.
Qed.

Lemma downgrade_set_simple : forall s ti,
    is_simple ti = true ->
    Typ.downgrade_set s ti = ti.
Proof.
  unfold is_simple.
  intros. rewrite orb_true_iff in H0.
  destruct H0.
  apply is_L_inv in H0. subst.
  rewrite downgrade_set_L. reflexivity.
  apply is_H_inv in H0. subst.
  reflexivity.
Qed.

Lemma downgrade_set_H_inv : forall s ti,
    downgrade_set s ti = H ->
    ti = H.
Proof.
  destruct ti; simpl;auto.
  discriminate.
Qed.

Lemma classify_set_mono2 : forall s t1 t2,
    order t1 t2 = true ->
    order (classify_set s t1) (classify_set s t2) = true.
Proof.
  unfold classify_set.
  intros.
  rewrite! BSet.fold_1.
  revert t1 t2 H0.
  induction (BSet.elements s).
  -  simpl. auto.
  - simpl. intros.
    apply IHl.
    apply classify_mono;auto.
Qed.

Lemma order_classify_set : forall p x,
    order x (classify_set p x) = true.
Proof.
  unfold classify_set.
  intros.
  rewrite BSet.fold_1.
  revert x.
  induction (BSet.elements p).
  -  simpl. intros. apply order_refl.
  - simpl. intros.
    eapply order_trans.
    apply order_classify.
    eauto.
Qed.


Lemma join_sym : forall t1 t2, join t1 t2 = join t2 t1.
Proof.
  unfold join.
  destruct t1,t2; try reflexivity.
  rewrite BSet.union_sym. reflexivity.
Qed.

Lemma join_assoc : forall t1 t2 t3, join (join t1 t2) t3 = join t1 (join t2 t3).
Proof.
  unfold join.
  intros.
  destruct t1,t2,t3; try reflexivity.
  rewrite BSet.union_assoc. reflexivity.
Qed.


Lemma join_H_l : forall ti, join H ti = H.
Proof.
  simpl. reflexivity.
Qed.

Lemma join_H_r : forall ti, join ti H = H.
Proof.
  intro. rewrite join_sym.
  apply join_H_l.
Qed.





Lemma meet_sym : forall t1 t2, meet t1 t2 = meet t2 t1.
Proof.
  unfold meet.
  destruct t1,t2; try reflexivity.
  rewrite BSet.inter_sym. reflexivity.
Qed.

Lemma meet_idem : forall t1, meet t1 t1 = t1.
Proof.
  unfold meet.
  destruct t1; try reflexivity.
  f_equal.
  apply BSet.inter_idem.
Qed.

Lemma join_bot_l : forall ti, join L ti = ti.
Proof.
  intros. simpl.
  destruct ti.
  reflexivity.
  rewrite BSet.union_empty_l.
  reflexivity.
Qed.


Lemma join_bot_r : forall t1, join t1 L = t1.
Proof.
  intro.
  rewrite join_sym.
  rewrite join_bot_l. reflexivity.
Qed.


Lemma downgrade_join : forall t1 t2,
    downgrade (join t1 t2) = join (downgrade t1) (downgrade t2).
Proof.
  intros.
  unfold downgrade.
  destruct  (join t1 t2) eqn:ISL.
  - destruct t1.
    + rewrite join_H_l. reflexivity.
    + rewrite join_bot_l.
      destruct t2 ; auto.
      discriminate.
  - destruct t1.
    discriminate.
    rewrite Typ.join_bot_l.
    destruct t2. discriminate.
    reflexivity.
Qed.


Lemma join_ub_l : forall t1 t2, order t1 (join t1 t2) = true.
Proof.
  unfold order,join.
  intros.
  destruct t1.
  reflexivity.
  destruct t2;auto.
  apply BSet.subset_union_l.
Qed.

Lemma join_lub : forall t1 t2 t3,
    order t1 t3 = true ->
    order t2 t3 = true ->
    order (join t1 t2) t3 = true.
Proof.
  unfold order.
  destruct t1,t2,t3; simpl; try congruence.
  - intros.
    apply BSet.subset_mem2.
    intros.
    rewrite BSet.mem_union_iff in H2.
    rewrite orb_true_iff in H2.
    destruct H2.
    clear H1.
    eapply BSet.subset_mem1;eauto.
    clear H0.
    eapply BSet.subset_mem1;eauto.
Qed.

Lemma join_idem : forall ti,
    join ti ti = ti.
Proof.
  destruct ti ; simpl; auto.
  f_equal. apply BSet.union_idem.
Qed.

Lemma meet_bot_l : forall t, meet L t = L.
Proof.
  destruct t0; simpl; auto.
  rewrite BSet.inter_empty.
  reflexivity.
Qed.


Lemma meet_bot_r : forall t1, meet t1 L = L.
Proof.
  intro.
  rewrite meet_sym.
  rewrite meet_bot_l. reflexivity.
Qed.

Lemma meet_top_l : forall t1, meet H t1 = t1.
Proof.
  intro.
  simpl. reflexivity.
Qed.

Lemma meet_top_r : forall t1, meet t1 H = t1.
Proof.
  intro.
  rewrite meet_sym.
  apply meet_top_l.
Qed.



Lemma meet_assoc : forall t1 t2 t3, meet (meet t1 t2) t3 = meet t1 (meet t2 t3).
Proof.
  intros.
  destruct t1.
  { simpl. reflexivity. }
  simpl.
  destruct t2; simpl; auto.
  destruct t3; simpl; auto.
  rewrite BSet.inter_assoc.
  reflexivity.
Qed.


Lemma order_mono : forall t1 t1' tu,
    order t1 t1' = true ->
    order (join t1 tu) (join t1' tu) = true.
Proof.
  intros.
  unfold order,join in *.
  destruct t1,t1',tu ; simpl in *; try congruence.
  apply BSet.subset_union2; auto.
  apply BSet.subset_refl.
Qed.


Lemma order_meet2 : forall t1 t2 t1' t2',
    order t1' t1 = true ->
    order t2' t2 = true ->
    order (meet t1' t2') (meet t1 t2) = true.
Proof.
  intros.
  destruct t1' ; simpl in *; auto.
  - destruct t1 ; try discriminate.
    simpl. auto.
  - destruct t1 ; simpl in *; auto.
    + destruct t2'; simpl; auto.
      simpl in H1. destruct t2; try discriminate; auto.
      destruct t2 ; simpl in H1 ; try discriminate; auto.
      eapply BSet.subset_trans;eauto.
      eapply BSet.subset_mem2.
      intros.
      rewrite BSet.mem_inter_iff in H2.
      rewrite andb_true_iff in H2.
      tauto.
    + destruct t2' ; simpl in *; auto.
      destruct t2 ; simpl; try discriminate.
      auto.
      destruct t2 ; simpl in *; try congruence.
      eapply BSet.subset_mem2.
      intros.
      rewrite BSet.mem_inter_iff in H2.
      rewrite andb_true_iff in H2.
      specialize  (BSet.subset_mem1 x _ _ H0 ).
      tauto.
      apply BSet.subset_mem2; intros.
      specialize  (BSet.subset_mem1 x _ _ H0 ).
      specialize  (BSet.subset_mem1 x _ _ H1 ).
      rewrite BSet.mem_inter_iff in *.
      rewrite andb_true_iff in *. tauto.
Qed.

Lemma order_meet_l : forall t1 t2,
    order (meet t1 t2) t1 = true.
Proof.
  unfold order,meet.
  destruct t1,t2; auto.
  apply BSet.subset_refl.
  apply BSet.subset_inter_l.
Qed.


Definition classify_set' (s:BSet.t) (x:t) :=
  match x with
  | H => H
  | I s' => if BSet.is_empty (BSet.inter s s') then I s' else H
  end.

Definition classify_set'' (s:BSet.t) (x:t) :=
  if is_L (Typ.meet  (I s) x) then x else H.

Lemma classify_set_eq2 : forall s x,
    classify_set' s x = classify_set'' s x.
Proof.
  unfold classify_set', classify_set''.
  destruct x; simpl.
  destruct (BSet.is_empty s);reflexivity.
  reflexivity.
Qed.



Lemma classify_H : forall s,
    classify_set s H = H.
Proof.
  unfold classify_set.
  intros.
  rewrite BSet.fold_1.
  induction (BSet.elements s).
  + simpl. reflexivity.
  + simpl. apply IHl.
Qed.

Lemma classify_set_eq : forall s x,
    classify_set s x = classify_set' s x.
Proof.
  destruct x; simpl.
  - apply classify_H.
  -
    destruct (BSet.is_empty (BSet.inter s s0)) eqn:E.
    +
      assert (EI : forall x, List.In x (BSet.elements s) ->
                             BSet.mem x s0 = true -> False).
      {
        intros.
        rewrite BSet.mem_elements in H0.
        apply BSet.is_empty_true in E.
        apply (f_equal (BSet.mem x)) in E.
        rewrite BSet.mem_inter_iff in E.
        rewrite BSet.mem_empty in E.
        rewrite andb_false_iff in E. intuition congruence.
      }
      unfold classify_set.
      rewrite BSet.fold_1.
      clear E.
      induction (BSet.elements s).
      *  simpl. reflexivity.
      * simpl.
        unfold classify at 2.
        destruct (mem a (I s0)) eqn:MA.
        exfalso.
        eapply EI;eauto.
        left. reflexivity.
        apply IHl.
        intros.
        eapply EI; eauto.
        simpl. tauto.
    + assert (EI : exists x, List.In x (BSet.elements s) /\
                               BSet.mem x s0 = true).
      {
        apply BSet.is_empty_false in E.
        destruct E.
        rewrite BSet.mem_inter_iff in H0.
        rewrite andb_true_iff in H0.
        destruct H0.
        exists x.
        split; auto.
        rewrite BSet.mem_elements. auto.
      }
      clear E.
      destruct EI.
      destruct H0.
      unfold classify_set.
      rewrite BSet.fold_1.
      induction (BSet.elements s).
      *  simpl in H0. tauto.
      * simpl.
        unfold classify at 2.
        destruct (mem a (I s0)) eqn:MA.
        { clear. induction l.
          reflexivity.
          simpl. auto. }
        {
          simpl in H0.
          destruct H0; subst.
          unfold mem in MA.
          congruence.
          apply IHl; auto.
        }
Qed.



Lemma classify_set_mono1 : forall s1 s2 ti,
    BSet.subset s1 s2 = true ->
    order (classify_set s1 ti) (classify_set s2 ti) = true.
Proof.
  intros.
  rewrite! classify_set_eq.
  unfold classify_set'.
  destruct ti; simpl; auto.
  destruct (BSet.is_empty (BSet.inter s2 s)) eqn:IE.
  assert (BSet.is_empty (BSet.inter s1 s) = true).
  {
    rewrite BSet.is_empty_true in IE.
    rewrite BSet.is_empty_true.
    apply BSet.uniq.
    intros.
    apply (f_equal (BSet.mem x)) in IE.
    rewrite BSet.mem_inter_iff in *.
    rewrite BSet.mem_empty in *.
    rewrite andb_false_iff in *.
    specialize (BSet.subset_mem1 x _ _ H0).
    destruct (BSet.mem x s1); intuition congruence.
  }
  rewrite H1. apply order_refl.
  destruct (BSet.is_empty (BSet.inter s1 s)); reflexivity.
Qed.


Lemma classify_set_g1_g2 : forall g1 g2 x,
    classify_set g1 (classify_set g2 x) = classify_set (BSet.union g1 g2) x.
Proof.
  intros.
  rewrite! classify_set_eq.
  unfold classify_set'.
  destruct x; simpl. auto.
  rewrite BSet.inter_union_distr.
  rewrite BSet.is_empty_union.
  destruct (BSet.is_empty (BSet.inter g2 s));
    destruct (BSet.is_empty (BSet.inter g1 s)); reflexivity.
Qed.



Lemma classify_set_subset_fix : forall g1 g2 ti,
    BSet.subset g1 g2 = true ->
    classify_set g2 ti = ti ->
    Typ.classify_set g1 ti = ti.
Proof.
  intros.
  rewrite <- H1.
  rewrite classify_set_g1_g2.
  f_equal.
  apply BSet.uniq.
  intros.
  rewrite BSet.mem_union_iff.
  specialize (BSet.subset_mem1 x _ _ H0).
  destruct (BSet.mem x g2) eqn:MG2.
  rewrite orb_comm. reflexivity.
  destruct (BSet.mem x g1); intuition congruence.
Qed.

Lemma classify_set_is_empty : forall s x,
    BSet.is_empty s = true ->
    classify_set s x = x.
Proof.
  intros.
  apply BSet.is_empty_true in H0.
  subst.
  rewrite classify_set_eq.
  rewrite classify_set_eq2.
  unfold classify_set''.
  rewrite meet_bot_l.
  reflexivity.
Qed.

Lemma classify_set_classify : forall x ti,
    classify_set (BSet.singleton x) ti = Typ.classify x ti.
Proof.
  intros.
  rewrite classify_set_eq.
  rewrite classify_set_eq2.
  unfold classify_set'', classify.
  destruct (mem x ti) eqn:MEM;
    destruct (is_L (meet (I (BSet.singleton x)) ti)) eqn:ISL; auto.
  - apply is_L_inv in ISL.
    destruct ti ; simpl in ISL, MEM; auto.
    apply inj_I in ISL.
    apply (f_equal (BSet.mem x)) in ISL.
    rewrite BSet.mem_inter_iff in ISL.
    rewrite BSet.mem_empty in ISL.
    rewrite BSet.singleton_xx in ISL.
    simpl in ISL; congruence.
  - destruct ti ; simpl; auto.
    simpl in *.
    exfalso.
    rewrite <- not_true_iff_false in ISL.
    apply ISL.
    apply BSet.is_empty_true.
    apply BSet.uniq.
    intros.
    rewrite BSet.mem_inter_iff.
    rewrite BSet.mem_empty.
    destruct (  BSet.mem x0 (BSet.singleton x)) eqn:MS.
    rewrite BSet.mem_singleton in MS. subst. auto.
    reflexivity.
Qed.

Lemma notH_isH : forall ti, ti <> H <-> is_H ti = false.
Proof.
  destruct ti; simpl; intuition congruence.
Qed.

Lemma is_L_singleton : forall p,
    Typ.is_L (Typ.singleton p) = false.
Proof.
  intros.
  destruct (is_L (singleton p)) eqn:ISL;auto.
  apply is_L_inv in ISL.
  apply singleton_not_L in ISL.
  tauto.
Qed.

Lemma order_downgrade_mono : forall t1 t2,
    order t1 t2 = true ->
    order (downgrade t1) (downgrade t2) = true.
Proof.
  unfold order.
  destruct t1,t2 ; simpl; auto.
Qed.

Lemma order_downgrade : forall t1,
    order (downgrade t1) t1 = true.
Proof.
  destruct t1 ; simpl; auto.
Qed.

Lemma downgrade_idem : forall ti, downgrade (downgrade ti) = downgrade ti.
Proof.
  destruct ti; reflexivity.
Qed.

Lemma downgrade_set_join : forall s t1 t2,
    downgrade_set s (join t1 t2) =
      join (downgrade_set s t1) (downgrade_set s t2).
Proof.
  intros.
  unfold join. destruct t1,t2.
  - simpl. reflexivity.
  - simpl. reflexivity.
  - simpl. reflexivity.
  - simpl. f_equal.
    apply BSet.diff_union_l.
Qed.

Lemma downgrade_set_guard : forall s t1 t2,
    downgrade_set s (guard t1 t2) =
      guard t1 (downgrade_set s t2).
Proof.
  unfold guard.
  intros. destruct (is_L t1) eqn:isL.
  apply downgrade_set_L.
  auto.
Qed.

Lemma downgrade_set_singleton : forall s x,
    downgrade_set s (singleton x) =
      if BSet.mem x s then L else singleton x.
Proof.
  unfold downgrade_set,singleton.
  intros.
  destruct (BSet.mem x s) eqn:M.
  -
  unfold L. f_equal.
  apply BSet.uniq.
  intros.
  rewrite BSet.mem_diff_iff.
  rewrite BSet.mem_empty.
  rewrite andb_false_iff.
  destruct (Pos.eq_dec x x0).
  right. subst. subset. subset.
  - f_equal.
    apply BSet.uniq.
    intros.
    subset.
Qed.

Lemma classify_set_downgrade_set_idem :
  forall s1 s2 ti,
         BSet.subset s2 s1 = true ->
         classify_set s2 (downgrade_set s1 ti) = downgrade_set s1 ti.
Proof.
  unfold downgrade_set.
  intros.
  rewrite !classify_set_eq.
  unfold classify_set'.
  destruct ti; simpl;auto.
  destruct (BSet.is_empty (BSet.inter s2 (BSet.diff s s1))) eqn:E1;auto.
  - exfalso.
    revert E1.
    apply eq_true_false_abs.
    rewrite BSet.is_empty_true.
    apply BSet.uniq.
    intro.
    rewrite BSet.subset_mem_iff in H0.
    specialize (H0 x).
    rewrite BSet.mem_inter_iff in *.
    rewrite BSet.mem_empty in *.
    rewrite andb_false_iff in *.
    rewrite BSet.mem_diff_iff.
    rewrite andb_false_iff.
    rewrite negb_false_iff.
    destruct (BSet.mem x s2);auto.
Qed.



Lemma downgrade_set_classify_set :
  forall s1 s2 ti
         (I : BSet.inter s1 s2 = BSet.empty),
    downgrade_set s1 (classify_set s2 ti) = classify_set s2 (downgrade_set s1 ti).
Proof.
  unfold downgrade_set.
  intros.
  rewrite !classify_set_eq.
  unfold classify_set'.
  destruct ti; simpl;auto.
  destruct (BSet.is_empty (BSet.inter s2 s)) eqn:E;
    destruct (BSet.is_empty (BSet.inter s2 (BSet.diff s s1))) eqn:E1;auto.
  - exfalso.
    apply BSet.is_empty_true in E.
    revert E1.
    apply eq_true_false_abs.
    rewrite BSet.is_empty_true.
    apply BSet.uniq.
    intro.
    apply (f_equal (BSet.mem x)) in E.
    rewrite BSet.mem_inter_iff in *.
    rewrite BSet.mem_empty in *.
    rewrite andb_false_iff in *.
    rewrite BSet.mem_diff_iff.
    destruct E. tauto.
    rewrite H0. simpl. intuition congruence.
  - exfalso.
    apply BSet.is_empty_true in E1.
    revert E.
    apply eq_true_false_abs.
    rewrite BSet.is_empty_true.
    apply BSet.uniq.
    intro.
    apply (f_equal (BSet.mem x)) in E1.
    apply (f_equal (BSet.mem x)) in I0.
    rewrite BSet.mem_empty in *.
    rewrite BSet.mem_inter_iff in *.
    rewrite BSet.mem_diff_iff in *.
    rewrite andb_false_iff in *.
    destruct E1. tauto.
    rewrite andb_false_iff in *.
    destruct H0. tauto.
    destruct I0;try tauto.
    rewrite H1 in H0.
    discriminate.
Qed.

Lemma downgrade_set_empty : forall ti,
    downgrade_set BSet.empty ti = ti.
Proof.
  unfold downgrade_set.
  destruct ti. auto.
  rewrite BSet.diff_empty_r.
  reflexivity.
Qed.

Lemma downgrade_set_idem : forall s t,
    BSet.inter (to_set t) s = BSet.empty ->
    downgrade_set s t = t.
Proof.
  unfold downgrade_set. intros.
  unfold to_set in H0.
  destruct t0.
  - reflexivity.
  - f_equal.
    apply BSet.uniq.
    intro.
    apply (f_equal (BSet.mem x)) in H0.
    rewrite BSet.mem_diff_iff.
    rewrite BSet.mem_inter_iff in H0.
    rewrite BSet.mem_empty in H0.
    rewrite andb_false_iff in H0.
    destruct H0. rewrite H0.
    reflexivity.
    rewrite H0.
    rewrite andb_comm. reflexivity.
Qed.

Lemma downgrade_set_still_ordered :
  forall d t1 t2,
    BSet.inter d (BSet.inter (Typ.to_set t1) (Typ.to_set t2)) = BSet.empty ->
    order t1 t2  = true ->
    order t1 (downgrade_set d t2) = true.
Proof.
  unfold downgrade_set.
  destruct t2.
  - intros. apply order_top.
  - destruct t1; simpl. auto.
    intros.
    rewrite BSet.subset_mem_iff in *.
    intro.
    rewrite BSet.mem_diff_iff.
    specialize (H1 x).
    rewrite andb_true_iff.
    intro.
    split;auto.
    destruct (BSet.mem x d) eqn:M;auto.
    specialize (H1 H2).
    apply (f_equal (BSet.mem x)) in H0.
    rewrite! BSet.mem_inter_iff in H0.
    rewrite BSet.mem_empty in H0.
    rewrite !andb_false_iff in H0.
    intuition congruence.
Qed.

Lemma to_set_downgrade_set : forall d ti,
    to_set (downgrade_set d ti) = BSet.diff (to_set ti) d.
Proof.
  unfold downgrade_set.
  destruct ti;simpl;auto.
  rewrite BSet.diff_empty_l. reflexivity.
Qed.

Lemma to_set_classify_set : forall s ti,
    to_set (classify_set s ti) = if BSet.is_empty (BSet.inter s (to_set ti))
                                 then to_set ti else BSet.empty.
Proof.
  intros.
  rewrite classify_set_eq.
  unfold classify_set'.
  destruct ti.
  -
    simpl. rewrite BSet.inter_sym. rewrite BSet.inter_empty.
    rewrite BSet.is_empty_empty. reflexivity.
  - simpl.
    destruct (BSet.is_empty (BSet.inter s s0));auto.
Qed.

Lemma classify_set_idem : forall s ti,
    (BSet.inter s (to_set ti) = BSet.empty) ->
    classify_set s ti = ti.
Proof.
  intros.
  rewrite classify_set_eq.
  unfold classify_set'.
  destruct ti; auto.
  simpl in H0. rewrite H0.
  rewrite BSet.is_empty_empty. reflexivity.
Qed.
