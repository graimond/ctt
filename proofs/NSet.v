(** Canonic PositiveSet.t *)
Require Import Std.
Require Import Bool ZArith ZifyBool Lia.
Require Import FSetPositive.
Require FMapPositive.
Import PositiveSet.
Import FMapPositive.

(*
  - [wf] ensures a normal form.
  - [normalise] builds a normal form from an arbitrary set
*)

Module WF.

Definition is_leaf (s:t) :=
  match s with
  | Leaf => true
  | _    => false
  end.

Lemma is_leaf_inv : forall s, is_leaf s = true <-> s = Leaf.
Proof.
  destruct s; simpl; intuition congruence.
Qed.

Fixpoint wf (s:t) :=
    match s with
    | Leaf => true
    | Node l o r => wf l && wf r && (if is_empty s then is_leaf s else true)
    end.


Fixpoint normalise (s: tree) :=
  match s with
  | Leaf => Leaf
  | Node l o r => match normalise l , o, normalise r with
                  | Leaf , false , Leaf => Leaf
                  | l , o , r => Node l o r
                  end
  end.

Lemma wf_normalise : forall s,
    wf (normalise s) = true.
Proof.
  induction s; simpl; auto.
  destruct (normalise s1) eqn:S1.
  -  destruct b; simpl.
     rewrite IHs2. reflexivity.
     destruct (normalise s2) eqn:S2.
     reflexivity.
     simpl in *.
     rewrite! andb_true_iff in *.
     dand.
     split_and;auto.
  - simpl in *.
    rewrite! andb_true_iff in *.
    dand.
    split_and;auto.
    destruct b ; auto.
    simpl. destruct b0; auto.
    simpl in *. destruct (is_empty t0_1) eqn:E1; auto.
    destruct (is_empty t0_2) eqn:E2;auto.
    destruct (is_empty (normalise s2)) eqn:E3; auto.
Qed.

Lemma mem_normalise : forall s x,
    mem x (normalise s) = mem x s.
Proof.
  induction s;simpl; auto.
  destruct x ; simpl; auto.
  - rewrite <- IHs2.
    destruct (normalise s1).
    destruct b. simpl. auto.
    destruct (normalise s2); simpl; auto.
    simpl. reflexivity.
  -rewrite <- IHs1.
    destruct (normalise s1).
    destruct b; auto.
    destruct (normalise s2); simpl; auto.
    simpl. reflexivity.
  -
    destruct (normalise s1); try reflexivity.
    destruct b; try reflexivity.
    destruct (normalise s2); try reflexivity.
Qed.

Lemma is_empty_leaf :
  forall s
         (WF : wf s = true),
    (forall x, mem x s = false) ->
  Leaf = s.
Proof.
  induction s; simpl.
  - auto.
  - intros.
    exfalso.
    rewrite !andb_true_iff in WF.
    dand.
    rewrite (H xH) in WF1.
    simpl in WF1.
    destruct (is_empty s1) eqn:E1; try discriminate.
    destruct (is_empty s2) eqn:E2; try discriminate.
    rewrite <- IHs2 in E2;auto. discriminate.
    intros.
    apply (H (xI x)).
    rewrite <- IHs1 in E1;auto. discriminate.
    intros.
    apply (H (xO x)).
Qed.

Lemma is_empty_mem2 : forall s, is_empty s = false -> exists x, mem x s = true.
Proof.
  induction s; simpl.
  - discriminate.
  - destruct b; try discriminate.
    simpl. intros.
    exists xH. reflexivity.
    simpl.
    destruct (is_empty s1) eqn:E1.
    intros.
    destruct (IHs2 H) as (x1 & M).
    exists (xI x1); auto.
    intros.
    destruct (IHs1 Logic.eq_refl) as (x1 & M).
    exists (xO x1);auto.
Qed.


Lemma elements_normalise : forall s,
    elements (normalise s) = elements s.
Proof.
  unfold elements.
  intro.
  generalize 1%positive.
  generalize (@nil elt).
  induction s;simpl; auto.
  - intros.
    rewrite <- IHs2.
    destruct (normalise s1).
    destruct b.
    + simpl.
      rewrite <- IHs1. simpl.
      reflexivity.
    + rewrite <- IHs1. simpl.
      destruct (normalise s2); simpl; auto.
    + rewrite <- IHs1.
      rewrite IHs2.
      simpl. rewrite IHs2.
      destruct b,b0;simpl;auto.
      rewrite <- IHs1. simpl. auto.
      rewrite <- IHs1; simpl;auto.
Qed.


Lemma uniq : forall s1 s2
    (WF1 : wf s1 = true) (WF2 : wf s2 = true),
    (forall x, mem x s1 = mem x s2) ->
    s1 = s2.
Proof.
  induction s1; simpl.
  - intros.
    eapply is_empty_leaf; auto.
  - intros.
    rewrite! andb_true_iff in WF1.
    dand.
    destruct s2.
    + exfalso.
      generalize (H xH).
      simpl. intro. subst.
      simpl in WF3.
      destruct (is_empty s1_1) eqn:E1; try discriminate.
      destruct (is_empty s1_2) eqn:E2; try discriminate.
      apply is_empty_mem2 in E2.
      destruct E2 as (x & MEM).
      specialize (H (xI x)).
      simpl in H. congruence.
      apply is_empty_mem2 in E1.
      destruct E1 as (x & MEM).
      specialize (H (xO x)).
      simpl in H. congruence.
    +
      simpl in WF2. rewrite! andb_true_iff in WF2.
      dand.
      f_equal; auto.
      * eapply IHs1_1; auto.
       intros.
       apply (H (xO x)).
      * apply (H xH).
      * eapply IHs1_2; auto.
        intros.
        apply (H (xI x)).
Qed.

    
Fixpoint subset (s1 s2: tree) : bool :=
  match s1 with
  | Leaf => true
  | Node l o r => match s2 with
                  | Leaf => false
                  | Node l' o' r' => subset l l' && subset r r' && (implb o o')
                  end
  end.

Lemma mem_subset1 : forall x s1 s2,
    subset s1 s2 = true ->
    mem x s1 = true -> mem x s2 = true.
Proof.
  induction x; simpl.
  - destruct s1; simpl; auto.
    discriminate.
    destruct s2 ; simpl; auto.
    intros.
    rewrite! andb_true_iff in H.
    dand.
    eapply IHx;eauto.
  - destruct s1; simpl; auto.
    discriminate.
    destruct s2 ; simpl; auto.
    intros.
    rewrite! andb_true_iff in H.
    dand.
    eapply IHx;eauto.
  - destruct s1;simpl.
    discriminate.
    destruct s2;simpl;auto.
    intros.
    rewrite! andb_true_iff in H.
    dand.
    destruct b,b0; try discriminate; auto.
Qed.

Lemma is_empty_mem1 : forall s, is_empty s = true -> forall x, mem x s = false.
Proof.
  induction s;simpl.
  - reflexivity.
  - destruct b; try discriminate.
    simpl. destruct (is_empty s1) eqn:E1.
    intros.
    destruct x;auto.
    discriminate.
Qed.


Lemma mem_subset2 :
  forall s1 (WF : wf s1 = true)  s2,
    (forall x, mem x s1 = true -> mem x s2 = true) ->
    subset s1 s2 = true.
Proof.
  induction s1.
  - reflexivity.
  -  intros.
     simpl in WF.
     rewrite! andb_true_iff in WF.
     dand.
     destruct s2; simpl.
     + destruct b; try discriminate.
       apply (H xH). reflexivity.
       simpl in WF1.
       destruct (is_empty s1_1) eqn:E1;try discriminate.
       destruct (is_empty s1_2) eqn:E2;try discriminate.
       apply is_empty_mem2 in E2.
       destruct E2 as (x & MEM).
       specialize (H (xI x)).
       simpl in H. exfalso. intuition.
       apply is_empty_mem2 in E1.
       destruct E1 as (x & MEM).
       specialize (H (xO x)).
       simpl in H. exfalso. intuition.
     +
       rewrite! andb_true_iff.
       split_and.
       * eapply IHs1_1;eauto.
       intros.
       specialize (H (xO x)).
       simpl in H. auto.
       * eapply IHs1_2;eauto.
       intros.
       specialize (H (xI x)).
       simpl in H. auto.
       * destruct b ; try discriminate; auto.
         specialize (H xH).
         simpl in H. simpl;auto.
Qed.

Lemma subset_refl : forall s, subset s s = true.
Proof.
  induction s; simpl; auto.
  rewrite !andb_true_iff. split_and;auto.
  destruct b; reflexivity.
Qed.

Lemma subset_trans :
  forall s1 s2 s3
         (WF : wf s1 = true),
    subset s1 s2 = true ->
    subset s2 s3 = true ->
    subset s1 s3 = true.
Proof.
  intros.
  eapply mem_subset2;auto.
  intros.
  eapply mem_subset1 in H; eauto.
  eapply mem_subset1 in H0; eauto.
Qed.

Lemma wf_is_empty : forall s (WF : wf s = true),
    is_leaf s = is_empty s.
Proof.
  induction s; simpl.
  - auto.
  - intros.
    rewrite !andb_true_iff in WF.
    dand.
    destruct b; auto.
    simpl in *.
    destruct (is_empty s1) eqn:E1; auto.
    destruct (is_empty s2) eqn:E2; auto.
Qed.

End WF.

Module PRESET.
  (* results over non-normalised *)
  Import PositiveSet.

  Lemma add_spec : forall x x' s,
      mem x (PositiveSet.add x' s) = if Pos.eq_dec x x' then true else mem x s.
  Proof.
    intros.
    specialize (add_spec x' x s).
    unfold In.
    destruct (Pos.eq_dec x x'); destruct (mem x (add x' s)); intuition try congruence.
    apply H; auto.
    destruct (mem x s); tauto.
  Qed.

  Lemma mleaf : forall x, mem x empty = false.
  Proof.
    destruct x; reflexivity.
  Qed.

  Lemma mem_union_iff : forall x s1 s2,
      mem x (union s1 s2) = (mem x s1 || mem x s2).
  Proof.
    intros.
    destruct (PositiveSet.mem x s1 || PositiveSet.mem x s2) eqn:M.
    - apply PositiveSet.mem_1.
      rewrite PositiveSet.union_spec.
      rewrite orb_true_iff in M.
      destruct M as [M|M];apply PositiveSet.mem_2 in M ; tauto.
    - rewrite orb_false_iff in M.
      destruct M.
      destruct (PositiveSet.mem x (PositiveSet.union s1 s2)) eqn:C; auto.
      apply PositiveSet.mem_2 in C.
      rewrite PositiveSet.union_spec in C.
      destruct C.
      apply PositiveSet.mem_1 in H1. congruence.
      apply PositiveSet.mem_1 in H1. congruence.
  Qed.


  Lemma mem_inter_iff : forall x s1 s2,
      mem x (inter s1 s2) = (mem x s1 && mem x s2).
  Proof.
    intros.
    destruct (PositiveSet.mem x s1 && PositiveSet.mem x s2) eqn:M.
    - apply PositiveSet.mem_1.
      rewrite PositiveSet.inter_spec.
      rewrite andb_true_iff in M.
      unfold In. auto.
    - rewrite andb_false_iff in M.
      destruct M;
        destruct (PositiveSet.mem x (PositiveSet.inter s1 s2)) eqn:C; auto;
      apply PositiveSet.mem_2 in C;
      rewrite PositiveSet.inter_spec in C;
      destruct C.
      apply PositiveSet.mem_1 in H1. congruence.
      apply PositiveSet.mem_1 in H1. congruence.
  Qed.

  Lemma mem_diff_iff : forall x s1 s2,
      mem x (diff s1 s2) = (mem x s1 && negb (mem x s2)).
  Proof.
    intros.
    destruct (PositiveSet.mem x s1 && (negb (PositiveSet.mem x s2))) eqn:M.
    - apply PositiveSet.mem_1.
      rewrite diff_spec.
      rewrite andb_true_iff in M.
      rewrite negb_true_iff in M.
      unfold In. destruct (mem x s2); intuition congruence.
    - rewrite andb_false_iff in M.
      destruct M;
        destruct (PositiveSet.mem x (PositiveSet.diff s1 s2)) eqn:C; auto;
      apply PositiveSet.mem_2 in C;
      rewrite PositiveSet.diff_spec in C;
      destruct C.
      unfold In in H0. congruence.
      unfold In in *.
      destruct (mem x s2) ; simpl in *; intuition congruence.
  Qed.



  Lemma subset_mem : forall x s1 s2,
      subset s1 s2 = true ->
      mem x s1 = true ->
      mem x s2 = true.
  Proof.
    unfold mem,subset.
    intros.
    apply PositiveSet.mem_1.
    apply PositiveSet.mem_2 in H0.
    apply PositiveSet.subset_2 in H.
    unfold PositiveSet.Subset in H.
    auto.
  Qed.

  Lemma is_empty_false :
    forall s
           (WF : WF.wf s = true),
      WF.is_leaf s = false ->
      exists x, mem x s = true.
  Proof.
    intros.
    rewrite WF.wf_is_empty in H; auto.
    apply WF.is_empty_mem2 ;auto.
  Qed.

  Lemma mem_elements : forall x s,
      List.In x (elements s) <-> mem x s = true.
  Proof.
    intros.
    assert (forall s, PositiveSet.mem x s = true <-> PositiveSet.In x s).
    {
      split.
      apply PositiveSet.mem_2.
      apply PositiveSet.mem_1.
    }
    unfold mem.
    rewrite H.
    split. intros.
    apply PositiveSet.elements_2.
    apply FMapPositive.PositiveMap.ME.MO.ListIn_In; auto.
    intro.
    apply PositiveSet.elements_1 in H0.
    induction (elements s).
    inv H0.
    inv H0. left. symmetry. apply H2.
    right. tauto.
  Qed.

  Lemma mem_elements' : forall x s,
      mem x s = List.existsb (fun y => Pos.eqb x y) (elements s).
  Proof.
    intros.
    destruct (mem x s) eqn:MEM.
    rewrite <- mem_elements in MEM.
    symmetry.
    rewrite List.existsb_exists.
    exists x. split ; auto.
    apply Pos.eqb_refl.
    destruct (List.existsb (fun y : positive => (x =? y)%positive) (elements s)) eqn:EX;auto.
    rewrite List.existsb_exists in EX.
    destruct EX. destruct H.
    apply Peqb_true_eq in H0.
    subst. rewrite mem_elements in H. congruence.
  Qed.




  Definition map (f : positive -> positive) (s : t) : t :=
    PositiveSet.fold (fun x s => PositiveSet.add (f x) s) s PositiveSet.empty.

  Lemma mem_fold_add : forall f l acc,
      forall x,
      mem x (List.fold_left
               (fun (a : t) (e : elt) => add (f e) a)
               l acc) = (mem x acc || List.existsb (fun y => Pos.eqb (f y) x) l).
  Proof.
    induction l.
    - simpl.
      intros. rewrite orb_false_r.
      reflexivity.
    - simpl.
      intros.
      rewrite IHl.
      rewrite add_spec.
      destruct (Pos.eq_dec x (f a)).
      + subst.
        rewrite Pos.eqb_refl.
        simpl. rewrite orb_true_r. reflexivity.
      + assert (((f a =? x)%positive = false)).
        { destruct (f a =? x)%positive eqn:E; auto.
          apply Peqb_true_eq in E.
          congruence.
        }
        rewrite H.
        simpl. reflexivity.
  Qed.

    
  Lemma mem_map : forall f s,
    forall x,  mem x (map f s) = List.existsb (fun y => Pos.eqb (f y)  x) (elements s).
  Proof.
    intros.
    unfold map.
    rewrite fold_1.
    rewrite mem_fold_add.
    rewrite mleaf. reflexivity.
  Qed.

  Lemma mem_map' :
    forall f s
           (INJ : forall x y, f x = f y -> x = y),
    forall x,  mem (f x) (map f s) = mem x s.
  Proof.
    intros.
    unfold map.
    rewrite fold_1.
    rewrite mem_fold_add.
    rewrite mleaf. simpl.
    apply eq_true_iff_eq.
    rewrite <- mem_elements.
    induction (elements s); simpl.
    intuition congruence.
    rewrite <- IHl.
    rewrite orb_true_iff.
    rewrite Pos.eqb_eq.
    intuition try congruence.
    apply INJ in H2. tauto.
  Qed.


  Lemma elements_union : forall p s1 s2,
      List.existsb p (elements (union s1 s2)) =
        (List.existsb p (elements s1) ||
           List.existsb p (elements s2)).
  Proof.
    intros.
    rewrite <- List.existsb_app.
    destruct  (List.existsb p (elements s1 ++ elements s2)) eqn:EAPP.
    -
      rewrite List.existsb_exists in *.
      destruct EAPP.
      destruct H. exists x.
      split ; auto.
      rewrite List.in_app_iff in H.
      rewrite mem_elements.
      rewrite mem_union_iff.
      destruct H; rewrite mem_elements in H.
      rewrite H. reflexivity.
      rewrite H. rewrite orb_comm. reflexivity.
    - destruct (List.existsb p (elements (union s1 s2))) eqn:EAPP';auto.
      rewrite List.existsb_exists in EAPP'.
      rewrite List.existsb_app in EAPP.
      destruct EAPP'.
      destruct H.
      rewrite mem_elements in H.
      rewrite mem_union_iff in H.
      destruct (mem x s1) eqn:MSI;try discriminate.
      rewrite <- mem_elements in MSI.
      assert (List.existsb p (elements s1) = true).
      {
        rewrite List.existsb_exists.
        exists x; split; auto.
      }
      rewrite H1 in EAPP; discriminate.
      simpl in H.
      rewrite <- mem_elements in H.
      assert (List.existsb p (elements s2) = true).
      {
        rewrite List.existsb_exists.
        exists x; split; auto.
      }
      rewrite H1 in EAPP.
      rewrite orb_comm in EAPP.
      discriminate.
  Qed.


End PRESET.


Module BSet.
  (* lift *)
  Record t :=
    mk
      {
        set : PositiveSet.t;
        prf : WF.wf set = true
      }.

  Definition empty := mk PositiveSet.empty (Logic.eq_refl (WF.wf PositiveSet.empty)).

  Definition nmake (s:PositiveSet.t) :=
    let s' := WF.normalise s in
    mk s' (WF.wf_normalise s).

  Definition union (s1 s2: t) := nmake (PositiveSet.union (set s1) (set s2)).

  Definition inter (s1 s2: t) := nmake (PositiveSet.inter (set s1) (set s2)).

  Definition diff (s1 s2: t) := nmake (PositiveSet.diff (set s1) (set s2)).

  Definition is_empty (s:t) := WF.is_leaf (set s).

  Definition subset (s1 s2:t) := WF.subset (set s1) (set s2).

  Definition singleton (x:positive) := nmake (PositiveSet.singleton x).

  Definition add (x:positive) (s:t) := nmake (PositiveSet.add x (set s)).


  Definition mem (x:positive) (s:t) := PositiveSet.mem x (set s).

  Lemma wf_set : forall s,
      WF.wf (set s) = true.
  Proof.
    destruct s; auto.
  Qed.

 #[global]  Hint Resolve wf_set : core.

  Lemma singleton_xx : forall x,
      mem x (singleton x) = true.
  Proof.
    unfold mem,singleton.
    intros.
    unfold PositiveSet.singleton.
    unfold nmake. simpl.
    rewrite WF.mem_normalise.
    rewrite PRESET.add_spec. destruct (Pos.eq_dec x x);auto.
  Qed.



  Lemma mem_singleton : forall x' x,
      mem x' (singleton x) = true  <-> x' = x.
  Proof.
    unfold mem,singleton.
    intros.
    unfold PositiveSet.singleton.
    unfold nmake. simpl.
    rewrite WF.mem_normalise.
    rewrite PRESET.add_spec.
    rewrite PRESET.mleaf.
    destruct (Pos.eq_dec x' x); intuition congruence.
  Qed.

  Lemma mem_union_iff : forall x s1 s2,
      mem x (union s1 s2) = (mem x s1 || mem x s2).
  Proof.
    intros.
    unfold mem,union,nmake; simpl.
    rewrite <- PRESET.mem_union_iff.
    rewrite WF.mem_normalise. reflexivity.
  Qed.

  Lemma mem_inter_iff : forall x s1 s2,
      mem x (inter s1 s2) = (mem x s1 && mem x s2).
  Proof.
    intros.
    unfold mem,inter,nmake; simpl.
    rewrite <- PRESET.mem_inter_iff.
    rewrite WF.mem_normalise. reflexivity.
  Qed.

  Lemma mem_diff_iff : forall x s1 s2,
      mem x (diff s1 s2) = (mem x s1 && negb (mem x s2)).
  Proof.
    intros.
    unfold mem,diff,nmake; simpl.
    rewrite <- PRESET.mem_diff_iff.
    rewrite WF.mem_normalise. reflexivity.
  Qed.

  Lemma subset_mem1 : forall x s1 s2,
      subset s1 s2 = true ->
      mem x s1 = true ->
      mem x s2 = true.
  Proof.
    intros.
    eapply WF.mem_subset1; eauto.
  Qed.

  Lemma subset_mem2 : forall s1 s2,
      (forall x, mem x s1 = true ->
                 mem x s2 = true) ->
      subset s1 s2 = true.
  Proof.
    intros.
    eapply WF.mem_subset2; eauto.
  Qed.

  Lemma subset_mem_iff : forall s1 s2,
      subset s1 s2 = true <->       (forall x, mem x s1 = true ->
                                               mem x s2 = true).
  Proof.
    split; intros.
    eapply subset_mem1; eauto.
    apply subset_mem2; auto.
  Qed.


  Lemma subset_diff_l : forall s1 s2,
      BSet.subset (BSet.diff s1 s2) s1 = true.
  Proof.
    intros.
    rewrite subset_mem_iff.
    intros.
    rewrite mem_diff_iff in H.
    rewrite andb_true_iff in H. tauto.
  Qed.

  Lemma subset_mem_f: forall x (s1 s2 : t),
      subset s1 s2 = true ->
      mem x s2 = false -> mem x s1 = false.
  Proof.
    intros.
    rewrite subset_mem_iff in H.
    specialize (H x).
    destruct (mem x s1) eqn:MEM;auto.
    intuition congruence.
  Qed.

  Lemma subset_refl : forall s,
      subset s s = true.
  Proof.
    unfold subset.
    intros.
    apply WF.subset_refl.
  Qed.

  Lemma subset_trans : forall s1 s2 s3,
      subset s1 s2 = true ->
      subset s2 s3 = true ->
      subset s1 s3 = true.
  Proof.
    unfold subset.
    intros.
    eapply WF.subset_trans; eauto.
  Qed.



  Lemma subset_union2 : forall s1 s1' s2 s2',
    subset s1 s1' = true ->
    subset s2 s2' = true ->
    subset (union s1 s2) (union s1' s2') = true.
  Proof.
    intros.
    rewrite subset_mem_iff in *.
    intros.
    rewrite mem_union_iff in *.
    rewrite orb_true_iff in *.
    specialize (H x).
    specialize (H0 x).
    tauto.
  Qed.

  Lemma subset_union_l : forall s1 s2,
      subset s1 (union s1 s2) = true.
  Proof.
    intros. apply subset_mem_iff.
    intros.
    rewrite mem_union_iff. rewrite orb_true_iff.
    tauto.
  Qed.

  Lemma uniq : forall s1 s2,
      (forall x, mem x s1 = mem x s2) ->
    s1 = s2.
  Proof.
    destruct s1,s2.
    intros.
    unfold mem in H. simpl in H.
    assert (set0 = set1).
    {
      apply WF.uniq; auto.
    }
    subst.
    f_equal.
    apply Eqdep_dec.UIP_dec.
    apply bool_dec.
  Qed.

  Lemma union_sym : forall s1 s2,
     union s1 s2 = union s2 s1.
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite !mem_union_iff.
    rewrite orb_comm. reflexivity.
  Qed.

  Lemma union_assoc : forall s1 s2 s3,
      union s1 (union s2 s3) =
        union (union s1 s2) s3.
  Proof.
    intros.
    apply uniq. intros.
    rewrite !mem_union_iff.
    rewrite orb_assoc. reflexivity.
  Qed.

  Lemma union_singleton : forall x s,
      add x s = union (singleton x) s.
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite mem_union_iff.
    unfold add.
    unfold mem, nmake.
    simpl.
    rewrite !WF.mem_normalise.
    unfold PositiveSet.singleton.
    rewrite !PRESET.add_spec.
    rewrite PRESET.mleaf.
    destruct (Pos.eq_dec x0 x);auto.
  Qed.

  Lemma mem_empty : forall x, mem x empty = false.
  Proof.
    unfold mem.
    intros.
    rewrite PRESET.mleaf.
    reflexivity.
  Qed.

  Lemma union_empty : forall s, union s empty = s.
  Proof.
    intros.
    apply uniq.
    intros. rewrite mem_union_iff.
    rewrite mem_empty.
    rewrite orb_comm. reflexivity.
  Qed.

  Lemma union_empty_l : forall s, union empty s = s.
  Proof.
    intros.
    apply uniq.
    intros. rewrite mem_union_iff.
    rewrite mem_empty.
    reflexivity.
  Qed.

  Lemma is_empty_true : forall s, is_empty s = true <-> s = empty.
  Proof.
    intros.
    split; intros.
    -
      apply uniq.
      intros.
      unfold is_empty in H.
      rewrite mem_empty.
      apply WF.is_leaf_inv in H.
      unfold mem. rewrite H. rewrite PRESET.mleaf.
      reflexivity.
    -  subst. reflexivity.
  Qed.

  Lemma is_empty_empty : is_empty empty = true.
  Proof.
    apply is_empty_true.
    reflexivity.
  Qed.

  Lemma subset_empty : forall g2 g1 s,
      BSet.subset g2 g1 = true ->
      BSet.inter g1 s = BSet.empty ->
      BSet.inter g2 s = BSet.empty.
  Proof.
    intros.
    apply uniq.
    intros.
    apply (f_equal (mem x)) in H0.
    specialize (subset_mem1 x _ _ H).
    rewrite mem_inter_iff in *.
    rewrite mem_empty in *.
    rewrite andb_false_iff in *.
    intuition.
    destruct (mem x g2); intuition congruence.
  Qed.

  Lemma union_idem : forall s,
      union s s = s.
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite !mem_union_iff.
    apply orb_diag.
  Qed.


  Lemma union_empty_inv : forall s1 s2,
      union s1 s2  = empty <-> (s1 = empty /\ s2 = empty).
  Proof.
    intros.
    split.
    - split;
      apply uniq;
      intros;
      apply (f_equal (mem x)) in H;
      rewrite mem_union_iff in H;
      rewrite orb_false_iff in H;
      rewrite mem_empty in *; tauto.
    - intros. destruct H; subst.
      rewrite union_idem.
      reflexivity.
  Qed.

  Definition fold {B: Type} (F : positive -> B -> B) (s:t) (acc: B) :=
    PositiveSet.fold F (set s) acc.

  Definition elements (s:t) := PositiveSet.elements (set s).



  Lemma fold_1 : forall {A: Type} f s (i:A),
      fold f s i = List.fold_left (fun (a : A) (e : elt) => f e a) (elements s) i.
  Proof.
    unfold fold. intros.
    rewrite  PositiveSet.fold_1.
    reflexivity.
  Qed.


  Lemma inter_sym : forall s1 s2,
      inter s1 s2 = inter s2 s1.
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite !mem_inter_iff.
    rewrite andb_comm. reflexivity.
  Qed.

  Lemma inter_idem : forall s,
      inter s s = s.
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite !mem_inter_iff.
    apply andb_diag.
  Qed.


  Lemma inter_empty : forall s,
      inter empty s = empty.
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite mem_inter_iff.
    rewrite mem_empty. reflexivity.
  Qed.

  Lemma inter_assoc : forall s1 s2 s3,
     inter (inter s1 s2) s3 = inter s1 (inter s2 s3).
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite !mem_inter_iff.
    rewrite andb_assoc.
    reflexivity.
  Qed.

  Lemma subset_inter_l : forall s1 s2,
      subset (inter s1 s2) s1 = true.
  Proof.
    intros.
    apply subset_mem2;auto.
    intros.
    rewrite mem_inter_iff in H. rewrite andb_true_iff in H. tauto.
  Qed.

  Lemma is_empty_false : forall s, is_empty s = false ->
                                   exists x, mem x s = true.
  Proof.
    intros.
    unfold mem.
    apply PRESET.is_empty_false; auto.
  Qed.

  Lemma inter_union_distr : forall s1 s2 s,
      inter (union s1 s2) s = union (inter s1 s) (inter s2 s).
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite mem_inter_iff.
    rewrite !mem_union_iff.
    rewrite !mem_inter_iff.
    apply andb_orb_distrib_l.
  Qed.
  
  Lemma union_distr: forall s1 s2 s3,
    union s1 (union s2 s3) = union (union s1 s2) (union s1 s3).
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite! mem_union_iff.
    destruct (mem x s1); auto.
  Qed.


  Lemma is_empty_union : forall s1 s2,
      is_empty (union s1 s2) = is_empty s1 && is_empty s2.
  Proof.
    intros.
    destruct (is_empty (union s1 s2)) eqn:E;auto.
    -
      apply is_empty_true in E.
      apply union_empty_inv in E.
      destruct E. subst.
      reflexivity.
    -
      destruct (is_empty s1) eqn:S1.
      apply is_empty_true in S1.
      subst.
      rewrite union_empty_l in E. rewrite E. reflexivity.
      reflexivity.
  Qed.

  Lemma subset_is_empty : forall s1,
      subset s1 empty = true -> s1 = empty.
  Proof.
    intros.
    apply uniq.
    intros.
    generalize (subset_mem1 x _ _ H).
    rewrite mem_empty. destruct (mem x s1); intuition congruence.
  Qed.

  Lemma subset_union_empty : forall s1 s2,
      subset (union s1 s2) empty = true <-> (s1 = empty /\ s2 = empty).
    Proof.
      split; intros.
      - apply union_empty_inv.
        apply uniq.
        intros.
        specialize (subset_mem1 x _ _ H).
        rewrite mem_union_iff.
        rewrite mem_empty.
        rewrite orb_false_iff.
        rewrite orb_true_iff.
        destruct (mem x s1) ,(mem x s2); intuition congruence.
      - destruct H.
        subst.
        rewrite union_empty. apply subset_refl.
    Qed.

  Lemma is_empty_singleton : forall x, is_empty (singleton x) = false.
  Proof.
    intros.
    destruct (is_empty (singleton x)) eqn:E; auto.
    apply is_empty_true in E.
    apply (f_equal (mem x)) in E.
    rewrite singleton_xx in E.
    rewrite mem_empty in E.
    discriminate.
  Qed.

  Lemma subset_empty_l : forall x, subset empty x = true.
  Proof.
    intros.
    apply subset_mem2.
    intros. rewrite mem_empty in H. discriminate.
  Qed.

  Lemma singleton_no_empty : forall p,
      singleton p = empty -> False.
  Proof.
    intros.
    rewrite <- is_empty_true in H.
    rewrite is_empty_singleton in H.
    discriminate.
  Qed.

  Lemma mem_elements : forall x s,
      List.In x (elements s) <-> mem x s = true.
  Proof.
    intros. unfold elements.
    rewrite PRESET.mem_elements.
    split ; auto.
  Qed.

  Lemma diff_empty_l : forall s,
    diff empty s = empty.
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite mem_diff_iff.
    rewrite mem_empty.
    reflexivity.
  Qed.

  Lemma diff_empty_r : forall s,
    diff s empty = s.
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite mem_diff_iff.
    rewrite mem_empty.
    simpl. rewrite andb_comm. reflexivity.
  Qed.



  Definition map (f : positive -> positive) (s : t) :=
    nmake (PRESET.map f (set s)).

  Definition exists_ (f: positive -> bool) (s:t) :=
      exists_ f (set s).



  Lemma mem_map : forall f s,
    forall x,  mem x (map f s) = List.existsb (fun y => Pos.eqb (f y)  x) (elements s).
  Proof.
    unfold mem,map;intros.
    simpl.
    rewrite WF.mem_normalise.
    rewrite PRESET.mem_map.
    reflexivity.
  Qed.

  Lemma mem_map' :
    forall f s
           (INJ : forall x y : positive, f x = f y -> x = y)
    ,
    forall x,  mem (f x) (map f s) = mem x s.
  Proof.
    unfold mem,map;intros.
    simpl.
    rewrite WF.mem_normalise.
    rewrite PRESET.mem_map'.
    reflexivity.
    auto.
  Qed.


  Lemma mem_elements' : forall x s,
      mem x s = List.existsb (fun y => Pos.eqb x y) (elements s).
  Proof.
    unfold mem;intros.
    simpl.
    rewrite PRESET.mem_elements'.
    reflexivity.
  Qed.

  Lemma elements_union : forall p s1 s2,
      List.existsb p (elements (union s1 s2)) =
        (List.existsb p (elements s1) ||
           List.existsb p (elements s2)).
  Proof.
    unfold elements,union.
    intros.
    simpl.
    rewrite <- PRESET.elements_union.
    rewrite WF.elements_normalise. reflexivity.
  Qed.

  Lemma append_assoc : forall p1 p2 p3,
      append (append p1 p2) p3 =
        append p1 (append p2 p3).
  Proof.
    induction p1; simpl.
    -  intros.
       rewrite IHp1.
       reflexivity.
    -  intros.
       rewrite IHp1.
       reflexivity.
    - reflexivity.
  Qed.

  Lemma rev_append_eq : forall p1 p2,
      rev_append p1 p2 =
        (append (rev_append p1 xH) p2).
  Proof.
    induction p1.
    - simpl. intros.
      rewrite (IHp1 3%positive).
      rewrite IHp1.
      rewrite append_assoc_1.
      reflexivity.
    - simpl. intros.
      rewrite (IHp1 2%positive).
      rewrite IHp1.
      rewrite append_assoc_0.
      reflexivity.
    -  reflexivity.
  Qed.

  Lemma rev_app_eq : forall p1 p2,
      rev (append p1 p2) =
        append (rev p2) (rev p1).
  Proof.
    unfold rev.
    induction p1;simpl.
    - intros.
      rewrite rev_append_eq.
      rewrite IHp1.
      rewrite append_assoc.
      f_equal.
      rewrite  (rev_append_eq p1 3%positive).
      reflexivity.
    - intros.
      rewrite rev_append_eq.
      rewrite IHp1.
      rewrite append_assoc.
      f_equal.
      rewrite  (rev_append_eq p1 2%positive).
      reflexivity.
    - intros.
      rewrite <- rev_append_eq.
      reflexivity.
  Qed.

  Lemma elements_singleton : forall i,
      elements (singleton i) = (i::nil)%list.
  Proof.
    unfold elements, singleton.
    simpl. intro.
    rewrite WF.elements_normalise.
    unfold PositiveSet.singleton.
    unfold PositiveSet.elements.
    unfold PositiveSet.empty.
    replace i with (FMapPositive.append (rev xH) i ) at 2.
    generalize 1%positive.
    induction i; simpl.
    - intros.
      rewrite IHi.
      change (xI p) with (append (xI xH) p).
      rewrite rev_app_eq.
      rewrite append_assoc.
      f_equal.
    - intros.
      rewrite IHi.
      change (xO p) with (append (xO xH) p).
      rewrite rev_app_eq.
      rewrite append_assoc.
      f_equal.
    - intros.
      rewrite append_neutral_r.
      reflexivity.
    - reflexivity.
  Qed.

  Lemma diff_union_l : forall s1 s2 s,
      BSet.diff (BSet.union s1 s2) s = BSet.union (BSet.diff s1 s) (BSet.diff s2 s).
  Proof.
    intros.
    apply uniq. intros.
    rewrite! mem_diff_iff.
    rewrite! mem_union_iff.
    rewrite! mem_diff_iff.
    destruct (mem x s1);
      destruct (mem x s2);
      destruct (mem x s);simpl;auto.
  Qed.

  Lemma diff_union_r : forall s1 s2 s,
      BSet.diff s (BSet.union s1 s2) = BSet.diff (BSet.diff s s1) s2.
  Proof.
    intros.
    apply uniq. intros.
    rewrite! mem_diff_iff.
    rewrite! mem_union_iff.
    rewrite negb_orb.
    rewrite andb_assoc.
    reflexivity.
  Qed.

  Lemma subset_diff_2 : forall s1 s2 s1' s2',
      subset s1 s1' = true->
      subset s2' s2 = true->
      subset (diff s1 s2) (diff s1' s2') = true.
  Proof.
    intros.
    rewrite subset_mem_iff in *.
    intros.
    rewrite mem_diff_iff in *.
    rewrite andb_true_iff in *.
    specialize (H x).
    specialize (H0 x).
    destruct H1. rewrite H1 in *. split;auto.
    destruct (mem x s2'); simpl;auto.
    rewrite H0 in H2. discriminate. auto.
  Qed.

  Lemma diff_inter_empty : forall s1 s2,
      BSet.diff (BSet.inter s1 s2) s2 = BSet.empty.
  Proof.
    intros. apply uniq.
    intro.
    rewrite mem_diff_iff.
    rewrite mem_inter_iff.
    rewrite mem_empty.
    rewrite !andb_false_iff.
    destruct (mem x s2); simpl; tauto.
  Qed.

  Lemma diff_idem : forall s1 s2,
      BSet.diff (BSet.diff s1 s2) s2 = BSet.diff s1 s2.
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite! mem_diff_iff.
    rewrite <- andb_assoc.
    f_equal.
    rewrite andb_diag. reflexivity.
  Qed.

  Lemma inter_diff_empty : forall s s',
      inter (BSet.diff s s') s' = BSet.empty.
  Proof.
    intros. apply uniq; intros.
    rewrite mem_inter_iff.
    rewrite mem_diff_iff.
    rewrite mem_empty.
    rewrite <- andb_assoc.
    rewrite andb_negb_l.
    rewrite andb_comm. reflexivity.
  Qed.

  Lemma inter_diff_upper_empty : forall s s' s'',
      BSet.subset s' s'' = true ->
      inter (BSet.diff s s'') s' = BSet.empty.
  Proof.
    intros. apply uniq; intros.
    rewrite mem_inter_iff.
    rewrite mem_diff_iff.
    rewrite mem_empty.
    rewrite subset_mem_iff in H.
    specialize (H x).
    rewrite <- andb_assoc.
    rewrite andb_false_iff.
    rewrite andb_false_iff.
    rewrite negb_false_iff.
    rewrite <- !not_true_iff_false.
    destruct (mem x s').
    tauto. right. right. discriminate.
  Qed.

End BSet.

Lemma mem_if : forall (b:bool) x s1 s2,
    BSet.mem x (if b then s1 else s2) = (b && (BSet.mem x s1) || negb b && BSet.mem x s2).
Proof.
  destruct b; simpl; intros.
  - rewrite orb_comm. reflexivity.
  - reflexivity.
Qed.

#[ global ] Opaque BSet.union.
#[ global ] Opaque BSet.set.

Lemma eqb_eq : forall (b1 b2:bool),
    (b1 = true <-> b2 = true) <-> b1 = b2.
Proof.
  destruct b1,b2; intuition congruence.
Qed.

Ltac is_not_bool b :=
  match b with
  | true => fail
  | false => fail
  |   _   => idtac
  end.


Ltac elim_eq_bool :=
  match goal with
  | |- @Logic.eq bool ?X ?Y => is_not_bool X ; is_not_bool Y ; rewrite <- eqb_eq
  | H : @Logic.eq bool ?X ?Y |- _ => is_not_bool X ; is_not_bool Y ; rewrite <- eqb_eq in H
  end.

Ltac subset :=
  try match goal with
    | |- BSet.subset ?A ?B = true =>
        rewrite BSet.subset_mem_iff ;
        intros
    | |- @Logic.eq BSet.t _ _ => apply BSet.uniq;intros
    end;
  repeat
    match goal with
    | H : BSet.subset ?X BSet.empty = true |- _ =>
        apply BSet.subset_is_empty in H; rewrite H in *
    | H : BSet.subset ?A ?B = true |- context[BSet.mem ?x]  => rewrite BSet.subset_mem_iff in H ; specialize (H x)
    | H : @Logic.eq BSet.t ?A ?B   |- context[BSet.mem ?x]  => apply (f_equal (BSet.mem x)) in H

    end;
  repeat
    (first [
         rewrite BSet.union_singleton in *
       | rewrite BSet.mem_union_iff in *
       | rewrite BSet.mem_inter_iff in *
       | rewrite BSet.mem_diff_iff in *
       | rewrite BSet.mem_empty in *
       | rewrite BSet.singleton_xx in  *
       | rewrite BSet.mem_singleton in *
       | rewrite mem_if in *
       | rewrite orb_true_iff in *
       | rewrite andb_true_iff in *
       | rewrite andb_false_iff in *
       | rewrite orb_false_iff in *
       | rewrite negb_false_iff in *
       | rewrite negb_true_iff in *
       | rewrite <- not_true_iff_false in *
       | rewrite orb_true_r in *
       | rewrite andb_false_r in *
       | elim_eq_bool
    ]);
  intuition try congruence.
