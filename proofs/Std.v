Require Import List.

Ltac inv H := inversion H ; try subst; clear H.
Ltac split_and :=
  repeat match goal with
  | |- ?A /\ ?B => apply conj
  end.

Ltac clear_true_false :=
  repeat match goal with
    | H : true = false -> _ |- _ => clear H
    | H : false = true -> _ |- _ => clear H
  end.

Definition of_sb {A B: Prop} (dec: sumbool A B)  :=
  if dec then true else false.

Definition map2_opt {A: Type} (f: A -> A -> A) (o1 o2:option A) : option A :=
  match o1 , o2 with
    | Some v1 , Some v2 => Some (f v1 v2)
  | _ , _ => None
  end.

Definition lift_option {A B: Type} (f : A -> option B) (o:option A) : option B :=
  match o with
  | None => None
  | Some v => f v
  end.

Definition from_option {A: Type} (d: A) (o: option A) :=
  match o with
  | None => d
  | Some v => v
  end.

Lemma lift_if_1 : forall {A B: Type} (F : A -> B) (b:bool) x y,
    (if b then F x else F y) = F (if b then x else y).
Proof.
  destruct b;reflexivity.
Qed.

Ltac dand := repeat match goal with
                 H : ?A /\ ?B |- _ =>
                   let f1 := fresh H in
                   let f2 := fresh H in
                   destruct H as (f1  & f2)
               end.
               
Ltac rewrite_simpl H H1:= rewrite H in H1; simpl in H1.


Ltac mt PtoQ notQ notP :=
  match type of PtoQ with
  | ?P -> _ => pose proof ((fun p => notQ (PtoQ p)) : ~ P) as notP
  end.
  
Ltac mp PtoQ P Q := 
  pose proof (PtoQ P) as Q.
  
Theorem MT: forall P Q: Prop, ~Q /\ (P -> Q) -> ~P.
Proof.
  intros P Q HNQPQ.
  destruct HNQPQ as [HNQ HPQ].
  intro HP.
  generalize (HPQ HP).
  intro HQ.
  contradiction.
Qed.


Lemma not_exists_forall :
  forall (A: Type) (P : A -> Prop),
  ~ (exists x, P x) <-> forall x, ~ P x.
Proof.
  split; intros.
    - intuition. apply H. exists x. auto.
    - unfold not. intro. destruct H0. specialize (H x). tauto.
Qed.

Lemma demorgan:
  forall A B, 
  ~ (A \/ B) <-> ~ A /\ ~ B.
Proof.
  split; tauto.
Qed.

Lemma existsb_map : forall {A B: Type} p (f: A -> B) l,
    List.existsb p (List.map f l) = List.existsb (fun x => p (f x)) l.
Proof.
  induction l ; simpl.
  - reflexivity.
  - rewrite IHl.
    reflexivity.
Qed.

Lemma existsb_morph : forall {A: Type} p p' (l: list A),
  (forall x, p x = p' x) ->
  List.existsb p l = List.existsb p' l.
Proof.
  induction l;simpl; auto.
  intros. rewrite IHl;auto.
  rewrite H. reflexivity.
Qed.

Definition singleton {A: Type} (x:A) := @eq _ x.

Definition union {A: Type} (s1 s2 : A -> Prop) :=
  fun x => s1 x \/ s2 x.

Definition inter {A: Type} (s1 s2 : A -> Prop) :=
  fun x => s1 x /\ s2 x.

Definition select  {A B: Type} (p : B -> Prop) (f : A -> B) : A -> Prop :=
    fun x => p (f x).

Definition codom {A B: Type} (f : A -> B) := fun y => exists x, f x = y.
