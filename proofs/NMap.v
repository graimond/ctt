(** Canonic PositiveSet.t *)
Require Import Std.
Require Import Bool ZArith.
Require Import FMapPositive.
Import PositiveMap.

(*
  - [wf] ensures a normal form.
  - [normalise] builds a normal form from an arbitrary map
*)

Module WF.
  Section S.
  Context {A: Type}.

Definition is_leaf (s:t A) :=
  match s with
  | Leaf _ => true
  | _    => false
  end.

Lemma is_leaf_inv : forall s, is_leaf s = true <-> s = Leaf _.
Proof.
  destruct s; simpl; intuition congruence.
Qed.

Fixpoint wf (s:t A) :=
    match s with
    | Leaf _ => true
    | Node l o r => wf l && wf r && (if is_empty s then is_leaf s else true)
    end.


Fixpoint normalise (s: tree A) : tree A :=
  match s with
  | Leaf _ => Leaf _
  | Node l o r => match normalise l , o, normalise r with
                  | Leaf _ , None , Leaf _ => Leaf _
                  | l , o , r => Node l o r
                  end
  end.

Lemma wf_normalise : forall s,
    wf (normalise s) = true.
Proof.
  induction s; simpl; auto.
  destruct (normalise s1) eqn:S1.
  -  destruct o; simpl.
     rewrite IHs2. reflexivity.
     destruct (normalise s2) eqn:S2.
     reflexivity.
     simpl in *.
     rewrite! andb_true_iff in *.
     dand.
     split_and;auto.
  - simpl in *.
    rewrite! andb_true_iff in *.
    dand.
    split_and;auto.
    destruct o ; auto.
    simpl. destruct o0; auto.
    simpl in *. destruct (is_empty t0_1) eqn:E1; auto.
    destruct (is_empty t0_2) eqn:E2;auto.
    destruct (is_empty (normalise s2)) eqn:E3; auto.
Qed.

Lemma find_normalise : forall s x,
    find x (normalise s) = find x s.
Proof.
  induction s;simpl; auto.
  destruct x ; simpl; auto.
  - rewrite <- IHs2.
    destruct (normalise s1); auto.
    destruct o. reflexivity.
      destruct (normalise s2); simpl; auto.
      rewrite gleaf. reflexivity.
  -rewrite <- IHs1.
    destruct (normalise s1);auto.
    destruct o; auto.
    destruct (normalise s2); simpl; auto.
    rewrite gleaf;reflexivity.
  -
    destruct (normalise s1); try reflexivity.
    destruct o; try reflexivity.
    destruct (normalise s2); try reflexivity.
Qed.

Lemma is_empty_leaf :
  forall s
         (WF : wf s = true),
    (forall x, find x s = None) ->
  Leaf _ = s.
Proof.
  induction s; simpl.
  - auto.
  - intros.
    rewrite !andb_true_iff in WF.
    dand.
    destruct o; simpl in WF1.
    + specialize (H xH). discriminate.
    +
      destruct (is_empty s1) eqn:E1; try discriminate.
      destruct (is_empty s2) eqn:E2; try discriminate.
      rewrite <- IHs2 in E2;auto. discriminate.
      intros.
      apply (H (xI x)).
      rewrite <- IHs1 in E1;auto. discriminate.
      intros.
      apply (H (xO x)).
Qed.

Lemma is_empty_mem2 : forall (s:t A), is_empty s = false -> exists x, find x s <> None.
Proof.
  induction s; simpl.
  - discriminate.
  - destruct o; try discriminate.
    simpl. intros.
    exists xH. simpl. congruence.
    simpl.
    destruct (is_empty s1) eqn:E1.
    intros.
    destruct (IHs2 H) as (x1 & M).
    exists (xI x1); auto.
    intros.
    destruct (IHs1 Logic.eq_refl) as (x1 & M).
    exists (xO x1);auto.
Qed.


Lemma uniq : forall s1 s2
    (WF1 : wf s1 = true) (WF2 : wf s2 = true),
    (forall x, find x s1 = find x s2) ->
    s1 = s2.
Proof.
  induction s1; simpl.
  - intros.
    eapply is_empty_leaf; auto.
    intros. rewrite  <- H. rewrite gleaf. reflexivity.
  - intros.
    rewrite! andb_true_iff in WF1.
    dand.
    destruct s2.
    + exfalso.
      generalize (H xH).
      simpl. intro. subst.
      destruct (is_empty s1_1) eqn:E1; try discriminate.
      destruct (is_empty s1_2) eqn:E2; try discriminate.
      apply is_empty_mem2 in E2.
      destruct E2 as (x & MEM).
      specialize (H (xI x)).
      simpl in H. congruence.
      apply is_empty_mem2 in E1.
      destruct E1 as (x & MEM).
      specialize (H (xO x)).
      simpl in H. congruence.
    +
      simpl in WF2. rewrite! andb_true_iff in WF2.
      dand.
      f_equal; auto.
      * eapply IHs1_1; auto.
       intros.
       apply (H (xO x)).
      * apply (H xH).
      * eapply IHs1_2; auto.
        intros.
        apply (H (xI x)).
Qed.



Lemma wf_is_empty : forall s (WF : wf s = true),
    is_leaf s = is_empty s.
Proof.
  induction s; simpl.
  - auto.
  - intros.
    rewrite !andb_true_iff in WF.
    dand.
    destruct o; auto.
    simpl in *.
    destruct (is_empty s1) eqn:E1; auto.
    destruct (is_empty s2) eqn:E2; auto.
Qed.

End S.
End WF.

Module PREMAP.
  (* results over non-normalised *)
  Import PositiveMap.

  Section S.
    Context {A: Type}.

  Lemma add_spec : forall x x' v (s:t A),
      find x (PositiveMap.add x' v s) = if Pos.eq_dec x x' then Some v else find x s.
  Proof.
    intros.
    rewrite PositiveMapAdditionalFacts.gsspec.
    destruct (E.eq_dec x x'); destruct (Pos.eq_dec x x'); congruence.
  Qed.


  Lemma is_empty_false :
    forall (s:t A)
           (WF : WF.wf s = true),
      WF.is_leaf s = false ->
      exists x, find x s <> None.
  Proof.
    intros.
    rewrite WF.wf_is_empty in H; auto.
    apply WF.is_empty_mem2 ;auto.
  Qed.

  Fixpoint map_option {A B: Type} (f: A -> option B) (m : PositiveMap.t A) : PositiveMap.t B :=
    match m with
    | PositiveMap.Leaf _ => PositiveMap.Leaf _
    | PositiveMap.Node l o r => PositiveMap.Node (map_option f l) (lift_option f o)  (map_option f r)
    end.

  Lemma find_map_option : forall {A B: Type} (f: A -> option B)  x (m:PositiveMap.t A),
      find x (map_option f m) = lift_option f (PositiveMap.find x m).
  Proof.
    induction x ; destruct m; simpl; auto.
  Qed.

  Definition filter {A: Type} (p : A -> bool) (m:PositiveMap.t A) : PositiveMap.t A :=
    map_option (fun v => if p v then Some v else None) m.


  Lemma no_dup_elements : forall (m: PositiveMap.t A),
      List.NoDup (elements m).
  Proof.
    intros.
    generalize (elements_3w m).
    intros. induction H.
    constructor.
    constructor;auto.
    intro.
    apply H.
    clear - H1.
    induction l.
    - simpl in H1. tauto.
    - simpl in H1.
      destruct H1. subst.
      constructor.
      reflexivity.
      apply SetoidList.InA_cons_tl; auto.
  Qed.

  Lemma InA_eq : forall {B: Type} (eq1 eq2: B -> B -> Prop)
                        (EQ : forall v1 v2, eq1 v1 v2 -> eq2 v1 v2)
                        x l
    ,
      SetoidList.InA eq1 x l -> SetoidList.InA eq2 x l.
  Proof.
    intros. induction H.
    - constructor. auto.
    - eapply SetoidList.InA_cons_tl;eauto.
  Qed.

  Lemma no_dup_keys : forall (m: PositiveMap.t A),
      List.NoDup (List.map fst (elements m)).
  Proof.
    intros.
    assert (forall x v1 v2, List.In (x,v1) (elements m) ->
                            List.In (x,v2) (elements m) ->
                            v1 = v2).
    {
      intros.
      assert (EQ := @RelationClasses.eq_equivalence).
      apply SetoidList.In_InA with (eqA := eq) in H; auto.
      apply SetoidList.In_InA with (eqA := eq) in H0;auto.
      assert (MapsTo x v1 m).
      {
        apply elements_2.
        eapply InA_eq with (2:=H) ;eauto.
        intros.
        unfold eq_key_elt. subst. unfold E.eq.
        tauto.
      }
      assert (MapsTo x v2 m).
      {
        apply elements_2.
        eapply InA_eq with (2:=H0) ;eauto.
        intros.
        unfold eq_key_elt. subst. unfold E.eq.
        tauto.
      }
      unfold MapsTo in H1,H2.
      congruence.
    }
    generalize (no_dup_elements m).
    induction (elements m).
    - constructor.
    -  intros.
       inv H0.
       simpl.
       destruct a as (k,v).
       simpl.
       constructor.
       intro.
       rewrite List.in_map_iff in H0.
       destruct H0 as ((k',v1'),H0).
       destruct H0. simpl in *.
       subst. specialize (H k v v1' (or_introl _  eq_refl) (or_intror _ H1)).
       subst. tauto.
       eapply IHl;eauto.
       intros.
       eapply H; simpl.
       right;eauto.
       right;eauto.
  Qed.

End S.



End PREMAP.


Module Map.
  (* lift *)
  Section S.
    Context {A: Type}.

  Record t :=
    mk
      {
        mp : PositiveMap.t A;
        prf : WF.wf mp = true
      }.




  Definition empty := mk (PositiveMap.empty A) (Logic.eq_refl (WF.wf (PositiveMap.empty A))).

  Definition nmake (s:PositiveMap.t A) :=
    let s' := WF.normalise s in
    mk s' (WF.wf_normalise s).

  Definition add (x:positive) (v: A) (s:t) := nmake (PositiveMap.add x v (mp s)).

  Definition rm (x:positive) (s:t) := nmake (PositiveMap.remove x (mp s)).

  Definition find (x:positive) (s:t) := PositiveMap.find x (mp s).

  Definition get  (d:A) (x:positive) (env: t) :=
    from_option d (find x env).

  Definition singleton (x:positive) (v:A) := add x v empty.

  Lemma wf_mp : forall s,
      WF.wf (mp s) = true.
  Proof.
    destruct s; auto.
  Qed.

  Hint Resolve wf_mp : core.

  Lemma uniq : forall s1 s2,
      (forall x, find x s1 = find x s2) ->
    s1 = s2.
  Proof.
    destruct s1,s2.
    intros.
    unfold find in H. simpl in H.
    assert (mp0 = mp1).
    {
      apply WF.uniq; auto.
    }
    subst.
    f_equal.
    apply Eqdep_dec.UIP_dec.
    apply bool_dec.
  Qed.

  Lemma get_empty : forall d x, get d x empty = d.
  Proof.
    unfold get.
    intros.
    unfold find. simpl.
    rewrite gempty. reflexivity.
  Qed.

  Definition fold {B: Type} (F : positive -> A -> B -> B) (s:t) (acc: B) :=
    PositiveMap.fold F (mp s) acc.

  Definition elements (s:t) := PositiveMap.elements (mp s).

  Lemma find_elements : forall x v s,
      List.In (x,v) (elements s) <-> find x s = Some v.
  Proof.
    intros.
    assert (forall (s:PositiveMap.t A) v, PositiveMap.find x s = Some v <-> PositiveMap.MapsTo x v s).
    {
      unfold MapsTo. tauto.
    }
    unfold find.
    rewrite H.
    split. intros.
    apply PositiveMap.elements_2.
    revert H0. unfold elements.
    clear H.
    revert x v.
    { induction (PositiveMap.elements (mp s)).
      - simpl. tauto.
      -  simpl ; intros.
         inv H0; subst.
         + constructor. unfold eq_key_elt.
         simpl. split ; auto. reflexivity.
         + apply IHl in H.
           apply SetoidList.InA_cons_tl; auto.
    }
    intro.
    apply PositiveMap.elements_1 in H0.
    unfold elements.
    induction (PositiveMap.elements (mp s)).
    inv H0.
    inv H0. left. symmetry. destruct a. destruct H2. simpl in * ;
                  f_equal;auto.
    right. tauto.
  Qed.

  Lemma fold_1 : forall {B: Type} f m (i:B),
      fold f m i =
       List.fold_left (fun (a : B) (p : key * A) => f (fst p) (snd p) a)
         (elements m) i.
  Proof.
    unfold fold. intros.
    rewrite  PositiveMap.fold_1.
    reflexivity.
  Qed.





  End S.

  Definition map {A B: Type} (f: A -> B) (m :@t A) : @t B :=
    nmake (PositiveMap.map f (mp m)).

  Definition filter {A: Type} (p: A -> bool) (m :@t A) : @t A :=
    nmake (PREMAP.filter p (mp m)).

  Lemma find_map : forall {A B:Type} (f:A -> B) m x,
      find x (map f m) = option_map f (find x m).
  Proof.
    unfold find,map.
    simpl. intros.
    rewrite WF.find_normalise.
    destruct (PositiveMap.find x (mp m)) eqn:FIND.
    - apply map_1 with (f:=f) in FIND.
      simpl. unfold MapsTo in FIND.
      auto.
    - simpl.
      destruct (  PositiveMap.find x (PositiveMap.map f (mp m))) eqn:FMAP;auto.
      assert (In x (PositiveMap.map f (mp m))).
      {
        unfold In. exists b. apply FMAP.
      }
      apply  map_2 in H. unfold In in H.
      destruct H. unfold MapsTo in H. congruence.
  Qed.

  Definition map2 {A B C: Type} (f: option A -> option B -> option C) (m1 :@t A) (m2: @t B): @t C :=
    nmake (PositiveMap.map2 f (mp m1) (mp m2)).

  Lemma find_map2 : forall {A B C: Type} (f : option A -> option B -> option C) (FN : f None None = None) m1 m2 x,
      find x (map2 f m1 m2) = f (find x m1) (find x m2).
  Proof.
    unfold find,map2.
    intros.
    simpl.
    rewrite WF.find_normalise.
    destruct (PositiveMap.find x (mp m1)) eqn:FIND1.
    - rewrite map2_1.
      rewrite FIND1. reflexivity.
      left. unfold In. exists a. apply FIND1.
    - destruct (PositiveMap.find x (mp m2)) eqn:FIND2.
      rewrite map2_1.
      rewrite FIND1. rewrite FIND2. reflexivity.
      right. unfold In. exists b. apply FIND2.
      destruct (PositiveMap.find x (PositiveMap.map2 f (mp m1) (mp m2))) eqn:FINDM.
      assert (INM : In x (PositiveMap.map2 f (mp m1) (mp m2))).
      { unfold In. exists c. apply FINDM. }
      apply map2_2  in INM.
      unfold In in INM. destruct INM as [INM|INM] ; destruct INM ;
        unfold MapsTo in * ; congruence.
      congruence.
  Qed.

  Lemma find_add : forall {A:Type} x y v (m: @t A),
      find x (add y v m) = if Pos.eq_dec x y then Some v else find x m.
  Proof.
    unfold find,add; intros.
    simpl.
   rewrite WF.find_normalise.
   rewrite PREMAP.add_spec.
   reflexivity.
  Qed.

  Lemma map_add : forall {A B:Type} (F:A -> B) m x y,
      map F (add x y m) = add x (F y) (map F m).
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite! find_add.
    rewrite! find_map.
    rewrite! find_add.
    destruct (Pos.eq_dec x0 x); subst.
    reflexivity.
    reflexivity.
  Qed.



  Lemma find_rm : forall {A:Type} x y (m: @t A),
      find x (rm y m) = if Pos.eq_dec x y then None else find x m.
  Proof.
    unfold find, rm.
    simpl. intros.
    rewrite WF.find_normalise.
    destruct (Pos.eq_dec x y); subst.
    - rewrite grs. reflexivity.
    - rewrite gro; auto.
  Qed.


  Lemma gss : forall {A:Type} x v (m: @t A),
      find x (add x v m) = Some v.
  Proof.
    intros.
    rewrite find_add.
    destruct (Pos.eq_dec x x); auto.
    congruence.
  Qed.

  Lemma gso : forall {A:Type} x y v (m: @t A),
      x <> y ->
      find x (add y v m) = find x m.
  Proof.
    intros.
    rewrite find_add.
    destruct (Pos.eq_dec x y); auto.
    congruence.
  Qed.

  Lemma gempty : forall {A:Type} x ,
      find x (@empty A) = None.
  Proof.
    unfold find,empty; simpl.
    intros.
    rewrite PositiveMap.gleaf.
    reflexivity.
  Qed.


  Lemma map_empty : forall {A B:Type} (F:A -> B),
      map F empty = empty.
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite! find_map.
    rewrite! gempty.
    reflexivity.
  Qed.



  Lemma find_singleton : forall {A:Type} x y (v:A),
      find x (singleton y v) = if Pos.eq_dec x y then Some v else None.
  Proof.
    unfold singleton; intros.
    rewrite find_add. rewrite gempty.
   reflexivity.
  Qed.




  Lemma add_swap : forall {A: Type} x y (v1 v2:A) e,
      x <> y ->
      add x v1 (add y v2 e) =
        add y v2 (add x v1 e).
  Proof.
    intros.
    apply uniq.
    intros.
    rewrite !find_add.
    destruct (Pos.eq_dec x0 x); destruct (Pos.eq_dec x0 y); try congruence.
  Qed.


  Definition invert (rho: @Map.t positive) :=
    Map.fold (fun x x' acc => Map.add x' x acc) rho (@Map.empty _).

  Lemma find_invert_keep : forall l acc,
      let F := fun a  (p : positive * positive) => Map.add (snd p) (fst p) a in
      forall x',
        ~ List.In x' (List.map snd l) ->
        Map.find x' (List.fold_left F l acc) = Map.find x' acc.
  Proof.
  induction l.
  - simpl. auto.
  - simpl ; intros.
    destruct a. simpl in H.
    destruct (Pos.eq_dec p0 x').
    + subst. tauto.
    + rewrite IHl ; try tauto.
      simpl.
      rewrite Map.gso by auto. reflexivity.
Qed.

Lemma find_none_elements : forall {A:Type} x (rho: @Map.t A),
    Map.find x rho = None ->
    List.In x (List.map fst (Map.elements rho)) -> False.
Proof.
  intros.
  rewrite List.in_map_iff in H0.
  destruct H0 as (x_x' & FST & IN).
  destruct x_x'; subst; simpl in *.
  rewrite Map.find_elements in IN.
  congruence.
Qed.

Definition eq_dec_2_pos (x y: positive * positive)  : { x = y} + {x <> y}.
Proof.
  generalize Pos.eq_dec.
  decide equality.
Qed.

Lemma find_invert_aux : forall l acc x',
    let F := (fun (a : @Map.t positive) (p : positive * positive) => Map.add (snd p) (fst p) a) in
    match  Map.find x' (List.fold_left F l acc) with
    | Some x  =>List.In (x,x') l \/ Map.find x' acc = Some x
    | None    => ~ List.In x' (List.map snd l) /\
                   Map.find x' acc = None
    end.
Proof.
  induction l; simpl.
  - intros. destruct (Map.find x' acc); tauto.
  - intros.
    specialize (IHl (Map.add (snd a) (fst a) acc) x').
    simpl in IHl.
    destruct (Map.find x'
                (List.fold_left
                   (fun (a0 : @Map.t positive)
                        (p : positive * positive) =>
                      Map.add (snd p) (fst p) a0) l
                   (Map.add (snd a) (fst a) acc))).
    +  destruct a; simpl in IHl.
       destruct IHl.
       tauto.
       rewrite Map.find_add in H.
       destruct (Pos.eq_dec x' p1).
       subst. inv H. tauto.
       tauto.
    + destruct a; simpl in *.
      destruct IHl.
      rewrite Map.find_add in H0.
      destruct (Pos.eq_dec x' p0).
      discriminate.
      intuition congruence.
Qed.


Lemma find_invert : forall x' rho,
    match Map.find x' (invert rho) with
    | Some x => Map.find x rho = Some x'
    | None   => forall x, Map.find x rho <> Some x'
    end.
Proof.
  unfold invert.
  intros.
  rewrite Map.fold_1.
  set (F := (fun (a : @Map.t positive) (p : positive * positive) => Map.add (snd p) (fst p) a)).
  generalize (find_invert_aux (Map.elements rho) (@Map.empty positive) x').
  simpl.
  change PositiveMap.key with positive.
  change (fun (a : @Map.t positive)
              (p : positive * positive) =>
            Map.add (snd p) (fst p) a) with F.
  destruct (Map.find x' (List.fold_left F (Map.elements rho) Map.empty)).
  - intros.
    destruct H.
    apply Map.find_elements. auto.
    rewrite Map.gempty in H.
    discriminate.
  - rewrite Map.gempty.
    rewrite List.in_map_iff.
    intuition try congruence.
    apply Map.find_elements in H.
    apply H0.
    exists (x,x').
    simpl ; split; auto.
Qed.

Definition map_option {A B: Type} (f: A -> option B) (m: @t A) : @t B :=
  nmake (PREMAP.map_option f (mp m)).

Lemma find_map_option : forall {A B: Type} (f: A -> option B)  x (m:@t A),
    find x (map_option f m) = lift_option f (find x m).
Proof.
  unfold find,map_option, nmake. simpl.
  intros. rewrite WF.find_normalise.
  apply PREMAP.find_map_option.
Qed.

Definition mapi {A B:Type} (f : positive -> A -> B) (m: @t A) : @t B :=
  nmake (PositiveMap.mapi f (mp m)).


Lemma find_mapi : forall {A B:Type} (f: positive -> A -> B) (m: @t A),
    forall x, find x (mapi f m) = option_map (f x) (find x m).
Proof.
  unfold find,nmake. simpl.
  intros. rewrite WF.find_normalise.
  rewrite gmapi.
  reflexivity.
Qed.

Definition upd {A:Type}(o1 o2 : option A) : option A :=
  match o2 with
  | None => o1
  | Some t => Some t
  end.

Definition update {A: Type} (e1 e2: @Map.t A ) :=
  map2 upd e1 e2.

Lemma find_update : forall {A: Type} x (GU G: @Map.t A),
    find x (update GU G) =
      match find x G with
      | Some v => Some v
      | None   => find x GU
      end.
Proof.
  unfold update.
  intros.
  rewrite Map.find_map2 by reflexivity.
  destruct (Map.find x GU), (Map.find x G);
    auto.
Qed.

Lemma update_empty : forall {A: Type} (m: @Map.t A),
  update empty m = m.
Proof.
  intros.
  apply uniq.
  intros.
  rewrite find_update.
  destruct (find x m); auto.
  rewrite gempty.
  reflexivity.
Qed.


Lemma update_empty_2 : forall {A: Type} (m: @Map.t A),
  update m empty = m.
Proof.
  intros.
  apply uniq.
  intros.
  rewrite find_update.
  rewrite gempty.
  reflexivity.
Qed.


Lemma update_assoc : forall {A: Type} (m1 m2 m3: @Map.t A),
    update m1 (update m2 m3) = update (update m1 m2) m3.
Proof.
  intros.
  apply uniq.
  intros.
  rewrite! find_update.
  destruct (find x m3); auto.
Qed.

Lemma update_add : forall {A: Type} (m1 m2: @Map.t A) x v,
    Map.update m1 (Map.add x v m2) =
      Map.add x v (Map.update m1 m2).
Proof.
  intros.
  apply uniq.
  intro.
  rewrite! find_update.
  rewrite! find_add.
  destruct (Pos.eq_dec x0 x); auto.
  rewrite! find_update.
  auto.
Qed.




Definition dom {A: Type} (e: @Map.t A) (x:positive) := Map.find x e <> None.


Definition codom {A: Type} (e: @Map.t A) (x:A) :=
  exists k, Map.find k e = Some x.

Lemma dom_map2 : forall {A B C: Type} (f : option A -> option B -> option C) (e1 : @Map.t A) (e2: @Map.t B),
    f None None = None ->
  forall x,
      dom (map2 f e1 e2) x -> dom e1 x \/ dom e2 x.
Proof.
  intros.
  unfold dom in *.
  rewrite find_map2 in H0 by auto.
  destruct (find x e1); destruct (find x e2);
    try intuition congruence.
Qed.

Lemma dom_map : forall {A B: Type} (f : A -> B) (e : @Map.t A),
    forall x, dom (map f e) x <->  dom e x.
Proof.
  unfold dom.
  intros. rewrite find_map.
  destruct (find x e); simpl; intuition congruence.
Qed.

Lemma dom_mapi : forall {A B: Type} (f : positive -> A -> B) (e : @Map.t A),
    forall x, dom (mapi f e) x <->  dom e x.
Proof.
  unfold dom.
  intros. rewrite find_mapi.
  destruct (find x e); simpl; intuition congruence.
Qed.


Lemma dom_invert : forall (rho : @Map.t positive),
    forall x, dom (invert rho) x <-> codom rho x.
Proof.
  unfold dom,codom.
  intros.
  generalize (find_invert x rho).
  destruct (find x (invert rho)).
  - intros. split ; intros.
    exists p;auto.
    congruence.
  - split ; intros. congruence.
    destruct H0. specialize (H x0).
    congruence.
Qed.

Lemma dom_add : forall {A: Type} (m: @Map.t A) x v,
    forall y, dom (add x v m) y -> union (dom m) (Std.singleton x) y.
Proof.
  unfold dom;intros.
  rewrite find_add in H.
  destruct (Pos.eq_dec y x).
  subst. unfold union,Std.singleton. tauto.
  left. auto.
Qed.

Lemma dom_add' : forall {A: Type} (m: @Map.t A) x v,
    forall y, union (dom m) (Std.singleton x) y -> dom (add x v m) y.
Proof.
  unfold dom;intros.
  rewrite find_add.
  destruct (Pos.eq_dec y x).
  congruence.
  intro.
  unfold union in H.
  unfold Std.singleton in H.
  intuition congruence.
Qed.

Lemma no_dup_elements : forall {A: Type} (m: @Map.t A),
    List.NoDup (elements m).
Proof.
  intros.
  eapply PREMAP.no_dup_elements.
Qed.

Lemma no_dup_keys : forall {A: Type} (m: @Map.t A),
    List.NoDup (List.map fst (elements m)).
Proof.
  intros.
  eapply PREMAP.no_dup_keys.
Qed.


End Map.

Require Import NSet.

Definition bs_dom {A: Type}(e: @Map.t A) :=
  Map.fold (fun x _ acc => BSet.union (BSet.singleton x) acc) e BSet.empty.

Definition bs_codom (e: @Map.t positive) : BSet.t :=
  Map.fold (fun _ x acc => BSet.union (BSet.singleton x) acc) e BSet.empty.

Lemma bs_dom_eq : forall {A: Type} (e: @Map.t A) x,
    BSet.mem x (bs_dom e) = true <-> Map.dom e x.
Proof.  unfold bs_dom.
  intros.
  rewrite Map.fold_1.
  unfold Map.dom.
  assert (ACC : forall s,
             BSet.mem x
               (List.fold_left (fun (a : BSet.t) (p : key * A) => BSet.union (BSet.singleton(fst p)) a)
                  (Map.elements e) s) = true <-> (Map.find x e <> None \/ BSet.mem x s = true)).
  {
    assert (FD: Map.find x e <> None <-> exists y, List.In (x,y) (Map.elements e)).
    {
      destruct (Map.find x e) eqn:FD ; try congruence.
      -
        rewrite <- Map.find_elements in FD.
      split; intros.
      exists a; auto.
      congruence.
    - split; intros.
      congruence.
      destruct H.
      rewrite Map.find_elements in H.
      congruence.
    }
    intro.
    rewrite FD.
    clear FD.
    revert s.
    set (F := (fun (a : BSet.t) (p : key * A) => BSet.union (BSet.singleton (fst p)) a)).
    induction (Map.elements e).
    - simpl.
      split ; intros.
      tauto.
      destruct H. destruct H.
      tauto. auto.
    - simpl.
      intro.
      rewrite IHl.
      clear IHl.
      unfold F.
      rewrite BSet.mem_union_iff.
      rewrite orb_true_iff.
      rewrite BSet.mem_singleton.
      destruct a; simpl.
      firstorder. subst.
      left. exists a. tauto.
      inv H. tauto.
  }
  rewrite ACC.
  rewrite BSet.mem_empty.
  intuition congruence.
Qed.

Lemma bs_codom_eq : forall (e: @Map.t positive) x,
    BSet.mem x (bs_codom e) = true <-> Map.codom e x.
Proof.  unfold bs_codom.
  intros.
  rewrite Map.fold_1.
  unfold Map.codom.
  assert (ACC : forall s,
             BSet.mem x
               (List.fold_left (fun (a : BSet.t) (p : key * key) => BSet.union (BSet.singleton (snd p)) a)
                  (Map.elements e) s) = true <-> ((exists k, Map.find k e = Some x) \/ BSet.mem x s = true)).
  {
    assert (FD: (exists k, Map.find k e =Some x) <-> (exists y, List.In (y,x) (Map.elements e))).
    {
      split;intros.
      destruct H.
      rewrite <- Map.find_elements in H.
      exists x0. auto.
      destruct H.
      rewrite Map.find_elements in H.
      exists x0. auto.
    }
    intro.
    rewrite FD.
    clear FD.
    revert s.
    set (F := (fun (a : BSet.t) (p : key * key) => BSet.union (BSet.singleton (snd p)) a)).
    induction (Map.elements e).
    - simpl.
      split ; intros.
      tauto.
      destruct H. destruct H.
      tauto. auto.
    - simpl.
      intro.
      rewrite IHl.
      clear IHl.
      unfold F.
      rewrite BSet.mem_union_iff.
      rewrite orb_true_iff.
      rewrite BSet.mem_singleton.
      destruct a; simpl.
      firstorder. subst.
      left. exists k. tauto.
      inv H. tauto.
  }
  rewrite ACC.
  rewrite BSet.mem_empty.
  intuition congruence.
Qed.

Lemma bs_dom_map2 : forall {A B C: Type} (f : option A -> option B -> option C) (e1 : @Map.t A) (e2: @Map.t B),
    f None None = None ->
    BSet.subset (bs_dom (Map.map2 f e1 e2)) (BSet.union (bs_dom e1) (bs_dom e2)) = true.
Proof.
  intros.
  apply BSet.subset_mem2.
  intros.
  rewrite bs_dom_eq in H0 by reflexivity.
  rewrite BSet.mem_union_iff.
  rewrite orb_true_iff.
  apply Map.dom_map2 in H0 ; auto.
  rewrite bs_dom_eq.
  rewrite bs_dom_eq.  auto.
Qed.

Lemma bs_dom_map : forall {A B: Type} (f : A -> B) (e : @Map.t A),
    bs_dom (Map.map f e) = bs_dom e.
Proof.
  intros.
  apply BSet.uniq.
  intros.
  apply eq_true_iff_eq.
  rewrite! bs_dom_eq.
  rewrite Map.dom_map.
  tauto.
Qed.


Lemma bs_codom_map : forall  (f : positive -> positive) (e : @Map.t positive),
    bs_codom (Map.map f e) = BSet.map f (bs_codom e).
Proof.
  intros.
  apply BSet.uniq.
  intros.
  apply eq_true_iff_eq.
  rewrite! bs_codom_eq.
  rewrite BSet.mem_map.
  rewrite List.existsb_exists.
  unfold Map.codom.
  split ; repeat intro.
  - destruct H as (k & H).
    rewrite Map.find_map in H.
    destruct (Map.find k e) eqn:FD; try discriminate.
    simpl in H. inv H.
    exists p.
    rewrite BSet.mem_elements.
    rewrite bs_codom_eq.
    unfold Map.codom.
    split ; auto.
    exists k; auto.
    apply Pos.eqb_refl.
  - destruct H as (p & IN & F).
    rewrite BSet.mem_elements in IN.
    rewrite bs_codom_eq in IN.
    unfold Map.codom in IN.
    destruct IN.
    apply Peqb_true_eq in F. subst.
    exists x0.
    rewrite Map.find_map.
    rewrite H. reflexivity.
Qed.



Lemma bs_dom_mapi : forall {A B: Type} (f : positive -> A -> B) (e : @Map.t A),
    bs_dom (Map.mapi f e) = bs_dom e.
Proof.
  intros.
  apply BSet.uniq.
  intros.
  apply eq_true_iff_eq.
  rewrite! bs_dom_eq.
  rewrite Map.dom_mapi.
  tauto.
Qed.

Lemma bs_dom_invert : forall (rho : @Map.t positive),
    bs_dom (Map.invert rho) =bs_codom rho.
Proof.
  intros.
  apply BSet.uniq.
  intros.
  apply eq_true_iff_eq.
  rewrite bs_dom_eq.
  rewrite bs_codom_eq.
  apply Map.dom_invert.
Qed.

Definition inj_map {A:Type} (rho : @Map.t A) :=
  forall x1 x2 x', Map.find x1 rho = Some x' ->
                   Map.find x2 rho = Some x' -> x1 = x2.

Lemma inj_map_inv : forall rho,
    inj_map rho ->
    Map.invert (Map.invert rho) = rho.
Proof.
  intros.
  apply Map.uniq.
  intros.
  generalize (Map.find_invert x (Map.invert rho)).
  destruct (Map.find x (Map.invert (Map.invert rho))).
  -
    generalize (Map.find_invert p rho).
    destruct (Map.find p (Map.invert rho)); congruence.
  - intros.
    destruct (Map.find x rho) eqn:FR; auto.
    exfalso.
    specialize (H0 p).
    generalize (Map.find_invert p rho).
    destruct (Map.find p (Map.invert rho)).
    intro.
    apply H0.
    f_equal.
    eapply H; eauto.
    congruence.
Qed.

Lemma bs_codom_invert : forall (rho : @Map.t positive),
    inj_map rho ->
    bs_codom (Map.invert rho) = bs_dom rho.
Proof.
  intros.
  apply BSet.uniq.
  intros.
  apply eq_true_iff_eq.
  rewrite bs_codom_eq.
  rewrite bs_dom_eq.
  rewrite <- Map.dom_invert.
  rewrite inj_map_inv; auto.
  tauto.
Qed.


(*Lemma dom_add : forall {A: Type} (m: @Map.t A) x v,
    forall y, dom (add x v m) y -> union (dom m) (Std.singleton x) y.
Proof.
  unfold dom;intros.
  rewrite find_add in H.
  destruct (Pos.eq_dec y x).
  subst. unfold union,Std.singleton. tauto.
  left. auto.
Qed.

Lemma dom_add' : forall {A: Type} (m: @Map.t A) x v,
    forall y, union (dom m) (Std.singleton x) y -> dom (add x v m) y.
Proof.
  unfold dom;intros.
  rewrite find_add.
  destruct (Pos.eq_dec y x).
  congruence.
  intro.
  unfold union in H.
  unfold Std.singleton in H.
  intuition congruence.
Qed.
*)
