Require Import ZArith Lia Bool.
Require Import Std NSet.
Require TEnv Typ.
Require Import FSetPositive.
Require Import FMapPositive.
Require Import Typing.


Definition sanitise_index (len:nat) (e:expr) :=
  Cond (Op And (Op Le (Cst 0) e)
          (Op Lt e (Cst len))) e (Cst 0).

Fixpoint sanitise_expr (S : var -> nat) (e:expr) : expr :=
  match e with
  | Var x => Var x
  | Cst c => Cst c
  | Op o e1 e2 => Op o (sanitise_expr S e1) (sanitise_expr S e2)
  | Cond e1 e2 e3 => Cond (sanitise_expr S e1) (sanitise_expr S e2) (sanitise_expr S e3)
  | Read x e      => let se := sanitise_expr S e in
                     Read x (sanitise_index (S x) (sanitise_expr S e))
  end.

Fixpoint sanitise_stmt (S: var -> nat)  (s:stmt)  :=
  match s with
  | Sskip => Sskip
  | Sassign x e =>  Sassign x (sanitise_expr S e)
  | Swrite  x e1 e2 => Swrite x (sanitise_index (S x) (sanitise_expr S e1)) (sanitise_expr S e2)
  | Sseq  s1 s2     => Sseq (Annot.upd (sanitise_stmt S (Annot.elt s1)) s1)
                         (Annot.upd (sanitise_stmt S (Annot.elt s2)) s2)
  | Sif a e s1 s2 s3 => Sif a (sanitise_expr S e)
                          (Annot.upd (sanitise_stmt S (Annot.elt s1)) s1)
                          (Annot.upd (sanitise_stmt S (Annot.elt s2)) s2)
                          (Annot.upd (sanitise_stmt S (Annot.elt s3)) s3)
  | Sfor x c1 c2 s => Sfor x c1 c2 (Annot.upd (sanitise_stmt S (Annot.elt s)) s)
  end.


Lemma typeof_expr_sanitise : forall S D G e t r,
    typeof_expr D G e t r  ->
    typeof_expr D G (sanitise_expr S e) t r.
Proof.
  intros. induction H; simpl.
  - constructor.
  - constructor.
  - constructor; auto.
  - constructor;auto.
  - eapply etypeof_expr.
    repeat constructor.
    eauto.
    eauto. eauto.
    rewrite! Typ.join_bot_r.
    rewrite! Typ.join_bot_l.
    rewrite! Typ.join_idem. auto.
    rewrite! Typ.join_bot_r.
    rewrite! Typ.join_bot_l.
    rewrite! Typ.join_idem.
    rewrite! BSet.union_empty.
    rewrite! BSet.union_empty_l.
    rewrite! BSet.union_idem. reflexivity.
    rewrite! Typ.join_bot_r.
    rewrite! Typ.join_bot_l.
    rewrite! Typ.join_idem. auto.
Qed.

Lemma sanitise_inj : forall S e1 e2,
    sanitise_expr S e1 = sanitise_expr S e2 ->
    e1 = e2.
Proof.
  induction e1 ; destruct e2 ; simpl ; intros ; try congruence.
  - inv H.
    f_equal ; intuition.
  - inv H.
    f_equal ; intuition.
  -  inv H.
     f_equal ; intuition.
Qed.

Lemma sanitise_index_inj : forall n e1 e2,
    (sanitise_index n e1) = sanitise_index n e2 ->
    e1 = e2.
Proof.
  unfold sanitise_index.
  intros. inv H. auto.
Qed.



Lemma of_sb_expr_eq_dec : forall S e1 e2,
    of_sb (expr_eq_dec (sanitise_expr S e1) (sanitise_expr S e2)) =
      of_sb (expr_eq_dec e1 e2).
Proof.
  intros.
  destruct (expr_eq_dec (sanitise_expr S e1) (sanitise_expr S e2)).
  destruct (expr_eq_dec e1 e2). subst. reflexivity.
  simpl. exfalso. apply n.
  apply sanitise_inj in e; auto.
  destruct (expr_eq_dec e1 e2); simpl; try congruence.
Qed.

Lemma of_sb_expr_eq_dec_sanitise_index : forall S e1 e2,
    of_sb (expr_eq_dec (sanitise_index S e1) (sanitise_index S e2)) =
      of_sb (expr_eq_dec e1 e2).
Proof.
  intros.
  destruct (expr_eq_dec (sanitise_index S e1) (sanitise_index S e2)).
  destruct (expr_eq_dec e1 e2). subst. reflexivity.
  simpl. exfalso. apply n.
  apply sanitise_index_inj in e; auto.
  destruct (expr_eq_dec e1 e2); simpl; try congruence.
Qed.



Fixpoint stmt_join_sanitise (S: var -> nat) (s1:stmt) :
  forall s2 sr,
    stmt_join s1 s2 = Some sr ->
    stmt_join (sanitise_stmt S s1) (sanitise_stmt S s2) =
      Some (sanitise_stmt S sr).
Proof.
  assert (A : forall a1 a2 ar,
             annot_join stmt_join a1 a2 = Some ar ->
             annot_join stmt_join (Annot.upd (sanitise_stmt S (Annot.elt a1)) a1)
               (Annot.upd (sanitise_stmt S (Annot.elt a2)) a2) = Some (Annot.upd (sanitise_stmt S (Annot.elt ar)) ar)).
  {
    intros.
    unfold annot_join, Annot.upd  in *.
    destruct (stmt_join (Annot.elt a1) (Annot.elt a2)) eqn:J ; try discriminate.
    inv H. simpl.
    apply stmt_join_sanitise with (S:= S) in J.
    rewrite J. reflexivity.
  }
  intros.
  destruct s1; destruct s2 ; simpl in *;
    repeat rewrite of_sb_expr_eq_dec;
    try discriminate;
    try reflexivity.
  - inv H. reflexivity.
  -  destruct ( of_sb (Pos.eq_dec x x0) && of_sb (expr_eq_dec e e0)); try discriminate. inv H. reflexivity.
  -
    destruct (Pos.eq_dec x x0); try discriminate ; subst.
    rewrite of_sb_expr_eq_dec_sanitise_index.
    rewrite of_sb_expr_eq_dec.
    destruct (of_sb (left eq_refl) && of_sb (expr_eq_dec e1 e0) &&
                of_sb (expr_eq_dec e2 e3)); try discriminate.
    inv H; simpl. congruence.
  - destruct (annot_join stmt_join s1 s2) eqn:J1; try discriminate.
    destruct (annot_join stmt_join s0 s3) eqn:J2; try discriminate.
    inv H.
    apply A in J1.
    apply A in J2.
    rewrite J1. rewrite J2.
    reflexivity.
  - destruct (of_sb (Pos.eq_dec t t0) && of_sb (expr_eq_dec e e0)); try discriminate.
    destruct (annot_join stmt_join s1 s2) eqn:J1; try discriminate.
    destruct (annot_join stmt_join s0 s4) eqn:J2; try discriminate.
    destruct (annot_join stmt_join s3 s5) eqn:J3; try discriminate.
    inv H.
    apply A in J1.
    apply A in J2.
    apply A in J3.
    rewrite J1. rewrite J2. rewrite J3.
    reflexivity.
  - destruct (of_sb (Pos.eq_dec x x0) && of_sb (Nat.eq_dec c1 c0) &&
                of_sb (Nat.eq_dec c2 c3) ); try discriminate.
    destruct (annot_join stmt_join s s0) eqn:J1; try discriminate.
    inv H.
    apply A in J1.
    rewrite J1.
    reflexivity.
Qed.


Fixpoint C_sanitise (S: var -> nat) (s:stmt) :
  C (sanitise_stmt S s) =
  C s.
Proof.
  destruct s; simpl; auto.
  - f_equal.
    auto. auto.
  - f_equal;auto.
    f_equal;auto.
    f_equal;auto.
Qed.

Lemma typeof_stmt_sanitise :
  forall  s a S D G k G' r g,
    typeof_stmt D a k G (Annot.mk r s g) G' ->
    typeof_stmt D a k G (Annot.mk r (sanitise_stmt S s) g) G'.
Proof.
  intro s.
  pattern s. apply stmt_sub.
  - intros. inv H. constructor.
  - intros. inv H.
    constructor.
    apply typeof_expr_sanitise; auto.
  - intros.
    inv H.
    simpl.
    apply typeof_expr_sanitise with (S:=S) in H8.
    apply typeof_expr_sanitise with (S:=S) in H10.
    eapply Twrite; eauto.
    unfold sanitise_index.
    eapply etypeof_expr.
    eapply TCond;eauto.
    repeat eapply TOp;eauto.
    eapply TCst.     eapply TCst. eapply TCst.
    + rewrite !BSet.union_empty_l.
    rewrite !BSet.union_empty.
    rewrite !BSet.union_idem.
    reflexivity.
    + rewrite Typ.join_bot_l.
      rewrite Typ.join_bot_r.
      rewrite !Typ.join_idem. reflexivity.
  - intros.
    apply seq_inv in H.
    destruct H as (G1 & T1 & T2 & R & GU).
    subst.
    destruct s1 as [r1 s1 g1].
    destruct s2 as [r2 s2 g2].
    simpl.
    econstructor.
    simpl.
    eapply IHs1;eauto.
    apply stmt_order_refl.
    eapply IHs2;eauto.
    apply stmt_order_refl.
  - intros.
    simpl in *.
    apply sif_inv in H.
    destruct H as  ( G1 & G2 & G3 & k' & t1 & rc & RES & TE & T1 & T2 & T3 & R  & GU &  K ).
      subst.
      eapply Tif; eauto.
      eapply typeof_expr_sanitise;eauto.
      eapply IHs1;eauto.
      apply stmt_order_refl.
      rewrite Annot.mk_dest. eauto.
      eapply IHs2;eauto.
      apply stmt_order_refl.
      rewrite Annot.mk_dest. eauto.
      eapply IHs3;eauto.
      apply stmt_order_refl.
      rewrite Annot.mk_dest. eauto.
(*    + destruct IFL as (x & b & c & FD & E & MEM & L & TY & SK1 & SK2 & GG & RR).
      subst.
      simpl.
      eapply TifL; eauto.
      destruct b; auto.
      eapply IHs1;eauto.
      apply stmt_order_refl.
      rewrite Annot.mk_dest. eauto. *)
  - intros.
    apply sfor_inv in H.
    destruct H as (RR & GG & (G2 & Go & T1 & ORD1 & ORD2 & CL &EQ)).
    simpl in *. subst.
    eapply IHfor with (S:=S) in T1; eauto.
    set (san := Annot.mk r1 (sanitise_stmt S s2) g1).
    change (r1) with (Annot.leaked san).
    change (g1) with (Annot.high san).
    eapply Tfor;eauto.
    unfold san.
    unfold Ca. simpl.
    rewrite C_sanitise.
    apply CL.
    apply stmt_order_refl.
Qed.
